package net.kayur.nlp.plugins.openlp;

import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.openlp.utils.FileUtils;
import opennlp.tools.sentdetect.SentenceDetectorME;
import opennlp.tools.sentdetect.SentenceModel;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class OpenNLPSentenceDetector implements SentenceDetector {
	private static final String DEFAULT_MODEL_FILENAME = "plugins/en-sent.bin";
	private String modelFilename;

	@Nullable
	private SentenceDetectorME sentenceDetector;


    public OpenNLPSentenceDetector() {
        this(DEFAULT_MODEL_FILENAME);
    }

    public OpenNLPSentenceDetector(String modelFilename) {
        this.modelFilename = modelFilename;
    }

    @Override
    public String getName() {
        return "OpenNLP Maximum Entropy SD";
    }

    public String[] detect(String text) {
        if (sentenceDetector == null) {
            sentenceDetector = FileUtils.tryIO(this::loadModel, "Unable to load sentence detector model.");
        }

        return sentenceDetector.sentDetect(text);
    }

    @NotNull
    private SentenceDetectorME loadModel() throws IOException {
        Path path = FileUtils.getPath(modelFilename);
        System.out.printf("Loading a sentence detector model from '%s'...", path);

        SentenceDetectorME detector;
        try (InputStream is = Files.newInputStream(path)) {
            detector = new SentenceDetectorME(new SentenceModel(is));
        }

        System.out.println("OK");
        return detector;
    }

    @Override
    public String toString() {
        return getName();
    }
}
