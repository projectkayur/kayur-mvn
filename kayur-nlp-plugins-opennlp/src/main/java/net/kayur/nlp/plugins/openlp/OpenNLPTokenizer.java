package net.kayur.nlp.plugins.openlp;

import net.kayur.nlp.plugins.api.Tokenizer;
import opennlp.tools.tokenize.WhitespaceTokenizer;
import org.jetbrains.annotations.Nullable;

public class OpenNLPTokenizer implements Tokenizer {
	@Nullable
    private opennlp.tools.tokenize.Tokenizer tokenizer;

    @Override
    public String getName() {
        return "OpenNLP Whitespace Tokenizer";
    }


    @Override
    public String[] tokenize(String text) {
	    if (tokenizer == null) {
	        tokenizer = WhitespaceTokenizer.INSTANCE;
        }

        return tokenizer.tokenize(text);
    }

    @Override
    public String toString() {
        return getName();
    }
}
