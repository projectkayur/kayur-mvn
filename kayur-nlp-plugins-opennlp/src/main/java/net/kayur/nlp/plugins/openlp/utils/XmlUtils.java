package net.kayur.nlp.plugins.openlp.utils;

import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public final class XmlUtils {
    private XmlUtils() {}

    public static <T> List<T> traverse(Path path, Function<String[], T> transform,
                                       String... attributes)
            throws IOException, SAXException, ParserConfigurationException, ParseException {

        Document document = fromXML(Files.readString(path));
        Element root = document.getDocumentElement();

        List<T> resultValues = new ArrayList<>();
        String[] attrValues = new String[attributes.length];

        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                for (int j = 0; j < attributes.length; j++) {
                    attrValues[j] = getRequiredAttribute(child, attributes[j]);
                }
                resultValues.add(transform.apply(attrValues));
            }
        }

        return resultValues;
    }

    private static Document fromXML(String xmlString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlString)));
    }

    private static String getRequiredAttribute(Node node, String attrName) throws ParseException {
        String value = null;

        NamedNodeMap attributes = node.getAttributes();
        if (attributes != null) {
            Node attrNode = attributes.getNamedItem(attrName);
            if (attrNode != null) {
                value = attrNode.getNodeValue();
            }
        }

        if (value == null) {
            String errorMsg = String.format("Node '%s' does not have required attribute '%s'",
                    node.getNodeName(), attrName);
            throw new ParseException(errorMsg, 0);
        }

        return value;
    }
}
