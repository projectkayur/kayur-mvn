package net.kayur.nlp.plugins.openlp;

import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.openlp.utils.FileUtils;
import net.kayur.nlp.plugins.openlp.utils.XmlUtils;
import opennlp.tools.postag.POSModel;
import opennlp.tools.postag.POSTaggerME;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class CustomStemmer implements Stemmer {
	private static final String DEFAULT_TAGGER_MODEL_FILENAME = "plugins/en-pos-maxent.bin";
	private static final String DEFAULT_VERB_LIST_FILENAME = "plugins/verbs.txt";
	private static final String DEFAULT_IRREGULAR_VERB_LIST_FILENAME = "plugins/irregular-verbs.xml";

	private String taggerModelFilename;
	private String verbListFilename;
	private String irregularVerbListFilename;

	private POSTaggerME tagger;
    private String[] verbs;
    private IrregularVerb[] irregularVerbs;
    private IrregularVerb[] irregularVerbsParticiples;

	private String[] tags = new String[0];

	public CustomStemmer() {
        this.taggerModelFilename = DEFAULT_TAGGER_MODEL_FILENAME;
        this.verbListFilename = DEFAULT_VERB_LIST_FILENAME;
        this.irregularVerbListFilename = DEFAULT_IRREGULAR_VERB_LIST_FILENAME;
    }

    public static CustomStemmerBuilder builder() {
	    return new CustomStemmerBuilder();
    }

    public static class CustomStemmerBuilder {
	    private CustomStemmer stemmer = new CustomStemmer();

	    public CustomStemmerBuilder taggerModelFilename(String filename) {
            stemmer.taggerModelFilename = filename;
            return this;
        }

        public CustomStemmerBuilder verbListFilename(String filename) {
            stemmer.verbListFilename = filename;
            return this;
        }

        public CustomStemmerBuilder irregularVerbListFilename(String filename) {
            stemmer.irregularVerbListFilename = filename;
            return this;
        }

        public CustomStemmer build() {
	        return stemmer;
        }
    }

    @Override
    public String getName() {
        return "Kayur Stemmer";
    }

    @Override
    public void init(String[] tokens) {
        if (tagger == null) {
            tagger = FileUtils.tryIO(this::loadTaggerModel, "Unable to load tagger model.");
        }

        tags = tagger.tag(tokens);
    }

	@Override
    public String process(String token, int index) {
        if (verbs == null) {
            verbs = FileUtils.tryIO(this::loadVerbs, "Unable to load list of verbs.");
        }

        if (irregularVerbs == null || irregularVerbsParticiples == null) {
            FileUtils.tryIO(this::loadIrregularVerbList, "Unable to load list of irregular verbs.");
        }

		String tag = tags[index];
		String recoveredVerb, result;
		
		switch (tag) {
            case "NNS":
                result = nounToSingular(token);
                break;
            case "VBZ":
                result = verbFrom3rdPerson(token);
                break;
            case "VBG":
                result = verbFromContinuous(token);
                break;
            case "VBN":
                recoveredVerb = fromIrregularVerbParticiple(token);
                result = recoveredVerb.isEmpty() ? regularVerbFromPast(token) : recoveredVerb;
                break;
            case "VBD":
                recoveredVerb = fromIrregularVerb(token);
                result = recoveredVerb.isEmpty() ? regularVerbFromPast(token) : recoveredVerb;
                break;
            default:
				result = token;
				break;
        }
        
        return result;
	}

    private String nounToSingular(String plural) {
        if (plural.endsWith("es")) {
            if (plural.endsWith("ses") || plural.endsWith("xes") || plural.endsWith("zes")
                    || plural.endsWith("ches") || plural.endsWith("shes")) {
                return plural.substring(0, plural.length() - 2);
            }
            if (plural.endsWith("ies")) {
                return plural.substring(0, plural.length() - 3) + "y";
            }
        }
        if (plural.endsWith("s")) {
            return plural.substring(0, plural.length() - 1);
        }
        return plural;
    }
	
    private String verbFrom3rdPerson(String verb) {
        int l = verb.length();
        String corrected = "";
        if (verb.endsWith("s")) {
            if (verb.endsWith("sses") || verb.endsWith("xes") || verb.endsWith("ches")
                    || verb.endsWith("shes") || verb.endsWith("oes")) {
                corrected = verb.substring(0, l - 2);
            } else if (verb.endsWith("ies") && (l > 3) && isConsonant(verb.charAt(l - 4))) {
                corrected = verb.substring(0, l - 3) + "y";
            } else {
                corrected = verb.substring(0, l - 1);
            }
            if (isUnknownVerb(corrected)) {
                // corrected verb was not found in dictionary, skip to avoid errors
                corrected = "";
            }
        }

        return corrected.isEmpty() ? verb : corrected;
    }
    
    private String verbFromContinuous(String verb) {
        int l = verb.length();
        String corrected = "";
        if (verb.endsWith("ying")) {
            corrected = verb.substring(0, l - 3);
            if (isUnknownVerb(corrected)) {
                corrected = verb.substring(0, l - 4) + "ie";
                if (isUnknownVerb(corrected)) {
                    corrected = "";
                }
            }
        } else if (verb.endsWith("ing")) {
            corrected = verb.substring(0, l - 3);
            if (isUnknownVerb(corrected)) {
                // check exceptions (doubling and ending -e)
                if (l >= 5 && (verb.charAt(l - 4) == verb.charAt(l - 5))) {
                    corrected = corrected.substring(0, corrected.length() - 1);
                } else {
                    corrected += "e";
                }

                // look up dictionary again
                if (isUnknownVerb(corrected)) {
                    corrected = "";
                }
            }
        }

        return corrected.isEmpty() ? verb : corrected;
    }    	
	
    private String fromIrregularVerbParticiple(String verb) {
        int index = Arrays.binarySearch(irregularVerbsParticiples, new IrregularVerb("", verb));
        return (index >= 0) ? irregularVerbsParticiples[index].verb : "";
    }	
	
	private String fromIrregularVerb(String verb) {
        int index = Arrays.binarySearch(irregularVerbs, new IrregularVerb("", verb));
        return (index >= 0) ? irregularVerbs[index].verb : "";
    }

    private String regularVerbFromPast(String verb) {
        int l = verb.length();
        String corrected = "";

        if (l >= 4 && verb.endsWith("ied") && isConsonant(verb.charAt(l - 4))) {
            corrected = verb.substring(0, l - 3) + "y";
            if (isUnknownVerb(corrected)) {
                corrected = "";
            }
        } else if (verb.endsWith("ed")) {
            corrected = verb.substring(0, l - 2);
            if (isUnknownVerb(corrected)) {
                // check exceptions (doubling and ending -e)
                if (l >= 4 && (verb.charAt(l - 3) == verb.charAt(l - 4))) {
                    corrected = corrected.substring(0, corrected.length() - 1);
                } else {
                    corrected += "e";
                }

                // look up dictionary again
                if (isUnknownVerb(corrected)) {
                    corrected = "";
                }
            }
        }

        return corrected.isEmpty() ? verb : corrected;
    }

    private boolean isConsonant(char c) {
        return ((c != 'a') && (c != 'e') && (c != 'i') && (c != 'o') && (c != 'u'));
    }
    
    private void loadIrregularVerbList() throws IOException {
        Path path = FileUtils.getPath(irregularVerbListFilename);
		System.out.printf("Loading an irregular verb list from '%s'...", path.toAbsolutePath());

        List<Pair<IrregularVerb, IrregularVerb>> verbs;
        try {
            verbs = XmlUtils.traverse(path,
                    (a -> new Pair<>(new IrregularVerb(a[0], a[1]), new IrregularVerb(a[0], a[2]))),
                    "base", "past", "past-participle");
        } catch (SAXException | ParserConfigurationException | ParseException e) {
            throw new IOException(e);
        }

        if (verbs.size() > 0) {
            irregularVerbs = verbs.stream()
                    .map(p -> p.first)
                    .sorted()
                    .toArray(IrregularVerb[]::new);
            irregularVerbsParticiples = verbs.stream()
                    .map(p -> p.second)
                    .sorted()
                    .toArray(IrregularVerb[]::new);
        }

        System.out.println("OK");
    }

    @NotNull
    private POSTaggerME loadTaggerModel() throws IOException {
        Path path = FileUtils.getPath(taggerModelFilename);
        System.out.printf("Loading a tagger model from '%s'...", path.toAbsolutePath());

        POSTaggerME tagger;

        try (InputStream is = Files.newInputStream(path)) {
            tagger = new POSTaggerME(new POSModel(is));
        }

        System.out.println("OK");
        return tagger;
    }

    @NotNull
    private String[] loadVerbs() throws IOException {
        Path path = FileUtils.getPath(verbListFilename);
        System.out.printf("Loading a verb list from '%s'...", path.toAbsolutePath());

        String[] verbs;

        try (Stream<String> lines = Files.lines(path)) {
            verbs = lines.sorted().toArray(String[]::new);
        }

        System.out.println("OK");
        return verbs;
    }

    private boolean isUnknownVerb(String verb) {
        return Arrays.binarySearch(verbs, verb) < 0;
    }

    private static class IrregularVerb implements Comparable<IrregularVerb> {
        final String verb;
        final String form;

        IrregularVerb(String v, String f) {
            verb = v;
            form = f;
        }

        @Override
        public int compareTo(@NotNull IrregularVerb another) {
            return this.form.compareTo(another.form);
        }
    }

    private static class Pair<T, U extends Comparable<U>> implements Comparable<Pair<T, U>> {
        T first;
        U second;

        Pair(T f, U s) {
            first = f;
            second = s;
        }

        @Override
        public int compareTo(@NotNull Pair<T, U> another) {
            if (another.second == null || this.second == null) {
                return 0;
            }
            return another.second.compareTo(this.second);
        }
    }

    @Override
    public String toString() {
        return getName();
    }
}
