package net.kayur.nlp.plugins.openlp.utils;

import java.io.IOException;

@FunctionalInterface
public interface IORunnable {
    void run() throws IOException;
}
