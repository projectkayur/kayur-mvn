package net.kayur.nlp.plugins.openlp.utils;

import java.io.IOException;

@FunctionalInterface
public interface IOProducer<T> {
    T produce() throws IOException;
}
