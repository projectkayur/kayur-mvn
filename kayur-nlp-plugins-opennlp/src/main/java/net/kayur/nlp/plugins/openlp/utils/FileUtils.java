package net.kayur.nlp.plugins.openlp.utils;

import org.jetbrains.annotations.NotNull;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public final class FileUtils {
    private FileUtils() {}

    public static Path getPath(String filename) throws FileNotFoundException {
        Path path = Paths.get(filename);

        if (!Files.exists(path)) {
            throw new FileNotFoundException(String.format("File does not exist: '%s'.", path.toAbsolutePath()));
        }

        return path;
    }

    @NotNull
    public static <T> T tryIO(IOProducer<T> producer, String errorMessage) {
        try {
            return producer.produce();
        } catch (IOException e) {
            throw new UncheckedIOException(errorMessage + " " + e.getMessage(), e);
        }
    }

    public static void tryIO(IORunnable runnable, String errorMessage) {
        try {
            runnable.run();
        } catch (IOException e) {
            throw new UncheckedIOException(errorMessage + " " + e.getMessage(), e);
        }
    }
}
