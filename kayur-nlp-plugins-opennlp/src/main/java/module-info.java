module net.kayur.plugins {
    requires net.kayur.nlp.plugins.api;

    requires org.apache.opennlp.tools;

    requires java.xml;
    requires org.jetbrains.annotations;

    provides net.kayur.nlp.plugins.api.Stemmer with net.kayur.nlp.plugins.openlp.CustomStemmer;
    provides net.kayur.nlp.plugins.api.SentenceDetector with net.kayur.nlp.plugins.openlp.OpenNLPSentenceDetector;
    provides net.kayur.nlp.plugins.api.Tokenizer with net.kayur.nlp.plugins.openlp.OpenNLPTokenizer;
}
