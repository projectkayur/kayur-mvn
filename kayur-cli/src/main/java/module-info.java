module net.kayur.cli {
    requires net.kayur;

    requires jline;
    requires commons.cli;

    requires org.jetbrains.annotations;

    // DI
    requires spring.context;
    requires spring.beans;

    exports net.kayur.ux.cli to spring.beans;
    exports net.kayur.ux.cli.command to spring.beans;
}
