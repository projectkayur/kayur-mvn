package net.kayur.ux.cli.command;

import net.kayur.core.configuration.MainConfiguration;
import net.kayur.helper.ParseUtils;
import net.kayur.ux.cli.ParsedDate;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.*;
import org.jetbrains.annotations.NotNull;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import static net.kayur.Msg.msg;

/**
 * Superclass for all commands.
 */
public abstract class AbstractCommand {
    protected final MainConfiguration config;

    // one static parser for all commands
    private static final CommandLineParser sParser = new DefaultParser();
    // each command may have a different list of options
    protected final Options mOptions = new Options();
    final List<String> mRequiredOptions = new ArrayList<>();
    protected boolean mAllowNoOptions = false;

    private static final DateFormat OPTION_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

    protected AbstractCommand(MainConfiguration config) {
        this.config = config;
    }

    // abstract

    public abstract ResultCode exec(String[] args) throws ParseException;

    public abstract String getDescription();


    //

    @NotNull
    public final Options getOptions() {
        return mOptions;
    }

    @NotNull
    protected final CommandLine parse(@NotNull String[] args) throws ParseException {
        return sParser.parse(mOptions, args);
    }

    @NotNull
    protected CommandLine check(@NotNull String[] args) throws ParseException {
        CommandLine line = parse(args);
        if (!mAllowNoOptions && args.length <= 1) {
            throw new ParseException("No options are provided.");
        }
        if (!mRequiredOptions.isEmpty() && mRequiredOptions.stream().noneMatch(line::hasOption)) {
            throw new ParseException(
                    String.format(
                            "At least one option of the following is required: %s.",
                            String.join(",", mRequiredOptions)));
        }
        return line;
    }

    // option getters

    @NotNull
    private String getRequired(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        if (!line.hasOption(option)) {
            throw new ParseException(String.format("Required option parameter '-%s' is missing.", option));
        }
        return line.getOptionValue(option);
    }


    // -- integer option getters --

    protected int getIntOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        return parseInt(getRequired(line, option));
    }

    protected long getLongOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        return parseLong(getRequired(line, option));
    }

    protected List<String> getCSV(@NotNull CommandLine line, @NotNull String option) {
        String[] values = line.getOptionValue(option).split(",");
        return Arrays.asList(values);
    }

    protected int getPositiveIntOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        return parsePositiveInt(getRequired(line, option));
    }

    protected int getPositiveIntOption(@NotNull CommandLine line, @NotNull String option,
                                   int defValue) throws ParseException {
        return line.hasOption(option) ? getPositiveIntOption(line, option) : defValue;
    }

    protected final int getNonnegativeIntOption(@NotNull CommandLine line,
                                                @NotNull String option) throws ParseException {
        return parseNonnegativeInt(getRequired(line, option));
    }

    private int parseInt(@NotNull String value) throws ParseException {
        int result;
        try {
            result = Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new ParseException(String.format("'%s' is not an integer.", value));
        }
        return result;
    }

    private long parseLong(@NotNull String value) throws ParseException {
        long result;
        try {
            result = Long.parseLong(value);
        } catch (NumberFormatException e) {
            throw new ParseException(String.format("'%s' is not an integer.", value));
        }
        return result;
    }

    private int parsePositiveInt(@NotNull String value) throws ParseException {
        int v = parseInt(value);
        if (v <= 0) {
            throw new ParseException(String.format("'%s' is not a positive integer.", value));
        }
        return v;
    }

    private int parseNonnegativeInt(@NotNull String value) throws ParseException {
        int v = parseInt(value);
        if (v < 0) {
            throw new ParseException(String.format("'%s' is not a non-negative integer.", value));
        }
        return v;
    }


    // -- string option getters --

    @NotNull
    final String getStringOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        return getRequired(line, option);
    }

    @NotNull
    final String getStringOption(@NotNull CommandLine line, @NotNull String option,
                                 @NotNull String defValue) throws ParseException {
        return line.hasOption(option) ? getStringOption(line, option) : defValue;
    }


    // -- date option getters --

    @NotNull
    Date getSingleDateOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        Date date = getDateOption(line, option).start;
        if (date == null) {
            throw new ParseException("Invalid date format of option -" + option);
        }
        return date;
    }

    @NotNull
    protected ParsedDate getDateOption(@NotNull CommandLine line, @NotNull String option) throws ParseException {
        // split to start and end date
        List<String> dateList = getCSV(line, option);
        int argc = dateList.size();

        if (argc == 0 || argc > 2) {
            throw new ParseException("Invalid format of option -" + option);
        }

        // convert start date
        Date startDate = ParseUtils.parseDate(dateList.get(0), OPTION_DATE_FORMAT);
        if (startDate == null) {
            throw new ParseException("Invalid date format of option -" + option);
        }

        // convert end date, if it is provided
        Date endDate = null;
        if (argc == 2) {
            endDate = ParseUtils.parseDate(dateList.get(1), OPTION_DATE_FORMAT);
            if (endDate == null) {
                throw new ParseException("Invalid date format of option -" + option);
            }
        }

        return new ParsedDate(startDate, endDate);
    }

    String requiredOption(String name) throws ParseException {
        throw new ParseException(msg("cmd_option_required", name));
    }
}
