package net.kayur.ux.cli.command;

import net.kayur.core.configuration.MainConfiguration;
import net.kayur.ux.cli.ResultCode;

import static net.kayur.Msg.msg;

/**
 * Initiates the application shutdown.
 */
public class CommandExit extends AbstractCommand {

    public CommandExit(MainConfiguration config) {
        super(config);
    }

    @Override
    public ResultCode exec(String[] args) {
        return ResultCode.EXIT;
    }

    @Override
    public String getDescription() {
        return msg("cmd_exit_desc");
    }
}
