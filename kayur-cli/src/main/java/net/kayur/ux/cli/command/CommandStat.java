package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.analysis.ClusterCore;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusKeyword;
import net.kayur.nlp.corpus.CorpusManager;
import net.kayur.nlp.statistics.Interval;
import net.kayur.nlp.statistics.LengthInWordsDistribution;
import net.kayur.ux.cli.ResultCode;
import net.kayur.ux.cli.TextPrinter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Map;

import static net.kayur.Msg.msg;

/**
 * Prints statistics for selected documents.
 * For each attribute, prints all different values stored in database.
 */
public class CommandStat extends AbstractCommand {
    private final CorpusManager corpusManager;

    public CommandStat(MainConfiguration config,
                       CorpusManager corpusManager) {
        super(config);
        this.corpusManager = corpusManager;

        mOptions.addOption("k", "keywords", true, "list N most frequent words");
        mOptions.addOption("ld", "length-distribution", true,
                "print distribution of documents by their lengths in words, with the specified interval length");
        mOptions.addOption("kd", "keyword-distribution", true,
                "print distribution of number of documents by number of keywords they contain");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        Corpus corpus = corpusManager.getCorpus();

        if (line.hasOption("k")) {
            int num = getPositiveIntOption(line, "k");
            printKeywords(corpus, num);
            return ResultCode.OK;
        }

        if (line.hasOption("ld")) {
            int step = getPositiveIntOption(line, "ld");
            printLengthInWordsDistribution(corpus, step);
            return ResultCode.OK;
        }

        if (line.hasOption("kd")) {
            int numKeywords = getPositiveIntOption(line, "kd");
            printNumDocumentsByNumKeywordsDistribution(corpus, numKeywords);
            return ResultCode.OK;
        }

        return ResultCode.FAILURE;
    }

    // Prints N the most frequent keywords in the corpus.
    private void printKeywords(Corpus corpus, int num) {
        assert num > 0;

        List<CorpusKeyword> keywords = corpus.getAllCorpusKeywords(num);

        // output

        int numRows = keywords.size();

        String[] headers = {"Document Frequency", "Keyword"};
        String[][] data  = new String[numRows][2];

        for (int i = 0; i < numRows; ++i) {
            CorpusKeyword keyword = keywords.get(i);
            data[i][0] = String.valueOf(keyword.documentFrequency);
            data[i][1] = keyword.keyword;
        }

        Disp.message(TextPrinter.table(headers, data));
    }

    // Prints distribution of documents by their length in words,
    // after processing and stop word removal.
    private void printLengthInWordsDistribution(Corpus corpus, int step) {
        assert step > 0;

        LengthInWordsDistribution ld = new LengthInWordsDistribution(corpus, step);
        Map<Interval, Integer> ldMap = ld.getResult();

        // output

        String[] headers = {"Interval", "Number of Documents"};
        String[][] data  = new String[ldMap.size()][headers.length];

        int row = 0;
        for (Map.Entry<Interval, Integer> entry : ldMap.entrySet()) {
            Interval interval = entry.getKey();
            int numDocuments = entry.getValue();

            data[row][0] = String.format("%d-%d", interval.start, interval.end);
            data[row][1] = String.valueOf(numDocuments);
            row++;
        }

        Disp.message(TextPrinter.table(headers, data));

        Disp.message(String.format("Number of documents: %d.", ld.numDocuments));
        Disp.message(String.format("Maximum number of words in a document: %d.", ld.maxNumWordsInDocument));
    }

    private void printNumDocumentsByNumKeywordsDistribution(Corpus corpus, int numKeywords) {

        ClusterCore core = new ClusterCore(corpus.getAllCorpusKeywords(numKeywords));
        int[] D = core.numDocumentsByNumKeywordsDistribution;

        // output
        String[] headers = {"Number of Keywords", "Number of Documents"};
        String[][] data  = new String[D.length][headers.length];

        if (D.length != 0) {
            data[0][0] = String.valueOf(0);
            data[0][1] = String.valueOf(corpus.numDocuments());
        }

        for (int i = 1; i < D.length; ++i) {
            data[i][0] = String.valueOf(i);
            data[i][1] = String.valueOf(D[i]);
        }

        Disp.message(TextPrinter.table(headers, data));
    }

    @Override
    public String getDescription() {
        return msg("cmd_stat_desc");
    }
}
