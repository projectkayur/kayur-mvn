package net.kayur.ux.cli;


import java.util.Arrays;
import java.util.Map;
import java.util.function.BiFunction;

import static net.kayur.Msg.msg;

public class TextPrinter {
    public static String printMetaStats(Map<String, Map<String, Integer>> stats) {
        if (stats.isEmpty()) {
            return msg("stat_no_metadata");
        }

        StringBuilder sb = new StringBuilder();

        stats.forEach((name, freqMap) -> {
            sb.append(name).append(" statistics:\n");
            freqMap.forEach((value, freq) -> sb.append(value).append(": ")
                    .append(freq)
                    .append(" documents\n"));
        });

        return sb.toString();
    }

    public static String table(String[] headers, String[][] content) {
        int numContentColumns = content.length > 0 ? content[0].length : 0;
        int numColumns = Math.max(headers.length, numContentColumns);
        int[] widths = new int[numColumns];
        int hWidth, cWidth;

        for (int j = 0; j < numColumns; ++j) {
            hWidth = (headers.length > j) ? headers[j].length() : 0;
            cWidth = maxLenInColumn(content, j);
            widths[j] = 2 + Math.max(hWidth, cWidth);
        }

        String hdr = headerRow(headers, widths);

        StringBuilder sb = new StringBuilder(hdr.length() );
        sb.append(hdr);
        sb.append(middleRow(headers, widths));
        sb.append(hdr);

        for (String[] row : content) {
            sb.append(middleRow(row, widths));
        }

        sb.append(hdr);

        return sb.toString();
    }

    private static int maxLenInColumn(String[][] data, int column) {
        int maxLen = 0, len;
        for (int i = 0; i < data.length; ++i) {
            len = data[i][column].length();
            if (len > maxLen) {
                maxLen = len;
            }
        }
        return maxLen;
    }

    private static String headerRow(char colSep, BiFunction<String, Integer, String> contentFunction,
                                    String[] columns, int[] widths) {
        StringBuilder sb = new StringBuilder();
        int n = columns.length;
        int m = widths.length;
        int width;

        for (int i = 0, len = Math.max(n, m); i < len; ++i) {
            sb.append(colSep);

            if (m > i) {
                width = widths[i];
            } else {
                width = columns[i].length();
            }

            String content = (n > i) ? columns[i] : "";

            sb.append(contentFunction.apply(content, width));
        }

        if (n != 0) {
            sb.append(colSep).append('\n');
        }

        return sb.toString();
    }

    private static String headerRow(String[] columns, int[] widths) {
        return headerRow('+', TextPrinter::borderRowContent, columns, widths);
    }

    private static String middleRow(String[] columns, int[] widths) {
        return headerRow('|', TextPrinter::middleContent, columns, widths);
    }

    private static String borderRowContent(String text, int width) {
        return fill('-', width + 2);
    }

    private static String middleContent(String text, int width) {
        return String.format(" %" + width + "s ", text);
    }

    private static String fill(char c, int num) {
        char[] charArray = new char[num];
        Arrays.fill(charArray, c);
        return new String(charArray);
    }
}
