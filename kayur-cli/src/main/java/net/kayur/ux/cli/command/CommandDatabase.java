package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.database.ConnectionHub;
import net.kayur.database.Embedded;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import static net.kayur.Msg.msg;

/**
 * Performs various operations on a database.
 */
public class CommandDatabase extends AbstractCommand {
    private final ConnectionHub connectionHub;

    public CommandDatabase(MainConfiguration config,
                           ConnectionHub connectionHub) {
        super(config);
        this.connectionHub = connectionHub;

        mOptions.addOption("b", "backup", true, msg("cmd_database_opt_backup"));
        mOptions.addOption("t", "test", false, msg("cmd_database_opt_test"));
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        if (line.hasOption("backup")) {
            String path = line.getOptionValue("backup");

            Embedded embedded = new Embedded(connectionHub, config.database);
            embedded.backupDatabase(path);
            embedded.shutdown();

            return ResultCode.OK;
        }

        if (line.hasOption("t")) {
            connectionHub.testConnection(config.database);
            Disp.message(msg("db_test_success"));
            return ResultCode.OK;
        }

        return ResultCode.FAILURE;
    }

    @Override
    public String getDescription() {
        return msg("cmd_database_desc");
    }
}
