package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.corpus.TextEngineController;
import net.kayur.nlp.statistics.Statistics;
import net.kayur.ux.cli.ResultCode;
import net.kayur.ux.cli.TextPrinter;
import net.kayur.web.Document;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static net.kayur.Msg.msg;

/**
 * Prints the list of selected documents, or the contents of a given document.
 */
public class CommandList extends AbstractCommand {
    private static final int DEFAULT_TITLE_DISPLAY_WIDTH = 96;
    private static final int MIN_TITLE_DISPLAY_WIDTH = 8;

    private final DocumentSelection selection;
    private final TextEngineController textEngineController;

    public CommandList(MainConfiguration config,
                       DocumentSelection selection,
                       TextEngineController textEngineController) {
        super(config);
        this.selection = selection;
        this.textEngineController = textEngineController;

        mAllowNoOptions = true; // list all documents

        mOptions.addOption("i", "index", true, "display i-th selected document");
        mOptions.addOption("r", "random", false, "display a random document from the selection");
        mOptions.addOption("m", "metadata", false, "print metadata statistics for selected documents");

        mOptions.addOption("l", "limit", true, "limit the number of documents to print");
        mOptions.addOption("w", "width", true, "specify maximum title length");

        mOptions.addOption("g", "group", true, "group documents by keywords in title");
    }

    @Override
    public ResultCode exec(String[] args) throws ParseException {
        CommandLine line = check(args);

        // parameters below require selection not empty
        if (selection.isEmpty()) {
            return ResultCode.FAILURE;
        }

        if (line.hasOption("i")) {
            Document issue = selection.getDocument(getPositiveIntOption(line, "i") - 1);
            Disp.message(issue.toString());
            return ResultCode.OK;
        }

        if (line.hasOption("r")) {
            Random rand = new Random();
            int randomIndex = rand.nextInt(selection.size());
            Document issue = selection.getDocument(randomIndex);
            Disp.message(issue.toString());
            return ResultCode.OK;
        }

        if (line.hasOption("m")) {
            Statistics statistics = new Statistics();
            Disp.message(TextPrinter.printMetaStats(statistics.getMetaStats(selection.get(),
                    0.01)));
            return ResultCode.OK;
        }

        // set max number of documents displayed
        int limit = getPositiveIntOption(line, "l", selection.size());

        // set maximum title length
        int width = getPositiveIntOption(line, "w", DEFAULT_TITLE_DISPLAY_WIDTH);
        if (width < MIN_TITLE_DISPLAY_WIDTH) {
            width = MIN_TITLE_DISPLAY_WIDTH; // avoid too small values (required for substring calls)
        }

        String[] keywords = null;
        if (line.hasOption("g")) {
            String keywordsList = line.getOptionValue("g");
            keywords = keywordsList.split(",");
        }

        printSelection(limit, width, keywords);

        return ResultCode.OK;
    }

    private void printSelection(int limit, int width, @Nullable String[] keywords) {
        int selectionSize = selection.size();
        int maxIndexLen = String.valueOf(selectionSize).length();

        List<String> entries = new ArrayList<>();

        for (int i = 0; i < limit; i++) {
            Document issue = selection.getDocument(i);

            String title = issue.getTitle();
            if (title.length() > width) {
                title = title.substring(0, width - 3) + "...";
            }

            entries.add(String.format("%" + maxIndexLen + "d. %s %s %s",
                    i + 1,
                    issue.getDateTime(),
                    issue.id.name,
                    title));
        }

        if (keywords != null) {
            entries = shuffle(entries, keywords);
        }

        Disp.message(String.join("\n", entries));
    }

    private List<String> shuffle(List<String> messages, String[] keywords) {
        int n = keywords.length;
        int issueNum = selection.size();

        boolean[][] kmatrix = new boolean[issueNum][n];

        for (int i = 0; i < issueNum; i++) {
            textEngineController.containsKeyword(selection.getDocument(i).getTitle(), keywords, kmatrix[i]);
        }

        int[] ik_sums = new int[n];
        for (int i = 0; i < n; i++) {
            ik_sums[i] = 0;
            for (int j = 0; j < issueNum; j++) {
                if (kmatrix[j][i]) {
                    ik_sums[i]++;
                }
            }
        }

        int max = 1;

        List<String> pool = new ArrayList<>(messages);
        List<String> resultPool = new ArrayList<>();

        int totalNumAdded = 0;

        while (max > 0) {
            int localmax = 0;
            int localmaxIndex = -1;
            for (int i = 0; i < n; i++) {
                if (ik_sums[i] > localmax) {
                    localmax = ik_sums[i];
                    localmaxIndex = i;
                }
            }
            max = localmax;
            if (localmaxIndex != -1) {
                int numAdded = 0;

                resultPool.add(String.format("--- keyword: %s", keywords[localmaxIndex]));
                for (int i = 0; i < issueNum; i++) {
                    if (kmatrix[i][localmaxIndex]) {
                        String s = messages.get(i);
                        if (pool.contains(s)) {
                            resultPool.add(s);
                            numAdded++;
                            pool.remove(s);
                        }
                    }
                }

                resultPool.add(String.format("Total %d documents with keyword: '%s'.\n", numAdded, keywords[localmaxIndex]));

                totalNumAdded += numAdded;

                ik_sums[localmaxIndex] = -1;
            } else {
                max = 0;
            }
        }

        int numOmitted = issueNum - totalNumAdded;
        if (numOmitted > 0) {
            resultPool.add(String.format("Omitted %d documents without keywords in title.", numOmitted));
        }
        //resultPool.addAll(pool);

        return resultPool;
    }

    @Override
    public String getDescription() {
        return msg("cmd_list_desc");
    }
}
