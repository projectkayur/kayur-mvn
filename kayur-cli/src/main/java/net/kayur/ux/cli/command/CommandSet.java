package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.database.DocumentRepository;
import net.kayur.ux.cli.ResultCode;
import net.kayur.ux.cli.command.AbstractCommand;
import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

/**
 * At this moment: only assigned tags on the selection.
 */
public class CommandSet extends AbstractCommand {
    private final DocumentSelection selection;
    private final DocumentRepository repository;

    public CommandSet(MainConfiguration config,
                      DocumentSelection selection,
                      DocumentRepository repository) {
        super(config);
        this.selection = selection;
        this.repository = repository;

        mOptions.addOption("t", "tag", true, "assign numeric tag to the selection");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        // parameters below require selection not empty
        if (selection.isEmpty()) {
            return ResultCode.FAILURE;
        }

        if (line.hasOption("t")) {
            int tag = getIntOption(line, "t");
            updateTag(tag);
            return ResultCode.OK;
        }

        return ResultCode.FAILURE;
    }

    private void updateTag(int tag) {
        // documents list of IDs of selected documents
        List<DocumentId> idList = selection.get().stream()
                .map(Document::getId)
                .collect(Collectors.toList());

        repository.setTag(idList, tag);

        // update representation
        selection.get().forEach(document -> document.setTag(tag));

        Disp.message("Tags were set successfully.");
    }

    @Override
    public String getDescription() {
        return "Set options and default values.";
    }
}
