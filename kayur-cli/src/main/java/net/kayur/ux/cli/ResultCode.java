package net.kayur.ux.cli;

public enum ResultCode {
    FAILURE, OK, EXIT, NONE
}
