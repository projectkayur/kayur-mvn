package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.database.DocumentRepository;
import net.kayur.database.SelectQueryParameters;
import net.kayur.helper.FileHelper;
import net.kayur.helper.SimpleTimer;
import net.kayur.ux.cli.ParsedDate;
import net.kayur.ux.cli.ResultCode;
import net.kayur.web.Document;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.List;

import static net.kayur.Msg.msg;

/**
 * Command to select bug reports from database.
 */
public class CommandSelect extends AbstractCommand {
    private final DocumentSelection selection;
    private final DocumentRepository repository;

    public CommandSelect(MainConfiguration config,
                         DocumentSelection selection,
                         DocumentRepository repository) {
        super(config);
        this.selection = selection;
        this.repository = repository;

        mOptions.addOption("m", "module", true, "filter by the module identifier");
        mOptions.addOption("d", "date", true, "filter by date interval, format: yyyy-MM-dd[,yyyy-MM-dd]");
        mOptions.addOption("id", true, "filter by document identifiers (comma-separated list)");
        mOptions.addOption("f", "file", true, "filter by document identifiers specified in a file (overrides -id)");
        mOptions.addOption("t", "tag", true, "filter by document tag");

        mOptions.addOption("l", "limit", true, "limit number of documents to select");

        mAllowNoOptions = true;
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        SelectQueryParameters params = new SelectQueryParameters();

        if (line.hasOption("l"))
            params.setLimit(getPositiveIntOption(line, "l"));

        if (line.hasOption("m"))
            params.setModule(getLongOption(line, "m"));

        if (line.hasOption("f")) {
            String filename = line.getOptionValue("f");
            try {
                params.setIdList(FileHelper.readLines(filename));
            } catch (IOException e) {
                Disp.error(e, msg("cmd_select_load_id_failed"));
                return ResultCode.FAILURE;
            }
        } else if (line.hasOption("id")) {
            params.setIdList(getCSV(line, "id"));
        }

        if (line.hasOption("d")) {
            ParsedDate dates = getDateOption(line, "d");
            params.setStartDate(dates.start);
            params.setEndDate(dates.end);
        }

        if (line.hasOption("t")) {
            params.setTag(getIntOption(line, "t"));
            params.setTagOperator("=");
        }

        SimpleTimer timer = new SimpleTimer();
        List<Document> documents = repository.select(params);
        long opTime = timer.time();

        // TODO: keep old selection option
        selection.set(documents);

        String resultMsg = String.format("Loaded %d bug reports in %d ms (%d s).",
                documents.size(), opTime, opTime / 1000);
        Disp.message(resultMsg);

        return ResultCode.OK;
    }

    @Override
    public String getDescription() {
        return msg("cmd_select_desc");
    }
}
