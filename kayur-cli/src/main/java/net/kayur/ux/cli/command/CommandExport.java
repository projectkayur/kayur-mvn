package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusManager;
import net.kayur.nlp.export.ExportController;
import net.kayur.nlp.export.ExportManagerParameters;
import net.kayur.nlp.export.ExportStatistics;
import net.kayur.nlp.formats.*;
import net.kayur.nlp.weights.TermWeight;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.kayur.Msg.msg;

/**
 * Exports a corpus into one of the supported formats.
 */
public class CommandExport extends AbstractCommand {
    private final CorpusManager corpusManager;
    private final ExportController exportController;

    public CommandExport(MainConfiguration config,
                         CorpusManager corpusManager,
                         ExportController exportController) {
        super(config);
        this.corpusManager = corpusManager;
        this.exportController = exportController;

        // file options
        mOptions.addOption("o", "output", true, "name of the output file");
        mOptions.addOption("ref", "reference", false, "save reference dictionary");
        mOptions.addOption("sortref", "sortreference", false, "sort reference dictionary");

        String formatList = "";
        for (String fmtName : ExportController.supportedFormats)
            formatList += fmtName + "|";
        formatList = "[" + formatList.substring(0, formatList.length() - 1) + "]";

        mOptions.addOption("f", "format", true, formatList);

        // text processing options
        mOptions.addOption("t", "title", false, "use title instead of document content");
        mOptions.addOption("h", "hapax", true, "minimum document frequency of words");
        mOptions.addOption("m", "model", true, "either b, f, tf, or tfidf (note: only WEKA supports float weights)");

        // ARFF options
        mOptions.addOption("noid", false, "exclude id attribute (ARFF only)");
        mOptions.addOption("nom", "nominal", false, "use nominal type attributes (ARFF only)");
        mOptions.addOption("rel", "relation", true, "name of the relation (ARFF only)");
        mOptions.addOption("cat", "category", false, "include category attribute (ARFF only)");

        mRequiredOptions.add("f");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {

        CommandLine line = check(args);

        // read export format
        String formatName = getStringOption(line, "f");
        ExportFormat format;

        switch (formatName) {
            case ExportController.FORMAT_CARROT_XML:
                format = new ExportFormatCarrotXML();
                break;
            case ExportController.FORMAT_CSV:
                format = new ExportFormatCSV();
                break;
            case ExportController.FORMAT_HDP:
                format = new ExportFormatHDP();
                break;
            case ExportController.FORMAT_JSON:
                format = new ExportFormatJSON();
                break;
            case ExportController.FORMAT_ARFF:
                ExportFormatARFF fmt = new ExportFormatARFF();
                fmt.setRelationName(getStringOption(line, "rel", ""));
                fmt.setUseNominalAttributes(line.hasOption("nom"));
                fmt.setIncludeIdAttribute(line.hasOption("noid"));
                fmt.setIncludeCategoryAttribute(line.hasOption("cat"));
                fmt.setCategoryMappingFile(config.nlp.getCategoryMapping());
                format = fmt;
                break;
            default:
                throw new ParseException("Unsupported export format.");
        }

        // get term weight model (default: TF-IDF)
        String termWeightName = getStringOption(line, "m", "tf-idf");
        TermWeight termWeight = findTermWeight(termWeightName);
        if (termWeight == null) {
            throw new ParseException("Invalid term weight model.");
        }

        ExportManagerParameters parameters = ExportManagerParameters.builder()
                .filename(getStringOption(line, "o")) // required
                .termWeight(termWeight)
                .hapax(line.hasOption("h") ? getPositiveIntOption(line, "h") : ExportController.DEFAULT_HAPAX) // word document frequency threshold
                .isSaveWordList(line.hasOption("ref") || line.hasOption("sort-ref"))
                .isSortedWordList(line.hasOption("sort-ref"))
                .format(format)
                .build();


        // throws exception if corpus is not ready
        Corpus corpus = corpusManager.getCorpus();
        ExportStatistics stats = exportController.export(corpus, parameters);

        Disp.message(String.format("File '%s' has been written successfully.\n%s", parameters.getFilename(), stats));
        return ResultCode.OK;
    }

    @Nullable
    private static TermWeight findTermWeight(@NotNull String name) {
        for (TermWeight weight : ExportController.supportedWeights)
            if (weight.name.equalsIgnoreCase(name) || weight.shortName.equalsIgnoreCase(name))
                return weight;
        return null;
    }

    @Override
    public String getDescription() {
        return msg("cmd_export_desc");
    }
}
