package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.*;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

/**
 * Prints list of all commands, or information on usage of a specific command.
 */
public class CommandHelp extends AbstractCommand {
    private final HelpFormatter helpFormatter;
    private final Map<String, String> commandDescriptions;
    private Map<String, AbstractCommand> commands;
    private int mCommandNameMaxLen = 1;

    public CommandHelp(MainConfiguration config) {
        super(config);

        helpFormatter = new HelpFormatter();
        helpFormatter.setSyntaxPrefix(msg("cmd_help_prefix") + ": ");

        commandDescriptions = new TreeMap<>();
        commands = new LinkedHashMap<>();
    }

    public void setCommandList(Map<String, AbstractCommand> commands) {
        this.commands = commands;

        // handle aliases
        Set<AbstractCommand> usedCommands = new HashSet<>();

        // create help entries for every command
        commands.forEach((cmdName, command) -> {
            if (!usedCommands.contains(command)) {
                commandDescriptions.put(cmdName, command.getDescription());
                int cmdNameLen = cmdName.length();
                if (cmdNameLen > mCommandNameMaxLen) {
                    mCommandNameMaxLen = cmdNameLen;
                }
                usedCommands.add(command);
            }
        });
    }

    @Override
    public ResultCode exec(String[] args) throws ParseException {
        switch (args.length) {
            case 1:
                printGeneralHelp();
                return ResultCode.NONE;
            case 2:
                return printHelp(args[1]);
            default:
                return ResultCode.FAILURE;
        }
    }

    @Override
    public String getDescription() {
        return msg("cmd_help_desc");
    }

    // Prints the list of available commands.
    private void printGeneralHelp() {
        String format = "%" + mCommandNameMaxLen + "s: %s";

        String cmdList = msg("cmd_general_help_prefix") + ":\n" +
                commandDescriptions.entrySet().stream()
                        .map(entry -> String.format(format, entry.getKey(), entry.getValue()))
                        .collect(Collectors.joining("\n"))
                + "\n";

        Disp.message(cmdList);
    }

    // Prints usage instructions for a particular command.
    private ResultCode printHelp(@NotNull String cmdName) {
        AbstractCommand command = commands.get(cmdName);
        if (command != null) {
            helpFormatter.printHelp(cmdName, command.getOptions());
            Disp.newLine();
            return ResultCode.OK;
        } else {
            Disp.message(msg("cmd_no_such_command", cmdName));
            return ResultCode.FAILURE;
        }
    }
}
