package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.ux.cli.ResultCode;
import net.kayur.web.Document;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.ModuleLoader;
import net.kayur.web.update.*;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

/**
 * Command to download new bug reports from a public database.
 */
public class CommandUpdate extends AbstractCommand {
    private final Fetcher fetcher;
    private final ModuleLoader moduleLoader;

    public CommandUpdate(MainConfiguration config,
                         Fetcher fetcher,
                         ModuleLoader moduleLoader) {
        super(config);
        this.fetcher = fetcher;
        this.moduleLoader = moduleLoader;

        mOptions.addOption("a", "available", false, "check the number of new documents available, but don't fetch anything");

        mOptions.addOption("m", "module", true, "module name or identifier (required)");
        mOptions.addOption("d", "date", true, "set limit date");
        mOptions.addOption("id", "startid", true, "document identifier to start with");

        mOptions.addOption("f", "force", false, "override documents that are already stored in database");
        mOptions.addOption("n", "number", true, "number of documents to download (default: all)");
        mOptions.addOption("r", "recover", false, "try to download again documents that were unavailable");

        mOptions.addOption("l", "list", false, "list available modules");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        if (line.hasOption("l")) {
            printModules();
            return ResultCode.OK;
        }

        // module option is needed for all operations below
        if (!line.hasOption("m")) {
            requiredOption("m");
        }

        // get module config
        String moduleName = getStringOption(line, "m");
        Optional<ModuleConfiguration> moduleConfig = findModule(moduleName);

        if (!moduleConfig.isPresent()) {
            Disp.error("Unable to get the module configuration.");
            return ResultCode.FAILURE;
        }

        UpdateParameters parameters = new UpdateParameters();
        parameters.setRecovery(line.hasOption("r"));

        parameters.setOverwrite(line.hasOption("f"));
        if (parameters.isOverwrite()) {
            Disp.info("Force update mode is enabled. Entries in database will be overwritten.");
        }

        if (line.hasOption("n")) {
            parameters.setLimit(getPositiveIntOption(line, "n"));
        }

        if (line.hasOption("id")) {
            Document lastDocument = new Document(-1, getStringOption(line, "id"));
            parameters.setLastProcessedDocument(lastDocument);
        }

        if (line.hasOption("d"))
            parameters.getLastProcessedDocument().setDateTime(getSingleDateOption(line, "d"));

        parameters.setCheckOnly(line.hasOption("a"));

        update(parameters, moduleConfig.get());

        return ResultCode.OK;
    }

    private void update(@NotNull UpdateParameters parameters, @NotNull ModuleConfiguration config) {
        Disp.message("Calculating update size...");
        fetcher.setModule(config);
        UpdateCheckResult updateCheckResult = fetcher.calculateUpdateSize(parameters);

        if (parameters.isCheckOnly()) {
            Disp.message("New %d documents are available.", updateCheckResult.numToUpdate);
            return;
        }

        UpdateSession session = fetcher.startUpdateSession(parameters, updateCheckResult);
        UpdateTracker tracker = session.getTracker();

        while (session.update()) {
            Disp.msgProgress(tracker.getIteration(), updateCheckResult.numToUpdate);
        }

        Disp.message(tracker.getFinalReport());
    }

    private void printModules() {
        String msg = moduleLoader.getModules().stream()
                .map(module -> String.format("%s (id = %d)", module.getName(), module.getId()))
                .collect(Collectors.joining("\n"));

        if (msg.isEmpty()) {
            msg = "No modules are available.";
        }

        Disp.message(msg);
    }

    private Optional<ModuleConfiguration> findModule(@NotNull String moduleId) {
        // check first whether it is a numeric id
        try {
            long id = Long.parseLong(moduleId);
            return findModule(config -> id == config.getId());
        } catch (NumberFormatException ignored) {
            final String moduleName = moduleId.toLowerCase();
            return findModule(config -> moduleName.equals(config.getName().toLowerCase()));
        }
    }

    private Optional<ModuleConfiguration> findModule(Predicate<ModuleConfiguration> predicate) {
        return moduleLoader.getModules().stream()
                .filter(predicate)
                .findFirst();
    }

    @Override
    public String getDescription() {
        return msg("cmd_update_desc");
    }
}
