package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.FilterExpr;
import net.kayur.nlp.corpus.TextEngineController;
import net.kayur.ux.cli.ResultCode;
import net.kayur.web.Document;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

/**
 * Applies outer level filters on document samples.
 */
public class CommandFilter extends AbstractCommand {
    private final DocumentSelection selection;
    private final TextEngineController textEngineController;

    public CommandFilter(MainConfiguration config,
                         DocumentSelection selection,
                         TextEngineController textEngineController) {
        super(config);
        this.selection = selection;
        this.textEngineController = textEngineController;

        // TODO: more filter levels, or test run of complete NLP toolchain for a given document
        mOptions.addOption("i", "index", true, "apply outer filters to i-th selected document");

        mOptions.addOption("l", "list", false, "display all active filters");
        mOptions.addOption("r", "reload", false, "reload filter list from the configuration file");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        if (line.hasOption("r")) {
            textEngineController.configureFilters(config);
            return ResultCode.OK;
        }

        if (line.hasOption("l")) {
            List<FilterExpr> filters = textEngineController.getLoadedFilters();

            if (filters.isEmpty()) {
                Disp.message("No filters are loaded.");
            } else {
                printFilters(filters);
            }

            return ResultCode.OK;
        }

        // parameters below require selection not empty
        if (selection.isEmpty()) {
            return ResultCode.FAILURE;
        }

        if (line.hasOption("i")) {
            Document issue = selection.getDocument(getPositiveIntOption(line, "i") - 1);
            applyFilter(issue);
            return ResultCode.OK;
        }

        return ResultCode.FAILURE;
    }

    private void printFilters(@NotNull List<FilterExpr> filters) {
        List<Integer> layers = filters.stream()
                .map(FilterExpr::getLayer)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        StringBuilder sb = new StringBuilder();
        for (int layer : layers) {
            sb.append(filters.stream()
                    .filter(f -> f.getLayer() == layer)
                    .map(f -> String.format("Replace `%s` with `%s`\n",
                            f.getRegEx(), f.getReplacementString()))
                    .reduce("Layer " + layer + " filters:\n", (a, s) -> a + s));
        }

        Disp.message(sb.toString());
    }

    private void applyFilter(@NotNull Document issue) {
        String issueText = issue.getText();
        printSection("Before Filters", issueText);

        String filteredText = textEngineController.filterText(issueText, 0);
        printSection("After Level 0 Filters", filteredText);
    }

    private static void printSection(@NotNull String name, @NotNull String contents) {
        String sectionStr = String.format("--> [%s: begin] <--\n%s\n--> [%s: end] <--",
                name, contents, name);
        Disp.message(sectionStr);
    }

    @Override
    public String getDescription() {
        return msg("cmd_filter_desc");
    }
}
