package net.kayur.ux.cli.command;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.helper.SimpleTimer;
import net.kayur.nlp.corpus.TextEngineController;
import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.corpus.TextProcessingParameters;
import net.kayur.nlp.plugins.api.TextEngineModule;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import java.util.List;

import static net.kayur.Msg.msg;

/**
 * Uses selected bug reports to generate an input file for a data mining workbench.
 */
public class CommandProcess extends AbstractCommand {
    private final DocumentSelection selection;
    private final TextEngineController textEngineController;
    private final TextEngineModuleLocator textEngineModuleLocator;

    public CommandProcess(MainConfiguration config,
                          DocumentSelection selection,
                          TextEngineController textEngineController,
                          TextEngineModuleLocator textEngineModuleLocator) {
        super(config);
        this.selection = selection;
        this.textEngineController = textEngineController;
        this.textEngineModuleLocator = textEngineModuleLocator;

        mOptions.addOption("a", "all", false, "process titles, content, and comments (default)");
        mOptions.addOption("t", "titles", false, "process titles");
        mOptions.addOption("c", "content", false, "process content");
        mOptions.addOption("cc", "comments", false, "process comments");
        mOptions.addOption("x","nocache", false, "disable caching");
        mOptions.addOption("m", "list-modules", false, "lists available modules");

        mAllowNoOptions = true;
    }

    @Override
    public ResultCode exec(String[] args) throws ParseException {
        CommandLine line = check(args);

        if (line.hasOption("m")) {
            return listTextEngineModules();
        }

        // verify that documents are selected
        if (selection.isEmpty()) {
            return ResultCode.FAILURE;
        }

        boolean processAll = line.hasOption("a") || !(line.hasOption("c") || line.hasOption("cc") || line.hasOption("t"));

        TextProcessingParameters builderParams = TextProcessingParameters.builder()
                .processContent(processAll || line.hasOption("c"))
                .processComments(processAll || line.hasOption("cc"))
                .processTitles(processAll || line.hasOption("t"))
                .enableCache(!line.hasOption("x"))
                .build();

        SimpleTimer timer = new SimpleTimer();
        textEngineController.buildCorpus(selection.get(), builderParams);

        Disp.message(msg("tp_success", timer.time()));
        return ResultCode.OK;
    }

    private ResultCode listTextEngineModules() {
        String result = "Sentence Detectors:\n" +
                formatTextEngineModuleNames(textEngineModuleLocator.getSentenceDetectors()) +
                "Tokenizers:\n" +
                formatTextEngineModuleNames(textEngineModuleLocator.getTokenizers()) +
                "Stemmers:\n" +
                formatTextEngineModuleNames(textEngineModuleLocator.getStemmers()) +
                "\n";

        Disp.message(result);
        return ResultCode.OK;
    }

    private String formatTextEngineModuleNames(List<? extends TextEngineModule> modules) {
        if (modules.isEmpty()) {
            return "  <No modules>\n";
        }

        StringBuilder sb = new StringBuilder();
        modules.forEach(module -> sb.append("  - ").append(module.getName()).append("\n"));
        return sb.toString();
    }

    @Override
    public String getDescription() {
        return msg("cmd_corpus_desc");
    }
}
