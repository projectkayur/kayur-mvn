package net.kayur.ux.cli;

import jline.TerminalFactory;
import jline.console.ConsoleReader;
import jline.console.history.FileHistory;
import net.kayur.Disp;
import net.kayur.core.App;
import net.kayur.helper.FileHelper;
import net.kayur.ux.cli.command.*;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static net.kayur.Msg.msg;

public class InputController implements ApplicationContextAware {
    private static final String CMD_HISTORY_FILENAME = ".kayurhst";
    private static final String PROMPT = "> ";

    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    public InputController() {

    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        registerCommands(applicationContext);
    }

    private void registerCommands(@NotNull ApplicationContext context) {
        register("config", context.getBean(CommandConfig.class));
        register("exit", context.getBean(CommandExit.class), "quit", "q");
        register("export", context.getBean(CommandExport.class));
        register("database", context.getBean(CommandDatabase.class), "db");
        register("stat", context.getBean(CommandStat.class));
        register("update", context.getBean(CommandUpdate.class));
        register("list", context.getBean(CommandList.class), "ls");
        register("set", context.getBean(CommandSet.class));
        register("refine", context.getBean(CommandRefine.class));
        register("select", context.getBean(CommandSelect.class), "sel");
        register("filter", context.getBean(CommandFilter.class));
        register("corpus", context.getBean(CommandProcess.class), "textprocessing", "tp");

        CommandHelp commandHelp = context.getBean(CommandHelp.class);
        commands.put("help", commandHelp);
        commandHelp.setCommandList(commands);
    }

    void startShell() throws IOException {
        ConsoleReader console = new ConsoleReader(App.NAME, System.in, System.out, TerminalFactory.create());
        console.setPrompt(PROMPT);

        // load command history
        File historyFile = (new File(CMD_HISTORY_FILENAME)).getAbsoluteFile();
        console.setHistory(new FileHistory(historyFile));

        // start main command processing cycle
        String cmdline;
        boolean processNext = true;
        while (processNext && (cmdline = console.readLine()) != null) {
            ResultCode resultCode = processCommand(cmdline);

            switch (resultCode) {
                case OK:
                    Disp.message(msg("cmd_success"));
                    break;
                case FAILURE:
                    Disp.message(msg("cmd_failure"));
                    break;
                case EXIT:
                    processNext = false;
                    break;
                case NONE:
                default:
                    break;
            }
        }

        // save command history
        FileHistory history = (FileHistory) console.getHistory();
        if (history != null) {
            history.flush();
        }
    }

    void executeScript(String scriptName) throws IOException {
        List<String> commands = FileHelper.readLinesSkipComments(scriptName);
        boolean opResult = true;

        for (String cmd : commands) {
            if (processCommand(cmd) == ResultCode.FAILURE) {
                opResult = false;
                break;
            }
        }

        Disp.message(msg(opResult ? "cmd_script_success" : "cmd_script_failure"));
    }

    private void register(String name, AbstractCommand command, String... aliases) {
        commands.put(name, command);
        for (String alias : aliases) {
            commands.put(alias, command);
        }
    }

    // Executes the given command.
    private ResultCode processCommand(String cmdline) {
        String[] args = getArgsWithQuoteHandling(cmdline);

        String cmdName = args[0].trim();

        // check for pressing ENTER without command input
        if (cmdName.isEmpty()) {
            return ResultCode.NONE;
        }

        AbstractCommand command = commands.get(cmdName);
        if (command == null) {
            Disp.message(msg("cmd_no_such_command", cmdName));
            return ResultCode.FAILURE;
        }

        ResultCode resultCode;
        try {
            resultCode = command.exec(args);
        } catch (ParseException e) {
            Disp.message(msg("cmd_incorrect_arguments", e.getMessage(), cmdName));
            resultCode = ResultCode.FAILURE;
        } catch (Exception e) {
            Disp.error(e, e.getMessage());
            resultCode = ResultCode.FAILURE;
        }
        return resultCode;
    }

    private String[] getArgsWithQuoteHandling(final String cmdline) {
        // replace spaces within quotes with a special symbol
        final String SPLIT_PATTERN = " +"; // one or more spaces
        final char SPACE_CHAR = ' ';
        final char REPLACEMENT_CHAR = 0x1e;

        char[] chars = cmdline.toCharArray();
        boolean isInsideQuotes = false;
        for (int i = 0, n = chars.length; i < n; i++) {
            if (chars[i] == '\"') {
                isInsideQuotes = !isInsideQuotes;
            } else if ((chars[i] == SPACE_CHAR) && isInsideQuotes) {
                chars[i] = REPLACEMENT_CHAR;
            }
        }

        String[] args = (String.copyValueOf(chars)).split(SPLIT_PATTERN);

        // revert special symbol back to SPACE
        for (int i = 0, n = args.length; i < n; i++) {
            args[i] = args[i].replace(REPLACEMENT_CHAR, SPACE_CHAR);
        }

        return args;
    }
}
