package net.kayur.ux.cli;

import java.util.Date;

public class ParsedDate {
    public final Date start;
    public final Date end;

    public ParsedDate(Date start, Date end) {
        this.start = start;
        this.end = end;
    }
}
