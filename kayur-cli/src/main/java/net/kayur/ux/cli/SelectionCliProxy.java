package net.kayur.ux.cli;

import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.UUID;

import static net.kayur.Msg.msg;

public class SelectionCliProxy implements DocumentSelection {
    @NotNull
    private final DocumentSelection selection;

    public SelectionCliProxy(@NotNull DocumentSelection selection) {
        this.selection = selection;
    }

    @Override
    @NotNull
    public Document getDocument(int pos) {
        if (pos >= 0 && pos < selection.size()) {
            return selection.getDocument(pos);
        } else {
            throw new IllegalArgumentException("Selection index is out of range.");
        }
    }

    @Override
    public void set(Collection<Document> items) {
        int oldSize = selection.size();
        selection.set(items);
        logSelectionChange(oldSize, selection.size());
    }

    @Override
    public void add(Collection<Document> items) {
        int oldSize = selection.size();
        selection.add(items);
        logSelectionChange(oldSize, selection.size());
    }

    private void logSelectionChange(int oldSize, int newSize) {
        if (oldSize != 0) {
            Disp.message("Selection change: %d -> %d bug reports.", oldSize, newSize);
        } else {
            Disp.message("Selected %d bug reports.", newSize);
        }
    }

    @Override
    public Collection<Document> get() {
        return selection.get();
    }

    @Override
    public boolean isEmpty() {
        boolean empty = selection.isEmpty();
        if (empty) {
            Disp.message(msg("msg_empty_selection"));
        }
        return empty;
    }

    @Override
    public int size() {
        return selection.size();
    }

    @Override
    public UUID id() {
        return selection.id();
    }
}
