package net.kayur.ux.cli;

import net.kayur.Disp;
import net.kayur.core.App;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;
import java.util.List;

/**
 * Research tool 'Kayur' for text mining assistance.
 *
 * Start-up class for CLI version of the tool.
 */
public class Kayur {

    // Entry point for REPL shell of the app.
    public static void main(String[] args) {
        var context = new ClassPathXmlApplicationContext("beans-cli.xml");
        var app = context.getBean(App.class);

        if (app.initialize()) {
            InputController inputController = context.getBean(InputController.class);

            if (args.length == 0) {
                // no args: start interactive shell and wait for commands
                startInteractiveShell(inputController);
            } else {
                // execute the scripts and exit
                executeScripts(inputController, List.of(args));
            }
        }

        app.shutdown();
    }

    private static void startInteractiveShell(InputController controller) {
        try {
            controller.startShell();
        } catch (IOException e) {
            Disp.error(e, "Failed to initialize an interactive shell.");
        }
    }

    private static void executeScripts(InputController controller, List<String> scriptNames) {
        for (var scriptName : scriptNames) {
            try {
                controller.executeScript(scriptName);
            } catch (IOException e) {
                Disp.error(e, String.format("Failed to execute script '%s'.", scriptName));
            }
        }
    }
}
