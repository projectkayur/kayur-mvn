package net.kayur.ux.cli.command;


import net.kayur.Disp;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.ux.cli.ResultCode;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;

import static net.kayur.Msg.msg;

public class CommandConfig extends AbstractCommand {
    private final TextEngineModuleLocator textEngineModuleLocator;

    public CommandConfig(MainConfiguration config, TextEngineModuleLocator textEngineModuleLocator) {
        super(config);
        this.textEngineModuleLocator = textEngineModuleLocator;

        mOptions.addOption("d", "default", false, msg("cmd_config_opt_default"));
    }

    @Override
    public ResultCode exec(String[] args) throws ParseException {
        CommandLine line = check(args);

        if (line.hasOption("d")) {
            config.initializeDefaultConfiguration(textEngineModuleLocator);
            Disp.message(msg("cfg_init_default"));
            return ResultCode.OK;
        }

        return ResultCode.FAILURE;
    }

    @Override
    public String getDescription() {
        return msg("cmd_config_desc");
    }
}
