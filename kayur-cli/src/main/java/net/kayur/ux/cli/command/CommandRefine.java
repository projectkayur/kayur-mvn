package net.kayur.ux.cli.command;

import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.nlp.analysis.ClusterCore;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusKeyword;
import net.kayur.nlp.corpus.CorpusManager;
import net.kayur.ux.cli.ResultCode;
import net.kayur.ux.cli.command.AbstractCommand;
import net.kayur.web.Document;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.ParseException;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.function.IntPredicate;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Command: filter selection by type, status, component, category, and some other attributes.
 */
public class CommandRefine extends AbstractCommand {
    private final DocumentSelection selection;
    private final CorpusManager corpusManager;

    public CommandRefine(MainConfiguration config,
                         DocumentSelection selection,
                         CorpusManager corpusManager) {
        super(config);
        this.selection = selection;
        this.corpusManager = corpusManager;

        mOptions.addOption("e", "every", true, "select every n-th document");
        mOptions.addOption("m", "metadata", true, "select documents that have the specified value in a metadata field (example: 'priority=HIGH')");
        mOptions.addOption("t", "tag", true, "select documents with the given tag");

        mOptions.addOption("n", "not", false, "negates the provided condition");

        mOptions.addOption("k", "keywords", true,
                "select documents that contain at least specified number of words out of N the most frequent words");
    }

    @Override
    public ResultCode exec(@NotNull String[] args) throws ParseException {
        CommandLine line = check(args);

        // parameters below require selection not empty
        if (selection.isEmpty()) {
            return ResultCode.FAILURE;
        }

        List<Document> newSelection;

        // --not option inverts the operation of --refine command
        boolean invertFlag = line.hasOption("n");

        // --every: refine by every N-th document
        if (line.hasOption("e")) {
            int refineBy = getPositiveIntOption(line, "e");

            // no flags => select every N-th document (N > 0)
            // --not => select all documents, except every N-th (N > 0)
            IntPredicate predicate = invertFlag ?
                    i -> i % refineBy != 0 :
                    i -> i % refineBy == 0;

            newSelection = IntStream.range(0, selection.size())
                    .filter(predicate)
                    .mapToObj(selection::getDocument)
                    .collect(Collectors.toList());
        }
        // --tag: refine by document's tag
        else if (line.hasOption("t")) {
            int tag = getIntOption(line, "t");

            Predicate<Integer> predicate = invertFlag ?
                    documentTag -> tag != documentTag :
                    documentTag -> tag == documentTag;

            newSelection = selection.get().stream()
                    .filter(document -> {
                        Integer documentTag = document.getTag();
                        return documentTag != null && predicate.test(documentTag);
                    })
                    .collect(Collectors.toList());
        }
        // --metadata: refine by metadata_name=value
        else if (line.hasOption("m")) {
            newSelection = new ArrayList<>();
            // TODO: selection by metadata
        } else if (line.hasOption("k")) {
            int minNumKeywords = getPositiveIntOption(line, "k");

            Corpus corpus = corpusManager.getCorpus();

            List<CorpusKeyword> keywords = corpus.getAllCorpusKeywords().stream()
                    .filter(keyword -> keyword.documentFrequency > 1)
                    .collect(Collectors.toList());

            ClusterCore core = new ClusterCore(keywords);
            List<String> idList = core.findDocumentsWithAtLeastNKeywords(minNumKeywords);

            newSelection = selection.get().stream()
                    .filter(document -> idList.contains(document.id.name))
                    .collect(Collectors.toList());
        } else {
            return ResultCode.FAILURE;
        }

        selection.set(newSelection);

        return ResultCode.OK;
    }

    /*
    private List<Document> refine(CommandLine line, String option, boolean invertFlag) {
        String value = line.getOptionValue(option); // case-sensitive

        // fix regular expression from user input to Java form
        // currently, user can provide only *
        if (value.contains("*") && !value.contains(".*")) {
            value = value.replaceAll("\\*", "\\.\\*");
        }

        if (value.contains("(") || value.contains(")")) {
            value = value.replaceAll("\\(", "\\\\\\(");
            value = value.replaceAll("\\)", "\\\\\\)");
        }

        // compile regular expression
        Pattern compiledPattern = Pattern.compile(value);

        return new ArrayList<>();
    }
    */

    @Override
    public String getDescription() {
        return "Narrow the set of selected documents.";
    }
}
