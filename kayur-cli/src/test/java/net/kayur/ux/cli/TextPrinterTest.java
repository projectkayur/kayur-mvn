package net.kayur.ux.cli;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;


class TextPrinterTest {
    @Test
    void table() {
        String[] headers = {"Sample", "Description"};
        String[][] table = {
                {"Information", "29.9", "Sample data"},
                {"Statistics", "1.23", "Sample"},
                {"Provision", "1.0", "U.S."},
                {"Environment", "12.5", "Paris"}
        };

        String result = TextPrinter.table(headers, table);
        System.out.println(result);

        String[] lines = result.split("\n");
        assertEquals(8, lines.length);

        long numUniqueLineLengths = Arrays.stream(lines).map(String::length).distinct().count();
        assertEquals(1, numUniqueLineLengths);
    }

    @Test
    void emptyTable() {
        String result = TextPrinter.table(new String[0], new String[0][0]);
        assertEquals("", result);
    }

}
