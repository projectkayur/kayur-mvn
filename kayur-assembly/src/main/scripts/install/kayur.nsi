# enable modern UI
!include "MUI2.nsh"

!define APPNAME "Kayur"
!define COMPANYNAME "kayur.net"
!define DESCRIPTION "A Web Mining Tool"
# These three must be integers
!define VERSIONMAJOR 1
!define VERSIONMINOR 5
!define VERSIONBUILD 1
# These will be displayed by the "Click here for support information" link in "Add/Remove Programs"
# It is possible to use "mailto:" links in here to open the email client
!define HELPURL "http://www.kayur.net" # "Support Information" link
!define UPDATEURL "http://www.kayur.net/download.php" # "Product Updates" link
!define ABOUTURL "http://www.kayur.net" # "Publisher" link
# This is the size (in kB) of all the files copied into "Program Files"
!define INSTALLSIZE 25503

RequestExecutionLevel user

# enable crc check
CRCCheck On

# define installer file properties
Name "Kayur"
InstallDir "$APPDATA\Kayur"
OutFile "kayur_installer.exe"
LicenseData "disclaimer.txt"
!define MUI_ICON "install.ico"
!define MUI_UNICON "install.ico"

# modern UI config
!insertmacro MUI_PAGE_LICENSE "disclaimer.txt"
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

# language
!insertmacro MUI_LANGUAGE "English"

# Install Section
Section "install"

# files to install
SetOutPath "$INSTDIR\model"
File "model\*.*"
SetOutPath "$INSTDIR\modules"
File "modules\*.xml"
SetOutPath "$INSTDIR\templates"
File "templates\*.xml"
SetOutPath "$INSTDIR\plugins"
File "plugins\*.*"
SetOutPath "$INSTDIR\bin"
File "bin\*.bat"
SetOutPath $INSTDIR
File "kayur.jar"
File "kayur.ico"
File "disclaimer.txt"

# desktop shortcut
CreateShortCut "$DESKTOP\Kayur.lnk" "$INSTDIR\bin\kayur.bat" "" "$INSTDIR\kayur.ico" 0

# start menu
CreateDirectory "$SMPROGRAMS\Kayur"
CreateShortCut "$SMPROGRAMS\Kayur\Kayur.lnk" "$INSTDIR\bin\kayur.bat" "" "$INSTDIR\kayur.ico" 0
CreateShortCut "$SMPROGRAMS\Kayur\Kayur (Shell).lnk" "$INSTDIR\bin\kayur-shell.bat" ""

# uninstaller registry keys and file
WriteRegStr HKCU "Software\Kayur" "DisplayName" "Kayur"

# Registry information for add/remove programs
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayName" "${APPNAME} - ${DESCRIPTION}"
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "InstallLocation" "$\"$INSTDIR$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayIcon" "$\"$INSTDIR\kayur.ico$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "Publisher" "${COMPANYNAME}"
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "HelpLink" "$\"${HELPURL}$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLUpdateInfo" "$\"${UPDATEURL}$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "URLInfoAbout" "$\"${ABOUTURL}$\""
WriteRegStr HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "DisplayVersion" "${VERSIONMAJOR}.${VERSIONMINOR}.${VERSIONBUILD}"
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMajor" ${VERSIONMAJOR}
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "VersionMinor" ${VERSIONMINOR}
# There is no option for modifying or repairing the install
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoModify" 1
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "NoRepair" 1
# Set the INSTALLSIZE constant (!defined at the top of this script) so Add/Remove Programs can accurately report the size
WriteRegDWORD HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}" "EstimatedSize" ${INSTALLSIZE}

WriteUninstaller $INSTDIR\uninstall.exe

SectionEnd

; uninstall

Section "Uninstall"

; delete uninstaller first
Delete $INSTDIR\uninstaller.exe

; delete all files in program folder and the folder itself
RMDir /r "$INSTDIR\*.*"
RMDir "$INSTDIR"

; delete start menu shortcuts and registry entries
Delete "$DESKTOP\Kayur.lnk"
Delete "$SMPROGRAMS\Kayur\*.*"
RmDir  "$SMPROGRAMS\Kayur"
DeleteRegKey HKCU "Software\Kayur"
DeleteRegKey HKCU "Software\Microsoft\Windows\CurrentVersion\Uninstall\${COMPANYNAME} ${APPNAME}"

SectionEnd
