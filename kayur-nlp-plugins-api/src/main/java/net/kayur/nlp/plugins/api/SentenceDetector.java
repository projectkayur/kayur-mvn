package net.kayur.nlp.plugins.api;

public interface SentenceDetector extends TextEngineModule {
    String[] detect(String text);
}
