package net.kayur.nlp.plugins.api;

public interface Stemmer extends TextEngineModule {

    void init(String[] tokens);

    String process(String token, int index);

}
