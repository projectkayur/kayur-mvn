package net.kayur.nlp.plugins.api;

public interface Tokenizer extends TextEngineModule {
    String[] tokenize(String sentence);
}
