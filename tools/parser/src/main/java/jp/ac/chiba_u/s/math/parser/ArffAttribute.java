package jp.ac.chiba_u.s.math.parser;

/**
 * A structure that represents an @attribute for ARFF format.
 */
public class ArffAttribute implements Comparable<ArffAttribute> {
    public final int mId;
    public final String mName;
    public final String mType;

    public ArffAttribute(int id, String name, String type) {
        mId = id;
        mName = name;
        mType = type;
    }

    @Override
    public int compareTo(ArffAttribute another) {
        return Integer.valueOf(this.mId).compareTo(another.mId);
    }
}
