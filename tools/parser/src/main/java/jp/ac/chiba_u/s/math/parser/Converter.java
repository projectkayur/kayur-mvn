package jp.ac.chiba_u.s.math.parser;

import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Contains the most important routines to translate input
 * in one format to output in another format.
 * The translation routines may differ between cases.
 */
public final class Converter {
    private Converter() {
    }

    /**
     * Translates HDP doc/word/topic assignments to ARFF format structure.
     *
     * @param documents list of structures from mode-word-assignments.dat
     * @return structure that describes data in ARFF format
     */
    public static ArffStructure<Double> hdpToArffStructure(List<HdpDocument> documents) {
        // get distinct topics among all documents
        List<ArffAttribute> attributeList = documents.stream()
                .flatMap(d -> d.getTopics().stream().map(pair -> pair.topic))
                .distinct()
                .sorted()
                // get pairs: attribute name, attribute type
                .map(topicId -> new ArffAttribute(topicId, String.format("topic%d", topicId), "numeric"))
                .collect(Collectors.toList());

        List<List<Pair<Integer, Double>>> valueList = new ArrayList<>();

        for (HdpDocument document : documents) {
            if (document.isEmpty()) {
                continue; // skip blank documents
            }

            // count the number of words related to each topic
            Map<Integer, Integer> topicCountMap = new HashMap<>();
            document.getTopics().forEach(pair -> Utils.count(topicCountMap, pair.topic));

            // total number of words
            double numWords = document.getTopics().size();

            valueList.add(topicCountMap.entrySet().stream()
                    // skip topics that do not occur in the document
                    .filter(e -> e.getValue() != 0)
                    // sort by topic ID
                    .sorted(Comparator.comparing(Map.Entry::getKey))
                    // ! number of words related to a topic divided by total number of words
                    .map(e -> new Pair<>(e.getKey(), ((double) e.getValue()) / numWords))
                    .collect(Collectors.toList()));
        }

        // return the structure that contains all information to write a file in ARFF format
        return new ArffStructure<>("topics", "%.3f", attributeList, valueList);
    }

    public static List<Pair<Integer, List<String>>> arffToClusterAssignments(ArffStructure<String> struct)
            throws ParseException {
        ArffAttribute clusterAttr = struct.mAttributeList.stream()
                .filter(a -> a.mName.equals("cluster"))
                .findFirst()
                .get();

        // split the '@attribute cluster' line to determine the number of clusters
        int numClusters = clusterAttr.mType.split(",").length;
        if (numClusters == 0) {
            throw new ParseException("No clusters are listed in 'cluster' attribute.", 0);
        }

        int numDocs = struct.getDocNum();

        for (List<Pair<Integer, String>> values : struct.mValueList) {
            // find cluster attribute, and get cluster id
            String cluster_id = "-1";
            for (Pair<Integer, String> pair : values) {
                if (pair.getFirst() == clusterAttr.mId) {
                    cluster_id = pair.getSecond();
                    break;
                }
            }

            /*

            // parse 'cluster' attribute pair
            attr_pair = parseAttributePair(values[values.length - 1]);
            if (attr_pair == null) {
                Disp.parseError(P_ERROR_ATTR_PARSE_FAILED, i);
                return null;
            }

            if (attr_pair.value.startsWith("cluster")) {
                id_attr_id = attr_pair.id - 1;

                subs_end_index = attr_pair.value.length() - 1;
                if (subs_end_index < P_PREFIX_LEN) {
                    Disp.parseError(P_ERROR_CLUSTER_ID_NOT_SET, i);
                    return null;
                }
                cluster_id_s = attr_pair.value.substring(P_PREFIX_LEN, subs_end_index);
                cluster_id = parsePositiveInt(cluster_id_s);
                if ((cluster_id < 1) || (cluster_id > topic_num)) {
                    Disp.parseError(P_ERROR_IMPROPER_CLUSTER_ID, i);
                    return null;
                }

                // parse ID attribute pair
                attr_pair = parseAttributePair(values[values.length - 2]);
                if (attr_pair == null) {
                    Disp.parseError(P_ERROR_ATTR_PARSE_FAILED, i);
                    return null;
                }

                if (attr_pair.id != id_attr_id) {
                    Disp.message("line %d: no ID attribute found! Skipping this.", i);
                    continue;
                }
                doc_id_s = attr_pair.value;
            } else {
                // No cluster attribute --> assign an instance to cluster 1
                cluster_id = 1;
                if (attr_pair.value.isEmpty()) {
                    Disp.parseError(P_ERROR_IMPROPER_DOC_ID, i);
                    return null;
                }
                doc_id_s = attr_pair.value.substring(0, attr_pair.value.length() - 1);
            }

            doc_id = parsePositiveInt(doc_id_s);
            if (doc_id == -1) {
                Disp.parseError(P_ERROR_IMPROPER_DOC_ID, i);
                return null;
            }

            t = corpusDescriptor.topicList.get(cluster_id - 1);
            t.size++;
            t.documents.add(doc_id);

            // doc_num++;*/
        }

        /*
        Collections.sort(corpusDescriptor.topicList, Collections.reverseOrder());

        String sDocNum = String.format("Documents: %d", corpus.documentNum);
        String sClusterNum = String.format("Number of Clusters: %d", corpus.topicNum);


        if (corpus.documentNum == 0) {
            Disp.message("Total number of documents equals zero! Exiting.");
            return;
        }

        if (!corpus.writeTopicAssignmentsIntoFiles(output_dir, false)) {
            Disp.message("Errors while saving cluster assignment data.");
        }



        List<String> logLines = new ArrayList<>();
        logLines.add(sDocNum);
        logLines.add(sClusterNum);

        // add wordTopics and their keywords (HDP mode)
        for (TopicHeader t : corpus.topicList) {
            String sClusterStat = String.format(
                    "Cluster %d: %d [%.1f p]",
                    t.id,
                    t.size,
                    100 * (double) t.size / (double) corpus.documentNum
            );
            logLines.add(sClusterStat);
        }

        // display results
        for (String s : logLines) {
            Disp.message(s);
        }

        // save log file
        String fn = path_wa;
        String fn_dir = "";
        if (fn.contains("/")) {
            fn = fn.substring(fn.lastIndexOf('/') + 1, fn.length() - 1);
            fn_dir = path_wa.substring(0, path_wa.lastIndexOf('/'));
        }
        if (fn.contains(".")) {
            fn = fn.substring(0, fn.lastIndexOf('.'));
        }
        fn = fn_dir + "/" + fn + ".log";

        fileHelper.saveLineByLine(fn, logLines.toArray(new String[logLines.size()]));
        */

        return new ArrayList<>();
    }
}
