package jp.ac.chiba_u.s.math.parser;

import java.util.List;

/**
 * Holds all information needed to write a file in ARFF format.
 */
public final class ArffStructure<T> {
    public final String mRelationName;
    public final String mAttributeFormat;
    public final List<ArffAttribute> mAttributeList;
    public final List<List<Pair<Integer, T>>> mValueList;

    public ArffStructure(String relationName, String attributeFormat,
                         List<ArffAttribute> attributeList,
                         List<List<Pair<Integer, T>>> valueList) {
        mRelationName = relationName;
        mAttributeFormat = attributeFormat;
        mAttributeList = attributeList;
        mValueList = valueList;
    }

    public int getDocNum() {
        return mValueList.size();
    }
}
