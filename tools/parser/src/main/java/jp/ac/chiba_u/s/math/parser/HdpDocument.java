package jp.ac.chiba_u.s.math.parser;

import java.util.List;

public class HdpDocument {
    private final int mId;
    private final List<WordTopicPair> mWordTopics;

    public HdpDocument(int id, List<WordTopicPair> topics) {
        mId = id;
        mWordTopics = topics;
    }

    public int getId() {
        return mId;
    }

    public void addTopic(WordTopicPair pair) {
        mWordTopics.add(pair);
    }

    public List<WordTopicPair> getTopics() {
        return mWordTopics;
    }

    public boolean isEmpty() {
        return mWordTopics.isEmpty();
    }
}
