package jp.ac.chiba_u.s.math.parser;

public final class Pair<T, U> {
    private final T mFirst;
    private final U mSecond;

    public Pair(T first, U second) {
        this.mFirst = first;
        this.mSecond = second;
    }

    public T getFirst() {
        return mFirst;
    }

    public U getSecond() {
        return mSecond;
    }
}
