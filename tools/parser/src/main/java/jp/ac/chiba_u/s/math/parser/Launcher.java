package jp.ac.chiba_u.s.math.parser;

import java.util.List;

/**
 * Parser and converter for HDP, Weka, and Kayur input formats.
 *
 * @version 2.0
 */
public class Launcher {
    private static void printUsage() {
        print("Usage: ./parser [hdp2arff | hdp-keywords | arff2clusters] [parameters]");
    }

    public static void main(String[] args) throws Exception {
        if (args.length == 0) {
            printUsage();
            return;
        }

        String mode = args[0];
        int numParameters = args.length - 1;

        switch (mode) {
            case "hdp2arff":
                if (numParameters != 2) {
                    print("Parameters: [path to mode-word-assignments.dat] [output file]");
                    return;
                }

                List<HdpDocument> hdpDocuments = ImportManager.parseHdpFile(args[1]);
                ArffStructure<Double> arffStructure = Converter.hdpToArffStructure(hdpDocuments);
                ExportManager.toArff(args[2], arffStructure);

                String statsMsg = String.format("Documents: %d.\nTopics: %d.",
                        hdpDocuments.size(), arffStructure.mAttributeList.size());
                print(statsMsg);

                break;
            case "hdp-keywords":
                if (numParameters != 1) {
                    print("Parameters: [path to topic/keywords file]");
                    return;
                }

                List<List<String>> keywords = ImportManager.parseHdpTopKeywordsFile(args[1]);

                for (int i = 0; i < keywords.size(); i++) {
                    String str = keywords.get(i).stream()
                            .reduce("", (a, s) -> a + " " + s);

                    print(String.format("Topic %d keywords: %s.", i, str));
                }

                break;
            case "arff2clusters":
                if (numParameters != 2) {
                    print("Parameters: [path to ARFF file] [output dir]");
                    return;
                }

                ArffStructure<String> struct = ImportManager.parseArff(args[1]);

                ExportManager.createDirectory(args[2]);
                ExportManager.toDocumentLists(args[2], Converter.arffToClusterAssignments(struct));

                break;
            default:
                printUsage();
                break;
        }
    }

    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }
}
