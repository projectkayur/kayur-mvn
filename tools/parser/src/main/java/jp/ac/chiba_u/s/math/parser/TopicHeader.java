package jp.ac.chiba_u.s.math.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Describes single cluster (topic)
 */
public class TopicHeader implements Comparable<TopicHeader> {
    public int id;
    public int size;
    public final List<Integer> documents = new ArrayList<Integer>();
    public List<String> keywords = new ArrayList<String>();

    public TopicHeader(int _id, int _size) {
        id = _id;
        size = _size;
    }

    @SuppressWarnings("NullableProblems")
    @Override
    public int compareTo(TopicHeader other) {
        return (this.size == other.size) ? 0 : ((this.size < other.size) ? -1 : 1);
    }
}
