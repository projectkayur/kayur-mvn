package jp.ac.chiba_u.s.math.parser;

public final class WordTopicPair implements Comparable<WordTopicPair> {
    public final int wordIndex;  // global word index
    public final int topic;      // topic of this word within the given document

    public WordTopicPair(int wi, int t) {
        wordIndex = wi;
        topic = t;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof WordTopicPair) {
            WordTopicPair another = (WordTopicPair) obj;
            return (this.wordIndex == another.wordIndex) && (this.topic == another.topic);
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(@SuppressWarnings("NullableProblems") WordTopicPair another) {
        return ((Integer)this.wordIndex).compareTo(another.wordIndex);
    }
}
