package jp.ac.chiba_u.s.math.parser;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Helper class that contains routines to write files in various formats.
 */
public final class ExportManager {
    private ExportManager() {
    }

    /**
     * Writes a file in WEKA's ARFF format.
     *
     * @param filename name of the file to create
     * @param struct structure that holds relation name, attributes and data values
     * @param <T> type of the attributes (all attributes must be of the same type)
     * @throws IOException
     */
    public static <T> void toArff(String filename, ArffStructure<T> struct) throws IOException {
        List<String> lines = new ArrayList<>();

        lines.add(String.format("@relation %s", struct.mRelationName));
        lines.add("");

        // write attribute list (@attribute name type)
        struct.mAttributeList.stream()
                .sorted()
                .forEach(a -> lines.add(String.format("@attribute %s %s", a.mName, a.mType)));

        // write instances
        lines.add("");
        lines.add("@data");

        StringBuilder builder = new StringBuilder();

        for (List<Pair<Integer, T>> values : struct.mValueList) {
            builder.setLength(0);
            builder.append("{");

            for (Pair<Integer, T> pair : values) {
                builder.append(String.format(", %d " + struct.mAttributeFormat, pair.getFirst(), pair.getSecond()));
            }

            builder.replace(1, 3, "");
            builder.append("}");

            lines.add(builder.toString());
        }

        write(filename, lines);
    }

    /**
     * Writes a list of document IDs for each cluster in a separate file.
     *
     * @param outputDir the directory where files will be placed
     * @param clusterAssignments list of pairs (cluster_id, list_of_document_ids)
     * @throws IOException
     */
    public static void toDocumentLists(String outputDir,
                                       List<Pair<Integer, List<String>>> clusterAssignments) throws IOException {
        final String SUFFIX = "_docs.txt";

        for (Pair<Integer, List<String>> clusterDescriptionPair : clusterAssignments) {
            int clusterId = clusterDescriptionPair.getFirst();
            String filename = FilenameUtils.concat(outputDir, String.format("%d%s", clusterId, SUFFIX));

            write(filename, clusterDescriptionPair.getSecond());
        }
    }

    /**
     * Writes a plain text file.
     *
     * @param filename name of the file to be created
     * @param lines lines to write
     * @throws IOException
     */
    public static void write(String filename, List<String> lines) throws IOException {
        FileUtils.writeLines(new File(filename), lines);
    }

    /**
     * Creates a directory.
     *
     * @param path name of the directory to be created
     * @return success / failure
     */
    public static boolean createDirectory(String path) {
        File p = new File(path);
        return p.exists() || p.mkdir();
    }
}
