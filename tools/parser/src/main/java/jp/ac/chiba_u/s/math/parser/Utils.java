package jp.ac.chiba_u.s.math.parser;

import java.util.Map;

/**
 * A helper class for general purpose functions.
 */
public final class Utils {
    private Utils() {
    }

    // Map utility functions

    public static <T> void count(Map<T, Integer> map, T key) {
        if (map.containsKey(key)) {
            map.put(key, map.get(key) + 1);
        } else {
            map.put(key, 1);
        }
    }

    public static <T> int max(Map<T, Integer> map) {
        int max = Integer.MIN_VALUE;
        for (Map.Entry<T, Integer> e : map.entrySet()) {
            if (e.getValue() > max) {
                max = e.getValue();
            }
        }
        return max;
    }
}
