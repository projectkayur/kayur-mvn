package jp.ac.chiba_u.s.math.parser;

/**
 * A structure to hold various statistics of conversion process.
 */
public class ConvertStatistics {
    public int numDocuments = 0;
    public int numClusters = 0;
}
