.PHONY: default
default:
	mvn package -Dmaven.test.skip=true

.PHONY: install
install: target
	mvn assembly:single
	tar xvfz ./target/kayur.tar.gz -C .

.PHONY: clean
clean:
	mvn clean

.PHONY: uninstall
uninstall:
	rm -rf ./kayur
