package net.kayur.nlp.formats;

import net.kayur.web.Document;
import net.kayur.nlp.dummy.NLPTestUtils;
import net.kayur.nlp.corpus.Corpus;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.assertNotEquals;


class ExportFormatJSONTest {
    @Test
    void export() {
        Document issue = new Document(1, "100");
        issue.setTitle("A simple document.");
        issue.setContent("A document for the JSON test.");
        issue.setComments(Arrays.asList("Comment one.", "Comment two."));

        Corpus corpus = NLPTestUtils.buildCorpus(Collections.singletonList(issue));

        ExportFormatJSON format = new ExportFormatJSON();
        ExportFormat.ExportResult result = format.export(corpus);

        assertNotEquals("", result.output);
        System.out.println(result.output);
    }
}
