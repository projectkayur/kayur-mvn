package net.kayur.nlp;

import net.kayur.Disp;
import net.kayur.nlp.statistics.Statistics;
import net.kayur.web.Document;
import net.kayur.web.Metadata;
import org.junit.jupiter.api.Test;

import java.util.*;


class StatisticsTest {
    private final Statistics statistics = new Statistics();

    @Test
    void getMetaStats() {
        List<Document> documents = new ArrayList<>();

        for (int i = 0; i < 1000; ++i) {
            Document d = new Document();
            List<Metadata> meta = new ArrayList<>();

            for (int j = 0, n = getInt(10); j < n; ++j) {
                String name = genStr(1, 5);
                String value = genStr(2, 3);
                if (value.equals("AA") || value.equals("BB") || value.equals("CC")) {
                    if (getInt(5) == 3)
                        meta.add(new Metadata(name, value));
                } else {
                    meta.add(new Metadata(name, value));
                }
            }

            d.setMeta(meta);
            documents.add(d);
        }

        Map<String, Map<String, Integer>> map = statistics.getMetaStats(documents, 0.1);

        Disp.message(printMetaStats(map));
    }

    public static String printMetaStats(Map<String, Map<String, Integer>> stats) {
        StringBuilder sb = new StringBuilder();

        stats.forEach((name, freqMap) -> {
            sb.append(name).append(" statistics:\n");
            freqMap.forEach((value, freq) -> sb.append(value).append(": ")
                    .append(freq)
                    .append(" documents\n"));
        });

        return sb.toString();
    }

    // utility functions

    private String genStr(int length, int variety) {
        Random random = new Random();
        byte[] symbols = new byte[length];

        for (int i = 0; i < symbols.length; i++)
            symbols[i] = (byte) (65 + random.nextInt(variety));

        return new String(symbols);
    }

    private int getInt(int bound) {
        Random random = new Random();
        return random.nextInt(bound);
    }
}