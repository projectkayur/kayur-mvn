package net.kayur.nlp.analysis;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DistributionTest {
    @Test
    void distribution() {
        List<Integer> numbers = Arrays.asList(1, 2, 1, 2, 2, 3, 3, 3, 4);
        int[] D = Distribution.of(numbers);

        assertEquals(5, D.length);
        assertEquals(0, D[0]);
        assertEquals(2, D[1]);
        assertEquals(3, D[2]);
        assertEquals(3, D[3]);
        assertEquals(1, D[4]);
    }

    @Test
    void leftCumulative() {
        List<Integer> numbers = Arrays.asList(1, 2, 1, 2, 2, 3, 3, 3, 4);
        int[] D = Distribution.of(numbers);
        Distribution.leftCumulative(D);

        assertEquals(5, D.length);
        assertEquals(1, D[4]);
        assertEquals(4, D[3]);
        assertEquals(7, D[2]);
        assertEquals(9, D[1]);
        assertEquals(9, D[0]);
    }
}
