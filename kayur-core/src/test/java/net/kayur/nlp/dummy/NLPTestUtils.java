package net.kayur.nlp.dummy;


import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.TextEngine;
import net.kayur.nlp.corpus.TextProcessingParameters;
import net.kayur.web.Document;

import java.util.List;

public final class NLPTestUtils {
    private static TextEngine getEngine() {
        TextEngine engine = new TextEngine();
        engine.setTokenizer(new DummyTokenizer());
        return engine;
    }

    public static Corpus buildCorpus(List<Document> documents) {
        TextProcessingParameters parameters = TextProcessingParameters.builder()
                .processTitles(true)
                .processContent(true)
                .processComments(true)
                .enableCache(false)
                .build();

        return getEngine().buildCorpus(documents, parameters);
    }
}
