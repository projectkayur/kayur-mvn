package net.kayur.nlp.dummy;


import net.kayur.nlp.plugins.api.Tokenizer;

public class DummyTokenizer implements Tokenizer {
    @Override
    public String[] tokenize(String sentence) {
        String filtered = sentence.replaceAll("[.,!?]", " ");
        return filtered.split(" +");
    }

    @Override
    public String getName() {
        return "Dummy Tokenizer";
    }
}
