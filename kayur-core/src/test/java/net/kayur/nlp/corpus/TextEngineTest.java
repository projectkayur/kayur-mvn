package net.kayur.nlp.corpus;

import net.kayur.TestUtils;
import net.kayur.helper.FileHelper;
import net.kayur.helper.XmlUtils;
import net.kayur.nlp.FilterExpr;
import net.kayur.nlp.plugins.openlp.CustomStemmer;
import net.kayur.nlp.plugins.openlp.OpenNLPSentenceDetector;
import net.kayur.nlp.plugins.openlp.OpenNLPTokenizer;
import net.kayur.web.Document;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class TextEngineTest {
    private static final String NLP_FILES_DIR = "../kayur-assembly/src/main/scripts";

    @Test
    void buildEmptyCorpus() {
        TextEngine engine = new TextEngine();
        TextProcessingParameters parameters = TextProcessingParameters.builder()
                .processTitles(false)
                .processContent(true)
                .processComments(false)
                .enableCache(false)
                .build();

        Corpus corpus = engine.buildCorpus(new ArrayList<>(), parameters);

        assertEquals(0, corpus.numDocuments());
        assertEquals(0, corpus.numWords());
        assertEquals(0.0, corpus.averageDocumentLength());
        assertEquals(0, corpus.getAllCorpusKeywords(10).size());
    }

    @Test
    void buildOneDocumentCorpus() throws Exception {
        TextEngine engine = new TextEngine();
        configureTextEngine(engine);

        Document document = loadDocument("lynx.txt");
        document.setTag(10);

        Collection<Document> documents = new ArrayList<>();
        documents.add(document);

        TextProcessingParameters parameters = TextProcessingParameters.builder()
                .processTitles(false)
                .processContent(true)
                .processComments(false)
                .enableCache(false)
                .build();

        Corpus corpus = engine.buildCorpus(documents, parameters);

        assertEquals(1, corpus.numDocuments());
        assertEquals(98, corpus.numWords());

        // document frequency should be 1 for all words
        List<CorpusKeyword> keywords = corpus.getAllCorpusKeywords(5);
        for (CorpusKeyword keyword : keywords) {
            assertEquals(1, keyword.documentFrequency);
        }

        // check document categories
        List<Integer> categories = corpus.getDocumentCategories();
        assertEquals(1, categories.size());
        assertEquals(10, (int) categories.get(0));

        List<String> words = corpus.getWords(false);
        System.out.println(words);
        assertEquals("lynx", words.get(0));
        assertEquals("solitary", words.get(1));
        assertEquals("cat", words.get(2));
        assertEquals("live", words.get(words.size() - 1));

        List<String> sortedWords = corpus.getWords(true);
        System.out.println(sortedWords);
        assertEquals("addition", sortedWords.get(0));
        assertEquals("young", sortedWords.get(sortedWords.size() - 1));

        assertEquals(98.0, corpus.averageDocumentLength());
    }

    private Document loadDocument(String filename) throws Exception {
        String text = TestUtils.readFile(filename);
        String[] parts = text.split("\n");
        int nextLineIndex = text.indexOf("\n") + 1;

        Document document = new Document();
        if (parts.length > 1 && nextLineIndex < text.length()) {
            String title = parts[0].trim();
            document.setTitle(title);
            String content = text.substring(text.indexOf("\n") + 1, text.length()).trim();
            document.setContent(content);
        } else {
            document.setContent(text);
        }

        return document;
    }

    private void configureTextEngine(TextEngine engine) throws Exception {
        engine.setTokenizer(new OpenNLPTokenizer());
        engine.setSentenceDetector(new OpenNLPSentenceDetector(NLP_FILES_DIR + "/plugins/en-sent.bin"));
        engine.setStemmer(CustomStemmer.builder()
                .taggerModelFilename(NLP_FILES_DIR + "/plugins/en-pos-maxent.bin")
                .verbListFilename(NLP_FILES_DIR + "/plugins/verbs.txt")
                .irregularVerbListFilename(NLP_FILES_DIR + "/plugins/irregular-verbs.xml")
                .build());

        engine.setStopWords(FileHelper.readLines(NLP_FILES_DIR + "/model/stopwords.txt"));

        String filtersXml = FileHelper.read(NLP_FILES_DIR + "/model/filters.xml");
        List<FilterExpr> filters = XmlUtils.traverse(filtersXml,
                a -> new FilterExpr(Integer.parseInt(a[0]), a[1], a[2]),
                "level", "input", "output");
        engine.setFilters(filters);
    }
}
