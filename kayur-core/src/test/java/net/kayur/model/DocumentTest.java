package net.kayur.model;

import net.kayur.web.Document;
import net.kayur.web.Metadata;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class DocumentTest {
    private static final long MODULE_ID = 102;

    private static Document issue;
    private static Document empty;

    @BeforeAll
    static void setup() {
        // a complete document with all fields set
        issue = new Document(MODULE_ID, "1000");
        issue.setDateTime(new Date());
        issue.setTitle("Lorem Ipsum");
        issue.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Praesent nunc neque, placerat vitae purus at, porttitor finibus tellus.");

        List<String> comments = Arrays.asList(
                "Quisque lacinia purus ipsum, et consectetur arcu gravida in.",
                "Phasellus nec dui vel risus congue blandit id ut mi."
        );
        issue.setComments(comments);

        List<Metadata> meta = Arrays.asList(
                new Metadata("Lorem", "ipsum"),
                new Metadata("Vestibulum", "turpis")
        );
        issue.setMeta(meta);

        // an empty document
        empty = new Document(MODULE_ID, "1001");
    }

    @Test
    void testGetDate() {
        assertNotNull(issue.getDateTime());
        assertNull(empty.getDateTime());
    }

    @Test
    void testGetText() {
        String text = issue.getText();
        System.out.println(text);
        // no double spacing
        assertFalse(text.contains("  "));
        // no extra space at the end
        assertFalse(text.endsWith(" "));
        // no new line
        assertFalse(text.contains("\n"));
    }

    @Test
    void testToString() {
        String issueStr = issue.toString();
        System.out.println(issueStr);
        assertTrue(issueStr.contains("Lorem Ipsum"));
        assertTrue(issueStr.contains("Lorem ipsum"));

        String emptyIssueStr = empty.toString();
        System.out.println(emptyIssueStr);
        assertTrue(emptyIssueStr.contains("1001")); // should contain module ID
    }
}
