package net.kayur.core.configuration;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class ConfigurationTest {
    private Parameter<String> p1;

    @BeforeEach
    void setUpEach() {
        p1 = new StringParameter("group 1", "param 1");
    }

    @Test
    void testGet() {
        Configuration cfg = new Configuration();
        cfg.set(p1, "value 1");

        assertEquals("value 1", cfg.get(p1));
        assertTrue(cfg.get(new StringParameter("group 1", "param 2")).isEmpty());
        assertTrue(cfg.get(new StringParameter("group 2", "param 1")).isEmpty());
    }

    @Test
    void getNonNull() {
        Configuration cfg = new Configuration();
        cfg.set(p1, "");
        assertThrows(ConfigurationException.class, () -> cfg.getValidated(p1));
    }

    @Test
    void getNonNullNotSet() {
        Configuration cfg = new Configuration();
        assertThrows(ConfigurationException.class, () -> cfg.getValidated(p1));
    }

    @Test
    void getInteger() {
        Configuration cfg = new Configuration();
        Parameter<Integer> param = new IntParameter("group 3", "param 1");

        assertEquals(cfg.get(param), param.getDefault());

        cfg.set(param, 123456);
        Integer val = cfg.get(param);
        assertEquals(123456, val.intValue());
    }

    private Configuration getGenericConfig() {
        Configuration cfg = new Configuration();
        cfg.set(new StringParameter("first", "p1"), "val-1");
        cfg.set(new StringParameter("first", "p2"), "val-2");
        cfg.set(new StringParameter("second", "p1"), "val-3");
        cfg.set(new StringParameter("second", "p2"), "val-4");
        cfg.set(new StringParameter("second", "p3"), "val-5");
        cfg.set(new StringParameter("third", "p1"), "val-6");
        return cfg;
    }

    @Test
    void testToXML() throws Exception {
        Configuration cfg = getGenericConfig();

        String result = cfg.toXML();
        System.out.println(result);

        assertTrue(result.contains("first") && result.contains("second") && result.contains("third"));

        for (int i = 1; i <= 6; i++) {
            assertTrue(result.contains(String.format("val-%d", i)));
        }

        assertTrue(result.indexOf("first") < result.indexOf("second"));
        assertTrue(result.indexOf("second") < result.indexOf("third"));
    }

    @Test
    void testFromXML() throws Exception {
        String result = getGenericConfig().toXML();
        Configuration cfg = Configuration.fromXmlString(result);
        assertEquals(cfg.get(new StringParameter("first", "p1")), "val-1");
        assertEquals(cfg.get(new StringParameter("first", "p2")), "val-2");
        assertEquals(cfg.get(new StringParameter("second", "p1")), "val-3");
        assertEquals(cfg.get(new StringParameter("second", "p2")), "val-4");
        assertEquals(cfg.get(new StringParameter("second", "p3")), "val-5");
        assertEquals(cfg.get(new StringParameter("third", "p1")), "val-6");
    }

    @Test
    void testEquals() {
        Configuration cfg1 = new Configuration();
        Configuration cfg2 = new Configuration();

        cfg1.set(p1, "x");
        cfg2.set(p1, "x");
        assertEquals(cfg1, cfg2);

        cfg2.set(p1, "y");
        assertNotEquals(cfg1, cfg2);
    }
}
