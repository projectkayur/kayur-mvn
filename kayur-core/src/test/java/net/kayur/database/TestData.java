package net.kayur.database;

import net.kayur.web.Document;
import net.kayur.web.Metadata;

import java.util.*;

public class TestData {
    public static final long TEST_MODULE = 100;
    private static final long OTHER_TEST_MODULE = 101;

    public static DatabaseConfiguration createDatabaseConfigForTests() {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setUsername("kayur");
        config.setPassword("kayur");
        config.setConnectionString("jdbc:mariadb://localhost:3306/kayur");
        config.setDriver("org.mariadb.jdbc.Driver");
        return config;
    }

    public static Document createEmptyIssue() {
        return new Document(TEST_MODULE, "1000");
    }

    public static Document createDocumentWithThreeComments() {
        Document issue = new Document(TEST_MODULE, "1001");
        issue.setTitle("Issue with comments, but no metadata.");
        issue.setContent("Some text.");

        List<String> comments = Arrays.asList(
                "First comment.",
                "Second comment.",
                "And finally the third comment.");
        issue.setComments(comments);

        return issue;
    }

    public static Document createIssueWithMeta() {
        Document issue = new Document(TEST_MODULE, "1002");
        issue.setDateTime(new Date());
        issue.setTitle("Lorem");
        issue.setTag(20);
        List<Metadata> meta = Collections.singletonList(
                new Metadata("Lorem", "not ipsum")
        );
        issue.setMeta(meta);
        return issue;
    }

    public static Document createDocumentTwoCommentsTwoMeta() {
        Document issue = new Document(TEST_MODULE, "1003");
        issue.setDateTime(new Date(100_000));
        issue.setTitle("Lorem Ipsum");
        issue.setContent("Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Praesent nunc neque, placerat vitae purus at, porttitor finibus tellus.");

        List<String> comments = Arrays.asList(
                "Quisque lacinia purus ipsum, et consectetur arcu gravida in.",
                "Phasellus nec dui vel risus congue blandit id ut mi."
        );
        issue.setComments(comments);

        List<Metadata> meta = Arrays.asList(
                new Metadata("Lorem", "ipsum"),
                new Metadata("Vestibulum", "turpis")
        );
        issue.setMeta(meta);

        return issue;
    }

    public static Document createLongIssue() {
        Document issue = new Document(TEST_MODULE, "1004");

        byte[] symbols = new byte[512 * 1024];
        Random random = new Random();
        for (int i = 0; i < symbols.length; i++)
            symbols[i] = (byte) (65 + random.nextInt(26));

        String text = new String(symbols);
        issue.setContent(text);
        issue.setComments(Collections.singletonList(text));
        return issue;
    }

    public static Document createOtherModuleIssue() {
        Document issue = new Document(OTHER_TEST_MODULE, "1000");
        issue.setTitle("Completely new theme.");
        issue.setContent("This is a document fetched by another module.");
        return issue;
    }

    public static List<Document> createDocuments() {
        List<Document> documents = new ArrayList<>();
        documents.add(createDocumentWithThreeComments());
        documents.add(createEmptyIssue());
        documents.add(createIssueWithMeta());
        documents.add(createLongIssue());
        documents.add(createOtherModuleIssue());
        documents.add(createDocumentTwoCommentsTwoMeta());
        return documents;
    }
}
