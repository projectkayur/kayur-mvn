package net.kayur.database;

import net.kayur.database.jpa.TComment;
import net.kayur.database.jpa.TIssue;
import net.kayur.database.jpa.TMeta;
import net.kayur.database.jpa.TMetaField;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;


class TruncationTest {
    private static TIssue generateIssue() {
        TIssue issue = new TIssue();
        issue.setTitle("Is there any word for 10 days in English?");
        issue.setContent("For example, I am preparing a report for 1-10 of the month, " +
                "then 11 to 20 and then 21 to 30. Is there any specific word for 10 days in English" +
                " like weekly report which consist of 7 days?)");
        issue.setComments(Arrays.asList(
                new TComment("If there were a common word then the idiom \"a week or ten days\" would likely reflect it. " +
                        "\"A third of the month\" is probably the best you can do."),
                new TComment("The auto industry used to report sales using a \"10-day period\"")
        ));
        issue.setMeta(Collections.singletonList(
                new TMeta(new TMetaField("URL"),
                        "http://english.stackexchange.com/questions/363469/is-there-any-word-for-10-days-in-english")
        ));

        return issue;
    }

    @Test
    void testTruncate() {
        Truncation t = new Truncation(new TruncationSettings());
        String s1 = "12345";
        assertEquals("1234", t.truncate(s1, 4));

        String s2 = "1234 5678 90";
        assertEquals("1234 5678", t.truncate(s2, 9));
        assertEquals("1234", t.truncate(s2, 8));

        String s3 = "abc defghijklmnopqr";
        assertEquals("abc defghijklmnopq", t.truncate(s3, 18, 1));
        assertEquals("abc defghijklmnopq", t.truncate(s3, 18, 14));
        assertEquals("abc", t.truncate(s3, 18, 15));
        assertEquals("abc", t.truncate(s3, 18, 32));
    }

    @Test
    void apply() {
        TIssue issue = generateIssue();
        String originalContent = issue.getContent();

        TruncationSettings settings = new TruncationSettings();
        settings.setTitleLen(36);
        settings.setCommentLen(20);
        settings.setMetaLen(24);

        Truncation tr = new Truncation(settings);
        tr.apply(issue);

        assertEquals("Is there any word for 10 days in", issue.getTitle());
        assertEquals("If there were a", issue.getComments().get(0).getComment());
        assertEquals("The auto industry", issue.getComments().get(1).getComment());
        assertEquals(issue.getContent(), originalContent);
        assertEquals("http://english.stackexch", issue.getMeta().get(0).getValue());
    }

    @Test
    void applyEmptyIssue() {
        TIssue issue = new TIssue();

        TruncationSettings settings = new TruncationSettings();
        settings.setContentLen(64);
        settings.setMetaLen(32);

        Truncation tr = new Truncation(settings);
        tr.apply(issue);

        assertNull(issue.getContent());
        assertEquals(0, issue.getMeta().size());
    }

    @Test
    void isEnabled() {
        TruncationSettings settings = new TruncationSettings();
        Truncation tr = new Truncation(settings);
        assertFalse(tr.isEnabled());

        settings.setTitleLen(64);
        assertTrue(tr.isEnabled());

        settings.setTitleLen(0);
        assertFalse(tr.isEnabled());

        settings.setMetaLen(32);
        assertTrue(tr.isEnabled());
    }

}