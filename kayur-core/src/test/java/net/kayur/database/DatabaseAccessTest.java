package net.kayur.database;

import net.kayur.TestConfig;
import net.kayur.database.jpa.TComment;
import net.kayur.database.jpa.TIssue;
import net.kayur.helper.SimpleTimer;
import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import net.kayur.web.Metadata;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static net.kayur.database.TestData.*;
import static org.junit.jupiter.api.Assertions.*;


class DatabaseAccessTest {
    private ConnectionHub connectionHub;
    private DatabaseConfiguration config;

    private DatabaseAccess access;

    DatabaseAccessTest() {
        connectionHub = new ConnectionHub();
        config = TestConfig.DATABASE_CONFIG;
    }

    @BeforeEach
    void setUpEach() {
        access = new DatabaseAccess(connectionHub.getConnection(config), config.truncation);
        assertTrue(access.findIssues().isEmpty());  // database should not contain any records
    }

    @AfterEach
    void tearDownEach() {
        // remove all documents
        access.remove(access.findIssues());
        access.remove(access.allMetaFields());
        // close connection
        access.close();
    }

    private List<DocumentId> getIdentifiers(List<TIssue> issues) {
        return issues.stream()
                .map(TIssue::toDocumentId)
                .collect(Collectors.toList());
    }

    private int randomSmallNat() {
        return 1 + new Random().nextInt(1000);
    }

    private Document createEmptyDocument() {
        return new Document(randomSmallNat(), "123456789");
    }

    private List<Document> createSampleDocumentCollection() {
        return Arrays.asList(createEmptyIssue(), createDocumentWithThreeComments(), createIssueWithMeta(),
                createOtherModuleIssue(), createLongIssue(), createDocumentTwoCommentsTwoMeta());
    }


    /**************************
     * tests
     **************************/

    @Test
    void storeAndFindOneEmptyIssue() {
        Document document = createEmptyDocument();
        access.storeIssue(document);
        assertNotNull(access.findIssue(document.id));
    }

    @Test
    void storeAndFindOneIssueWithComments() {
        Document document = createEmptyDocument();
        document.setTitle("New Document");
        document.setContent("Test document content");
        document.setComments(Arrays.asList("First comment.", "Second comment.", "Third comment."));
        access.storeIssue(document);

        TIssue result = access.findIssue(document.id);
        assertNotNull(result);
        assertEquals(document.id, result.toDocumentId());
        assertEquals(document.getTitle(), result.getTitle());
        assertEquals(document.getContent(), result.getContent());
        assertEquals(3, result.getComments().size());

        String joined = result.getComments().stream()
                .map(TComment::getComment)
                .collect(Collectors.joining(" "));
        document.getComments().forEach(comment -> assertTrue(joined.contains(comment)));
    }

    @Test
    void storeAndFindOneIssueWithCommentsAndMeta() {
        Document document = createEmptyDocument();
        document.setComments(Arrays.asList("First one.", "Second one."));
        document.setMeta(Arrays.asList(
                new Metadata("category", "data science"),
                new Metadata("pages", "17")));
        access.storeIssue(document);

        TIssue result = access.findIssue(document.id);

        assertNotNull(result);
        assertEquals(2, result.getComments().size());
        assertEquals(2, result.getMeta().size());
    }

    @Test
    void storeAndFindMultipleIssues() {
        List<Document> documents = createSampleDocumentCollection();
        access.storeIssues(documents);
        assertEquals(documents.size(), access.findIssues().size());
    }

    @Test
    void updateIssueContent() {
        Document document = createEmptyDocument();
        document.setContent("some text");
        access.storeIssue(document);

        TIssue result = access.findIssue(document.id);
        assertNotNull(result);
        assertEquals("some text", result.getContent());

        document.setContent("new content");
        access.storeIssue(document);

        TIssue updatedResult = access.findIssue(document.id);
        assertNotNull(updatedResult);
        assertEquals("new content", updatedResult.getContent());
    }

    @Test
    void deleteIssue() {
        Document document = createDocumentTwoCommentsTwoMeta();
        access.storeIssue(document);
        assertNotNull(access.findIssue(document.id));

        access.deleteIssue(document.id);
        assertNull(access.findIssue(document.id));
    }

    @Test
    void findIssuesByTag() {
        Document d1 = createEmptyDocument(), d2 = createEmptyDocument(), d3 = createEmptyDocument();
        access.storeIssues(Arrays.asList(d1, d2, d3));

        int tag = randomSmallNat();
        access.setNumericTag(Arrays.asList(d1.id, d2.id), tag);

        SelectQueryParameters parameters = new SelectQueryParameters();
        parameters.setTag(tag);
        parameters.setTagOperator("=");
        List<TIssue> result = access.findIssues(parameters);
        List<DocumentId> resultIdentifiers = getIdentifiers(result);

        assertEquals(2, result.size());
        assertTrue(resultIdentifiers.contains(d1.id));
        assertTrue(resultIdentifiers.contains(d2.id));
    }

    @Test
    void findIssuesByModuleId() {
        Document d1 = new Document(2, "D1");
        Document d2 = new Document(3, "D2");
        Document d3 = new Document(2, "D3");
        access.storeIssues(Arrays.asList(d1, d2, d3));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setModule(2);
        List<TIssue> result = access.findIssues(params);
        List<DocumentId> resultIdentifiers = getIdentifiers(result);

        assertEquals(2, result.size());
        assertTrue(resultIdentifiers.contains(d1.id));
        assertTrue(resultIdentifiers.contains(d3.id));
    }

    @Test
    void findIssuesByDate() {
        Document d1 = new Document(1, "D1");
        d1.setDateTime(new Date(900));
        Document d2 = new Document(1, "D2");
        d2.setDateTime(new Date(1000));
        Document d3 = new Document(1, "D3");
        d3.setDateTime(new Date(1100));
        access.storeIssues(Arrays.asList(d1, d2, d3));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setStartDate(new Date(950));
        params.setEndDate(new Date(1100));
        List<TIssue> result = access.findIssues(params);
        List<DocumentId> resultIdentifiers = getIdentifiers(result);

        assertEquals(2, result.size());
        assertTrue(resultIdentifiers.contains(d2.id));
        assertTrue(resultIdentifiers.contains(d3.id));
    }

    @Test
    void findIssuesByDateSortAscDesc() {
        Document d1 = new Document(1, "D1");
        d1.setDateTime(new Date(2000));
        Document d2 = new Document(1, "D2");
        d2.setDateTime(new Date(1000));
        Document d3 = new Document(1, "D3");
        d3.setDateTime(new Date(3000));
        access.storeIssues(Arrays.asList(d1, d2, d3));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setAscOrder(true);
        List<TIssue> result = access.findIssues(params);

        assertEquals(d2.id, result.get(0).toDocumentId());
        assertEquals(d1.id, result.get(1).toDocumentId());
        assertEquals(d3.id, result.get(2).toDocumentId());

        params.setAscOrder(false);
        result = access.findIssues(params);

        assertEquals(d3.id, result.get(0).toDocumentId());
        assertEquals(d1.id, result.get(1).toDocumentId());
        assertEquals(d2.id, result.get(2).toDocumentId());
    }

    @Test
    void findIssuesByDateSameStartAndEndDates() {
        Document d1 = new Document(1, "D1");
        d1.setDateTime(new Date(1000));
        Document d2 = new Document(1, "D2");
        d2.setDateTime(new Date(1001));
        access.storeIssues(Arrays.asList(d1, d2));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setStartDate(new Date(1000));
        params.setEndDate(new Date(1000));
        List<TIssue> result = access.findIssues(params);
        assertEquals(1, result.size());
        assertEquals(d1.id, result.get(0).toDocumentId());
    }

    @Test
    void findIssuesByIdPattern() {
        Document d1 = new Document(1, "D1");
        Document d2 = new Document(1, "E1");
        Document d3 = new Document(1, "D2");
        access.storeIssues(Arrays.asList(d1, d2, d3));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setDocumentId("D%");
        List<TIssue> result = access.findIssues(params);
        List<DocumentId> resultIdentifiers = getIdentifiers(result);

        assertEquals(2, result.size());
        assertTrue(resultIdentifiers.contains(d1.id));
        assertTrue(resultIdentifiers.contains(d3.id));
    }

    @Test
    void findIssuesByIdList() {
        Document d1 = createEmptyDocument(), d2 = createEmptyDocument(), d3 = createEmptyDocument();
        access.storeIssues(Arrays.asList(d1, d2, d3));

        List<TIssue> result = access.findIssues(Arrays.asList(d1.id, d3.id));
        List<DocumentId> resultIdentifiers = getIdentifiers(result);

        assertEquals(2, result.size());
        assertTrue(resultIdentifiers.contains(d1.id));
        assertTrue(resultIdentifiers.contains(d3.id));
    }

    @Test
    void findIssuesByContentPattern() {
        Document d1 = createEmptyDocument();
        d1.setContent("This is a story about an ordinary girl, who...");
        Document d2 = createEmptyDocument();
        d2.setContent("Remarkable stories about greatest discoveries");
        access.storeIssues(Arrays.asList(d1, d2));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setContent("%story%");
        List<TIssue> result = access.findIssues(params);

        assertEquals(1, result.size());
        assertEquals(d1.id, result.get(0).toDocumentId());
    }

    @Test
    void findIssuesByTitlePattern() {
        Document d1 = createEmptyDocument();
        d1.setTitle("Interesting stories about cats");
        Document d2 = createEmptyDocument();
        d2.setTitle("Inappropriate results");
        Document d3 = createEmptyDocument();
        d3.setTitle("Everything about cats you should know");
        access.storeIssues(Arrays.asList(d1, d2, d3));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setTitle("%cats%");
        List<TIssue> result = access.findIssues(params);

        assertEquals(2, result.size());
        List<DocumentId> ids = getIdentifiers(result);
        assertTrue(ids.contains(d1.id));
        assertTrue(ids.contains(d3.id));
    }

    @Test
    void findLatestIssue() {
        Document d1 = new Document(1, "D1");
        d1.setDateTime(new Date(1000));
        Document d2 = new Document(1, "D2");
        d2.setDateTime(new Date(2000));
        Document d3 = new Document(1, "D3");
        d3.setDateTime(new Date(1500));
        access.storeIssues(Arrays.asList(d1, d2, d3));

        TIssue issue = access.getLatestIssue(1);
        assertNotNull(issue);
        assertEquals(d2.id, issue.toDocumentId());
    }

    @Test
    void findLatestIssueNoIssuesStored() {
        assertNull(access.getLatestIssue(1000));
    }

    @Test
    void defaultNumericTagDoesNotExist() {
        access.storeIssues(createSampleDocumentCollection());

        SelectQueryParameters parameters = new SelectQueryParameters();
        parameters.setTag(0);
        parameters.setTagOperator("=");
        assertTrue(access.findIssues(parameters).isEmpty());
    }

    @Test
    void secondDatabaseAccessIsMuchFasterThanFirst() {
        ConnectionHub hub = new ConnectionHub();

        // first database config
        DatabaseConfiguration cfg1 = DatabaseConfiguration.from(TestConfig.DATABASE_CONFIG);

        SimpleTimer timer = new SimpleTimer();
        new DatabaseAccess(hub.getConnection(cfg1)).close();
        long firstAccessTime = timer.time();
        System.out.printf("First database access: %d ms.\n", firstAccessTime);

        // second database config, which equals to the first one
        DatabaseConfiguration cfg2 = DatabaseConfiguration.from(TestConfig.DATABASE_CONFIG);

        timer.reset();
        new DatabaseAccess(hub.getConnection(cfg2)).close();
        long secondAccessTime = timer.time();
        System.out.printf("Second database access: %d ms.\n", secondAccessTime);

        assertTrue(secondAccessTime < (firstAccessTime / 10));
    }

}
