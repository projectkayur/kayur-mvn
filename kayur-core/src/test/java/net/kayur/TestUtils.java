package net.kayur;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class TestUtils {
    public static String readFile(String filename) throws URISyntaxException, IOException {
        URL url = TestUtils.class.getClassLoader().getResource(filename);
        if (url == null) {
            throw new IllegalArgumentException("No such file in test resources.");
        }

        return new String(Files.readAllBytes(Paths.get(url.toURI())), "UTF-8");
    }
}
