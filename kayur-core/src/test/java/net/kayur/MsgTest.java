package net.kayur;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MsgTest {

    @Test
    void msg() {
        var result = Msg.msg("wizard_dialog_title", "a");

        assertEquals("New Module Wizard", result);
    }

}
