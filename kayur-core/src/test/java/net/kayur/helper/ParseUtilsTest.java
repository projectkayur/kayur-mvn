package net.kayur.helper;

import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static net.kayur.helper.ParseUtils.*;
import static org.junit.jupiter.api.Assertions.assertEquals;


class ParseUtilsTest {
    @Test
    void parseArgList() {
        String s = "x=123,test=now,value=something";
        Map<String, String> map = parseArgumentList(s);
        assertEquals(map.size(), 3);
        assertEquals(map.get("x"), "123");
        assertEquals(map.get("test"), "now");
        assertEquals(map.get("value"), "something");
    }

    @Test
    void parseArgListEmpty() {
        assertEquals(0, parseArgumentList("").size());
    }

    @Test
    void testReplaceVars() {
        String s;
        Map<String, String> map = new HashMap<>();
        map.put("id", "123");
        map.put("name", "www.site.com");

        s = "http://www.site.com/${id}";
        assertEquals("http://www.site.com/123", replaceVariables(s, map));
        s = "http://www.site.com/${id}/${id}/text.html";
        assertEquals("http://www.site.com/123/123/text.html", replaceVariables(s, map));
        s = "http://${name}/${id}.html";
        assertEquals("http://www.site.com/123.html", replaceVariables(s, map));
        s = "id = ${id}, version = ${id}";
        assertEquals("id = 123, version = 123", replaceVariables(s, map));
        s = "${id}";
        assertEquals("123", replaceVariables(s, map));
        s = "${id";
        assertEquals("${id", replaceVariables(s, map));
        s = "$id}";
        assertEquals("$id}", replaceVariables(s, map));
        s = "$id";
        assertEquals("$id", replaceVariables(s, map));
        s = "$${id}";
        assertEquals("$123", replaceVariables(s, map));
        s = "${someVar}"; // variable not defined
        assertEquals("${someVar}", replaceVariables(s, map));
    }
}
