package net.kayur.helper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FileHelperTest {
    @Test
    void testStripComments() {
        assertEquals("", FileHelper.stripComment(""));
        assertEquals("", FileHelper.stripComment("# a comment"));
        assertEquals("command(args);", FileHelper.stripComment("command(args); # a comment"));
        assertEquals("test", FileHelper.stripComment("test#"));
    }
}
