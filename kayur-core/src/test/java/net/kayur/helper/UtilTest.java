package net.kayur.helper;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class UtilTest {
    @Test
    void parseIntegerRemoveNoise() throws Exception {
        String value = "9,457,123";
        assertEquals(9457123, ParseUtils.parseIntegerRemoveNoise(value));
    }



    @Test
    void parseLong() throws Exception {
        assertThrows(ParseException.class, () -> ParseUtils.parseLong(""));
        assertEquals(1234567890123456L, ParseUtils.parseLong("1234567890123456"));
    }
}
