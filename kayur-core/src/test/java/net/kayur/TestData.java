package net.kayur;

import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.generator.IdGenerator;
import net.kayur.web.module.generator.IdGeneratorFactory;
import net.kayur.web.module.generator.SearchGenerator;

public class TestData {
    public static ModuleConfiguration generateGentooBugzillaConfig() {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.setId(1000);
        cfg.setName("Gentoo Bugzilla");

        cfg.set(ModuleConfiguration.OPTION_DOC_URL_TEMPLATE,
                "https://bugs.gentoo.org/show_bug.cgi?id=${id}");
        cfg.setRule(ModuleConfiguration.OPTION_CONTENT_RULE, "[short_desc_nonedit_display]", "");
        cfg.setRule(ModuleConfiguration.OPTION_COMMENTS_RULE, "[bz_comment_text,cls,*]", "");
        cfg.setRule(ModuleConfiguration.OPTION_TITLE_RULE, "[short_desc_nonedit_display]", "");
        cfg.set(ModuleConfiguration.OPTION_DOC_DATE_FMT, "yyyy-MM-dd HH:mm:ss z");
        cfg.setRule(ModuleConfiguration.OPTION_DATE_RULE, "[bz_comment_time,cls]", "");

        // metadata
        cfg.setMetaRule("status", "[static_bug_status]", "");
        cfg.setMetaRule("product", "[field_container_product]", "");

        cfg.set(IdGenerator.OPTION_ID_GENERATOR_CLS, IdGeneratorFactory.TYPE_ITERATIVE);

        return cfg;
    }

    public static ModuleConfiguration generateSlashdotSearchConfig() {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.setId(2000);
        cfg.setName("Slashdot Search on Windows");

        cfg.set(ModuleConfiguration.OPTION_DOC_URL_TEMPLATE,
                "https:${id}");

        cfg.set(IdGenerator.OPTION_ID_GENERATOR_CLS, IdGeneratorFactory.TYPE_SEARCH);

        cfg.set(SearchGenerator.OPTION_INITIAL_SEARCH_PAGE_URL,
                "https://slashdot.org/index2.pl?fhfilter=Windows");
        cfg.set(SearchGenerator.OPTION_NEXT_SEARCH_PAGE_URL,
                "https://slashdot.org/?page=${page}&view=search&fhfilter=Windows");
        cfg.setRule(SearchGenerator.OPTION_LINKS_RULE,
                "[article,tag][story-title,cls][a,tag][href,attr]", "");

        return cfg;
    }
}
