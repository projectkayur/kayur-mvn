package net.kayur;

import net.kayur.database.DatabaseConfiguration;

public class TestConfig {
    public static final DatabaseConfiguration DATABASE_CONFIG =
            new DatabaseConfiguration("jdbc:h2:mem:kayur;INIT=CREATE SCHEMA IF NOT EXISTS kayur");
}
