package net.kayur.web.module;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.web.WebModuleException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;


class FetchModuleTest {
    @Test
    void testDocsNumberEmptyConfig() {
        assertThrows(ConfigurationException.class,
                () -> new FetchModule(new ModuleConfiguration()).getNumberOfAvailableDocuments());
    }

    @DisplayName("Get valid number of documents")
    @Test
    void testDocsNumber() {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.set(ModuleConfiguration.OPTION_NUM_DOCS_URL, "https://en.wikipedia.org/wiki/618");
        cfg.setRule(ModuleConfiguration.OPTION_NUM_DOCS_RULE, "[firstHeading]", "");

        FetchModule module = new FetchModule(cfg);
        assertEquals(module.getNumberOfAvailableDocuments(), 618L);
    }

    @Test
    void testDocsNumberNotValidNumber() {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.set(ModuleConfiguration.OPTION_NUM_DOCS_URL, "https://en.wikipedia.org/wiki/618");
        cfg.setRule(ModuleConfiguration.OPTION_NUM_DOCS_RULE, "[References]", "");

        FetchModule module = new FetchModule(cfg);
        assertThrows(WebModuleException.class, module::getNumberOfAvailableDocuments);
    }
}
