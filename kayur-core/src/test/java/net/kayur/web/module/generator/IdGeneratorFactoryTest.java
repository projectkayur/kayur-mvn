package net.kayur.web.module.generator;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.web.module.ModuleConfiguration;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


class IdGeneratorFactoryTest {
    @Test
    void testCreateIterative() throws Exception {
        ModuleConfiguration config = new ModuleConfiguration();
        config.set(IdGenerator.OPTION_ID_GENERATOR_CLS, IdGeneratorFactory.TYPE_ITERATIVE);

        assertNotEquals(IdGeneratorFactory.create(config), null);
    }

    @Test
    void testCreateSearch() throws Exception {
        ModuleConfiguration config = new ModuleConfiguration();
        config.set(IdGenerator.OPTION_ID_GENERATOR_CLS, IdGeneratorFactory.TYPE_SEARCH);

        config.set(SearchGenerator.OPTION_INITIAL_SEARCH_PAGE_URL, "http://www.google.com");
        config.set(SearchGenerator.OPTION_NEXT_SEARCH_PAGE_URL, "http://www.google.com");

        assertNotEquals(IdGeneratorFactory.create(config), null);
    }

    @Test
    void testCreateCustom() throws Exception {
        ModuleConfiguration config = new ModuleConfiguration();
        config.set(IdGenerator.OPTION_ID_GENERATOR_CLS, "net.kayur.MockGenerator");

        assertThrows(ConfigurationException.class,
                () -> IdGeneratorFactory.create(config));
    }
}
