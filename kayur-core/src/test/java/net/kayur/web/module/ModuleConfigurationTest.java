package net.kayur.web.module;

import net.kayur.core.configuration.StringParameter;
import net.kayur.web.parser.ExtractionRule;
import org.junit.jupiter.api.Test;

import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;


public class ModuleConfigurationTest {
    @Test
    void getMetaRules() throws Exception {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.set(new StringParameter("meta", "priority_seq"), "[priority]");
        cfg.set(new StringParameter("meta", "priority_ex"), "priority:,,1");
        cfg.set(new StringParameter("meta", "status_seq"), "[info,cls][status]");
        cfg.set(new StringParameter("meta", "status_ex"), "status:,,1");

        Map<String, ExtractionRule> rules = cfg.getMetaRules();
        assertEquals(2, rules.size());
        assertNotNull(rules.get("priority"));
        assertNotNull(rules.get("status"));
    }

    @Test
    public void testEqualsHashcode() throws Exception {
        ModuleConfiguration cfg1 = new ModuleConfiguration();
        cfg1.setId(100);
        cfg1.setName("test");

        ModuleConfiguration cfg2 = cfg1;

        assertTrue(cfg1.equals(cfg2));

        cfg2 = new ModuleConfiguration();

        assertFalse(cfg1.equals(cfg2));

        cfg2.setId(100);
        cfg2.setName("test");

        assertTrue(cfg1.equals(cfg2));

        cfg1.set(new StringParameter("g1", "param1"), "value1");
        cfg2.set(new StringParameter("g1", "param1"), "value1");

        assertTrue(cfg1.equals(cfg2));

        cfg2.set(new StringParameter("g1", "param1"), "value2");

        assertFalse(cfg1.equals(cfg2));
    }

}