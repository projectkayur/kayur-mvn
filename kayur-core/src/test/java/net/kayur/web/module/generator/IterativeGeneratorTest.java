package net.kayur.web.module.generator;

import net.kayur.web.module.ModuleConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class IterativeGeneratorTest {
    private ModuleConfiguration config;

    @BeforeEach
    void setUp() {
        config = new ModuleConfiguration();
    }

    @Test
    void testDefaults() {
        config.set(IterativeGenerator.OPTION_START_ID, 0);
        config.set(IterativeGenerator.OPTION_STEP, 1);

        IterativeGenerator generator = new IterativeGenerator(config);

        assertEquals("0", generator.currentDocumentId());
        assertTrue(generator.selectNextDocument());
        assertEquals("1", generator.currentDocumentId());
        assertTrue(generator.selectNextDocument());
        assertEquals("2", generator.currentDocumentId());
    }

    @Test
    void testOverflow() {
        config.set(IterativeGenerator.OPTION_START_ID, Integer.MAX_VALUE - 1);
        config.set(IterativeGenerator.OPTION_STEP, 1);

        IterativeGenerator generator = new IterativeGenerator(config);
        assertTrue(generator.selectNextDocument());
        assertFalse(generator.selectNextDocument());

        assertEquals(String.valueOf(Long.MAX_VALUE), generator.currentDocumentId());
    }

    @Test
    void testReverse() {
        config.set(IterativeGenerator.OPTION_START_ID, 100);
        config.set(IterativeGenerator.OPTION_STEP, -1);

        var generator = new IterativeGenerator(config);

        assertEquals("100", generator.currentDocumentId());
        assertTrue(generator.selectNextDocument());
        assertEquals("99", generator.currentDocumentId());
    }


    @Test
    void underflow() {
        config.set(IterativeGenerator.OPTION_START_ID, Integer.MIN_VALUE + 1);
        config.set(IterativeGenerator.OPTION_STEP, -1);

        var generator = new IterativeGenerator(config);
        assertTrue(generator.selectNextDocument());
        assertFalse(generator.selectNextDocument());

        assertEquals(String.valueOf(Long.MIN_VALUE), generator.currentDocumentId());
    }
}
