package net.kayur.web.module;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class TemplateEngineTest {

    @Test
    void countMatches() {
        assertEquals(0, TemplateEngine.countMatches("", ""));
        assertEquals(0, TemplateEngine.countMatches("", "%s"));
        assertEquals(0, TemplateEngine.countMatches("%", "%s"));
        assertEquals(1, TemplateEngine.countMatches("%s", "%s"));
        assertEquals(1, TemplateEngine.countMatches(" %s", "%s"));
        assertEquals(1, TemplateEngine.countMatches(" %s ", "%s"));
        assertEquals(2, TemplateEngine.countMatches(" %s %s", "%s"));
        assertEquals(2, TemplateEngine.countMatches("%s%s", "%s"));
        assertEquals(2, TemplateEngine.countMatches("%s %s", "%s"));
        assertEquals(2, TemplateEngine.countMatches("%s %s ", "%s"));
    }

}
