package net.kayur.web.update;

import net.kayur.TestConfig;
import net.kayur.TestData;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.core.configuration.NLPConfiguration;
import net.kayur.database.ConnectionHub;
import net.kayur.database.DocumentRepositoryAdapter;
import net.kayur.database.SelectQueryParameters;
import net.kayur.web.Document;
import net.kayur.web.Metadata;
import net.kayur.web.module.ModuleConfiguration;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class UpdateSessionTest {
    private DocumentRepositoryAdapter repository;

    UpdateSessionTest() {
        var mainConfig = new MainConfiguration(new NLPConfiguration(), TestConfig.DATABASE_CONFIG);

        repository = new DocumentRepositoryAdapter(mainConfig, new ConnectionHub());
    }

    @Test
    void getId() {
        ModuleConfiguration config = new ModuleConfiguration();
        UpdateSession session = new UpdateSession(repository, config, new UpdateParameters());
        assertEquals(config.getId(), session.getId());
    }

    @Test
    void updateNoIterations() {
        UpdateParameters params = new UpdateParameters();
        params.numIterations = 0;

        UpdateSession session = new UpdateSession(repository, new ModuleConfiguration(), params);

        assertFalse(session.update());
        assertEquals(session.getTracker().getIteration(), 0);
    }

    @Test
    void updateOneIterative() {
        final String DOC_ID = "16";

        ModuleConfiguration gentooCfg = TestData.generateGentooBugzillaConfig();
        UpdateParameters parameters = new UpdateParameters();
        parameters.numIterations = 1;
        parameters.setLastProcessedDocument(new Document(gentooCfg.getId(), DOC_ID));

        UpdateSession session = new UpdateSession(repository, gentooCfg, parameters);
        session.update();

        assertEquals(0, session.getTracker().totalErrors());
        assertEquals(1, session.getTracker().getIteration());

        // reset state

        int fetchedDocumentIndex = Integer.parseInt(DOC_ID) + 1;

        List<String> ids = Collections.singletonList(String.valueOf(fetchedDocumentIndex));

        SelectQueryParameters params = new SelectQueryParameters();
        params.setModule(gentooCfg.getId());
        params.setIdList(ids);
        List<Document> docs = repository.select(params);

        assertEquals(1, docs.size());

        Document doc = docs.get(0);

        List<Metadata> meta = doc.getMeta();
        assertEquals(2, meta.size());

        for (Metadata datum : meta) {
            switch (datum.field) {
                case "status":
                    assertEquals("RESOLVED FIXED", datum.value);
                    break;
                case "product":
                    assertEquals("Gentoo Linux", datum.value);
                    break;
                default:
                    fail("Unexpected metadata");
            }
        }

        repository.delete(doc.id);
    }

    @Test
    void updateOneSearch() {
        ModuleConfiguration slashdotCfg = TestData.generateSlashdotSearchConfig();
        UpdateParameters parameters = new UpdateParameters();
        parameters.numIterations = 1;
        parameters.setOverwrite(true);

        UpdateSession session = new UpdateSession(repository, slashdotCfg, parameters);
        session.update();

        assertEquals(0, session.getTracker().totalErrors());
        assertEquals(1, session.getTracker().getIteration());
    }
}
