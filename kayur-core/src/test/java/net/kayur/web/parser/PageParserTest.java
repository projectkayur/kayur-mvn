package net.kayur.web.parser;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


public class PageParserTest {
    private static final ExtractionRule EMPTY_RULE =
            new ExtractionRule(AccessSequence.EMPTY_SEQUENCE, ExtractionPattern.EMPTY_PATTERN);

    @Test
    public void testFetchSimple() throws IOException {
        new PageParser().fetch("https://bugzilla.gnome.org/show_bug.cgi?id=700000");
    }

    @Test
    public void testFetchNotFound() throws IOException {
        assertThrows(IOException.class, () -> new PageParser().fetch("http://google.com/nonexisting.html"));
    }

    @Disabled
    public void testFetchDelay() throws IOException {
        RequestSettings settings = new RequestSettings();
        settings.setAccessDelayMs(2000);
        PageParser parser = new PageParser(settings);

        Instant start = Instant.now();
        parser.fetch("http://google.com");
        parser.fetch("http://google.com");
        Instant end = Instant.now();
        assertTrue(ChronoUnit.MILLIS.between(start, end) >= 2000);
    }

    @Test
    public void testGetTextEmptySeq() throws Exception {
        PageParser parser = new PageParser();
        WebDocument document = parser.fetch("https://www.google.com");
        assertEquals(document.getData(EMPTY_RULE), null);
    }

    @Test
    public void testGetTextTitle() throws Exception {
        PageParser parser = new PageParser();
        WebDocument document = parser.fetch("https://www.google.com");
        String result = document.getText(AccessSequence.fromString("[_,title]"));
        assertTrue(result.equals("Google"));
    }

    @Test
    public void testGetTextAttribute() throws Exception {
        PageParser parser = new PageParser();
        WebDocument document = parser.fetch("https://bugzilla.gnome.org/show_bug.cgi?id=700000");
        String result = document.getText(AccessSequence.fromString("[titles,id][td,tag,2][id,attr]"));
        assertTrue(result.equals("information"));
    }

    @Test
    public void testGetTextElement() throws Exception {
        PageParser parser = new PageParser();
        WebDocument document = parser.fetch("https://bugzilla.gnome.org/show_bug.cgi?id=700000");
        String result = document.getText(AccessSequence.fromString("[subtitle,id]"));
        assertTrue(result.startsWith("Should have") && result.endsWith("a domain"));
    }

    @Test
    public void testGetTextCollectionOneStarEnd() throws Exception {
        PageParser parser = new PageParser();
        WebDocument document = parser.fetch("https://bugzilla.gnome.org/show_bug.cgi?id=700001");

        ExtractionRule rule = new ExtractionRule(AccessSequence.fromString("[bz_comment_table,cls][div,tag,*]"),
                ExtractionPattern.EMPTY_PATTERN);
        List<String> list = document.getDataList(rule);

        for (String s : list) {
            System.out.println("=====================================");
            System.out.println(s);
            System.out.println("=====================================");
        }

        //assertTrue(list.size() == 6);
    }
}
