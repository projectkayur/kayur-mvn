package net.kayur.web.parser;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


public class ExtractionPatternTest {
    @Test
    public void testExtract() throws Exception {
        assertEquals(ExtractionPattern.fromString("abcd,,1").extract("abcd-1234567"), "1234567");
        assertEquals(ExtractionPattern.fromString("abcde,,1").extract("abcd-1234567"), null);
        assertEquals(ExtractionPattern.fromString("ab,ba,0").extract("abtestba"), "test");
        assertEquals(ExtractionPattern.fromString("x, ,2").extract("x  123 "), "123");
        assertEquals(ExtractionPattern.fromString(",end,2").extract("--1234 5678end"), "1234 5678");
        assertEquals(ExtractionPattern.fromString(",end,2").extract("--1234 5678en"), null);
        assertEquals(ExtractionPattern.fromString("a,").extract("xvga12"), "12");
        assertEquals(ExtractionPattern.fromString(",b").extract("xvgab"), "xvga");
        assertEquals(ExtractionPattern.fromString(",").extract("test"), "test");
    }

    @Test
    public void testThirdParamNotInteger() throws ParseException {
        assertThrows(ParseException.class, () -> ExtractionPattern.fromString("a,b,c"));
    }

    @Test
    public void testTokenTooLong() throws ParseException {
        assertThrows(ParseException.class, () -> ExtractionPattern.fromString("a,b,0,1"));
    }

    @Test
    public void testToString() throws Exception {
        ExtractionPattern pattern = ExtractionPattern.fromString("a,b,  3 ");
        assertEquals(pattern.toString(), "a,b,3");
    }
}
