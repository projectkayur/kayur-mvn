package net.kayur.web.parser;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class AccessSequenceTest {
    @Test
    public void testFromStringValid() throws ParseException {
        AccessSequence seq;
        AccessSequence.fromString("");
        AccessSequence.fromString("[something]");
        AccessSequence.fromString("[something, cls]");
        seq = AccessSequence.fromString("[something, tag, 2]");
        assertTrue(seq.size() == 1);
        seq = AccessSequence.fromString("[something,tag][other,cls][more,tag,10]");
        assertTrue(seq.size() == 3);
        seq = AccessSequence.fromString("[ with, cls ][  more , tag   ]");
        assertTrue(seq.size() == 2);
        assertTrue(seq.tokens.size() == 2);
    }

    @Test
    public void testInvalidBrackets() throws ParseException {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[a, []]"));
    }

    @Test
    public void testWrongSubtokenNum() throws ParseException {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[a, tag, 0, 1]"));
    }

    @Test
    public void testEmptySubtoken() throws ParseException {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[  ]"));
    }

    @Test
    public void testInvalidIndex() throws ParseException {
        AccessSequence seq = AccessSequence.fromString("[something, tag, 2]");
        assertThrows(IndexOutOfBoundsException.class, () -> seq.getToken(1));
    }

    @Test
    public void testSubsequence() throws ParseException {
        AccessSequence seq = AccessSequence.fromString("[something,tag][other,cls][more,tag,10]");
        AccessSequence seq2 = seq.subsequence(2);
        assertTrue(seq2.size() == 2);
        assertTrue(seq2.getToken(1).name.equals("other"));

        AccessSequence seq3 = seq.subsequence(3);
        assertTrue(seq3.size() == 3);
        assertTrue(seq3.getToken(2).name.equals("more"));
    }

    @Test
    public void testSubsequenceInvalidSize() throws Exception {
        AccessSequence seq = AccessSequence.fromString("[something,tag][other,cls][more,tag,10]");
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> seq.subsequence(4));
    }

    @Test
    public void testToString() throws ParseException {
        final String SEQ_STR = "[a,tag,1][b,cls,2]";
        AccessSequence seq = AccessSequence.fromString(SEQ_STR);
        assertTrue(seq.toString().equals(SEQ_STR));
        assertTrue(AccessSequence.EMPTY_SEQUENCE.toString().isEmpty());
    }

    @Test
    public void testInvalidInPlaceAttr() throws ParseException {
        AccessSequence seq = AccessSequence.fromString("[a,tag,1][b,attr][c,tag,2]");
        assertThrows(ParseException.class, seq::validate);
    }

    @Test
    public void testGetNonExistingToken() throws Exception {
        AccessSequence seq = AccessSequence.fromString("[one,tag][two,tag]");
        assertThrows(ArrayIndexOutOfBoundsException.class, () -> seq.getToken(2));
    }

    @Test
    public void testAttrInTheMiddle() throws Exception {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[one,tag][two,attr][three,tag]").validate());
    }

    @Test
    public void testAttrOneElement() throws Exception {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[two,attr,0]").validate());
    }

    @Test
    public void testTitleLast() throws Exception {
        assertThrows(ParseException.class, () -> AccessSequence.fromString("[one,tag][two,tag][three,title]").validate());
    }

    @Test
    public void testEmptyRuleValid() throws Exception {
        AccessSequence seq = AccessSequence.fromString("");
        assertTrue(seq == seq.validate());
    }
}
