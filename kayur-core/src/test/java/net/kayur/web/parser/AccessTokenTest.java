package net.kayur.web.parser;

import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class AccessTokenTest {
    @Test
    public void testFromString() throws Exception {
        AccessToken token;

        token = AccessToken.fromString("[id]");
        assertTrue(token.name.equals("id"));

        token = AccessToken.fromString("[test, cls]");
        assertTrue(token.name.equals("test") && token.type.equals(AccessToken.Type.CLS));

        token = AccessToken.fromString("[elem, tag, 2]");
        assertTrue(token.name.equals("elem") && token.type.equals(AccessToken.Type.TAG) && token.position == 2);
    }

    @Test
    public void testToString() throws Exception {
        AccessToken token = AccessToken.fromString("[el, tag, 10]");
        assertTrue(token.toString().equals(AccessToken.TOKEN_START + "el,tag,10" + AccessToken.TOKEN_END));
        System.out.println(token.toString());
    }

    @Test
    public void testInvalidName() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[,cls,2]"));
    }

    @Test
    public void testInvalidType() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[name,faulty,1]"));
    }

    @Test
    public void testInvalidPosition() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[more,cls,12.99]"));
    }

    @Test
    public void testTooManySubtokens() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[first,tag,1,something]"));
    }

    @Test
    public void testMissingEnd() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[first,cls,2"));
    }

    @Test
    public void testMissingStart() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("first,cls,2]"));
    }

    @Test
    public void testMissingContent() throws Exception {
        assertThrows(ParseException.class, () -> AccessToken.fromString("[]"));
    }
}
