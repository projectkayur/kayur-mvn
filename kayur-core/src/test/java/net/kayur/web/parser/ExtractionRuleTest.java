package net.kayur.web.parser;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ExtractionRuleTest {
    @Test
    public void testIsEmpty() throws Exception {
        ExtractionRule rule = new ExtractionRule(AccessSequence.EMPTY_SEQUENCE, ExtractionPattern.EMPTY_PATTERN);
        assertTrue(rule.isEmpty());

        AccessSequence seq = AccessSequence.fromString("[tag]");
        ExtractionPattern pattern = ExtractionPattern.fromString(",,1");
        rule = new ExtractionRule(seq, pattern);
        assertFalse(rule.isEmpty());

        rule = new ExtractionRule(seq, ExtractionPattern.EMPTY_PATTERN);
        assertFalse(rule.isEmpty());

        rule = new ExtractionRule(AccessSequence.EMPTY_SEQUENCE, pattern);
        assertFalse(rule.isEmpty());
    }
}
