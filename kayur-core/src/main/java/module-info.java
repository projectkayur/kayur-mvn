module net.kayur {
    requires transitive net.kayur.nlp.plugins.api;
    uses net.kayur.nlp.plugins.api.Stemmer;
    uses net.kayur.nlp.plugins.api.SentenceDetector;
    uses net.kayur.nlp.plugins.api.Tokenizer;

    requires java.logging;
    requires java.persistence;
    requires java.sql;
    requires java.xml;
    requires java.xml.bind;  // JAXB

    requires org.hibernate.orm.core;
    opens net.kayur.database.jpa;

    requires org.jsoup;

    requires org.apache.commons.io;
    requires org.jetbrains.annotations;

    requires com.fasterxml.jackson.core;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.yaml;
    requires com.fasterxml.jackson.annotation;
    opens net.kayur.core.configuration to com.fasterxml.jackson.databind;
    opens net.kayur.database to com.fasterxml.jackson.databind;

    exports net.kayur;
    exports net.kayur.core;
    exports net.kayur.core.configuration;
    exports net.kayur.database;
    exports net.kayur.helper;
    exports net.kayur.nlp;
    exports net.kayur.nlp.analysis;
    exports net.kayur.nlp.corpus;
    exports net.kayur.nlp.export;
    exports net.kayur.nlp.formats;
    exports net.kayur.nlp.statistics;
    exports net.kayur.nlp.weights;
    exports net.kayur.web;
    exports net.kayur.web.module;
    exports net.kayur.web.module.generator;
    exports net.kayur.web.parser;
    exports net.kayur.web.update;
}
