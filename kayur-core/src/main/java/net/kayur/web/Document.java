package net.kayur.web;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

public class Document {
    @NotNull
    public final DocumentId id;

    @Nullable
    private Integer tag;
    @Nullable
    private Date dateTime;

    @NotNull
    private String title = "";
    @NotNull
    private String content = "";

    private final List<String> comments = new ArrayList<>();
    private final List<Metadata> meta = new ArrayList<>();

    // cons

    public Document() {
        this.id = DocumentId.EMPTY;
    }

    public Document(long moduleId, @NotNull String docId) {
        this.id = new DocumentId(moduleId, docId);
    }

    //

    public String getText(boolean useTitle, boolean useContent, boolean useComments) {
        StringBuilder builder = new StringBuilder();

        if (useTitle) {
            builder.append(title);
        }

        if (useContent) {
            if (useTitle) {
                builder.append(" ");
            }
            builder.append(content);
        }

        if (useComments && !comments.isEmpty()) {
            if (useTitle || useContent) {
                builder.append(" ");
            }
            builder.append(String.join(" ", comments));
        }

        return builder.toString();
    }

    public String getText() {
        return getText(true, true, true);
    }

    @Override
    public String toString() {
        String commentsBlock = String.join("\n----------------\n", comments);

        String metaBlock = meta.stream()
                .map(datum -> datum.field + " -> " + datum.value)
                .collect(Collectors.joining("\n"));

        return msg("document_full", id, tag, dateTime, title, content, commentsBlock, metaBlock);
    }

    // trivial getters, setters

    @Nullable
    public Integer getTag() {
        return this.tag;
    }

    @Nullable
    public Date getDateTime() {
        return this.dateTime;
    }

    @NotNull
    public String getTitle() {
        return this.title;
    }

    @NotNull
    public String getContent() {
        return this.content;
    }

    public List<String> getComments() {
        return this.comments;
    }

    public List<Metadata> getMeta() {
        return this.meta;
    }

    public void setTag(@Nullable Integer tag) {
        this.tag = tag;
    }

    public void setDateTime(@Nullable Date dateTime) {
        this.dateTime = dateTime;
    }

    public void setTitle(@NotNull String title) {
        this.title = title;
    }

    public void setContent(@NotNull String content) {
        this.content = content;
    }

    public void setComments(List<String> list) {
        comments.clear();
        comments.addAll(list);
    }

    public void setMeta(List<Metadata> list) {
        meta.clear();
        meta.addAll(list);
    }

    // TEMP fix for GUI tables

    @NotNull
    public DocumentId getId() {
        return id;
    }
}
