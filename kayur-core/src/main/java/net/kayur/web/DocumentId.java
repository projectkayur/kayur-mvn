package net.kayur.web;


import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import static net.kayur.Msg.msg;

public class DocumentId {
    static DocumentId EMPTY = new DocumentId(-1, "");

    public final long module;
    @NotNull
    public final String name;

    public DocumentId(long moduleId, @NotNull String documentName) {
        this.module = moduleId;
        this.name = documentName;
    }

    public boolean isValid() {
        return !name.isEmpty() && module >= 0;
    }

    @Override
    public String toString() {
        return msg("document_id", name, module);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocumentId that = (DocumentId) o;
        return module == that.module &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(module, name);
    }
}
