package net.kayur.web.parser;

import net.kayur.Disp;
import org.jetbrains.annotations.NotNull;
import org.jsoup.Connection;
import org.jsoup.Jsoup;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static net.kayur.Msg.msg;


public final class PageParser {
    private static final int RESPONSE_OK = 200;

    @NotNull
    private final RequestSettings settings;

    private final Object accessLock = new Object();
    private Instant lastAccessTime = Instant.now();

    // trust all certificates
    static {
        System.setProperty("jsse.enableSNIExtension", "false");
        try {
            SSLContext context = SSLContext.getInstance("SSL");
            context.init(null, new TrustManager[] {new DummyTrustManager()}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);
        } catch (NoSuchAlgorithmException | KeyManagementException e) {
            Disp.error(e, msg("web_ssl_disable_failure"));
        }
    }

    public PageParser() {
        this(new RequestSettings());
    }

    public PageParser(@NotNull RequestSettings settings) {
        this.settings = settings;
    }

    public WebDocument fetch(@NotNull String url) throws IOException {

        // wait between subsequent accesses

        int delay = settings.getAccessDelayMs();

        if (delay > 0) {
            long timeSinceLastAccess = ChronoUnit.MILLIS.between(lastAccessTime, Instant.now());
            if (timeSinceLastAccess < delay) {
                try {
                    Thread.sleep(delay - timeSinceLastAccess);
                } catch (InterruptedException e) {
                    Disp.warning(e, "Unable to delay connection request.");
                }
            }
        }

        // connect to a web page and check response
        Connection.Response response = Jsoup.connect(url)
                .userAgent(settings.getUserAgent())
                .followRedirects(settings.isFollowRedirects())
                .timeout(settings.getTimeoutMs())
                .execute();

        synchronized (accessLock) {
            lastAccessTime = Instant.now();
        }

        int responseCode = response.statusCode();
        if (responseCode != RESPONSE_OK)
            throw new IOException(msg("web_bad_response", responseCode, url));

        return new WebDocument(response.parse());
    }
}
