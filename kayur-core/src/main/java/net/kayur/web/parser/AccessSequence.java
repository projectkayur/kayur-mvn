package net.kayur.web.parser;

import net.kayur.Msg;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class AccessSequence {
    public static final AccessSequence EMPTY_SEQUENCE = new AccessSequence();

    @NotNull
    public final List<AccessToken> tokens;

    // private constructors

    private AccessSequence() {
        this(Collections.emptyList());
    }

    private AccessSequence(@NotNull List<AccessToken> list) {
        tokens = Collections.unmodifiableList(list);
    }

    //

    @NotNull
    public AccessToken getToken(int i) {
        if (i < 0 || i >= size())
            throw new ArrayIndexOutOfBoundsException(i);
        return tokens.get(i);
    }

    @NotNull
    public AccessToken getLastToken() {
        return getToken(size() - 1);
    }

    @NotNull
    public AccessToken getFirstToken() {
        return getToken(0);
    }

    public int size() {
        return tokens.size();
    }

    public boolean isEmpty() {
        return tokens.isEmpty();
    }

    public AccessSequence subsequence(int n) {
        return subsequence(0, n);
    }

    public AccessSequence subsequence(int s, int n) {
        if (n < 0 || n > size())
            throw new ArrayIndexOutOfBoundsException(n);
        if (s < 0 || s > n)
            throw new ArrayIndexOutOfBoundsException(s);
        if (n - s == 0)
            return AccessSequence.EMPTY_SEQUENCE;
        return new AccessSequence(tokens.subList(s, n));
    }

    @NotNull
    public static AccessSequence fromString(@NotNull String source) throws ParseException {
        if (source.isEmpty())
            return EMPTY_SEQUENCE;

        List<AccessToken> tokenList = new ArrayList<>();

        char[] chars = source.toCharArray();
        int startToken = 0, endToken = 0;
        boolean insideToken = false;

        for (int i = 0; i < chars.length; i++) {
            if (chars[i] == AccessToken.TOKEN_START) {
                if (insideToken)
                    throw new ParseException(Msg.msg("rule_nested"), i);
                startToken = i;
                insideToken = true;
            } else if (chars[i] == AccessToken.TOKEN_END) {
                endToken = i;
                insideToken = false;

                String token = source.substring(startToken, endToken + 1);
                tokenList.add(AccessToken.fromString(token));
            }
        }

        return new AccessSequence(tokenList);
    }

    public AccessSequence validate() throws ParseException {
        if (isEmpty()) return this; // empty sequence is valid

        int size = tokens.size();

        // 1. ATTR can be only at the last position
        for (int i = 0; i < size - 1; i++) {
            if (tokens.get(i).type == AccessToken.Type.ATTR)
                throw new ParseException(Msg.msg("rule_invalid_attr_pos"), i);
        }

        // 2. ATTR cannot be the only token in a sequence
        if ((size == 1) && tokens.get(0).type == AccessToken.Type.ATTR)
            throw new ParseException(Msg.msg("rule_attr_with_no_predecessor"), 0);

        // 3. TITLE can be only the first element
        for (int i = 1; i < size; i++) {
            if (tokens.get(i).type == AccessToken.Type.TITLE)
                throw new ParseException(Msg.msg("rule_invalid_title_pos"), i);
        }

        return this;
    }

    @Override
    public String toString() {
        return tokens.stream()
                .map(AccessToken::toString)
                .collect(Collectors.joining());
    }
}
