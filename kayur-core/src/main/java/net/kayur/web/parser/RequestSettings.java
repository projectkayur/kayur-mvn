package net.kayur.web.parser;

import org.jetbrains.annotations.Nullable;


public final class RequestSettings {
    public static final int DEFAULT_TIMEOUT = 60_000;
    public static final int DEFAULT_ACCESS_DELAY = 100;
    public static final boolean DEFAULT_FOLLOW_REDIRECTS_SETTING = true;
    public static final String DEFAULT_USER_AGENT = "Mozilla";

    private String userAgent = DEFAULT_USER_AGENT;

    private boolean followRedirects = DEFAULT_FOLLOW_REDIRECTS_SETTING;

    private int timeoutMs = DEFAULT_TIMEOUT; // time (ms) to wait response from the web page

    private int accessDelayMs = DEFAULT_ACCESS_DELAY; // time (ms) between subsequent accesses to web pages on the site


    public void setTimeoutMs(int timeout) {
        timeoutMs = (timeout >= 0) ? timeout : 0;
    }

    public void setAccessDelayMs(int delay) {
        accessDelayMs = (delay >= 0) ? delay : 0;
    }

    public void setUserAgent(@Nullable String userAgent) {
        if (userAgent != null && !userAgent.isEmpty()) {
            this.userAgent = userAgent;
        }
    }

    public void setFollowRedirects(boolean followRedirects) {
        this.followRedirects = followRedirects;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public boolean isFollowRedirects() {
        return followRedirects;
    }

    public int getTimeoutMs() {
        return timeoutMs;
    }

    public int getAccessDelayMs() {
        return accessDelayMs;
    }
}
