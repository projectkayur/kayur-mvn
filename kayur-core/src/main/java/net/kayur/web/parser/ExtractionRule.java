package net.kayur.web.parser;

import org.jetbrains.annotations.NotNull;

/**
 * Translation rule, as defined in the paper.
 */
public final class ExtractionRule {
    @NotNull
    public final AccessSequence sequence;
    @NotNull
    public final ExtractionPattern pattern;

    public ExtractionRule(@NotNull AccessSequence sequence, @NotNull ExtractionPattern pattern) {
        this.sequence = sequence;
        this.pattern = pattern;
    }

    public boolean isEmpty() {
        return sequence.isEmpty() && pattern.isEmpty();
    }
}
