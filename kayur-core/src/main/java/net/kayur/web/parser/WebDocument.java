package net.kayur.web.parser;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Collector;
import org.jsoup.select.Elements;
import org.jsoup.select.Evaluator;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

public class WebDocument {
    @NotNull
    private final Document document;

    public WebDocument(@NotNull Document document) {
        this.document = document;
    }

    @Nullable
    public String getData(@NotNull ExtractionRule rule) throws ParseException {
        if (rule.isEmpty()) return null;
        return rule.pattern.extract(getText(rule.sequence));
    }

    /**
     * See {@link #getText(AccessSequence, Element)}.
     */
    @NotNull
    String getText(@NotNull AccessSequence sequence) throws ParseException {
        return getText(sequence, document);
    }


    /**
     * Gets text of a DOM element.
     *
     * @param sequence access sequence
     * @param root     relative root element
     * @return text of a DOM element
     * @throws ParseException when the specified DOM element does not exist
     */
    @NotNull
    private String getText(@NotNull AccessSequence sequence, @NotNull Element root) throws ParseException {
        String text;

        if (sequence.isEmpty()) {
            // get entire text of the root node
            text = root.text();
        } else if (sequence.getFirstToken().type == AccessToken.Type.TITLE) {
            text = getDocumentTitle();
        } else if (sequence.getLastToken().type == AccessToken.Type.ATTR) {
            // get attribute value of the last DOM element in the sequence
            text = getAttribute(getElement(sequence.subsequence(sequence.size() - 1), root),
                    sequence.getLastToken().name);
        } else {
            text = getElement(sequence, root).text();
        }

        return text.trim();
    }

    @NotNull
    private String getDocumentTitle() throws ParseException {
        String title = document.title();
        if (title == null) {
            throw new ParseException(msg("web_document_no_title"), 0);
        }
        return title;
    }

    /**
     * Extracts a collection of texts from a web page.
     *
     * @param rule extraction rule
     * @return a list of texts
     * @throws ParseException if the rule specifies non-existing DOM element(s), or
     * it is not possible to extract text according to the pattern specified by the rule
     */
    @NotNull
    public List<String> getDataList(@NotNull ExtractionRule rule) throws ParseException {
        List<String> list = new ArrayList<>();

        if (rule.isEmpty()) return list;

        getTextCollection(rule.sequence, document, list);

        return rule.pattern.isEmpty() ? list :
                list.stream()
                        .map(rule.pattern::extract)
                        .collect(Collectors.toList());
    }


    @SuppressWarnings("ForLoopReplaceableByForEach")
    private void getTextCollection(@NotNull AccessSequence sequence, @NotNull Element root,
                                   @NotNull List<String> list) throws ParseException {

        int splitIndex = findFirstMultiToken(sequence); // token with position == *

        if (splitIndex == -1) {
            list.add(getText(sequence, root));
            return;
        }

        AccessToken multiToken = sequence.getToken(splitIndex);
        AccessSequence prefixSeq = sequence.subsequence(splitIndex); // sequence before multi-token

        Elements children = getElementSet(multiToken, getElement(prefixSeq, root));

        if (splitIndex == sequence.size() - 1) {
            for (int i = 0; i < children.size(); i++)
                list.add(children.get(i).text());
        } else {
            for (int i = 0; i < children.size(); i++)
                getTextCollection(sequence.subsequence(splitIndex + 1, sequence.size()),
                        children.get(i), list);
        }
    }
    //getElementSet();

/*
        // get list of elements:
        // (n - 1) tokens are treated as a path to the root the contains element list
        // n-th token describes child elements to get
        int size = outerSeq.size();
        Element rootElement = (size != 1) ? getElement(outerSeq.subsequence(size - 1), documentRoot) : documentRoot;
        AccessToken childElementToken = outerSeq.getToken(size - 1); // last token in the sequence

        Elements childSet = getElementSet(childElementToken, rootElement);

        if (childSet == null)
            throw new ParseException(Msg.format1("web_missing_element_set", outerSeq.toString()), 0);

        List<String> resultList = new ArrayList<>();

        // for each child element get contents specified by inner sequence
        //noinspection ForLoopReplaceableByForEach
        for (int i = 0; i < childSet.size(); i++)
            try {
                resultList.add(get(innerSeq, childSet.get(i)));
            } catch (ParseException e) {
                Disp.warning(e.getMessage());
            }

        return resultList; */
    //  return new ArrayList<>();
    //}

    private int findFirstMultiToken(@NotNull AccessSequence sequence) {
        for (int i = 0; i < sequence.size(); i++)
            if (sequence.getToken(i).position == AccessToken.ANY_POSITION)
                return i;
        return -1;
    }

//    private Set<Integer> findMultiTokens(AccessSequence sequence) {
//        return sequence.getTokens().stream()
//                .filter(token -> token.position == AccessSequence.ANY_POSITION)
//                .map(token -> token.position)
//                .collect(Collectors.toSet());
//    }

    @Nullable
    private Element getElement(@NotNull AccessToken t, @NotNull Element relativeRoot) {
        Elements elementSet;
        switch (t.type) {
            case CLS:
                elementSet = relativeRoot.getElementsByClass(t.name);
                break;
            case TAG:
                elementSet = relativeRoot.getElementsByTag(t.name);
                break;
            case ID:
                return relativeRoot.getElementById(t.name);
            default:
                return null;
        }

        // for CLS and TAG:
        if (elementSet == null) {
            return null;
        }

        int setSize = elementSet.size();
        int index = (t.position < 0) ? setSize + t.position : t.position;
        return (index < setSize && index >= 0) ? elementSet.get(index) : null;
    }

    @NotNull
    private Element getElement(@NotNull AccessSequence seq, @NotNull Element root) throws ParseException {
        Element e = root;
        for (AccessToken token : seq.tokens) {
            e = getElement(token, e);
            if (e == null) {
                throw new ParseException(msg("web_missing_element", token.toString(), seq.toString()), 0);
            }
        }
        return e;
    }

    /**
     * Get a set of DOM elements of the specified parent node
     * with properties defined by the access token.
     *
     * @param token access token that specifies DOM elements to get
     * @param root  parent node
     * @return a set of DOM elements that satisfy criteria
     */
    @NotNull
    private Elements getElementSet(@NotNull AccessToken token, @NotNull Element root) throws ParseException {
        switch (token.type) {
            case CLS:
                return root.getElementsByClass(token.name);
            case TAG:
                //return root.select(":root > " + token.name);
                //Elements set = root.getElementsByTag(token.name);
                return getSiblings(getFirstElementByTagBFS(root, token.name),
                        e -> e.tagName().equals(token.name));
            case ATTR:
                return root.getElementsByAttribute(token.name);
            default:
                throw new ParseException(msg("web_incorrect_token_for_element_set", token), 0);
        }
    }

    @Nullable
    private Element getFirstElementByTagBFS(Element root, String tagName) {
        Elements set = Collector.collect(new Evaluator.Tag(tagName), root);
        return set.first();
    }

    @NotNull
    private Elements getSiblings(@Nullable Element element, Predicate<Element> predicate) {
        Elements resultSet = new Elements();

        if (element != null) {
            resultSet.add(element); // add current element, because it's not counted in siblings
            Elements siblings = element.siblingElements();

            for (int i = 0; i < siblings.size(); i++) {
                Element e = siblings.get(i);
                if (predicate.test(e))
                    resultSet.add(e);
            }
        }

        return resultSet;
    }

    @NotNull
    private String getAttribute(@NotNull Element element, @NotNull String attributeName) throws ParseException {
        if (!element.attributes().hasKey(attributeName))
            throw new ParseException(msg("web_missing_attribute", element.nodeName(), attributeName), 0);

        return element.attr(attributeName);
    }
}
