package net.kayur.web.parser;

import net.kayur.Msg;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;
import java.util.Arrays;
import java.util.Optional;

import static net.kayur.Msg.msg;


public class AccessToken {
    private static final Type DEFAULT_TYPE = Type.ID;
    private static final String DEFAULT_POSITION = "0";

    private static final char ANY_POSITION_SYMBOL = '*';
    public static final int ANY_POSITION = Integer.MAX_VALUE;

    public static final char TOKEN_START = '[';
    public static final char TOKEN_END   = ']';
    private static final String TOKEN_SYMBOLS_REGEXP = "\\" + TOKEN_START + "|" + "\\" + TOKEN_END;
    private static final String SEPARATOR_REGEXP = ",";

    @NotNull
    public final String name;
    @NotNull
    public final Type type;
    public final int position;

    public AccessToken(@NotNull String name, @NotNull Type type, String position) throws ParseException {
        this.name = parseName(name);
        this.type = type;
        this.position = parsePosition(position);
    }

    public enum Type {
        CLS, ID, TAG, ATTR, TITLE;

        @NotNull
        public static Type fromString(@NotNull String source) throws ParseException {
            Optional<Type> result = Arrays.stream(Type.values())
                    .filter(type -> type.toString().equalsIgnoreCase(source))
                    .findFirst();

            if (!result.isPresent())
                throw new ParseException(Msg.msg("rule_unsupported_dom_element_type"), 0);

            return result.get();
        }

        @Override
        public String toString() {
            return super.toString().toLowerCase();
        }
    }

    @NotNull
    public static AccessToken fromString(@NotNull String source) throws ParseException {
        if (source.charAt(0) != TOKEN_START || source.charAt(source.length() - 1) != TOKEN_END)
            throw new ParseException(Msg.msg("rule_invalid_token"), 0);

        String[] items = source.replaceAll(TOKEN_SYMBOLS_REGEXP, "").split(SEPARATOR_REGEXP);
        int numItems = items.length;

        if (numItems <= 0 || numItems > 3)
            throw new ParseException(msg("rule_invalid_subtoken_num", source), 0);

        String name = items[0];
        Type type   = (numItems >= 2) ? parseType(items[1]) : DEFAULT_TYPE;
        String pos  = (numItems >= 3) ? items[2] : DEFAULT_POSITION;

        return new AccessToken(name, type, pos);
    }

    /**
     * Parses DOM element name.
     *
     * @param source input String
     * @return DOM element name
     * @throws ParseException if the name is empty
     */
    @NotNull
    private static String parseName(String source) throws ParseException {
        String name = source.trim();
        if (name.isEmpty()) {
            throw new ParseException(msg("rule_missing_dom_element_name", source), 0);
        }
        return name;
    }

    /**
     * Parses DOM element type.
     *
     * @param source input String
     * @return DOM element type from the corresponding enum
     * @throws ParseException if type is not declared in the corresponding enum
     */
    @NotNull
    private static Type parseType(String source) throws ParseException {
        try {
            return Type.fromString(source.trim()); // DOM element type
        } catch (ParseException e) {
            throw new ParseException(msg("rule_unsupported_element_type", source), 0);
        }
    }

    /**
     * Parses DOM element sibling number.
     *
     * @param source input String
     * @return sibling index
     * @throws ParseException if not a valid integer
     */
    private static int parsePosition(String source) throws ParseException {
        source = source.trim();

        if (source.length() == 1 && source.charAt(0) == ANY_POSITION_SYMBOL) {
            return ANY_POSITION;
        }

        try {
            return Integer.parseInt(source);
        } catch (NumberFormatException e) {
            throw new ParseException(msg("rule_invalid_dom_element_index", source), 0);
        }
    }

    @Override
    public String toString() {
        return String.format("%s%s,%s,%d%s", TOKEN_START, name, type.toString(), position, TOKEN_END);
    }
}
