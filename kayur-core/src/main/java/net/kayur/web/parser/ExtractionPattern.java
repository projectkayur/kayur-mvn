package net.kayur.web.parser;


import net.kayur.Msg;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;

import static net.kayur.Msg.msg;

public final class ExtractionPattern {
    public static final ExtractionPattern EMPTY_PATTERN = new ExtractionPattern();

    @NotNull
    private final String startKey;
    @NotNull
    private final String endKey;
    private final int offset;

    private static final String TOKEN_SEP_REGEXP = ",";

    private ExtractionPattern() {
        startKey = endKey = "";
        offset = 0;
    }

    public ExtractionPattern(@NotNull String startKey, @NotNull String endKey, int offset) {
        this.startKey = startKey;
        this.endKey = endKey;
        this.offset = offset;
    }

    @NotNull
    public static ExtractionPattern fromString(@NotNull String source) throws ParseException {
        if (source.isEmpty())
            return EMPTY_PATTERN;

        String[] tokens = source.split(TOKEN_SEP_REGEXP);
        if (tokens.length > 3)
            throw new ParseException(Msg.msg("rule_too_long_pattern"), 3);

        String beginSeq = (tokens.length >= 1) ? tokens[0] : "";
        String endSeq = (tokens.length >= 2) ? tokens[1] : "";

        int offset = 0;

        if (tokens.length == 3) {
            try {
                offset = Integer.parseInt(tokens[2].trim());
            } catch (NumberFormatException e) {
                throw new ParseException(msg("rule_invalid_offset", tokens[2]), 2);
            }
        }

        return new ExtractionPattern(beginSeq, endSeq, offset);
    }

    public boolean isEmpty() {
        return startKey.isEmpty() && endKey.isEmpty() && (offset == 0);
    }

    @Override
    public String toString() {
        return startKey + TOKEN_SEP_REGEXP + endKey + TOKEN_SEP_REGEXP + offset;
    }

    @Nullable
    public String extract(@Nullable String source) {
        if (source == null) return null;
        if (this.isEmpty() || source.isEmpty()) return source;

        int startKeyIndex = source.indexOf(startKey);
        if (startKeyIndex == -1)
            return null; // no starting char sequence is found

        int beg = startKeyIndex + startKey.length() + offset;

        if (!endKey.isEmpty()) {
            int end = source.indexOf(endKey, beg);
            if (end == -1)
                return null; // the keyword is found, but no stop symbol
            return source.substring(beg, end);
        } else {
            return source.substring(beg);
        }
    }
}
