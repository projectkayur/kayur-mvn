package net.kayur.web.update;


import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.database.DocumentRepository;
import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import net.kayur.web.module.FetchModule;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.generator.IdGenerator;
import net.kayur.web.module.generator.IdGeneratorFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.PersistenceException;
import java.io.IOException;
import java.text.ParseException;

import static net.kayur.Msg.msg;

/**
 * Manages and tracks the process of fetching multiple documents
 * from the same remote resource.
 * Hence, at most one session should be opened for a given module.
 */
public class UpdateSession {
    private final @NotNull DocumentRepository repository;
    private final @NotNull FetchModule module;
    private final @NotNull IdGenerator idGenerator;
    private final @NotNull UpdateParameters parameters;

    private final @NotNull UpdateTracker tracker;


    UpdateSession(@NotNull DocumentRepository repository, @NotNull ModuleConfiguration config,
                  @NotNull UpdateParameters parameters) {
        this.repository = repository;
        this.parameters = parameters;

        this.module = new FetchModule(config);

        this.idGenerator = IdGeneratorFactory.create(config);
        this.idGenerator.init(parameters.getLastProcessedDocument());

        this.tracker = new UpdateTracker();
        updateTrackerNextDocumentId();
    }

    public UpdateTracker getTracker() {
        return tracker;
    }

    /**
     * Returns session identifier, which equals to the identifier of the module
     * for which the session is opened.
     * As a consequence, at most one session is permitted per module.
     *
     * @return session ID
     */
    public long getId() {
        return module.id;
    }

    /**
     * Fetches one document from a remote source and stores it in the database.
     * Selects the next document identifier to process.
     *
     * @return true when the update process should be continued afterwards,
     * or false otherwise (the limit of iterations is reached, or ID generator is exhausted)
     */
    public boolean update() throws ConfigurationException {
        if (tracker.getIteration() >= parameters.numIterations)
            return false;

        String documentId = idGenerator.currentDocumentId();

        if (!parameters.isExcluded(documentId))
            update(documentId);
        else
            tracker.skip();

        tracker.iter();

        return selectNextDocument();
    }

    private boolean selectNextDocument() {
        boolean hasNext = idGenerator.selectNextDocument();
        if (hasNext)
            updateTrackerNextDocumentId();
        return hasNext;
    }

    private void updateTrackerNextDocumentId() {
        tracker.setNextDocumentId(idGenerator.currentDocumentId());
    }

    private void update(String documentId) throws ConfigurationException {
        store(fetch(documentId));
    }

    // Fetches a document from a remote source.
    @Nullable
    private Document fetch(String documentId) throws ConfigurationException {
        Document issue = null;
        UpdateError error = null;

        try {
            issue = module.fetch(documentId);
        } catch (IOException e) {
            error = new UpdateError(UpdateErrorType.FETCH, e);
        } catch (ParseException e) {
            error = new UpdateError(UpdateErrorType.PARSE, e);
        }

        if (error != null) {
            registerError(new DocumentId(module.id, documentId), error);
        }

        return issue;
    }

    // Stores a non-null document in the database.
    // In [RECOVERY mode] removes old error records related to that document,
    // if it is stored succesfully.
    private void store(@Nullable Document issue) {
        if (issue == null) return;

        UpdateError error = null;

        try {
            repository.store(issue);
        } catch (PersistenceException e) {
            error = new UpdateError(UpdateErrorType.STORE, e);
        }

        if (error != null) {
            registerError(issue.id, error);
        } else if (parameters.isRecovery()) {
            try {
                repository.clearError(issue.id);
            } catch (PersistenceException e) {
                Disp.warning(msg("update_remove_error_failure"));
            }
        }
    }

    // Registers an error in the session tracker, logs the error message, and
    // stores error details in the database.
    private void registerError(DocumentId id, @NotNull UpdateError error) {
        tracker.registerError(error);

        String description = msg("update_error", id.name, error.getDescription());
        Disp.error(error.getException(), description);

        try {
            repository.setError(id, error.getCode());
        } catch (PersistenceException e) {
            Disp.warning(msg("update_record_error_failure"));
        }
    }
}
