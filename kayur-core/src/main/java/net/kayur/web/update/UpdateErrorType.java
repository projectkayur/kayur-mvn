package net.kayur.web.update;

import static net.kayur.Msg.msg;

/**
 * Enumerates all classes of errors that can occur during document acquisition via web mining.
 */
enum UpdateErrorType {
    FETCH, PARSE, STORE;

    int code() {
        switch (this) {
            case FETCH:
                return 0x1;
            case PARSE:
                return 0x2;
            case STORE:
                return 0x3;
            default:
                return 0x0;
        }
    }

    String description() {
        switch (this) {
            case FETCH:
                return msg("update_fetch_error");
            case PARSE:
                return msg("update_parse_error");
            case STORE:
                return msg("update_store_error");
            default:
                return "";
        }
    }
}
