package net.kayur.web.update;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.database.DocumentRepository;
import net.kayur.web.Document;
import net.kayur.web.WebModuleException;
import net.kayur.web.module.FetchModule;
import net.kayur.web.module.ModuleConfiguration;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import static net.kayur.Msg.msg;

/**
 * Web Import controller.
 */
public class ModuleController implements Fetcher {
    private final DocumentRepository repository;

    private final Map<Integer, FetchModule> moduleCache = new HashMap<>();

    @Nullable
    private FetchModule module;

    public ModuleController(DocumentRepository repository) {
        this.repository = repository;
    }

    @NotNull
    private FetchModule getModule(ModuleConfiguration config) {
        int hash = config.hashCode();
        if (!moduleCache.containsKey(hash)) {
            moduleCache.put(hash, new FetchModule(config));
        }
        return moduleCache.get(hash);
    }

    @NotNull
    public UpdateSession startUpdateSession(@NotNull UpdateParameters params, @NotNull UpdateCheckResult updateCheckResult) {
        if (module == null) {
            throw new ConfigurationException("Module is not set.");
        }

        params.numIterations = updateCheckResult.numToUpdate;

        if (!params.isOverwrite()) {
            long moduleId = module.id;
            params.setExcludedIdList(repository.selectDocumentIdentifiers(moduleId));
        }

        // if Start ID is not specified, use the last stored document
        if (params.getLastProcessedDocument().id.name.isEmpty())
            params.setLastProcessedDocument(updateCheckResult.lastStoredDocument);

        // TODO: recovery
        /*if (params.recovery) {
            List<String> issueList = mDatabaseManager.getFetchFailedIssuesId(mWorkingModule.getId());
            if (issueList != null) {
                mWorkingModule.init(issueList); // initialize list of id's for the module
                amountOfWork = issueList.size();
            }
        }*/

        return new UpdateSession(repository, module.config, params);
    }

    @Override
    public void setModule(@NotNull ModuleConfiguration config) {
        this.module = getModule(config);
    }

    @NotNull
    public Document test(@NotNull ModuleConfiguration config, @NotNull String documentId) {
        FetchModule module = getModule(config);
        try {
            return module.fetch(documentId);
        } catch (IOException e) {
            throw new WebModuleException(msg("web_fetch_failure"), e);
        } catch (ParseException e) {
            throw new WebModuleException(msg("module_test_parse_failure"), e);
        }
    }

    @NotNull
    public UpdateCheckResult calculateUpdateSize(@NotNull UpdateParameters params) {
        if (module == null) {
            throw new ConfigurationException("Module is not set.");
        }

        final long moduleId = module.id;

        Document lastStored = repository.selectLastDocument(moduleId)
                .orElse(new Document());

        final long numStored = repository.count(moduleId);

        long numToUpdate, numAvailable;

        if (params.getLimit() > 0) {
            numAvailable = 0;
            numToUpdate = params.getLimit();
        } else {
            numAvailable = module.getNumberOfAvailableDocuments();
            numToUpdate = numAvailable - numStored;
        }

        if (numToUpdate < 0) {
            throw new ConfigurationException(msg("update_num_to_update_mismatch"));
        }

        return new UpdateCheckResult(numAvailable, numToUpdate, lastStored);
    }
}
