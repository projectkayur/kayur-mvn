package net.kayur.web.update;


/**
 * Contains information about an error that can occur during document acquisition via web mining.
 */
class UpdateError {
    private final UpdateErrorType type;
    private final Exception exception;

    UpdateError(UpdateErrorType type, Exception exception) {
        this.type = type;
        this.exception = exception;
    }

    UpdateErrorType getType() {
        return type;
    }

    int getCode() {
        return type.code();
    }

    String getDescription() {
        return type.description();
    }

    Exception getException() {
        return exception;
    }
}
