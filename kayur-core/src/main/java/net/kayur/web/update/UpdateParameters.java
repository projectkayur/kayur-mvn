package net.kayur.web.update;

import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Parameters for update functionality.
 */
public class UpdateParameters {
    private int limit = 0;
    private boolean recovery = false;
    private boolean overwrite = false; // overwrite existing entries in the database
    private boolean checkOnly = false; // only check number of available documents, but don't fetch them

    long numIterations = 0;

    @NotNull
    private Document lastProcessedDocument = new Document();

    private final List<String> excludedIdList = new ArrayList<>();

    void setExcludedIdList(@NotNull List<String> list) {
        excludedIdList.clear();
        excludedIdList.addAll(list);
        Collections.sort(excludedIdList);
    }

    boolean isExcluded(String documentId) {
        return Collections.binarySearch(excludedIdList, documentId) >= 0;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(int limit) {
        this.limit = limit;
    }

    public boolean isRecovery() {
        return recovery;
    }

    public void setRecovery(boolean recovery) {
        this.recovery = recovery;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    public boolean isCheckOnly() {
        return checkOnly;
    }

    public void setCheckOnly(boolean checkOnly) {
        this.checkOnly = checkOnly;
    }

    @NotNull
    public Document getLastProcessedDocument() {
        return lastProcessedDocument;
    }

    public void setLastProcessedDocument(@NotNull Document lastProcessedDocument) {
        this.lastProcessedDocument = lastProcessedDocument;
    }
}
