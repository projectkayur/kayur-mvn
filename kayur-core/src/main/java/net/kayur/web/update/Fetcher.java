package net.kayur.web.update;

import net.kayur.web.Document;
import net.kayur.web.module.ModuleConfiguration;
import org.jetbrains.annotations.NotNull;

public interface Fetcher {
    void setModule(@NotNull ModuleConfiguration config);

    @NotNull UpdateCheckResult calculateUpdateSize(@NotNull UpdateParameters params);

    @NotNull Document test(@NotNull ModuleConfiguration config, @NotNull String documentId);

    @NotNull UpdateSession startUpdateSession(@NotNull UpdateParameters parameters,
                                              @NotNull UpdateCheckResult updateCheckResult);
}
