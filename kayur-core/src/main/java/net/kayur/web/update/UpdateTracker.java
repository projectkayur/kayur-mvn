package net.kayur.web.update;


import java.util.HashMap;
import java.util.Map;

import static net.kayur.Msg.msg;

/**
 * Update statistics.
 */
public class UpdateTracker {
    private final Map<UpdateErrorType, Integer> numErrors;
    private int numSkipped = 0;
    private int iteration = 0;
    private String nextDocumentId = "";


    UpdateTracker() {
        this.numErrors = new HashMap<>();
        for (UpdateErrorType type : UpdateErrorType.values()) {
            this.numErrors.put(type, 0);
        }
    }

    public String getNextDocumentId() {
        return nextDocumentId;
    }

    public void setNextDocumentId(String nextDocumentId) {
        this.nextDocumentId = nextDocumentId;
    }

    void registerError(UpdateError error) {
        UpdateErrorType type = error.getType();
        numErrors.put(type, 1 + numErrors.get(type));
    }

    void skip() {
        numSkipped++;
    }

    void iter() {
        iteration++;
    }


    int totalErrors() {
        return numErrors.entrySet().stream()
                .mapToInt(Map.Entry::getValue)
                .sum();
    }

    private int numImported() {
        return iteration - numSkipped - totalErrors();
    }

    private String doneStatusMessage() {
        return (numSkipped == 0) ?
                msg("update_done_status", iteration - totalErrors()) :
                msg("update_done_skipped_status", numImported(), numSkipped);
    }

    private String processingMessage() {
        return msg("update_processing_document", nextDocumentId);
    }

    private String errorStatusMessage() {
        return msg("update_error_status", totalErrors(),
                numErrors.get(UpdateErrorType.FETCH),
                numErrors.get(UpdateErrorType.PARSE),
                numErrors.get(UpdateErrorType.STORE));
    }

    public int getIteration() {
        return iteration;
    }

    public String getProcessingReport() {
        String report = processingMessage();

        if (iteration > 0) {
            report += "\n" + doneStatusMessage();
        }

        if (totalErrors() > 0) {
            report += "\n" + errorStatusMessage();
        }

        return report;
    }

    public String getFinalReport() {
        String report = doneStatusMessage();

        if (totalErrors() > 0) {
            report += "\n" + errorStatusMessage();
        }

        return report;
    }
}
