package net.kayur.web.update;


import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;


/**
 * Represents how many documents available on a web resource comparing
 * to the number of documents stored in the database for the same resource.
 */
public class UpdateCheckResult {
    // the total number of documents on a web resource
    public final long numDocumentsTotal;

    // number of documents to fetch, usually the number of documents on a web resource
    // minus the number of documents already stored in a database
    public final long numToUpdate;

    // last document stored in the database
    @NotNull
    public final Document lastStoredDocument;

    public UpdateCheckResult(long numDocumentsTotal, long numToUpdate, @NotNull Document lastStoredDocument) {
        this.numDocumentsTotal = numDocumentsTotal;
        this.numToUpdate = numToUpdate;
        this.lastStoredDocument = lastStoredDocument;
    }
}
