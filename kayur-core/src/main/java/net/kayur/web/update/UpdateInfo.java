package net.kayur.web.update;

import static net.kayur.Msg.msg;


public class UpdateInfo {
    public final int numAvailable;
    public final String lastIdentifier;

    public UpdateInfo(int numAvailable, String lastIdentifier) {
        this.numAvailable = numAvailable;
        this.lastIdentifier = lastIdentifier;
    }

    public String getStatusMessage() {
        return msg("checkstatus_dialog_status_msg", numAvailable, lastIdentifier);
    }
}
