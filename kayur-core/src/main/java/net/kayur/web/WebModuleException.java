package net.kayur.web;

public class WebModuleException extends RuntimeException {
    public WebModuleException(String message) {
        super(message);
    }

    public WebModuleException(String message, Throwable e) {
        super(message, e);
    }
}
