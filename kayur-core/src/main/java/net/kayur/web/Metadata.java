package net.kayur.web;


public class Metadata {
    public final String field;
    public final String value;

    public Metadata(String field, String value) {
        this.field = field;
        this.value = value;
    }
}
