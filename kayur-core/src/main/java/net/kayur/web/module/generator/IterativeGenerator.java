package net.kayur.web.module.generator;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.IntParameter;
import net.kayur.core.configuration.Parameter;
import net.kayur.helper.ParseUtils;
import net.kayur.web.Document;
import net.kayur.web.module.ModuleConfiguration;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;

public class IterativeGenerator extends IdGenerator {

    private long currentId;
    public static final Parameter<Integer> OPTION_START_ID = new IntParameter(GROUP_GENERATOR, "start_id");

    private final int step;
    private static final int DEFAULT_STEP = 1;
    public static final Parameter<Integer> OPTION_STEP = new IntParameter(GROUP_GENERATOR, "step");


    IterativeGenerator(@NotNull ModuleConfiguration config) {
        Integer stepValue = config.get(OPTION_STEP);
        step = (stepValue != 0) ? stepValue : DEFAULT_STEP;

        currentId = config.get(OPTION_START_ID);
    }

    @Override
    public String currentDocumentId() {
        return String.valueOf(currentId);
    }

    @Override
    public boolean selectNextDocument() {
        if (step > 0 && (currentId + step < currentId)) {
            return false;
        }

        if (step < 0 && (currentId + step > currentId)) {
            return false;
        }

        currentId += step;
        return true;
    }

    @Override
    public void init(Document last) throws ConfigurationException {
        String lastProcessedId = last.id.name;
        if (!lastProcessedId.isEmpty()) {
            try {
                currentId = ParseUtils.parseLong(lastProcessedId) + 1;
            } catch (ParseException e) {
                throw new ConfigurationException(e);
            }
        }
    }
}
