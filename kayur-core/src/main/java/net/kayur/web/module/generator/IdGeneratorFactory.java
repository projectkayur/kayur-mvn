package net.kayur.web.module.generator;


import net.kayur.Msg;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.web.module.ModuleConfiguration;
import org.jetbrains.annotations.NotNull;

public final class IdGeneratorFactory {
    public static final String TYPE_ITERATIVE = "iterative";
    public static final String TYPE_SEARCH = "search";

    private IdGeneratorFactory() {
    }

    @NotNull
    public static IdGenerator create(@NotNull ModuleConfiguration config) {

        String clsName = config.get(IdGenerator.OPTION_ID_GENERATOR_CLS);

        if (clsName.isEmpty()) {
            return new DummyGenerator();
        }

        switch (clsName) {
            case TYPE_ITERATIVE:
                return new IterativeGenerator(config);
            case TYPE_SEARCH:
                return new SearchGenerator(config);
            default:
                throw new ConfigurationException(Msg.msg("plugin_generator_not_supported"));
        }
    }

}
