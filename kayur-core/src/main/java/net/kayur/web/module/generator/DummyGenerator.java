package net.kayur.web.module.generator;


import net.kayur.core.configuration.ConfigurationException;
import net.kayur.web.Document;

public class DummyGenerator extends IdGenerator {
    DummyGenerator() {
        super();
    }

    @Override
    public String currentDocumentId() {
        return null;
    }

    @Override
    public boolean selectNextDocument() {
        return false;
    }

    @Override
    public void init(Document last) throws ConfigurationException {

    }
}
