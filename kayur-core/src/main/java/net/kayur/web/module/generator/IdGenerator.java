package net.kayur.web.module.generator;


import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Parameter;
import net.kayur.core.configuration.StringParameter;
import net.kayur.web.Document;

public abstract class IdGenerator {
    public static final String GROUP_GENERATOR = "generator";
    public static final Parameter<String> OPTION_ID_GENERATOR_CLS = new StringParameter(GROUP_GENERATOR, "class");

    public abstract String currentDocumentId();
    public abstract boolean selectNextDocument();
    public abstract void init(Document last) throws ConfigurationException;
}
