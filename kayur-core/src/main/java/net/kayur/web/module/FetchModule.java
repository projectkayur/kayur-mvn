package net.kayur.web.module;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.helper.ParseUtils;
import net.kayur.web.Document;
import net.kayur.web.Metadata;
import net.kayur.web.WebModuleException;
import net.kayur.web.parser.ExtractionRule;
import net.kayur.web.parser.PageParser;
import net.kayur.web.parser.RequestSettings;
import net.kayur.web.parser.WebDocument;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.*;

import static net.kayur.Msg.msg;


public class FetchModule {
    public final long id;
    public final ModuleConfiguration config;

    @NotNull
    private final String documentUrlTemplate;

    @NotNull
    private final ExtractionRule titleRule;
    @NotNull
    private final ExtractionRule contentRule;
    @NotNull
    private final ExtractionRule commentsRule;

    @NotNull
    private final ExtractionRule dateRule;
    @Nullable
    private final DateFormat dateFormat;

    @NotNull
    private final String numDocumentsUrl;
    @NotNull
    private final ExtractionRule numDocumentsRule;

    private final Map<String, ExtractionRule> metadataRules;

    @NotNull
    private final PageParser parser;

    private final Map<String, String> variables = new HashMap<>();
    private static final String ID_KEY = "id";


    public FetchModule(@NotNull ModuleConfiguration config) {
        this.id = config.getId();
        this.config = config;

        try {
            titleRule = config.getRule(ModuleConfiguration.OPTION_TITLE_RULE);
            contentRule = config.getRule(ModuleConfiguration.OPTION_CONTENT_RULE);
            commentsRule = config.getRule(ModuleConfiguration.OPTION_COMMENTS_RULE);
            dateRule = config.getRule(ModuleConfiguration.OPTION_DATE_RULE);
            numDocumentsRule = config.getRule(ModuleConfiguration.OPTION_NUM_DOCS_RULE);
            metadataRules = config.getMetaRules();
        } catch (ParseException e) {
            throw new ConfigurationException(msg("module_instantiation_failure", e.getMessage()), e);
        }

        documentUrlTemplate = config.get(ModuleConfiguration.OPTION_DOC_URL_TEMPLATE);
        dateFormat = config.getDateFormat(ModuleConfiguration.OPTION_DOC_DATE_FMT);

        numDocumentsUrl = config.get(ModuleConfiguration.OPTION_NUM_DOCS_URL);

        // construct request settings from the module configuration
        RequestSettings requestSettings = new RequestSettings();
        requestSettings.setTimeoutMs(config.get(ModuleConfiguration.OPTION_WAIT_TIMEOUT));
        requestSettings.setAccessDelayMs(config.get(ModuleConfiguration.OPTION_ACCESS_DELAY));
        requestSettings.setFollowRedirects(config.get(ModuleConfiguration.OPTION_FOLLOW_REDIRECTS));
        requestSettings.setUserAgent(config.get(ModuleConfiguration.OPTION_USER_AGENT));
        this.parser = new PageParser(requestSettings);
    }

    @NotNull
    public Document fetch(String documentId) throws IOException, ParseException {
        // download a web document
        String url = getUrl(documentId);
        Disp.debug(msg("module_accessing_url", url));

        WebDocument document = parser.fetch(url);

        Disp.debug(msg("module_document_fetched", documentId));

        Document issue = new Document(id, documentId);

        // get date
        Date date = null;
        if (dateFormat != null) {
            String dateStr = document.getData(dateRule);
            if (dateStr != null && !dateStr.isEmpty()) {
                date = dateFormat.parse(dateStr);
            }
        }
        issue.setDateTime(date);

        // get main fields

        String title = document.getData(titleRule);
        if (title != null)
            issue.setTitle(title);

        String content = document.getData(contentRule);
        if (content != null)
            issue.setContent(content);

        List<String> comments = document.getDataList(commentsRule);
        issue.setComments(comments);

        List<Metadata> meta = new ArrayList<>();
        for (Map.Entry<String, ExtractionRule> entry : metadataRules.entrySet()) {
            String tag = entry.getKey();
            ExtractionRule rule = entry.getValue();
            meta.add(new Metadata(tag, document.getData(rule)));
        }
        issue.setMeta(meta);

        // TODO: add filtering based on metadata?

        return issue;
    }

    // Obtains the complete URL of a document based on its identifier.
    private String getUrl(String documentId) {
        if (documentUrlTemplate.isEmpty())
            throw new ConfigurationException("Document URL template is not set.");

        variables.put(ID_KEY, documentId);
        return ParseUtils.replaceVariables(documentUrlTemplate, variables);
    }

    /**
     * Gets the number of available documents on a web resource.
     *
     * @return number of documents
     */
    public long getNumberOfAvailableDocuments() {
        if (numDocumentsUrl.isEmpty() || numDocumentsRule.isEmpty()) {
            throw new ConfigurationException(msg("module_num_docs_empty_cfg"));
        }

        WebDocument document;
        try {
            document = parser.fetch(numDocumentsUrl);
        } catch (IOException e) {
            throw new WebModuleException(msg("web_fetch_failure"), e);
        }

        long numDocuments;
        try {
            String numDocumentsStr = document.getData(numDocumentsRule);
            numDocuments = ParseUtils.parseIntegerRemoveNoise(numDocumentsStr);
        } catch (ParseException e) {
            throw new WebModuleException(msg("module_num_documents_parse_failure"), e);
        }

        Disp.info(msg("module_num_docs_status", numDocuments));
        return numDocuments;
    }
}
