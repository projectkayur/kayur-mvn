package net.kayur.web.module;


public class MetadataRuleHolder {
    private String field;
    private String rule;
    private String pattern;

    public MetadataRuleHolder() {
        this.field = "";
        this.rule = "";
        this.pattern = "";
    }

    public MetadataRuleHolder(String field, String rule, String pattern) {
        this.field = field;
        this.rule = rule;
        this.pattern = pattern;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public String getRule() {
        return rule;
    }

    public void setRule(String rule) {
        this.rule = rule;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }
}
