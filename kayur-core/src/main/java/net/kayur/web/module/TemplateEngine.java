package net.kayur.web.module;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Parameter;
import org.jetbrains.annotations.NotNull;

import static net.kayur.Msg.msg;

/**
 * Module generation from templates.
 */
public class TemplateEngine {
    private final ModuleLoader moduleLoader;

    public TemplateEngine(ModuleLoader moduleLoader) {
        this.moduleLoader = moduleLoader;
    }

    @NotNull
    public ModuleConfiguration generateJiraConfig(@NotNull String projectName, @NotNull String userUrl) {
        ModuleConfiguration cfg = moduleLoader.loadTemplate("jira-template.xml");
        String url = normalizeUrl(userUrl);

        setTemplateParameter(cfg, ModuleConfiguration.OPTION_NUM_DOCS_URL, url, projectName.toUpperCase());
        setTemplateParameter(cfg, ModuleConfiguration.OPTION_DOC_URL_TEMPLATE, url, projectName, projectName);

        return cfg;
    }

    @NotNull
    public ModuleConfiguration generateBugzillaConfig(@NotNull String userUrl) {
        ModuleConfiguration cfg = moduleLoader.loadTemplate("bugzilla-template.xml");
        String url = normalizeUrl(userUrl);

        setTemplateParameter(cfg, ModuleConfiguration.OPTION_NUM_DOCS_URL, url);
        setTemplateParameter(cfg, ModuleConfiguration.OPTION_DOC_URL_TEMPLATE, url);

        return cfg;
    }

    private void setTemplateParameter(@NotNull ModuleConfiguration cfg, @NotNull Parameter<String> parameter,
                                      @NotNull Object... args) {
        String template = cfg.get(parameter);

        if (countMatches(template, "%s") != args.length)
            throw new ConfigurationException(msg("cfg_incorrect_template", parameter));

        cfg.set(parameter, String.format(template, args));
    }

    static int countMatches(String source, String substr) {
        if (source.isEmpty() || substr.isEmpty()) {
            return 0;
        }

        int index = -1;
        int numMatches = -1;

        do {
            index = source.indexOf(substr, index + 1);
            ++numMatches;
        } while (index != -1);

        return numMatches;
    }

    @NotNull
    private String normalizeUrl(@NotNull String url) {
        // if a user did not provide protocol in the URL, use the prefix "http://"
        String s1 = (url.startsWith("http://") || url.startsWith("https://")) ? url : ("http://" + url);
        // remove tailing slash
        return s1.endsWith("/") ? s1.substring(0, s1.length() - 1) : s1;
    }
}
