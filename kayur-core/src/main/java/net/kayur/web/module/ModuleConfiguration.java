package net.kayur.web.module;

import net.kayur.core.configuration.*;
import net.kayur.web.parser.AccessSequence;
import net.kayur.web.parser.ExtractionPattern;
import net.kayur.web.parser.ExtractionRule;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.*;

import static net.kayur.Msg.msg;

public class ModuleConfiguration extends Configuration implements Comparable<ModuleConfiguration> {
    private static final String GROUP_MODULE = "module";
    private static final String GROUP_ACCESS = "access";
    private static final String GROUP_META = "meta";

    private static final String POSTFIX_SEP = "_";
    private static final String AS_POSTFIX = "seq";
    private static final String EX_POSTFIX = "ex";

    private long id;
    private static final Parameter<Long> ID = new LongParameter(GROUP_MODULE, "id");

    private String name;
    private static final Parameter<String> NAME = new StringParameter(GROUP_MODULE, "name");

    public static final Parameter<String> OPTION_DOC_URL_TEMPLATE = new StringParameter(GROUP_MODULE, "doc_url_template");
    public static final Parameter<String> OPTION_TITLE_RULE = new StringParameter(GROUP_MODULE, "title_rule");
    public static final Parameter<String> OPTION_CONTENT_RULE = new StringParameter(GROUP_MODULE, "content_rule");
    public static final Parameter<String> OPTION_COMMENTS_RULE = new StringParameter(GROUP_MODULE, "comments_rule");
    public static final Parameter<String> OPTION_DATE_RULE = new StringParameter(GROUP_MODULE, "date_rule");
    public static final Parameter<String> OPTION_DOC_DATE_FMT = new StringParameter(GROUP_MODULE, "date_format");
    public static final Parameter<String> OPTION_NUM_DOCS_URL = new StringParameter(GROUP_MODULE, "num_docs_url");
    public static final Parameter<String> OPTION_NUM_DOCS_RULE = new StringParameter(GROUP_MODULE, "num_docs_rule");

    public static final Parameter<Integer> OPTION_WAIT_TIMEOUT = new IntParameter(GROUP_ACCESS, "wait_timeout");
    public static final Parameter<Integer> OPTION_ACCESS_DELAY = new IntParameter(GROUP_ACCESS, "access_delay");
    public static final Parameter<Boolean> OPTION_FOLLOW_REDIRECTS = new BoolParameter(GROUP_ACCESS, "follow_redirects");
    public static final Parameter<String> OPTION_USER_AGENT = new StringParameter(GROUP_ACCESS, "user_agent");


    private static long lastModuleCreatedTime = 0;


    public ModuleConfiguration() {
        setId(generateId());
        setName(generateName());
    }

    public ModuleConfiguration(@NotNull Configuration cfg) {
        super(cfg);

        Long idValue = get(ID);
        if (idValue <= 0)
            idValue = generateId();
        setId(idValue);

        String nameValue = get(NAME);
        if (nameValue.isEmpty())
            nameValue = generateName();
        setName(nameValue);
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int compareTo(@NotNull ModuleConfiguration other) {
        return name.toLowerCase().compareTo(other.name.toLowerCase());
    }

    @Override
    public String toString() {
        return name;
    }

    public void setId(long id) {
        this.id = id;
        set(ID, id);
    }

    private long generateId() {
        long time = Instant.now().getEpochSecond();

        if (time <= lastModuleCreatedTime)
            time = lastModuleCreatedTime + 1;

        lastModuleCreatedTime = time;
        return time;
    }

    private String generateName() {
        return "module-" + String.valueOf(id);
    }

    public void setName(String name) {
        this.name = name;
        set(NAME, name);
    }

    public static Parameter<String> getAccessSequenceParameter(Parameter parameter) {
        return new StringParameter(parameter.group, parameter.name + POSTFIX_SEP + AS_POSTFIX);
    }

    public static Parameter<String> getExtractionPatternParameter(Parameter parameter) {
        return new StringParameter(parameter.group, parameter.name + POSTFIX_SEP + EX_POSTFIX);
    }

    @NotNull
    public ExtractionRule getRule(Parameter parameter) {
        return new ExtractionRule(
                getAccessSequence(getAccessSequenceParameter(parameter)),
                getExtractionPattern(getExtractionPatternParameter(parameter))
        );
    }

    public void setRule(Parameter parameter, String sequenceValue, String patternValue) {
        set(getAccessSequenceParameter(parameter), sequenceValue);
        set(getExtractionPatternParameter(parameter), patternValue);
    }

    public void removeMetaRule(String field) {
        remove(new StringParameter(GROUP_META, field + POSTFIX_SEP + AS_POSTFIX));
        remove(new StringParameter(GROUP_META, field + POSTFIX_SEP + EX_POSTFIX));
    }

    public void setMetaRule(MetadataRuleHolder holder) {
        setMetaRule(holder.getField(), holder.getRule(), holder.getPattern());
    }

    public void setMetaRule(String tag, String sequenceValue, String patternValue) {
        setRule(new StringParameter(GROUP_META, tag), sequenceValue, patternValue);
    }

    @NotNull
    private AccessSequence getAccessSequence(Parameter<String> parameter) {
        String sequenceStr = get(parameter);
        try {
            return AccessSequence.fromString(sequenceStr).validate();
        } catch (ParseException e) {
            throw new ConfigurationException(
                    msg("rule_access_sequence_parse_failure", parameter.name, e.getErrorOffset()),
                    e);
        }
    }

    @NotNull
    private ExtractionPattern getExtractionPattern(Parameter<String> parameter) {
        String patternStr = get(parameter);
        try {
            return ExtractionPattern.fromString(patternStr);
        } catch (ParseException e) {
            throw new ConfigurationException(
                    msg("rule_extraction_pattern_parse_failure", parameter.name, e.getErrorOffset()),
                    e);
        }
    }

    Map<String, ExtractionRule> getMetaRules() throws ParseException {
        Map<String, String> groupMap = getGroup(GROUP_META);
        Map<String, ExtractionRule> resultMap = new HashMap<>();
        Set<String> usedTags = new HashSet<>();

        for (Map.Entry<String, String> entry : groupMap.entrySet()) {
            String tag = stripRulePrefix(entry.getKey());
            if (!usedTags.contains(tag)) {
                Parameter<String> p = new StringParameter(GROUP_META, tag);
                resultMap.put(tag, getRule(p));
                usedTags.add(tag);
            }
        }

        return resultMap;
    }

    public List<MetadataRuleHolder> getMetadataRuleHolders() throws ParseException {
        List<MetadataRuleHolder> result = new ArrayList<>();

        Map<String, String> groupMap = getGroup(GROUP_META);
        Set<String> usedNames = new HashSet<>();

        for (Map.Entry<String, String> entry : groupMap.entrySet()) {
            String field = stripRulePrefix(entry.getKey());
            if (!usedNames.contains(field)) {
                Parameter<String> p = new StringParameter(GROUP_META, field);
                String rule = get(getAccessSequenceParameter(p));
                String pattern = get(getExtractionPatternParameter(p));
                result.add(new MetadataRuleHolder(field, rule, pattern));
                usedNames.add(field);
            }
        }

        return result;
    }

    private String stripRulePrefix(String parameterName) throws ParseException {
        String[] tokens = parameterName.split(POSTFIX_SEP);
        if (tokens.length != 2)
            throw new ParseException(String.format("Incorrect parameter name for metadata parameter '%s'.", parameterName), 0);

        String postfix = tokens[1];
        if (!postfix.equals(AS_POSTFIX) && !postfix.equals(EX_POSTFIX))
            throw new ParseException(String.format("Incorrect parameter name postfix for metadata parameter '%s': should be '%s' or '%s'.",
                    parameterName, POSTFIX_SEP + AS_POSTFIX, POSTFIX_SEP + EX_POSTFIX), tokens[0].length() + 1);

        return tokens[0];
    }

    @Nullable
    public DateFormat getDateFormat(Parameter<String> parameter) {
        String formatStr = get(parameter);
        if (formatStr.isEmpty())
            return null;

        DateFormat format;

        try {
            format = new SimpleDateFormat(formatStr, Locale.US);
        } catch (IllegalArgumentException e) {
            throw new ConfigurationException(msg("rule_invalid_date_format", formatStr));
        }

        return format;
    }
}
