package net.kayur.web.module;

import net.kayur.Disp;
import net.kayur.core.configuration.Configuration;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Structure;
import net.kayur.helper.FileHelper;
import net.kayur.web.module.generator.IdGenerator;
import net.kayur.web.module.generator.IdGeneratorFactory;
import org.jetbrains.annotations.NotNull;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

public class ModuleLoader {
    public static final String NEW_MODULE_NAME = "New Module";

    private final List<ModuleConfiguration> modules = new ArrayList<>();
    private final Map<Long, Integer> configChangesMap = new HashMap<>();

    public ModuleLoader() {
        loadModules();
    }

    @NotNull
    private ModuleConfiguration loadModuleConfig(String filename) {
        try {
            return new ModuleConfiguration(Configuration.fromXmlFile(filename));
        } catch (IOException | ParseException | SAXException | ParserConfigurationException e) {
            throw new ConfigurationException("Cannot load module configuration.", e);
        }
    }

    private void loadModules() {
        // search for module configuration files
        Collection<File> configFiles = Structure.listModules();

        List<ModuleConfiguration> cfgList = new ArrayList<>(configFiles.size());
        for (File f : configFiles) {
            ModuleConfiguration cfg;
            try {
                cfg = loadModuleConfig(f.getAbsolutePath());
            } catch (ConfigurationException e) {
                Disp.warning(e, msg("module_load_failure", f.getName()));
                continue;
            }
            cfgList.add(cfg);
        }

        // we need at least one module
        if (cfgList.size() > 0) {
            setModules(cfgList);
            Disp.info(msg("module_load_report", modules.size()));
        } else {
            Disp.warning("No modules were found.");
        }
    }

    private void setModules(List<ModuleConfiguration> configs) {
        modules.clear();
        for (ModuleConfiguration config : configs) {
            modules.add(config);
            configChangesMap.put(config.getId(), config.hashCode());
        }
        Collections.sort(modules);
    }

    public List<ModuleConfiguration> getChangedModules() {
        List<ModuleConfiguration> changedModules = new ArrayList<>();
        for (ModuleConfiguration cfg : modules) {
            Integer hash = configChangesMap.get(cfg.getId());
            if (hash == null || hash != cfg.hashCode()) {
                changedModules.add(cfg);
            }
        }
        return changedModules;
    }

    @NotNull
    public ModuleConfiguration loadTemplate(@NotNull String templateName) {
        return loadModuleConfig(Structure.getTemplatePath(templateName));
    }

    public void save(@NotNull ModuleConfiguration config) {

        // TODO: input validation for date format, sequences, and patterns.

        String path = Structure.getModulePath(config.getId());
        try {
            FileHelper.write(path, config.toXML());
        } catch (TransformerException | IOException | ParserConfigurationException e) {
            throw new ConfigurationException("Cannot save the module '" + config.getName() + "'.", e);
        }

        // remember that we have saved this module
        configChangesMap.put(config.getId(), config.hashCode());
    }

    @NotNull
    public List<Long> findModuleIdentifiers(@NotNull String namePattern) {
        final String pattern = namePattern.toLowerCase();
        return modules.stream()
                .filter(cfg -> cfg.getName().toLowerCase().contains(pattern))
                .map(ModuleConfiguration::getId)
                .collect(Collectors.toList());
    }

    public void addModule(ModuleConfiguration cfg) {
        modules.add(cfg);
        Collections.sort(modules);
    }

    @NotNull
    public ModuleConfiguration addEmptyModule(@NotNull String name) {
        ModuleConfiguration cfg = new ModuleConfiguration();
        cfg.setName(name.isEmpty() ? NEW_MODULE_NAME : name);
        cfg.set(IdGenerator.OPTION_ID_GENERATOR_CLS, IdGeneratorFactory.TYPE_ITERATIVE);

        addModule(cfg);
        return cfg;
    }

    public List<ModuleConfiguration> getModules() {
        return modules;
    }
}
