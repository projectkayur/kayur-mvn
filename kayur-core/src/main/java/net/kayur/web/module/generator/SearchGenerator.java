package net.kayur.web.module.generator;


import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Parameter;
import net.kayur.core.configuration.StringParameter;
import net.kayur.helper.ParseUtils;
import net.kayur.web.Document;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.parser.ExtractionRule;
import net.kayur.web.parser.PageParser;
import net.kayur.web.parser.WebDocument;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SearchGenerator extends IdGenerator {

    // TODO: optimization when list of results is sorted by date
    // TODO: advanced search options (Quick Search as on NVD, etc.)

    private int currentListIndex = 0;
    private int currentPage = 0;
    private int numLinksPerPage = 0;
    private final List<String> idList = new ArrayList<>();

    @NotNull
    private final String initialSearchPageUrl;
    public static final Parameter<String> OPTION_INITIAL_SEARCH_PAGE_URL = new StringParameter(GROUP_GENERATOR, "init_search_page_url");

    @NotNull
    private final String nextSearchPageUrl;
    public static final Parameter<String> OPTION_NEXT_SEARCH_PAGE_URL = new StringParameter(GROUP_GENERATOR, "next_search_page_url");

    @NotNull
    private final ExtractionRule searchPageLinksRule;
    public static final Parameter<String> OPTION_LINKS_RULE = new StringParameter(GROUP_GENERATOR, "links_rule");

    private static final String PAGE_KEY = "page";
    private static final String START_ID_KEY = "start";
    private final Map<String, String> variables = new HashMap<>();


    SearchGenerator(@NotNull ModuleConfiguration cfg) {
        initialSearchPageUrl = cfg.getValidated(OPTION_INITIAL_SEARCH_PAGE_URL);
        nextSearchPageUrl = cfg.getValidated(OPTION_NEXT_SEARCH_PAGE_URL);

        searchPageLinksRule = cfg.getRule(OPTION_LINKS_RULE);
    }

    @Override
    public String currentDocumentId() {
        return idList.get(currentListIndex);
    }

    @Override
    public boolean selectNextDocument() {
        if (currentListIndex == 0) {
            try {
                return pollDocumentIdList();
            } catch (IOException | ParseException e) {
                return false;
            }
        }

        boolean listNotExhausted = (currentListIndex > 0) && (currentListIndex < idList.size());

        if (listNotExhausted)
            currentListIndex--;

        return listNotExhausted;
    }

    @Override
    public void init(Document last) throws ConfigurationException {
        boolean isAvailable;

        try {
            isAvailable = pollDocumentIdList();
        } catch (IOException | ParseException e) {
            throw new ConfigurationException(e);
        }

        if (!isAvailable)
            throw new ConfigurationException("No documents are available at the remote resource.");
    }

    /**
     * Creates a list of all available document IDs.
     *
     * @return list of IDs of all available documents
     */
    private boolean pollDocumentIdList() throws IOException, ParseException {
        PageParser parser = new PageParser();

        String url = (currentPage == 0) ? initialSearchPageUrl : getNextSearchPageUrl();
        WebDocument document = parser.fetch(url);

        idList.clear();
        idList.addAll(document.getDataList(searchPageLinksRule));

        if (currentPage == 0)
            numLinksPerPage = idList.size();

        boolean notEmpty = !idList.isEmpty();

        if (notEmpty) {
            currentListIndex = idList.size() - 1;
            currentPage++;
        }

        return notEmpty;
    }

    @NotNull
    private String getNextSearchPageUrl() {
        int startId = currentPage * numLinksPerPage;

        variables.put(PAGE_KEY, String.valueOf(currentPage));
        variables.put(START_ID_KEY, String.valueOf(startId));

        return ParseUtils.replaceVariables(nextSearchPageUrl, variables);
    }


    /*
     * Create the list of all issues ID's on page loaded into parser.
     *
     * @param parser    Parser object that contains current search page
     * @param limitDate add only issues newer than given date
     * @return list of issues ID's or null, if failure
     */
    /*
    private List<String> processSearchPage(PageParser parser, Date limitDate) throws Exception {
        assert parser != null;

        // set new root to quickly access child elements (if specified in config)
        if (config.AS_SEARCH_ROOT != null) {
            if (!parser.setNewRoot(config.AS_SEARCH_ROOT)) {
                Disp.error("Failed to set up new root element.");
                return null;
            }
        }

        // get list of issues ID's
        List<String> issueIdList = parser.getTextFromMultipleElements(config.AS_SEARCH_ENTRIES);
        if (issueIdList == null) {
            Disp.error("Failed to get list of issues.");
            return null;
        }

        if (limitDate != null) {
            // resize the list of issues to contain only those with newer than specified date
            int issues_num = issueIdList.size();
            int neu_issues = countNewIssues(parser, limitDate, issues_num);
            if ((neu_issues > 0) && (neu_issues < issues_num)) {
                return issueIdList.subList(0, neu_issues - 1);
            } else if (neu_issues == 0) {
                // there are no new issues in the list
                return new ArrayList<String>();
            } else if (neu_issues == Cnst.CINT_FAILURE) {
                Disp.warning("Because the dates of some issues cannot be determined, specified limit date value is ignored.");
            }
        }

        return issueIdList;
    }
    */

    /*
     * Calculate to which extent the list of issues ID's should be resized to contain only new issues.
     *
     * @param parser     parser object that contains a search page
     * @param limitDate  date, after which the issues are considered old
     * @param issues_num number of issues in the list
     * @return number of new issues, or -1 in case of failure
     */
    /*
    private int countNewIssues(PageParser parser, Date limitDate, int issues_num) throws Exception {
        assert parser != null;
        assert limitDate != null;
        assert issues_num >= 0;

        // get date of the last issue
        String dateStr = parser.parseText(config.AS_SEARCH_LAST_DATE_ON_PAGE, config.PK_SEARCH_DATE_ON_PAGE);
        Date lastDateOnPage = parseDateOnPage(dateStr);
        if (lastDateOnPage == null) {
            Disp.warning("The date of the last issue on page cannot be confirmed.");
            return Cnst.CINT_FAILURE;
        }

        if (lastDateOnPage.after(limitDate)) {
            // all issues are new
            return issues_num;
        } else {
            // no need to process further pages
            mStopOnThisPage = true;

            // get dates of all issues on this page
            List<String> tmpDateList = parser.getTextFromMultipleElements(config.AS_SEARCH_DATES);
            if (tmpDateList == null) {
                Disp.warning("Cannot get dates of all issues on page.");
                return Cnst.CINT_FAILURE;
            }

            // check whether size of list of dates equals size of list of issues
            if (tmpDateList.size() != issues_num) {
                Disp.warning("Lists of issues and their dates have different lengths.");
                return Cnst.CINT_FAILURE;
            }

            // count issues with dates newer than specified
            int k;
            for (k = 0; k < issues_num; k++) {
                // get the date of k-th issue
                dateStr = PageParser.extractByKey(tmpDateList.get(k), config.PK_SEARCH_DATE_ON_PAGE);
                Date dt = parseDateOnPage(dateStr);
                if (dt == null) {
                    Disp.warning("Cannot confirm dates of some issues.");
                    return Cnst.CINT_FAILURE;
                }

                if (dt.before(limitDate))
                    break;
            }
            return k;
        }
    }
    */
}
