package net.kayur.helper;

import net.kayur.Disp;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

/**
 * Helper class that contains methods to work with files.
 */
public final class FileHelper {
    private static final Charset DEFAULT_CHARSET = StandardCharsets.UTF_8;
    private static final String COMMENT_SYMBOL = "#";

    public static void write(String filename, @NotNull String data) throws IOException {
        Files.write(Paths.get(filename), data.getBytes(DEFAULT_CHARSET));
    }

    public static void write(String filename, List<String> lines) throws IOException {
        Files.write(Paths.get(filename), lines, DEFAULT_CHARSET);
    }

    @NotNull
    public static String read(String filename) throws IOException {
        return Files.readString(Paths.get(filename), DEFAULT_CHARSET);
    }

    public static List<String> readLines(String filename) throws IOException {
        return Files.readAllLines(Paths.get(filename), DEFAULT_CHARSET);
    }

    // Loads file as a list of lines. Commented lines are ignored.
    public static List<String> readLinesSkipComments(String filename) throws IOException {
        return Files.lines(Paths.get(filename))
                .map(FileHelper::stripComment)
                .filter(String::isEmpty)
                .collect(Collectors.toList());
    }

    @NotNull
    static String stripComment(@NotNull String line) {
        int commentPos = line.indexOf(COMMENT_SYMBOL);
        if (commentPos > 0) {
            return line.substring(0, commentPos).trim();
        } else if (commentPos == 0) {
            return "";
        } else {
            return line.trim();
        }
    }

    public static void createDirectory(String path) throws IOException {
        Path requestedPath = Paths.get(path);
        File requestedFile = requestedPath.toFile();
        if (requestedFile.exists()) {
            if (!requestedFile.isDirectory()) {
                throw new IOException(msg("io_cannot_create_dir_file_exists", requestedFile.getAbsolutePath()));
            }
        } else {
            Files.createDirectory(requestedPath);
        }
    }

    // Using Apache FileUtils

    public static Collection<File> listFiles(String path, String... ext) {
        File directory = new File(path);
        if (!directory.exists()) {
            Disp.warning(String.format("Directory '%s' does not exist.", directory.getAbsolutePath()));
            return List.of();
        }

        if (!directory.isDirectory()) {
            Disp.warning(String.format("Entry '%s' is not a directory.", directory.getAbsolutePath()));
            return List.of();
        }

        return FileUtils.listFiles(new File(path), ext, true); // search recursively
    }

    // Using Apache FilenameUtils

    public static String normalizePath(@NotNull String path) {
        return FilenameUtils.normalize(path);
    }

    public static String concatPath(String prefix, String name) {
        return FilenameUtils.concat(prefix, name);
    }

    public static String getFullPath(String fullName) {
        return FilenameUtils.getFullPath(fullName);
    }

    public static String getBaseName(String fullName) {
        return FilenameUtils.getBaseName(fullName);
    }

    // Appends file extension when it is necessary.
    public static String appendExtension(String filename, String ext) {
        return FilenameUtils.isExtension(filename, ext) ? filename : filename + "." + ext;
    }
}
