package net.kayur.helper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static net.kayur.Msg.msg;

public final class ParseUtils {
    public static String replaceVariables(@NotNull String s, Map<String, String> values) {
        for (Map.Entry<String, String> entry : values.entrySet()) {
            s = s.replaceAll("\\$\\{" + entry.getKey() + "\\}", entry.getValue());
        }
        return s;
    }

    public static Map<String, String> parseArgumentList(@NotNull String args) {
        final String ARG_SEPARATOR = ",";
        final String VALUE_SEPARATOR = "=";

        Map<String, String> map = new HashMap<>();

        String[] tokens = args.split(ARG_SEPARATOR);

        for (String token : tokens) {
            String[] pair = token.split(VALUE_SEPARATOR);
            if (pair.length == 2) {
                map.put(pair[0], pair[1]);
            }
        }

        return map;
    }

    public static long parseLong(@Nullable String value) throws ParseException {
        if (value == null || value.isEmpty()) {
            throw new ParseException(msg("error_value_not_integer", msg("value_empty")), 0);
        }

        try {
            return Long.parseLong(value);
        } catch (NumberFormatException e) {
            String errorMsg = msg("error_value_not_integer", value);
            throw new ParseException(errorMsg, 0);
        }
    }

    public static long parseIntegerRemoveNoise(String value) throws ParseException {
        if (value != null) {
            value = value.replaceAll("[^\\d]", "");
        }
        return parseLong(value);
    }

    public static int parseInteger(@Nullable String value) throws ParseException {
        if (value == null || value.isEmpty())
            throw new ParseException(msg("error_value_not_integer", msg("value_empty")), 0);

        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            String msg = msg("error_value_not_integer", value);
            throw new ParseException(msg, 0);
        }
    }

    public static int parsePositiveInteger(String s) throws ParseException {
        int x = parseInteger(s);
        if (x <= 0) {
            throw new ParseException(String.format("'%d' is not a positive integer.", x), 0);
        }
        return x;
    }

    public static int parseNonnegativeInt(String s) throws ParseException {
        int x = parseInteger(s);
        if (x < 0) {
            throw new ParseException(String.format("'%d' is not a nonnegative integer.", x), 0);
        }
        return x;
    }

    @Nullable
    public static Date parseDate(@NotNull String dateStr, @NotNull DateFormat format) {
        Date date;
        try {
            date = format.parse(dateStr);
        } catch (ParseException e) {
            date = null;
        }
        return date;
    }


    public static int parseFormattedNonnegativeInt(String s) throws ParseException {
        String s1 = s.replaceAll("[,.]", "");
        return parseNonnegativeInt(s1);
    }
}
