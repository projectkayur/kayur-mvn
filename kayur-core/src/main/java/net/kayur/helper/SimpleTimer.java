package net.kayur.helper;

import java.util.concurrent.TimeUnit;

public class SimpleTimer {
    private long start;

    public SimpleTimer() {
        start = System.nanoTime();
    }

    public long time() {
        long stop = System.nanoTime();
        return TimeUnit.NANOSECONDS.toMillis(stop - start);
    }

    public void reset() {
        start = System.nanoTime();
    }
}
