package net.kayur.helper;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Provides utility functions for XML parsing.
 */
public final class XmlUtils {

    private XmlUtils() {
    }

    @NotNull
    public static String getRequiredAttribute(@NotNull Node node, @NotNull String attrName) throws ParseException {
        String value = null;

        NamedNodeMap attributes = node.getAttributes();
        if (attributes != null) {
            Node attrNode = attributes.getNamedItem(attrName);
            if (attrNode != null) {
                value = attrNode.getNodeValue();
            }
        }

        if (value == null) {
            String errorMsg = String.format("Node '%s' does not have required attribute '%s'",
                    node.getNodeName(), attrName);
            throw new ParseException(errorMsg, 0);
        }

        return value;
    }

    @NotNull
    public static List<Node> getChildNodesByName(@NotNull Node node, @NotNull String name) {
        List<Node> selectedNodes = new ArrayList<>();
        NodeList nodes = node.getChildNodes();
        if (nodes != null) {
            for (int i = 0; i < nodes.getLength(); i++) {
                Node child = nodes.item(i);
                if (child.getNodeName().equals(name)) {
                    selectedNodes.add(child);
                }
            }
        }
        return selectedNodes;
    }

    @NotNull
    public static Node getUniqueChild(@NotNull Node parent, @NotNull String name) throws ParseException {
        Node result = null;
        int childNum = 0;

        NodeList nodes = parent.getChildNodes();

        if (nodes != null) {
            for (int i = 0; i < nodes.getLength(); i++) {
                Node child = nodes.item(i);
                if (child.getNodeName().equals(name)) {
                    if (++childNum > 1) {
                        String errorMsg = String.format("Node '%s' is not unique within '%s'.",
                                name, parent.getNodeName());
                        throw new ParseException(errorMsg, i);
                    }
                    result = child;
                }
            }
        }

        if (result == null) {
            String errorMsg = String.format("Node '%s' does not exist within '%s'.", name, parent.getNodeName());
            throw new ParseException(errorMsg, 0);
        }

        return result;
    }

    @NotNull
    public static <T> List<T> traverse(@NotNull String xmlString, Function<String[], T> transform,
                                       @NotNull String... attributes)
            throws IOException, SAXException, ParserConfigurationException, ParseException {

        Document document = XmlUtils.fromXML(xmlString);
        Element root = document.getDocumentElement();

        List<T> resultValues = new ArrayList<>();
        String[] attrValues = new String[attributes.length];

        NodeList children = root.getChildNodes();
        for (int i = 0; i < children.getLength(); i++) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                for (int j = 0; j < attributes.length; j++) {
                    attrValues[j] = XmlUtils.getRequiredAttribute(child, attributes[j]);
                }
                resultValues.add(transform.apply(attrValues));
            }
        }

        return resultValues;
    }

    public static Document newDocument() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(true);
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.newDocument();
    }

    @NotNull
    public static String toXML(String rootNodeName, String elementNodeName, String[] attributes, List<String[]> values)
            throws ParserConfigurationException, TransformerException {

        Document doc = XmlUtils.newDocument();
        Element rootElement = doc.createElement(rootNodeName);

        int n = attributes.length;

        for (String[] tuple : values) {
            Element element = doc.createElement(elementNodeName);
            for (int i = 0; i < n; ++i) {
                element.setAttribute(attributes[i], tuple[i]);
            }
            rootElement.appendChild(element);
        }

        doc.appendChild(rootElement);

        return XmlUtils.toXML(doc);
    }

    @NotNull
    public static String toXML(@NotNull Document doc) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(doc), new StreamResult(writer));

        return writer.getBuffer().toString();
    }

    @NotNull
    public static Document fromXML(@NotNull String xmlString) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        return builder.parse(new InputSource(new StringReader(xmlString)));
    }
}
