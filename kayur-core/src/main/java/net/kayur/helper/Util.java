package net.kayur.helper;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

public class Util {
    public static <T> void count(@NotNull Map<T, Integer> map, @NotNull T key) {
        map.put(key, 1 + map.getOrDefault(key, 0));
    }

    public static <T> int maxKey(@NotNull Map<Integer, T> map) {
        int max = Integer.MIN_VALUE;
        for (Map.Entry<Integer, T> e : map.entrySet()) {
            if (e.getKey() > max) {
                max = e.getKey();
            }
        }
        return max;
    }
}
