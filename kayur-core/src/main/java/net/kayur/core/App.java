package net.kayur.core;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationLoader;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.core.configuration.Structure;
import net.kayur.database.ConnectionHub;
import net.kayur.nlp.corpus.TextEngineModuleLocator;

import java.io.FileNotFoundException;
import java.io.IOException;

import static net.kayur.Msg.msg;

/**
 * Represents program state.
 */
public class App {
    public static final String NAME = "Kayur";
    public static final String VERSION = "1.5.3";
    private static final String DATE = "Aug 8, 2017";

    private final MainConfiguration config;
    private final ConfigurationLoader configurationLoader;
    private final ConnectionHub connectionHub;
    private final TextEngineModuleLocator textEngineModuleLocator;

    public App(MainConfiguration config,
               ConfigurationLoader configurationLoader,
               ConnectionHub connectionHub,
               TextEngineModuleLocator textEngineModuleLocator) {
        this.config = config;
        this.configurationLoader = configurationLoader;
        this.connectionHub = connectionHub;
        this.textEngineModuleLocator = textEngineModuleLocator;
    }

    /**
     * Reads configuration and initializes all tool components.
     *
     * @return false, if configuration cannot be loaded
     */
    public boolean initialize() {
        Structure.createWorkDirectories();

        enableLogToFile();

        Disp.info(msg("app_init_msg", NAME, VERSION, DATE));

        return loadMainConfig();
    }

    /**
     * Performs the proper application shutdown.
     */
    public void shutdown() {
        Disp.info(msg("app_shutdown_start"));

        // closes all database connections; shutdowns the embedded database, if it is used
        connectionHub.shutdown();

        Disp.info(msg("app_shutdown_completed"));

        // shutdown logger
        Disp.closeLogger();
    }



    private static void enableLogToFile() {
        String logFilename = Structure.getLogPath();
        try {
            Disp.appendFileHandler(logFilename);
        } catch (IOException e) {
            Disp.error(e, msg("io_cannot_create_log_file", logFilename));
        }
    }


    // main config related

    /**
     * Loads main configuration from the file with predefined name.
     * If the file is not found, tries to load default values and recreate it.
     *
     * @return false, if config cannot be loaded or re-initialized
     */
    private boolean loadMainConfig() {
        boolean opResult = true;

        try {
            config.setFrom(configurationLoader.load(Structure.getMainConfigPath(), MainConfiguration.class));
        } catch (FileNotFoundException e) {
            Disp.info(msg("cfg_recreate_msg"));
            config.initializeDefaultConfiguration(textEngineModuleLocator);

            try {
                configurationLoader.saveMainConfig(config);
            } catch (IOException e1) {
                Disp.warning(e1, msg("cfg_recreate_save_failure"));
                // we do not return false here, because the tool can work with the unsaved config
            }

        } catch (IOException e) {
            Disp.error(e, msg("cfg_main_load_error"));
            opResult = false;
        }

        return opResult;
    }
}
