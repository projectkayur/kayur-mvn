package net.kayur.core;

import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

public class DocumentSelectionImpl implements DocumentSelection {
    private final List<Document> selection;
    @NotNull
    private UUID id;

    public DocumentSelectionImpl() {
        selection = new ArrayList<>();
        id = UUID.randomUUID();
    }

    @Override
    public void set(Collection<Document> items) {
        selection.clear();
        add(items);
    }

    @Override
    public void add(Collection<Document> items) {
        // TODO: avoid duplicates
        selection.addAll(items);
        id = UUID.randomUUID();
    }

    @Override
    public List<Document> get() {
        return selection;
    }

    @Override
    public @NotNull Document getDocument(int pos) {
        return selection.get(pos);
    }

    @Override
    public int size() {
        return selection.size();
    }

    @Override
    public @NotNull UUID id() {
        return this.id;
    }

    @Override
    public boolean isEmpty() {
        return selection.isEmpty();
    }
}
