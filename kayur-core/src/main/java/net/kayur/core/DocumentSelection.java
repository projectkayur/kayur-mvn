package net.kayur.core;

import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.UUID;

public interface DocumentSelection {
    void set(Collection<Document> documents);
    void add(Collection<Document> documents);

    Collection<Document> get();

    @NotNull Document getDocument(int pos);

    boolean isEmpty();

    int size();

    UUID id();
}
