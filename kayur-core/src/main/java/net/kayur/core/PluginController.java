package net.kayur.core;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Structure;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;

import static net.kayur.Msg.msg;

public class PluginController {

    public PluginController() {
        locatePlugins();
    }

    private Map<String, Class<?>> loadedClasses = new HashMap<>();
    private URL[] pluginJars = new URL[0];

    // Enumerates available plugins.
    private void locatePlugins() {
        Collection<File> libraries = Structure.listPlugins();
        List<URL> urls = new ArrayList<>();

        for (File f : libraries) {
            try {
                URL pluginUrl = f.toURI().toURL();
                urls.add(pluginUrl);
                Disp.debug(msg("plugin_found", pluginUrl.toString()));
            } catch (MalformedURLException e) {
                Disp.warning(msg("plugin_malformed_url", f.getAbsolutePath()));
            }
        }

        pluginJars = urls.toArray(new URL[0]);
        Disp.debug(msg("plugin_located", urls.size()));
    }

    @NotNull
    public Object createPluginObject(@NotNull String clsName) {
        return createPluginObject(clsName, null);
    }

    @NotNull
    public Object createPluginObject(@NotNull String clsName, @Nullable ClassLoader loader) {
        try {
            return findClass(clsName, loader).getDeclaredConstructor().newInstance();
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            throw new ConfigurationException(msg("plugin_cls_cannot_instantiate", clsName), e);
        }
    }

    private Class<?> findClass(@NotNull String clsName, @Nullable ClassLoader loader) {
        if (clsName.isEmpty()) {
            throw new ConfigurationException(msg("plugin_cls_name_empty"));
        }

        // first, check if we already loaded the class
        if (loadedClasses.containsKey(clsName)) {
            return loadedClasses.get(clsName);
        }

        // otherwise, load it from the plugin jar libraries
        if (pluginJars.length == 0)
            throw new ConfigurationException(msg("plugin_none_available", clsName));

        ClassLoader urlLoader = URLClassLoader.newInstance(pluginJars, loader);

        Class<?> cls;
        try {
            cls = Class.forName(clsName, true, urlLoader); // initialize = true
        } catch (ClassNotFoundException e) {
            throw new ConfigurationException(msg("plugin_cls_not_found", clsName), e);
        }

        loadedClasses.put(clsName, cls);
        Disp.debug(msg("plugin_loaded", clsName));

        return cls;
    }
}
