package net.kayur.core.configuration;

/**
 * Exception class for configuration issues.
 */
public class ConfigurationException extends RuntimeException {
    public ConfigurationException(String message) {
        super(message);
    }

    public ConfigurationException(Throwable e) {
        super(e);
    }

    public ConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }
}
