package net.kayur.core.configuration;


import net.kayur.helper.ParseUtils;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;

public class LongParameter extends Parameter<Long> {
    public LongParameter(String group, String name) {
        super(group, name);
    }

    @NotNull
    @Override
    public String export(@NotNull Long value) {
        return String.valueOf(value);
    }

    @NotNull
    @Override
    public Long convert(@NotNull String str) throws ParseException {
        return ParseUtils.parseLong(str);
    }

    @NotNull
    @Override
    public Long getDefault() {
        return 0L;
    }
}
