package net.kayur.core.configuration.jackson;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import net.kayur.nlp.plugins.api.TextEngineModule;

import java.io.IOException;

public class TextEngineModuleConfigSerializer extends StdSerializer<TextEngineModule> {

    public TextEngineModuleConfigSerializer() {
        this(null);
    }

    private TextEngineModuleConfigSerializer(Class<TextEngineModule> t) {
        super(t);
    }

    @Override
    public void serialize(TextEngineModule module, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeStringField("name", module.getName());
        jsonGenerator.writeEndObject();
    }
}
