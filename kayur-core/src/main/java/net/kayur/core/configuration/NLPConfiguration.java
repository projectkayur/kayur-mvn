package net.kayur.core.configuration;

import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.api.Tokenizer;
import net.kayur.nlp.plugins.dummy.DummySentenceDetector;
import net.kayur.nlp.plugins.dummy.DummyStemmer;
import net.kayur.nlp.plugins.dummy.DummyTokenizer;
import org.jetbrains.annotations.NotNull;

// Configuration of Natural Language Processing (NLP) components of the App.
public class NLPConfiguration {
    private boolean enableFilters;
    private @NotNull String filterList = "";

    private boolean enableStopWords;
    private @NotNull String stopWordsList = "";

    private boolean enableSentenceDetector;
    @NotNull
    private SentenceDetector sentenceDetector = new DummySentenceDetector();

    private boolean enableStemmer;
    @NotNull
    private Stemmer stemmer = new DummyStemmer();

    @NotNull
    private Tokenizer tokenizer = new DummyTokenizer();

    private @NotNull String categoryMapping = "";


    public void setFrom(@NotNull NLPConfiguration config) {
        enableFilters = config.enableFilters;
        filterList = config.filterList;
        enableStopWords = config.enableStopWords;
        stopWordsList = config.stopWordsList;
        enableSentenceDetector = config.enableSentenceDetector;
        sentenceDetector = config.sentenceDetector;
        enableStemmer = config.enableStemmer;
        stemmer = config.stemmer;
        tokenizer = config.tokenizer;
        categoryMapping = config.categoryMapping;
    }

    public boolean isEnableFilters() {
        return enableFilters;
    }

    public void setEnableFilters(boolean enableFilters) {
        this.enableFilters = enableFilters;
    }

    @NotNull
    public String getFilterList() {
        return filterList;
    }

    public void setFilterList(@NotNull String filterList) {
        this.filterList = filterList;
    }

    public boolean isEnableStopWords() {
        return enableStopWords;
    }

    public void setEnableStopWords(boolean enableStopWords) {
        this.enableStopWords = enableStopWords;
    }

    @NotNull
    public String getStopWordsList() {
        return stopWordsList;
    }

    public void setStopWordsList(@NotNull String stopWordsList) {
        this.stopWordsList = stopWordsList;
    }

    public boolean isEnableSentenceDetector() {
        return enableSentenceDetector;
    }

    public void setEnableSentenceDetector(boolean enableSentenceDetector) {
        this.enableSentenceDetector = enableSentenceDetector;
    }

    @NotNull
    public SentenceDetector getSentenceDetector() {
        return sentenceDetector;
    }

    public void setSentenceDetector(@NotNull SentenceDetector sentenceDetector) {
        this.sentenceDetector = sentenceDetector;
    }

    public boolean isEnableStemmer() {
        return enableStemmer;
    }

    public void setEnableStemmer(boolean enableStemmer) {
        this.enableStemmer = enableStemmer;
    }

    @NotNull
    public Stemmer getStemmer() {
        return stemmer;
    }

    public void setStemmer(@NotNull Stemmer stemmer) {
        this.stemmer = stemmer;
    }

    @NotNull
    public Tokenizer getTokenizer() {
        return tokenizer;
    }

    public void setTokenizer(@NotNull Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    @NotNull
    public String getCategoryMapping() {
        return categoryMapping;
    }

    public void setCategoryMapping(@NotNull String categoryMapping) {
        this.categoryMapping = categoryMapping;
    }
}
