package net.kayur.core.configuration;

import net.kayur.Disp;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import static net.kayur.Msg.msg;
import static net.kayur.helper.FileHelper.*;

/**
 * Represents the structure of the App on the disk (directories, files).
 */
public final class Structure {
    // directories
    private static final String executionDir;
    private static final String LOG_DIR = "log";
    private static final String MODULE_DIR = "modules";
    private static final String PLUGIN_DIR = "plugins";
    private static final String TEMPLATE_DIR = "templates";
    private static final String TP_MODEL_DIR = "model";

    private static final String[] ALL_DIRS = {
            LOG_DIR, MODULE_DIR, PLUGIN_DIR, TEMPLATE_DIR, TP_MODEL_DIR
    };

    // files
    private static final String LOG_FILENAME = "kayur.log";
    private static final String MAIN_CONFIG_FILENAME = "config.yaml";
    private static final String MODULE_NAME_PATTERN = "m-%d.xml";

    // extensions
    private static final String MODULE_EXT = "xml";
    private static final String[] PLUGIN_EXT = { "jar", "class" };

    //

    static {
        executionDir = new File(".").getAbsolutePath();
    }

    public static void createWorkDirectories() {
        for (String dir : ALL_DIRS) {
            try {
                createDirectory(concatPath(executionDir, dir));
            } catch (IOException e) {
                Disp.warning(msg("io_cannot_create_dir", dir));
            }
        }
    }

    public static String getLogPath() {
        return concatPath(executionDir, concatPath(LOG_DIR, LOG_FILENAME));
    }

    public static String getMainConfigPath() {
        return concatPath(executionDir, MAIN_CONFIG_FILENAME);
    }

    public static String getModulePath(Long moduleId) {
        String filename = String.format(MODULE_NAME_PATTERN, moduleId);
        return concatPath(executionDir, concatPath(MODULE_DIR, filename));
    }

    public static Collection<File> listModules() {
        return listFiles(concatPath(executionDir, MODULE_DIR), MODULE_EXT);
    }

    public static String getTemplatePath(String filename) {
        return concatPath(executionDir, concatPath(TEMPLATE_DIR, filename));
    }

    static String getTextProcessingModelsPath(String filename) {
        return concatPath(executionDir, concatPath(TP_MODEL_DIR, filename));
    }

    public static Collection<File> listPlugins() {
        return listFiles(concatPath(executionDir, PLUGIN_DIR), PLUGIN_EXT);
    }
}
