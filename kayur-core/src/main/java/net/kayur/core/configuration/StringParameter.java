package net.kayur.core.configuration;


import org.jetbrains.annotations.NotNull;

/**
 * A configuration parameter that is associated with a value of type String.
 * The most used parameter type.
 */
public class StringParameter extends Parameter<String> {
    public StringParameter(String group, String name) {
        super(group, name);
    }

    @NotNull
    @Override
    public String export(@NotNull String value) {
        return value.trim();
    }

    @NotNull
    @Override
    public String convert(@NotNull String str) {
        return str.trim();
    }

    @NotNull
    @Override
    public String getDefault() {
        return "";
    }
}
