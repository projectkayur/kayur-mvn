package net.kayur.core.configuration.jackson;

import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.plugins.api.SentenceDetector;

public class SentenceDetectorConfigDeserializer extends TextEngineModuleConfigDeserializer<SentenceDetector> {

    public SentenceDetectorConfigDeserializer(TextEngineModuleLocator textEngineModuleLocator) {
        super(textEngineModuleLocator);
    }

    @Override
    SentenceDetector getTextEngineModuleByName(TextEngineModuleLocator locator, String name) {
        return locator.findSentenceDetectorByName(name);
    }
}
