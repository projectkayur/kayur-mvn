package net.kayur.core.configuration.jackson;

import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.plugins.api.Stemmer;

public class StemmerConfigDeserializer extends TextEngineModuleConfigDeserializer<Stemmer> {

    public StemmerConfigDeserializer(TextEngineModuleLocator textEngineModuleLocator) {
        super(textEngineModuleLocator);
    }

    @Override
    Stemmer getTextEngineModuleByName(TextEngineModuleLocator locator, String name) {
        return locator.findStemmerByName(name);
    }
}
