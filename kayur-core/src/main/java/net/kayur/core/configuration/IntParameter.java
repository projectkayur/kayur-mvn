package net.kayur.core.configuration;


import net.kayur.helper.ParseUtils;
import org.jetbrains.annotations.NotNull;

import java.text.ParseException;

public class IntParameter extends Parameter<Integer> {
    public IntParameter(String group, String name) {
        super(group, name);
    }

    @NotNull
    @Override
    public String export(@NotNull Integer value) {
        return String.valueOf(value);
    }

    @NotNull
    @Override
    public Integer convert(@NotNull String str) throws ParseException {
        return ParseUtils.parseInteger(str);
    }

    @NotNull
    @Override
    public Integer getDefault() {
        return 0;
    }
}
