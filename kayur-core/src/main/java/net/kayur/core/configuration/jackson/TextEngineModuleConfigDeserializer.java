package net.kayur.core.configuration.jackson;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import net.kayur.nlp.corpus.TextEngineModuleLocator;

import java.io.IOException;

abstract class TextEngineModuleConfigDeserializer<T> extends StdDeserializer<T> {
    private final TextEngineModuleLocator textEngineModuleLocator;

    public TextEngineModuleConfigDeserializer(TextEngineModuleLocator textEngineModuleLocator) {
        this(textEngineModuleLocator, null);
    }

    private TextEngineModuleConfigDeserializer(TextEngineModuleLocator locator, Class<?> vc) {
        super(vc);
        this.textEngineModuleLocator = locator;
    }

    @Override
    public T deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        String name = node.get("name").asText();

        return getTextEngineModuleByName(textEngineModuleLocator, name);
    }

    abstract T getTextEngineModuleByName(TextEngineModuleLocator locator, String name);
}
