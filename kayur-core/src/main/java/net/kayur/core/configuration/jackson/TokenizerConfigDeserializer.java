package net.kayur.core.configuration.jackson;

import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.plugins.api.Tokenizer;

public class TokenizerConfigDeserializer extends TextEngineModuleConfigDeserializer<Tokenizer> {

    public TokenizerConfigDeserializer(TextEngineModuleLocator textEngineModuleLocator) {
        super(textEngineModuleLocator);
    }

    @Override
    Tokenizer getTextEngineModuleByName(TextEngineModuleLocator locator, String name) {
        return locator.findTokenizerByName(name);
    }
}
