package net.kayur.core.configuration;

import net.kayur.Disp;
import net.kayur.helper.FileHelper;
import net.kayur.helper.XmlUtils;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

import static net.kayur.Msg.msg;

/**
 * Generic two-level configuration that can be stored in XML file.
 */
public class Configuration {
    private static final String ROOT_NODE = "config";
    private static final String PARAMETER_NODE = "parameter";
    private static final String NAME_ATTRIBUTE = "name";
    private static final String VALUE_ATTRIBUTE = "value";

    // (groupName -> map(parameterName -> value))
    private final Map<String, Map<String, String>> parameterMap = new HashMap<>();

    public Configuration() {
    }

    public Configuration(@NotNull Configuration other) {
        assign(other);
    }


    public void assign(Configuration other) {
        parameterMap.clear();
        other.parameterMap.forEach((groupName, map) -> this.parameterMap.put(groupName, new HashMap<>(map)));
    }

    public <T> void remove(Parameter<T> parameter) {
        Map<String, String> map = parameterMap.get(parameter.group);
        map.remove(parameter.name);
    }

    /**
     * Adds a configuration parameter and its value to the map.
     *
     * @param parameter (group, name) pair
     * @param value     parameter value, can be null
     */
    public final <T> void set(Parameter<T> parameter, @NotNull T value) {
        String strValue = parameter.export(value);

        Map<String, String> map = parameterMap.get(parameter.group);

        if (map != null) {
            map.put(parameter.name, strValue);
        } else {
            Map<String, String> newMap = new HashMap<>();
            newMap.put(parameter.name, strValue);
            parameterMap.put(parameter.group, newMap);
        }
    }

    /**
     * Retrieves value of a configuration parameter from the map.
     *
     * @param parameter (group, name) pair
     * @return value of the specified parameter located in the specified group, if it is present;
     *         the default value otherwise
     */
    @NotNull
    public final <T> T get(Parameter<T> parameter) {
        T result = parameter.getDefault();

        Map<String, String> map = parameterMap.get(parameter.group);

        if (map != null && map.containsKey(parameter.name)) {
            String value = map.get(parameter.name);
            if (!value.isEmpty()) {
                try {
                    result = parameter.convert(value);
                } catch (ParseException e) {
                    Disp.warning(e, msg("cfg_value_incorrect", parameter.group, parameter.name));
                }
            }
        }

        return result;
    }

    public Map<String, String> getGroup(@NotNull String groupName) {
        if (parameterMap.containsKey(groupName))
            return parameterMap.get(groupName);
        else
            return new HashMap<>();
    }

    /**
     * Retrieves a value of a configuration parameter from the map.
     * The value must be present and to be of the correct type.
     *
     * @param parameter (group, name) pair
     * @return value of the specified parameter located in the specified group
     * @throws ConfigurationException when a) the parameter is not set or b) it is set, but its value cannot be
     *         converted to the parameter type
     */
    @NotNull
    public final <T> T getValidated(Parameter<T> parameter) {
        Map<String, String> map = parameterMap.get(parameter.group);

        if (map == null || !map.containsKey(parameter.name))
            throw new ConfigurationException(msg("cfg_value_not_set", parameter.group, parameter.name));

        String value = map.get(parameter.name);

        if (value.isEmpty())
            throw new ConfigurationException(msg("cfg_value_not_set", parameter.group, parameter.name));

        try {
            return parameter.convert(value);
        } catch (ParseException e) {
            throw new ConfigurationException(msg("cfg_value_incorrect", parameter.group, parameter.name), e);
        }
    }

    /**
     * Writes the configuration to XML format.
     *
     * @return a String in XML format
     * @throws ParserConfigurationException when the XML builder cannot be instantiated (should not happen)
     * @throws TransformerException         when the Transformer cannot convert the tree to an XML structure (should not happen)
     */
    @NotNull
    public final String toXML() throws ParserConfigurationException, TransformerException {

        Document doc = XmlUtils.newDocument();
        Element rootElement = doc.createElement(ROOT_NODE);

        parameterMap.entrySet().stream()
                .sorted(Comparator.comparing(Map.Entry::getKey))  // sort groups by name
                .forEach(e -> {
                    Element groupElement = doc.createElement(e.getKey());  // creates <groupName> level one tag
                    e.getValue().entrySet().stream()
                            .sorted(Comparator.comparing(Map.Entry::getKey))  // sort parameters by name within each group
                            .forEach(f -> {
                                // creates <parameter name="f.getKey()" value="f.getValue()"> level two tag for each parameter
                                Element parameterElement = doc.createElement(PARAMETER_NODE);
                                parameterElement.setAttribute(NAME_ATTRIBUTE, f.getKey());
                                parameterElement.setAttribute(VALUE_ATTRIBUTE, f.getValue());
                                groupElement.appendChild(parameterElement);
                            });
                    rootElement.appendChild(groupElement);
                });

        doc.appendChild(rootElement);

        return XmlUtils.toXML(doc);
    }

    public static Configuration fromXmlFile(String filename) throws IOException, ParseException, SAXException, ParserConfigurationException {
        return fromXmlString(FileHelper.read(filename));
    }

    /**
     * Creates a configuration object from an XML string.
     *
     * @param xmlString source XML string
     * @return a Configuration object
     * @throws IOException when XML document cannot be instantiated from an XML string
     * @throws SAXException when XML document cannot be instantiated from an XML string
     * @throws ParserConfigurationException when XML document cannot be instantiated from an XML string
     * @throws ParseException when source XML is not a valid App's configuration
     */
    public static Configuration fromXmlString(@NotNull String xmlString) throws IOException, SAXException,
            ParserConfigurationException, ParseException {

        Document document = XmlUtils.fromXML(xmlString);
        Element root = document.getDocumentElement();
        NodeList nodes = root.getChildNodes();

        if (nodes == null)
            return new Configuration(); // empty configuration file

        Configuration config = new Configuration();

        for (int i = 0, n = nodes.getLength(); i < n; i++) {
            Node child = nodes.item(i);
            if (child.getNodeType() == Node.ELEMENT_NODE) {
                String group = child.getNodeName();
                for (Node parameter : XmlUtils.getChildNodesByName(child, PARAMETER_NODE)) {
                    String name = XmlUtils.getRequiredAttribute(parameter, NAME_ATTRIBUTE);
                    String value = XmlUtils.getRequiredAttribute(parameter, VALUE_ATTRIBUTE);
                    config.set(new StringParameter(group, name), value);
                }
            }
        }

        return config;
    }

    @Override
    public int hashCode() {
        return parameterMap.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o == null || !(o instanceof Configuration)) return false;
        Configuration that = (Configuration) o;
        return parameterMap.equals(that.parameterMap);
    }
}
