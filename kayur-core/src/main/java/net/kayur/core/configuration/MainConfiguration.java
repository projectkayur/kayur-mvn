package net.kayur.core.configuration;

import net.kayur.database.DatabaseConfiguration;
import net.kayur.nlp.corpus.TextEngineModuleLocator;
import org.jetbrains.annotations.NotNull;

// The main configuration of the App (stored in config.xml).
public class MainConfiguration {

    public final NLPConfiguration nlp;
    public final DatabaseConfiguration database;

    public MainConfiguration() {
        this(new NLPConfiguration(), new DatabaseConfiguration());
    }

    public MainConfiguration(NLPConfiguration nlpConfiguration, DatabaseConfiguration databaseConfiguration) {
        this.nlp = nlpConfiguration;
        this.database = databaseConfiguration;
    }

    public void initializeDefaultConfiguration(TextEngineModuleLocator textEngineModuleLocator) {
        nlp.setFilterList(Structure.getTextProcessingModelsPath("filters.xml"));
        nlp.setStopWordsList(Structure.getTextProcessingModelsPath("stopwords.txt"));
        nlp.setCategoryMapping(Structure.getTextProcessingModelsPath("categories.txt"));

        nlp.setTokenizer(textEngineModuleLocator.getDefaultTokenizer());
        nlp.setSentenceDetector(textEngineModuleLocator.getDefaultSentenceDetector());
        nlp.setStemmer(textEngineModuleLocator.getDefaultStemmer());

        nlp.setEnableFilters(true);
        nlp.setEnableStopWords(true);
        nlp.setEnableSentenceDetector(true);
        nlp.setEnableStemmer(true);

        final int DERBY_LONG_TEXT_LIMIT = 32700;

        database.setDriver("org.apache.derby.jdbc.EmbeddedDriver");
        database.setConnectionString("jdbc:derby:kayurdb;create=true");
        database.setUsername("");
        database.setPassword("");
        database.truncation.setTitleLen(DERBY_LONG_TEXT_LIMIT);
        database.truncation.setContentLen(DERBY_LONG_TEXT_LIMIT);
        database.truncation.setCommentLen(DERBY_LONG_TEXT_LIMIT);
        database.truncation.setMetaLen(DERBY_LONG_TEXT_LIMIT);
    }

    public void setFrom(@NotNull MainConfiguration other) {
        nlp.setFrom(other.nlp);
        database.setFrom(other.database);
    }
}
