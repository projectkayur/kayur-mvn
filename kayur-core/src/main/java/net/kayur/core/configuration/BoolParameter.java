package net.kayur.core.configuration;


import org.jetbrains.annotations.NotNull;

public class BoolParameter extends Parameter<Boolean> {

    public BoolParameter(String group, String name) {
        super(group, name);
    }

    @NotNull
    @Override
    public String export(@NotNull Boolean value) {
        return String.valueOf(value);
    }

    @NotNull
    @Override
    public Boolean convert(@NotNull String str) {
        return str.toLowerCase().equals("true");
    }

    @NotNull
    @Override
    public Boolean getDefault() {
        return false;
    }
}
