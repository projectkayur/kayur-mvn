package net.kayur.core.configuration;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import net.kayur.core.configuration.jackson.SentenceDetectorConfigDeserializer;
import net.kayur.core.configuration.jackson.StemmerConfigDeserializer;
import net.kayur.core.configuration.jackson.TextEngineModuleConfigSerializer;
import net.kayur.core.configuration.jackson.TokenizerConfigDeserializer;
import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.api.TextEngineModule;
import net.kayur.nlp.plugins.api.Tokenizer;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

// Loads and saves configuration files.
public class ConfigurationLoader {
    private @NotNull ObjectMapper mapper;

    public ConfigurationLoader(TextEngineModuleLocator textEngineModuleLocator) {
        mapper = new ObjectMapper(new YAMLFactory());
        mapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        mapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        SimpleModule module = new SimpleModule();
        module.addSerializer(TextEngineModule.class, new TextEngineModuleConfigSerializer());
        module.addDeserializer(Tokenizer.class, new TokenizerConfigDeserializer(textEngineModuleLocator));
        module.addDeserializer(SentenceDetector.class, new SentenceDetectorConfigDeserializer(textEngineModuleLocator));
        module.addDeserializer(Stemmer.class, new StemmerConfigDeserializer(textEngineModuleLocator));
        mapper.registerModule(module);
    }

    public <T> T load(String filename, Class<T> cls) throws IOException {
        File configFile = new File(filename);
        if (!configFile.exists() || configFile.length() == 0) {
            throw new FileNotFoundException();
        }

        return mapper.readValue(new File(filename), cls);
    }

    public void save(String filename, Object configuration) throws IOException {
        mapper.writeValue(new File(filename), configuration);
    }

    public void saveMainConfig(MainConfiguration config) throws IOException {
        save(Structure.getMainConfigPath(), config);
    }
}
