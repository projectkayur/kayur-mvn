package net.kayur.core.configuration;


import org.jetbrains.annotations.NotNull;

import java.text.ParseException;

/**
 * A configuration parameter that is associated with a value of type T.
 *
 * @param <T> type of the value (String, Integer, ...)
 */
public abstract class Parameter<T> {
    public final String group;
    public final String name;

    public Parameter(String group, String name) {
        this.group = group;
        this.name = name;
    }

    /**
     * Converts the value to a textual form, so that it can be stored
     * in a configuration file.
     *
     * @param value the value to convert
     * @return string representation of the value
     */
    @NotNull
    public abstract String export(@NotNull T value);

    /**
     * Converts the stored value from a textual form back to its proper type.
     *
     * @param str the textual representation of the value
     * @return the value converted to its proper type
     * @throws ParseException if the stored value cannot be converted
     */
    @NotNull
    public abstract T convert(@NotNull String str) throws ParseException;

    /**
     * Returns default value associated with the type
     */
    @NotNull
    public abstract T getDefault();
}
