package net.kayur.database;

import net.kayur.database.jpa.TIssue;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static net.kayur.Disp.warning;
import static net.kayur.Msg.msg;

/**
 * Performs text truncation for TIssue, so that a document can be stored in the database.
 * Stores truncation settings.
 */
public final class Truncation {
    private static final int MAX_SPACE_DISTANCE = 64; // maximum distance to search for a whitespace

    private final TruncationSettings settings;

    public Truncation(TruncationSettings settings) {
        this.settings = settings;
    }

    public boolean isEnabled() {
        return settings.isEnabled();
    }

    /**
     * Performs truncation of fields of the given document according to the limits set.
     *
     * @param issue a document
     */
    public void apply(@NotNull TIssue issue) {

        int limit = settings.getTitleLen();
        if (limit > 0) {
            issue.setTitle(truncate(issue.getTitle(), limit));
        }

        limit = settings.getContentLen();
        if (limit > 0) {
            issue.setContent(truncate(issue.getContent(), limit));
        }

        int eachCommentLimit = settings.getCommentLen();
        if (eachCommentLimit > 0) {
            issue.getComments().forEach(c -> c.setComment(truncate(c.getComment(), eachCommentLimit)));
        }

        int eachMetaLimit = settings.getMetaLen();
        if (eachMetaLimit > 0) {
            issue.getMeta().forEach(m -> m.setValue(truncate(m.getValue(), eachMetaLimit)));
        }
    }

    String truncate(@Nullable String input, int limit) {
        return truncate(input, limit, MAX_SPACE_DISTANCE);
    }

    /**
     * Truncates a string, avoiding incomplete words.
     *
     * @param str              input string
     * @param limit            maximum allowed length
     * @param spaceSearchLimit max distance to search for spaces
     * @return a truncated string that contains {@code limit} symbols
     */
    String truncate(@Nullable String str, int limit, int spaceSearchLimit) {
        if (limit > 0 && str != null && str.length() > limit) {
            // issue a warning
            warning(msg("db_text_limit_apply", str.length(), limit));

            // get the substring of length 'limit'
            String maxSubstring = str.substring(0, limit);

            // return the full substring if the next symbol is space
            if (str.charAt(limit) == ' ')
                return maxSubstring;

            // otherwise try to find the space symbol at a position that is the closest one to the end
            int lastSpaceIndex = maxSubstring.lastIndexOf(' ');

            // space is found and is within the space search range
            if ((lastSpaceIndex > 0) && ((spaceSearchLimit <= 0) || (limit - lastSpaceIndex <= spaceSearchLimit)))
                return maxSubstring.substring(0, lastSpaceIndex); // get a substring up to the last space symbol

            // space is not found or it is not within the space search range
            return maxSubstring;
        }
        return str;
    }
}
