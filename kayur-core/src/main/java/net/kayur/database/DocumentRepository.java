package net.kayur.database;

import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;

public interface DocumentRepository {
    void store(@NotNull Document issue);

    List<Document> select(@NotNull SelectQueryParameters params);

    long count(long moduleId);

    void delete(@NotNull DocumentId id);

    void setError(@NotNull DocumentId id, int code);

    void clearError(@NotNull DocumentId id);

    //
    // QUESTIONABLE
    //

    // usually we need to set the same tag to multiple documents
    void setTag(List<DocumentId> documents, int tag);

    // used by Module Controller
    List<String> selectDocumentIdentifiers(long moduleId);

    // used by Module Controller
    Optional<Document> selectLastDocument(long moduleId);
}
