package net.kayur.database;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.helper.FileHelper;
import org.jetbrains.annotations.NotNull;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.time.Instant;
import java.time.format.DateTimeFormatter;

import static net.kayur.Msg.msg;


/**
 * Manages the embedded database (Apache Derby).
 */
public class Embedded {
    static final String DRIVER_NAME = "org.apache.derby.jdbc.EmbeddedDriver";
    private static final String DERBY_SHUTDOWN_STRING = "jdbc:derby:;shutdown=true";
    private static final String DERBY_SHUTDOWN_OK_CODE = "XJ015";

    private final @NotNull DatabaseConfiguration config;
    private final @NotNull ConnectionHub connectionHub;

    public Embedded(@NotNull ConnectionHub connectionHub,
                    @NotNull DatabaseConfiguration cfg) {
        if (!cfg.getDriver().equals(DRIVER_NAME)) {
            throw new ConfigurationException(msg("embedded_db_incompatible_driver"));
        }

        this.config = cfg;
        this.connectionHub = connectionHub;
    }

    private Connection connect(DatabaseConfiguration cfg) throws SQLException {
        return connectionHub.getDirectConnection(cfg);
    }

    /**
     * Shutdowns embedded Derby database. Does nothing, if other database is used.
     *
     * @return operation success / failure
     */
    public boolean shutdown() {
        boolean properShutdownExc = false;

        DatabaseConfiguration shutdownConfig = DatabaseConfiguration.from(config);
        shutdownConfig.setConnectionString(DERBY_SHUTDOWN_STRING);

        try {
            connect(shutdownConfig);
        } catch (SQLException ex) {
            if (ex.getSQLState().equals(DERBY_SHUTDOWN_OK_CODE)) {
                properShutdownExc = true;
                Disp.debug("Embedded Derby database shutdown.");
            } else {
                Disp.warning(ex, "Embedded Derby database did not shutdown properly.");
            }
        } catch (ConfigurationException ex) {
            ex.printStackTrace();
        }

        return properShutdownExc;
    }

    /**
     * Backs up the embedded database.
     *
     * @param path name of the directory to create a backup copy
     * @throws DatabaseException if connection to the database cannot be established, or backup is unsuccessful
     */
    public void backupDatabase(@NotNull String path) {
        String todayDateStr = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(Instant.now());
        String backupDir = FileHelper.concatPath(path, todayDateStr);

        Disp.info("Starting embedded Derby database backup...");

        try (Connection conn = connect(config);
             CallableStatement cs = conn.prepareCall("CALL SYSCS_UTIL.SYSCS_BACKUP_DATABASE(?)")) {
            cs.setString(1, backupDir);
            cs.execute();
        } catch (SQLException e) {
            throw new DatabaseException(msg("embedded_db_backup_error"), e);
        }

        Disp.info("Backed up database to '%s'.", backupDir);
    }
}
