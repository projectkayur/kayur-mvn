package net.kayur.database;

import java.util.Objects;

/**
 * A simple structure to hold database connection parameters.
 */
public class DatabaseConfiguration {
    private String connectionString = "";
    private String driver = "";
    private String username = "";
    private String password = "";
    public final TruncationSettings truncation;


    public DatabaseConfiguration(String connectionString) {
        this();
        this.connectionString = connectionString;
    }

    public DatabaseConfiguration() {
        this.truncation = new TruncationSettings();
    }

    public void setFrom(DatabaseConfiguration other) {
        this.driver = other.driver;
        this.connectionString = other.connectionString;
        this.username = other.username;
        this.password = other.password;
        this.truncation.setFrom(other.truncation);
    }

    public static DatabaseConfiguration from(DatabaseConfiguration other) {
        DatabaseConfiguration config = new DatabaseConfiguration();
        config.setFrom(other);
        return config;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DatabaseConfiguration that = (DatabaseConfiguration) o;
        return Objects.equals(driver, that.driver) &&
                Objects.equals(connectionString, that.connectionString) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(driver, connectionString, username);
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getConnectionString() {
        return connectionString;
    }

    public void setConnectionString(String connectionString) {
        this.connectionString = connectionString;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
