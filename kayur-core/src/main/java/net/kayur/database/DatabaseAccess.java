package net.kayur.database;

import net.kayur.Disp;
import net.kayur.Msg;
import net.kayur.database.jpa.*;
import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

/**
 * Establishes a connection to the database, and performs various operations on stored data.
 */
final class DatabaseAccess implements AutoCloseable {
    private final @NotNull EntityManager entityManager;
    private final @Nullable Truncation truncation;

    DatabaseAccess(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
        this.truncation = null;
    }

    DatabaseAccess(@NotNull EntityManager entityManager, @Nullable TruncationSettings truncationSettings) {
        this.entityManager = entityManager;
        this.truncation = new Truncation(truncationSettings);
        Disp.debug(msg("db_connection_success"));
    }

    @Override
    public void close() {
        entityManager.close();
    }

    void storeIssues(Collection<Document> documents) {
        documents.forEach(this::storeIssue);
    }

    /**
     * Stores a document in the database.
     * Requires: document identifier is not empty.
     *
     * @param document the document to store
     */
    void storeIssue(@NotNull Document document) {
        DocumentId id = document.getId();

        if (!id.isValid())
            throw new PersistenceException(Msg.msg("db_persist_no_document_id"));

        Disp.debug(msg("db_persist_start", id));

        TIssue issue = findIssue(id);

        if (issue == null)
            issue = new TIssue();

        sync(issue, document);

        if (truncation != null && truncation.isEnabled()) {
            truncation.apply(issue);
        }

        persist(issue);

        Disp.debug(msg("db_persist_success", id));
    }

    // Copies data from a Document object to an existing TIssue object (mutates it).
    private void sync(@NotNull TIssue issue, @NotNull Document document) {
        issue.setModuleId(document.id.module);
        issue.setDocumentId(document.id.name);
        issue.setTag(document.getTag());

        Date date = document.getDateTime();
        if (date != null)
            issue.setTimestamp(new Timestamp(date.getTime()));

        issue.setTitle(document.getTitle());
        issue.setContent(document.getContent());

        issue.setComments(document.getComments().stream()
                .map(TComment::new)
                .collect(Collectors.toList()));

        issue.setMeta(document.getMeta().stream()
                .map(datum -> new TMeta(getMetaField(datum.field), datum.value))
                .collect(Collectors.toList()));
    }

    /**
     * Removes a document from the database.
     *
     * @param id document identifier
     */
    void deleteIssue(@NotNull DocumentId id) {
        TIssue issue = findIssue(id);

        if (issue == null)
            throw new PersistenceException(String.format("Document %s is not found.", id));

        remove(issue);
        Disp.debug(msg("db_remove", id));
    }

    /**
     * Performs SELECT query to get document entities from the database.
     *
     * @param params a structure that holds SELECT query parameters
     * @return list of entities that match the criteria provided
     */
    @NotNull
    List<TIssue> findIssues(@NotNull SelectQueryParameters params) {
        Map<String, Object> conditionMap = new HashMap<>();

        // construct SQL query

        String sql = "SELECT x FROM TIssue x";

        /*
        if (params.priority != null || params.status != null || params.component != null || params.type != null) {
            qs += " JOIN x.priority p JOIN x.status s JOIN x.type t JOIN x.component c";
        }
        */
        String sqlCond;
        try {
            sqlCond = " WHERE "
                    + appendInCondition("x", "moduleId", params.getModules(), conditionMap)
                    + appendCondition("x", "timestamp", ">=", params.getStartDate(), conditionMap)
                    + appendCondition("x", "timestamp", "<=", params.getEndDate(), conditionMap)
                    + appendCondition("x", "content", "LIKE", params.getContent(), conditionMap)
                    + appendCondition("x", "title", "LIKE", params.getTitle(), conditionMap)
                    + appendCondition("x", "documentId", "LIKE", params.getDocumentId(), conditionMap)
                    + appendCondition("x", "documentId", "IN", params.getIdList(), conditionMap)
                    + appendCondition("x", "tag", params.getTagOperator(), params.getTag(), conditionMap);
        /*
                + appendCondition("p", "priority", "LIKE", params.priority, parameterMap)
                + appendCondition("s", "status", "LIKE", params.status, parameterMap)
                + appendCondition("t", "type", "LIKE", params.type, parameterMap)
                + appendCondition("c", "component", "LIKE", params.component, parameterMap);
        */
        } catch (EmptyInListException e) {
            Disp.info(e.getMessage());
            return new ArrayList<>();
        }

        if (conditionMap.size() > 0)
            sql += sqlCond;

        if (params.getAscOrder() != null) {
            if (params.getAscOrder())
                sql += " ORDER BY CASE WHEN x.timestamp IS NULL THEN 0 ELSE 1 END, x.timestamp ASC";
            else
                sql += " ORDER BY CASE WHEN x.timestamp IS NULL THEN 1 ELSE 0 END, x.timestamp DESC";
        }

        // output debug info
        String sqlDebugMsg = sql;
        for (Map.Entry<String, Object> e : conditionMap.entrySet())
            sqlDebugMsg = sqlDebugMsg.replaceAll(":" + e.getKey(), "'" + e.getValue().toString() + "'");
        Disp.debug(msg("db_execute_query", sqlDebugMsg));

        // create query object
        TypedQuery<TIssue> query = entityManager.createQuery(sql, TIssue.class);

        // set query parameters
        conditionMap.forEach(query::setParameter);

        // set maximum number of fetched entries
        Integer limit = params.getLimit();
        if (limit != null && limit > 0)
            query.setMaxResults(limit); // limit must be positive

        try {
            return query.getResultList(); // execute query
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    /**
     * Appends a condition to a SELECT query.
     *
     * @param table        name of the table
     * @param field        name of the mapped field in entity's class
     * @param condition    a binary operator
     * @param value        the provided argument of the binary operator
     * @param parameterMap the map for query parameters and corresponding values
     * @return a part of an SQL query string
     */
    @NotNull
    private String appendCondition(@NotNull String table, @NotNull String field,
                                   @Nullable String condition, @Nullable Object value,
                                   @NotNull Map<String, Object> parameterMap) {
        String s = "";
        if (value != null && condition != null) {
            int pNum = parameterMap.size();
            String paramName = String.format("p%d", pNum);
            s = String.format(
                    "%s%s.%s %s :%s",
                    (pNum > 0) ? " AND " : "",
                    table,
                    field,
                    condition,
                    paramName
            );
            parameterMap.put(paramName, value);
        }
        return s;
    }

    @NotNull
    private String appendInCondition(@NotNull String table, @NotNull String field, @Nullable List list,
                                     @NotNull Map<String, Object> parameterMap) throws EmptyInListException {

        if (list != null && list.isEmpty())
            throw new EmptyInListException("Filtering by " + field + " is set, but the list of values is empty. " +
                    "No records can be returned.");

        return appendCondition(table, field, "IN", list, parameterMap);
    }

    /**
     * Gets a single document entity object from the database.
     *
     * @param id document identifier
     * @return a document entity with given identifier, or {@code null} if it's not found
     */
    @Nullable
    TIssue findIssue(@NotNull DocumentId id) {
        try {
            return entityManager.createNamedQuery("TIssue.find", TIssue.class)
                    .setParameter("moduleId", id.module)
                    .setParameter("documentId", id.name)
                    .getSingleResult();
        } catch (NoResultException ignored) {
            return null;
        }
    }

    /**
     * Gets a list of documents by their identifiers.
     *
     * @param documentIdList list of document identifiers
     * @return list of documents
     */
    @NotNull
    List<TIssue> findIssues(List<DocumentId> documentIdList) {
        List<Long> uniqueModules = documentIdList.stream()
                .map(id -> id.module)
                .distinct()
                .collect(Collectors.toList());

        List<TIssue> issues = new ArrayList<>();

        for (Long module : uniqueModules) {
            List<String> moduleDocuments = documentIdList.stream()
                    .filter(id -> id.module == module)
                    .map(id -> id.name)
                    .collect(Collectors.toList());
            issues.addAll(findIssues(module, moduleDocuments));
        }

        return issues;
    }

    List<TIssue> findIssues() {
        return findIssues(SelectQueryParameters.SELECT_ALL);
    }

    /**
     * Gets a list of all documents with given identifiers that belong to the specified module.
     *
     * @param module module identifier
     * @param idList list of document identifiers
     * @return list of documents
     */
    List<TIssue> findIssues(long module, List<String> idList) {
        SelectQueryParameters parameters = new SelectQueryParameters();
        parameters.setModule(module);
        parameters.setIdList(idList);
        return findIssues(parameters);
    }

    /**
     * Queries the database to get the newest stored document.
     *
     * @param module identifier of a web module
     * @return the newest document or {@code null} if it's not found
     */
    @Nullable
    TIssue getLatestIssue(long module) {
        SelectQueryParameters parameters = new SelectQueryParameters();
        parameters.setModule(module);
        parameters.setAscOrder(false);
        parameters.setLimit(1);

        List<TIssue> resultList = findIssues(parameters);

        if (resultList.isEmpty()) {
            Disp.debug(msg("db_select_no_entries_for_module", module));
            return null;
        }

        if (resultList.size() > 1)
            Disp.warning(Msg.msg("db_select_more_than_one"));

        TIssue issue = resultList.get(0);
        Disp.debug(msg("db_select_latest_success", module, issue.getDocumentId()));

        return issue;
    }

    @NotNull
    private TMetaField getMetaField(@NotNull String name) {
        try {
            return entityManager.createNamedQuery("TMetaField.find", TMetaField.class)
                    .setParameter("field", name)
                    .getSingleResult();
        } catch (NoResultException ignored) {
            TMetaField field = new TMetaField(name);
            persist(field);
            return field;
        }
    }

    List<TMetaField> allMetaFields() {
        return entityManager.createNamedQuery("TMetaField.all", TMetaField.class)
                .getResultList();
    }

    private boolean exists(TMetaField field) {
        try {
            entityManager.createNamedQuery("TMetaField.exists")
                    .setParameter("field", field.getName())
                    .getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }

    private void persist(@NotNull Object obj) {
        transaction(obj, entityManager::persist);
    }

    void remove(@NotNull Object obj) {
        transaction(obj, entityManager::remove);
    }

    private void transaction(Object obj, Consumer<Object> fun) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            if (obj instanceof Collection<?>) {
                Collection<?> collection = (Collection<?>) obj;
                collection.forEach(fun);
            } else {
                fun.accept(obj);
            }
            transaction.commit();
        } catch (PersistenceException e) {
            Disp.error(e, Msg.msg("db_em_operation_failed"));
            transaction.rollback();
            throw new PersistenceException(e);
        }
    }

    private void transaction(Runnable action) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        try {
            action.run();
            transaction.commit();
        } catch (PersistenceException e) {
            Disp.error(e, Msg.msg("db_em_operation_failed"));
            transaction.rollback();
            throw new PersistenceException(e);
        }
    }

    long getDocumentsNumber(long moduleId) {
        return ((Number) entityManager.createNamedQuery("TIssue.count")
                .setParameter("moduleId", moduleId)
                .getSingleResult())
                .longValue();
    }



    /*
     * Returns the entity object with the given value from a secondary table.
     * Creates such object, if it does not exist.
     *
     * @param namedQueue name of an SQL query to find the entity
     * @param value attribute value
     * @param cls entity class name
     * @param <T> entity class
     * @return found or new entity with the given value
     */
    /*
    private <T extends SingleValueSetter> T getAttributeEntity(@NotNull String namedQueue, @NotNull String value,
                                                               @NotNull Class<T> cls) {
        TypedQuery<T> q = entityManager.createNamedQuery(namedQueue, cls);
        q.setParameter("val", value); // named query must contain such parameter

        T result;
        try {
            result = q.getSingleResult();
        } catch (NoResultException e) {
            try {
                result = cls.newInstance();
                result.setValue(value);
                entityManager.persist(result); // do not forget that to actually store in DB, need to call tr.commit()
            } catch (InstantiationException | IllegalAccessException e1) {
                throw new InvalidParameterException();
            }
        }
        return result;
    }
    */

    /*
     * Checks whether the document with given ID is present in database.
     *
     * @param localId id of the document
     * @param module  id of the fetch module
     * @return true, if the issue exists, false otherwise
     */
    /*
    public boolean entryExists(@NotNull String localId, long module) {
        try {
            Query query = entityManager.createNamedQuery("TIssueEntity.exists");
            query.setParameter("id", localId);
            query.setParameter("module", module);
            query.getSingleResult();
        } catch (NoResultException e) {
            return false;
        }
        return true;
    }
    */

    List<String> getIssueIdList(long module) {
        final String QUERY = "SELECT x.documentId FROM TIssue x WHERE x.moduleId = " + module + " ORDER BY x.id ASC";

        TypedQuery<String> query = entityManager.createQuery(QUERY, String.class);
        try {
            return query.getResultList();
        } catch (NoResultException e) {
            return new ArrayList<>();
        }
    }

    /**
     * Sets a numeric tag to given documents.
     *
     * @param idList list of document identifiers
     * @param tag numeric tag
     */
    void setNumericTag(List<DocumentId> idList, int tag) {
        List<TIssue> issues = findIssues(idList);
        issues.forEach(issue -> issue.setTag(tag));
        persist(issues);
    }


    /* ***********************
     * Fetch errors handling
     *************************/

    /**
     * Stores or updates information about an error occurred during document fetching.
     *
     * @param id document identifier
     * @param errorCode error code
     */
    void logError(DocumentId id, int errorCode) {
        TError error;

        try {
            error = entityManager.createNamedQuery("TError.get", TError.class)
                    .setParameter("moduleId", id.module)
                    .setParameter("documentId", id.name)
                    .getSingleResult();
        } catch (NoResultException e) {
            error = new TError(id);
        }

        error.setType(errorCode);
        persist(error);
    }

    /**
     * Removes the error description record related to fetching of a particular document.
     *
     * @param id document identifier
     */
    void removeErrorRecord(DocumentId id) {
        Query q = entityManager.createNamedQuery("TError.delete");
        q.setParameter("moduleId", id.module);
        q.setParameter("documentId", id.name);

        transaction(q::executeUpdate);
    }

    /*
    public List<String> getFetchFailedIssuesId(int module) {
        List<String> rs = null;
        try {
            TypedQuery<String> q = entityManager.createNamedQuery("TFetcherrors.getIdAll", String.class);
            q.setParameter("module", module);
            rs = q.getResultList();
        } catch (Exception e) {
            Disp.warning(e, "Unable to get list of documents to recover.");
        }

        return rs;
    }
    */
}
