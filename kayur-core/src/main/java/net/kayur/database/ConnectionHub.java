package net.kayur.database;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import org.hibernate.cfg.AvailableSettings;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import static net.kayur.Msg.msg;

public class ConnectionHub {
    private static final String PERSISTENCE_UNIT_NAME = "net.kayur.database.jpa";

    @Nullable
    private EntityManagerFactory emFactory;
    @Nullable
    private DatabaseConfiguration currentDatabaseConfiguration;

    @Nullable
    private Embedded embedded = null;

    EntityManager getConnection(@NotNull DatabaseConfiguration cfg) {
        String logMessage;
        if (cfg.getUsername().isEmpty()) {
            logMessage = msg("db_connection_start", cfg.getConnectionString());
        } else {
            logMessage = msg("db_connection_start_using_credentials", cfg.getUsername(), cfg.getConnectionString());
        }
        Disp.debug(logMessage);

        EntityManagerFactory factory = getEmFactory(cfg);

        if (cfg.getDriver().equals(Embedded.DRIVER_NAME)) {
            embedded = new Embedded(this, cfg);
        }

        return factory.createEntityManager();
    }

    @NotNull
    private EntityManagerFactory getEmFactory(@NotNull DatabaseConfiguration config) {
        if (config.equals(currentDatabaseConfiguration) && emFactory != null) {
            return emFactory;
        }

        emFactory = createEntityManagerFactory(config);
        currentDatabaseConfiguration = config;
        return emFactory;
    }


    Connection getDirectConnection(DatabaseConfiguration cfg) throws SQLException {
        validateConfig(cfg);

        if (cfg.getDriver().equals(Embedded.DRIVER_NAME)) {
            embedded = new Embedded(this, cfg);
        }

        if (cfg.getUsername().isEmpty()) {
            return DriverManager.getConnection(cfg.getConnectionString());
        } else {
            return DriverManager.getConnection(cfg.getConnectionString(), cfg.getUsername(), cfg.getPassword());
        }
    }

    private EntityManagerFactory createEntityManagerFactory(@NotNull DatabaseConfiguration config) {
        return Persistence.createEntityManagerFactory(
                PERSISTENCE_UNIT_NAME,
                toConnProperties(config));
    }

    private Map<String, Object> toConnProperties(DatabaseConfiguration cfg) {
        validateConfig(cfg);

        String username = cfg.getUsername().isEmpty() ? null : cfg.getUsername();
        String password = cfg.getPassword().isEmpty() ? null : cfg.getPassword();

        // overrides settings in persistence.xml
        Map<String, Object> connProperties = new HashMap<>();
        connProperties.put(AvailableSettings.JPA_JDBC_URL, cfg.getConnectionString());
        connProperties.put(AvailableSettings.JPA_JDBC_USER, username);
        connProperties.put(AvailableSettings.JPA_JDBC_PASSWORD, password);
        //connProperties.put(AvailableSettings.JPA_JDBC_DRIVER, cfg.driver);

        return connProperties;
    }

    /**
     * Tries to establish a database connection given a particular configuration.
     *
     * @param cfg database configuration
     * @throws ConfigurationException when connection string is empty, or specified driver cannot be loaded
     */
    public void testConnection(DatabaseConfiguration cfg) {
        try (Connection conn = getDirectConnection(cfg)) {
            conn.getSchema();
        } catch (SQLException e) {
            e.printStackTrace();
            throw new ConfigurationException(msg("db_connection_failure"), e);
        }
    }

    private void validateConfig(DatabaseConfiguration cfg) {
        if (cfg.getConnectionString().isEmpty())
            throw new ConfigurationException(msg("db_conn_str_not_specified"));
    }

    /**
     * Closes all connections to the database.
     * Shutdowns the embedded database (in case when it is used).
     */
    public void shutdown() {
        if (emFactory != null) {
            emFactory.close();
        }

        if (embedded != null) {
            embedded.shutdown();
        }
    }
}
