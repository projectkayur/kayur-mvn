package net.kayur.database;

import net.kayur.core.configuration.MainConfiguration;
import net.kayur.database.jpa.TIssue;
import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * An adapter that transforms database entity objects into Document objects.
 */
public class DocumentRepositoryAdapter implements DocumentRepository {
    private final ConnectionHub connectionHub;
    private final DatabaseConfiguration config;

    public DocumentRepositoryAdapter(MainConfiguration mainConfig,
                                     ConnectionHub connectionHub) {
        this.connectionHub = connectionHub;
        this.config = mainConfig.database;
    }
    
    private DatabaseAccess getAccess() {
        return new DatabaseAccess(connectionHub.getConnection(config), config.truncation);
    }

    @Override
    public List<Document> select(@NotNull SelectQueryParameters params) {
        try (DatabaseAccess conn = getAccess()) {
            return toDocumentList(conn.findIssues(params));
        }
    }

    @Override
    public void store(@NotNull Document issue) {
        try (DatabaseAccess connection = getAccess()) {
            connection.storeIssue(issue);
        }
    }

    @Override
    public long count(long moduleId) {
        try (DatabaseAccess conn = getAccess()) {
            return conn.getDocumentsNumber(moduleId);
        }
    }

    @Override
    public void delete(@NotNull DocumentId id) {
        try (DatabaseAccess conn = getAccess()) {
            conn.deleteIssue(id);
        }
    }

    @Override
    public void setError(@NotNull DocumentId id, int code) {
        try (DatabaseAccess connection = getAccess()) {
            connection.logError(id, code);
        }
    }

    @Override
    public void clearError(@NotNull DocumentId id) {
        try (DatabaseAccess connection = getAccess()) {
            connection.removeErrorRecord(id);
        }
    }

    private List<Document> toDocumentList(List<TIssue> list) {
        return list.stream()
                .map(TIssue::toDocument)
                .collect(Collectors.toList());
    }

    //
    // QUESTIONABLE
    //

    @Override
    public void setTag(List<DocumentId> documents, int tag) {
        try (DatabaseAccess conn = getAccess()) {
            conn.setNumericTag(documents, tag);
        }
    }

    @Override
    public List<String> selectDocumentIdentifiers(long moduleId) {
        try (DatabaseAccess connection = getAccess()) {
            return connection.getIssueIdList(moduleId);
        }
    }

    @Override
    public Optional<Document> selectLastDocument(long moduleId) {
        try (DatabaseAccess conn = getAccess()) {
            TIssue lastIssue = conn.getLatestIssue(moduleId);
            return lastIssue != null ?
                    Optional.of(lastIssue.toDocument()) :
                    Optional.empty();
        }
    }
}
