package net.kayur.database;

import net.kayur.helper.ParseUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Parameters to SELECT SQL statement.
 */
public class SelectQueryParameters {
    public static final SelectQueryParameters SELECT_ALL = new SelectQueryParameters();

    private @Nullable Date startDate;
    private @Nullable Date endDate;
    private @Nullable String documentId;
    private @Nullable String title;
    private @Nullable String content; // text and comments
    private @Nullable Integer tag;
    private @Nullable String tagOperator;
    private @Nullable Boolean ascOrder; // enables sorting, currently by date field only
    private @Nullable Integer limit;
    private @Nullable List<Long> modules;
    private @Nullable List<String> idList;  // TODO: this is only available in CLI

    private final @NotNull Map<String, String> meta = new HashMap<>(); // TODO: SELECT by Metadata fields


    // setters

    public void setDocumentId(@Nullable String value) {
        this.documentId = emptyToNull(value);
    }

    public void setTitle(@Nullable String value) {
        this.title = emptyToNull(value);
    }

    public void setContent(@Nullable String value) {
        this.content = emptyToNull(value);
    }

    public void setTagOperator(@Nullable String value) {
        this.tagOperator = emptyToNull(value);
    }

    public void setMeta(@Nullable String metaStr) {
        meta.clear();

        if (metaStr != null) {
            meta.putAll(ParseUtils.parseArgumentList(metaStr));
        }
    }

    public void setModule(long id) {
        modules = Collections.singletonList(id);
    }


    // helper methods

    @Nullable
    private String emptyToNull(@Nullable String s) {
        return (s == null || s.isEmpty()) ? null : s;
    }


    // trivial getters and setters

    @Nullable
    public List<Long> getModules() {
        return modules;
    }

    public void setModules(@Nullable List<Long> modules) {
        this.modules = modules;
    }

    @Nullable
    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(@Nullable Date startDate) {
        this.startDate = startDate;
    }

    @Nullable
    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(@Nullable Date endDate) {
        this.endDate = endDate;
    }

    @Nullable
    public String getDocumentId() {
        return documentId;
    }

    @Nullable
    public String getTitle() {
        return title;
    }

    @Nullable
    public String getContent() {
        return content;
    }

    @Nullable
    public Integer getTag() {
        return tag;
    }

    public void setTag(@Nullable Integer tag) {
        this.tag = tag;
    }

    @Nullable
    public String getTagOperator() {
        return tagOperator;
    }

    @Nullable
    public Boolean getAscOrder() {
        return ascOrder;
    }

    public void setAscOrder(@Nullable Boolean ascOrder) {
        this.ascOrder = ascOrder;
    }

    @Nullable
    public Integer getLimit() {
        return limit;
    }

    public void setLimit(@Nullable Integer limit) {
        this.limit = limit;
    }

    @NotNull
    public Map<String, String> getMeta() {
        return meta;
    }

    @Nullable
    public List<String> getIdList() {
        return idList;
    }

    public void setIdList(@Nullable List<String> idList) {
        this.idList = idList;
    }
}
