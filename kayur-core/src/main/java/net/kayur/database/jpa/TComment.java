package net.kayur.database.jpa;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "t_comments", schema = "kayur")
public class TComment {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(columnDefinition = "clob")
    @Lob
    private String comment;

    @ManyToOne
    @JoinColumn(name = "issue_id")
    private TIssue issue;

    public TComment() {}

    public TComment(String comment) {
        this.comment = comment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TComment comment1 = (TComment) o;
        return Objects.equals(comment, comment1.comment);
    }

    @Override
    public int hashCode() {
        return Objects.hash(comment);
    }

    @Override
    public String toString() {
        return comment;
    }


    // trivial getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public TIssue getIssue() {
        return issue;
    }

    public void setIssue(TIssue issue) {
        this.issue = issue;
    }
}
