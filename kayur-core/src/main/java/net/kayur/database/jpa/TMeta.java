package net.kayur.database.jpa;


import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "t_meta", schema = "kayur")
public class TMeta {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(nullable = false)
    private String value;

    @ManyToOne
    @JoinColumn(name = "issue_id")
    private TIssue issue;

    @ManyToOne
    private TMetaField field;


    public TMeta() {}

    public TMeta(@NotNull TMetaField field, @NotNull String value) {
        this.field = field;
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TMeta tMeta = (TMeta) o;
        return Objects.equals(value, tMeta.value) &&
                Objects.equals(field.getId(), tMeta.field.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, field.getId());
    }

    @Override
    public String toString() {
        return field.getName() + " -> " + value;
    }


    // trivial getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public TIssue getIssue() {
        return issue;
    }

    public void setIssue(TIssue issue) {
        this.issue = issue;
    }

    public TMetaField getField() {
        return field;
    }

    public void setField(TMetaField field) {
        this.field = field;
    }
}
