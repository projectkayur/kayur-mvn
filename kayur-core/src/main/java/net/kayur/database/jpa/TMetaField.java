package net.kayur.database.jpa;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "t_meta_fields", schema = "kayur")
@NamedQueries({
        @NamedQuery(name = "TMetaField.find",
                query = "SELECT x FROM TMetaField x WHERE x.name LIKE :field"),
        @NamedQuery(name="TMetaField.exists",
                query = "SELECT 1 FROM TMetaField x WHERE x.name LIKE :field"),
        @NamedQuery(name = "TMetaField.all",
                query = "SELECT x FROM TMetaField x")
})
public class TMetaField {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "field_name", unique = true, nullable = false)
    private String name;


    public TMetaField() {}

    public TMetaField(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TMetaField that = (TMetaField) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }


    // trivial getters and setters

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
