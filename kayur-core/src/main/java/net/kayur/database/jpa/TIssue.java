package net.kayur.database.jpa;

import net.kayur.web.Document;
import net.kayur.web.DocumentId;
import net.kayur.web.Metadata;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;
import java.util.stream.Collectors;


/**
 * The Uniform Document Format structure that represents a generic document.
 * It contains the following data:
 * - document title
 * - content
 * - comments
 * - date
 * - metadata
 */
@Entity
@Table(name = "t_issues", schema = "kayur")
@NamedQueries({
        @NamedQuery(name = "TIssue.find",
                query = "SELECT x FROM TIssue x WHERE x.moduleId = :moduleId AND x.documentId = :documentId"),
        @NamedQuery(name = "TIssue.delete",
                query = "DELETE FROM TIssue x WHERE x.moduleId = :moduleId AND x.documentId = :documentId"),
        @NamedQuery(name = "TIssue.count",
                query = "SELECT COUNT (x.documentId) FROM TIssue x WHERE x.moduleId = :moduleId")
})
public class TIssue {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "document_id", nullable = false)
    private String documentId;

    @Column(name = "module_id", nullable = false)
    private long moduleId;

    private Integer tag;

    private Timestamp timestamp;

    private String title;

    @Column(columnDefinition = "clob")
    @Lob
    private String content;

    @OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TComment> comments = new ArrayList<>();

    @OneToMany(mappedBy = "issue", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<TMeta> meta = new ArrayList<>();


    public TIssue() {
    }

    public TIssue(long moduleId, String documentId) {
        this.moduleId = moduleId;
        this.documentId = documentId;
    }

    public void setComments(List<TComment> comments) {
        this.comments.clear();
        this.comments.addAll(comments);
        this.comments.stream()
                .filter(comment -> comment.getIssue() != this)
                .forEach(comment -> comment.setIssue(this));
    }

    public void setMeta(List<TMeta> meta) {
        this.meta.clear();
        this.meta.addAll(meta);
        this.meta.stream()
                .filter(m -> m.getIssue() != this)
                .forEach(m -> m.setIssue(this));
    }

    public Document toDocument() {
        Document document = new Document(moduleId, documentId);
        document.setTag(tag);

        document.setDateTime(timestamp);

        if (title != null) {
            document.setTitle(title);
        }

        if (content != null) {
            document.setContent(content);
        }

        List<String> commentList = comments.stream()
                .map(TComment::getComment)
                .collect(Collectors.toList());
        document.setComments(commentList);

        List<Metadata> metadataList = this.meta.stream()
                .map(m -> new Metadata(m.getField().getName(), m.getValue()))
                .collect(Collectors.toList());
        document.setMeta(metadataList);

        return document;
    }

    public DocumentId toDocumentId() {
        return new DocumentId(moduleId, documentId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TIssue tIssue = (TIssue) o;
        return moduleId == tIssue.moduleId &&
                Objects.equals(documentId, tIssue.documentId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moduleId, documentId);
    }


    // trivial getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public long getModuleId() {
        return moduleId;
    }

    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getTag() {
        return tag;
    }

    public void setTag(Integer tag) {
        this.tag = tag;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<TComment> getComments() {
        return comments;
    }

    public List<TMeta> getMeta() {
        return meta;
    }
}
