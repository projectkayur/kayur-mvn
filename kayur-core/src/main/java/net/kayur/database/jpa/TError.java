package net.kayur.database.jpa;

import net.kayur.web.DocumentId;

import javax.persistence.*;
import java.util.Objects;


@Entity
@Table(name = "t_errors", schema = "kayur")
@NamedQueries({
        @NamedQuery(name = "TError.get",
                query = "SELECT x FROM TError x WHERE x.moduleId = :moduleId AND x.documentId = :documentId"),
        @NamedQuery(name = "TError.delete",
                query = "DELETE FROM TError x WHERE x.moduleId = :moduleId AND x.documentId = :documentId")
})
public class TError {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "module_id", nullable = false)
    private long moduleId;

    @Column(name = "document_id", nullable = false)
    private String documentId;

    @Column(nullable = false)
    private int type;


    public TError() {
    }

    public TError(DocumentId id) {
        this.moduleId = id.module;
        this.documentId = id.name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TError tError = (TError) o;
        return Objects.equals(moduleId, tError.moduleId) &&
                Objects.equals(documentId, tError.documentId) &&
                Objects.equals(type, tError.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(moduleId, documentId, type);
    }


    // trivial getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public long getModuleId() {
        return moduleId;
    }

    public void setModuleId(long moduleId) {
        this.moduleId = moduleId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
