package net.kayur.database;

public class TruncationSettings {
    private int titleLen;
    private int contentLen;
    private int commentLen;
    private int metaLen;

    public boolean isEnabled() {
        return (titleLen > 0) || (contentLen > 0) || (commentLen > 0) || (metaLen > 0);
    }

    public void setFrom(TruncationSettings other) {
        this.titleLen = other.titleLen;
        this.contentLen = other.contentLen;
        this.commentLen = other.commentLen;
        this.metaLen = other.metaLen;
    }

    public int getTitleLen() {
        return titleLen;
    }

    public void setTitleLen(int titleLen) {
        if (titleLen < 0) {
            titleLen = 0;
        }
        this.titleLen = titleLen;
    }

    public int getContentLen() {
        return contentLen;
    }

    public void setContentLen(int contentLen) {
        if (contentLen < 0) {
            contentLen = 0;
        }
        this.contentLen = contentLen;
    }

    public int getCommentLen() {
        return commentLen;
    }

    public void setCommentLen(int commentLen) {
        if (commentLen < 0) {
            commentLen = 0;
        }
        this.commentLen = commentLen;
    }

    public int getMetaLen() {
        return metaLen;
    }

    public void setMetaLen(int metaLen) {
        if (metaLen < 0) {
            metaLen = 0;
        }
        this.metaLen = metaLen;
    }
}
