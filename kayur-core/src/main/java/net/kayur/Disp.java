package net.kayur;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.*;

/**
 * Handles logging and output messages.
 */
public class Disp {
    private static final String LOGGER_NAME = "net/kayur";
    private static final Level DEFAULT_LOG_LEVEL = Level.FINE;

    private static final Logger logger;

    static {
        logger = Logger.getLogger(LOGGER_NAME);
        logger.setUseParentHandlers(false);  // disable console output
        setLevel(DEFAULT_LOG_LEVEL);

        debug("Logger initialized successfully.");
    }

    public static void appendFileHandler(String filename) throws IOException {
        appendHandler(new FileHandler(filename, true)); // append = true
        debug(String.format("Enabled logging to file '%s'", filename));
    }

    public static void setLevel(Level level) {
        logger.setLevel(level);
        for (Handler handler : logger.getHandlers())
            handler.setLevel(level);
    }

    public static void appendHandler(Handler handler) {
        handler.setFormatter(new CustomFormatter());
        handler.setLevel(DEFAULT_LOG_LEVEL);
        logger.addHandler(handler);

        debug("Additional log handler is initialized.");
    }

    public static void closeLogger() {
        for (Handler handler : logger.getHandlers())
            handler.close();

        debug("Logger shutdown.");
    }

    private final static class CustomFormatter extends Formatter {
        private final SimpleDateFormat mDateFormat = new SimpleDateFormat("yy/MM/dd HH:mm:ss");

        public synchronized String format(LogRecord record) {
            return String.format(
                    "[%s] %s: %s\n",  // level: message [date]
                    mDateFormat.format(new Date(record.getMillis())),
                    record.getLevel().getName(),
                    formatMessage(record)
            );
        }
    }


    // Not logged
    public static void message(String txt) {
        System.out.println(txt);
        System.out.flush();
    }

    public static void newLine() {
        message("");
    }

    public static void message(String txt, Object... args) {
        message(String.format(txt, args));
    }

    public static void msgProgress(long current, long max) {
        if (current == 0)
            System.out.printf("Progress: [%d/%d] ", current, max);
        else
            System.out.printf("\rProgress: [%d/%d] ", current, max);

        if (current == max)
            message("");
    }


    // Level.FINE
    public static void debug(String s) {
        logger.log(Level.FINE, s);
        message("DEBUG: %s", s);
    }

    public static void debug(String s, Object... args) {
        debug(String.format(s, args));
    }


    // Level.INFO
    public static void info(String s) {
        logger.log(Level.INFO, s);
        message("INFO: %s", s);
    }

    public static void info(String s, Object... args) {
        info(String.format(s, args));
    }


    // Level.WARNING
    public static void warning(@Nullable Exception e, String msg) {
        if (e != null)
            msg = formatCause(e, msg);
        warning(msg);
    }

    public static void warning(String msg) {
        logger.log(Level.WARNING, msg);
        message("WARNING: %s", msg);
    }


    // Level.SEVERE
    public static void error(Throwable e, String msg) {
        if (e != null) {
            error(formatCause(e, msg));
            //e.printStackTrace();
        } else {
            error(msg);
        }
    }

    public static void error(String msg) {
        logger.log(Level.SEVERE, msg);
        message("ERROR: %s", msg);
    }


    private static String formatCause(@NotNull Throwable e, String description) {
        Throwable cause = findCause(e);

        String details;
        String causeMsg = cause.getMessage();

        if (causeMsg != null && !causeMsg.isEmpty())
            details = causeMsg;
        else
            details = "not available.";

        String errorClass = cause.getClass().getName();

        return String.format("%s\n  Reason: %s.\n  Details: %s", description, errorClass, details);
    }

    public static @NotNull Throwable findCause(@NotNull Throwable e) {
        final int MAX_EXCEPTION_MSG_DEPTH = 8;

        int depth = 0;
        Throwable cause = e;

        while ((++depth <= MAX_EXCEPTION_MSG_DEPTH) && cause.getCause() != null) {
            cause = cause.getCause();
        }

        return cause;
    }
}
