package net.kayur;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Formats localized messages from a resource file.
 */
public class Msg {
    private static final String LANGUAGE = "en";
    private static final String COUNTRY = "US";
    public static final Locale currentLocale = new Locale(LANGUAGE, COUNTRY);

    private static final ResourceBundle messages = ResourceBundle.getBundle("i18n.strings", currentLocale);

    public static String msg(String key, Object... args) {
        if (args.length == 0) {
            return messages.getString(key);
        } else {
            var fmt = new MessageFormat(messages.getString(key), currentLocale);
            return fmt.format(args);
        }
    }
}
