package net.kayur.nlp.export;


import net.kayur.helper.FileHelper;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.formats.ExportFormat;
import net.kayur.nlp.weights.*;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static net.kayur.Msg.msg;

public final class ExportController {
    public static final int DEFAULT_HAPAX = 2;

    public static final String FORMAT_ARFF = "Weka ARFF";
    public static final String FORMAT_CARROT_XML = "Carrot XML";
    public static final String FORMAT_HDP = "LDA/HDP input file";
    public static final String FORMAT_JSON = "JSON";
    public static final String FORMAT_CSV = "CSV";

    private static final String WORDLIST_FILE_SUFFIX = "_words.txt";

    public static final List<String> supportedFormats = Arrays.asList(
            FORMAT_ARFF, FORMAT_CARROT_XML, FORMAT_HDP, FORMAT_JSON, FORMAT_CSV);

    public static final TermWeight TERM_WEIGHT_BOOLEAN = new TermWeightBoolean();
    public static final TermWeight TERM_WEIGHT_FREQUENCY = new TermWeightFrequency();
    public static final TermWeight TERM_WEIGHT_TF = new TermWeightTF();
    public static final TermWeight TERM_WEIGHT_TFIDF = new TermWeightTFIDF();

    public static final List<TermWeight> supportedWeights = Arrays.asList(
            TERM_WEIGHT_BOOLEAN, TERM_WEIGHT_FREQUENCY, TERM_WEIGHT_TF, TERM_WEIGHT_TFIDF);

    static {
        Collections.sort(supportedFormats);
        Collections.sort(supportedWeights);
    }

    /**
     * Exports a corpus (a collection of processed texts) into a format
     * supported by data mining workbenches.
     *
     * @param corpus corpus structure
     * @param params export parameters, such as format name and term weights
     * @return ExportStatistics statistics
     */
    public ExportStatistics export(@NotNull Corpus corpus, @NotNull ExportManagerParameters params) {
        // update word weights for each document in the corpus (mutates the corpus)
        corpus.calculateWeights(params.getTermWeight());
        corpus.setHapax(params.getHapax());

        // convert to the output format
        ExportFormat.ExportResult result = params.getFormat().export(corpus);

        String filename = FileHelper.appendExtension(params.getFilename(), params.getFormat().extension);
        try {
            FileHelper.write(filename, result.output);
        } catch (IOException e) {
            throw new ExportException(msg("export_write_failure", filename), e);
        }

        ExportStatistics stats = result.statistics;
        stats.append("Output file", filename);

        if (params.isSaveWordList()) {
            List<String> words = corpus.getWords(params.isSortedWordList());

            String outputDir = FileHelper.getFullPath(params.getFilename());
            String dictionaryName = FileHelper.getBaseName(params.getFilename()) + WORDLIST_FILE_SUFFIX;
            String dictionaryPath = FileHelper.concatPath(outputDir, dictionaryName);
            try {
                FileHelper.write(dictionaryPath, words);
            } catch (IOException e) {
                throw new ExportException(msg("export_dictionary_write_failure", dictionaryPath), e);
            }

            stats.append("Word list: ", dictionaryPath);
        }

        return stats;
    }
}
