package net.kayur.nlp.export;


import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Contains various data describing the export process.
 */
public class ExportStatistics {
    private final Map<String, String> values;

    public static final ExportStatistics EMPTY = new ExportStatistics(new LinkedHashMap<>());

    public ExportStatistics(Map<String, String> values) {
        this.values = values;
    }

    public void append(String key, String value) {
        Map<String, String> map = new LinkedHashMap<>();
        map.putAll(values);

        values.clear();
        values.put(key, value);
        values.putAll(map);
    }

    @Override
    public String toString() {
        return values.entrySet().stream()
                .map(entry -> entry.getKey() + ": " + entry.getValue())
                .collect(Collectors.joining("\n"));
    }
}
