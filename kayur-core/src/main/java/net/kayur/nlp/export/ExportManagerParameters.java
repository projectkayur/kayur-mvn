package net.kayur.nlp.export;

import net.kayur.nlp.formats.ExportFormat;
import net.kayur.nlp.weights.TermWeight;
import org.jetbrains.annotations.NotNull;


public class ExportManagerParameters {
    private String filename;
    private TermWeight termWeight;
    private ExportFormat format;

    private int hapax;
    private boolean isSaveWordList;
    private boolean isSortedWordList;


    private ExportManagerParameters() {}

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private ExportManagerParameters parameters;

        private Builder() {
            this.parameters = new ExportManagerParameters();
        }

        public Builder filename(@NotNull String filename) {
            parameters.filename = filename;
            return this;
        }

        public Builder termWeight(TermWeight termWeight) {
            parameters.termWeight = termWeight;
            return this;
        }

        public Builder format(ExportFormat format) {
            parameters.format = format;
            return this;
        }

        public Builder hapax(int hapax) {
            parameters.hapax = hapax;
            return this;
        }

        public Builder isSaveWordList(boolean isSaveWordList) {
            parameters.isSaveWordList = isSaveWordList;
            return this;
        }

        public Builder isSortedWordList(boolean isSortedWordList) {
            parameters.isSortedWordList = isSortedWordList;
            return this;
        }

        public ExportManagerParameters build() {
            return parameters;
        }
    }

    public String getFilename() {
        return filename;
    }

    public TermWeight getTermWeight() {
        return termWeight;
    }

    public ExportFormat getFormat() {
        return format;
    }

    public int getHapax() {
        return hapax;
    }

    public boolean isSaveWordList() {
        return isSaveWordList;
    }

    public boolean isSortedWordList() {
        return isSortedWordList;
    }
}
