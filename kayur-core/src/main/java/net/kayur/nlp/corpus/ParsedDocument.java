package net.kayur.nlp.corpus;

import java.util.List;


/**
 * Represents a document using the "bag of words" approach.
 */
public class ParsedDocument {
    public final String id;
    public final int category;
    public final List<IndexedWord> words;

    public ParsedDocument(String id, int category, List<IndexedWord> words) {
        this.id = id;
        this.category = category;
        this.words = words;
    }


    public boolean isEmpty() {
        return words.isEmpty();
    }


    public int numWords() {
        return words.size();
    }
}
