package net.kayur.nlp.corpus;


public class TextProcessingParameters {
    private boolean processTitles;
    private boolean processContent;
    private boolean processComments;
    private boolean enableCache;

    private TextProcessingParameters() {}

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private TextProcessingParameters parameters = new TextProcessingParameters();

        public Builder processTitles(boolean processTitles) {
            parameters.processTitles = processTitles;
            return this;
        }

        public Builder processContent(boolean processContent) {
            parameters.processContent = processContent;
            return this;
        }

        public Builder processComments(boolean processComments) {
            parameters.processComments = processComments;
            return this;
        }

        public Builder enableCache(boolean enableCache) {
            parameters.enableCache = enableCache;
            return this;
        }

        public TextProcessingParameters build() {
            return parameters;
        }
    }

    public boolean isProcessTitles() {
        return processTitles;
    }

    public boolean isProcessContent() {
        return processContent;
    }

    public boolean isProcessComments() {
        return processComments;
    }

    public boolean isEnableCache() {
        return enableCache;
    }
}
