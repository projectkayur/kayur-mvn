package net.kayur.nlp.corpus;

import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.api.TextEngineModule;
import net.kayur.nlp.plugins.api.Tokenizer;
import net.kayur.nlp.plugins.dummy.DummySentenceDetector;
import net.kayur.nlp.plugins.dummy.DummyStemmer;
import net.kayur.nlp.plugins.dummy.DummyTokenizer;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.ServiceLoader;
import java.util.function.Supplier;

public class TextEngineModuleLocator {

    public List<SentenceDetector> getSentenceDetectors() {
        return fromIterable(ServiceLoader.load(SentenceDetector.class));
    }

    public SentenceDetector getDefaultSentenceDetector() {
        var sentenceDetectors = getSentenceDetectors();
        return sentenceDetectors.isEmpty() ? new DummySentenceDetector() : sentenceDetectors.get(0);
    }

    public SentenceDetector findSentenceDetectorByName(String name) {
        return findTextEngineModuleByName(getSentenceDetectors(), name, DummySentenceDetector::new);
    }

    public List<Tokenizer> getTokenizers() {
        return fromIterable(ServiceLoader.load(Tokenizer.class));
    }

    public Tokenizer findTokenizerByName(String name) {
        return findTextEngineModuleByName(getTokenizers(), name, DummyTokenizer::new);
    }

    public Tokenizer getDefaultTokenizer() {
        var tokenizers = getTokenizers();
        return tokenizers.isEmpty() ? new DummyTokenizer() : tokenizers.get(0);
    }

    public List<Stemmer> getStemmers() {
        return fromIterable(ServiceLoader.load(Stemmer.class));
    }

    public Stemmer findStemmerByName(String name) {
        return findTextEngineModuleByName(getStemmers(), name, DummyStemmer::new);
    }

    public Stemmer getDefaultStemmer() {
        var stemmers = getStemmers();
        return stemmers.isEmpty() ? new DummyStemmer() : stemmers.get(0);
    }

    private <T extends TextEngineModule> List<T> fromIterable(Iterable<T> iterable) {
        List<T> result = new ArrayList<>();
        iterable.forEach(result::add);
        result.sort(Comparator.comparing(TextEngineModule::getName));
        return result;
    }

    private <T extends TextEngineModule> T findTextEngineModuleByName(List<T> modules, String name, Supplier<T> defaultValue) {
        for (T module : modules) {
            if (module.getName().equalsIgnoreCase(name)) {
                return module;
            }
        }
        return defaultValue.get();
    }
}
