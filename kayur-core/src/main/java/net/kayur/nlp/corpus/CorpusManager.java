package net.kayur.nlp.corpus;

import net.kayur.core.configuration.ConfigurationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.UUID;

import static net.kayur.Msg.msg;

public class CorpusManager {
    @Nullable
    private Corpus corpus;
    @Nullable
    private UUID id;

    @NotNull
    public Corpus getCorpus() throws ConfigurationException {
        if (corpus == null) {
            throw new ConfigurationException(msg("corpus_not_ready"));
        }
        return corpus;
    }

    public void setCorpus(@Nullable Corpus corpus) {
        this.corpus = corpus;
        this.id = UUID.randomUUID();
    }

    public boolean isEmpty() {
        return corpus == null;
    }

    @Nullable
    public UUID getId() {
        return id;
    }
}

