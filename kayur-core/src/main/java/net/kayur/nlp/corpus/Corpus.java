package net.kayur.nlp.corpus;

import net.kayur.nlp.weights.TermWeight;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Represents collection of documents with global word index.
 */
public class Corpus {
    public final List<ParsedDocument> documents;
    public final Map<Integer, String> indexToWord;
    public final Map<Integer, Integer> indexToDocumentFrequency;

    private int hapax = 1;

    public Corpus(List<ParsedDocument> documents, Map<Integer, String> indexToWord,
                  Map<Integer, Integer> indexToDocumentFrequency) {
        this.documents = documents;
        this.indexToWord = indexToWord;
        this.indexToDocumentFrequency = indexToDocumentFrequency;
    }

    public void setHapax(int hapax) {
        this.hapax = hapax;
    }


    // Calculates weights of words for each document in the corpus, according to the given term weight model.
    public void calculateWeights(TermWeight model) {
        model.setUp(this);
        documents.forEach(model::calculate);
    }


    // Returns a list of indexed words from a document that belong to this corpus.
    // Words with document frequency less than 'hapax' are ignored.
    public List<IndexedWord> getIndexedWords(ParsedDocument document) {
        return document.words.stream()
                .filter(word -> isIncludeWord(word.index))
                .collect(Collectors.toList());
    }


    // Returns a list of unique words in this corpus.
    // Words with document frequency less than 'hapax' are ignored.
    public List<CorpusKeyword> getCorpusKeywords() {
        return indexToDocumentFrequency.entrySet().stream()
                .filter(entry -> isIncludeWord(entry.getKey()))
                .map(entry -> {
                    int index = entry.getKey();
                    int df = entry.getValue();
                    return new CorpusKeyword(index, indexToWord.get(index), df);
                })
                .collect(Collectors.toList());
    }


    // Returns the complete list of unique words in this corpus.
    public List<CorpusKeyword> getAllCorpusKeywords() {
        return getAllCorpusKeywords(indexToDocumentFrequency.size());
    }


    // Returns the list of top N unique words in this corpus with the highest document frequency.
    public List<CorpusKeyword> getAllCorpusKeywords(int num) {
        if (num < 0) {
            throw new IllegalArgumentException("Number of keywords should be a non-negative integer.");
        }

        if (num == 0) {
            return new ArrayList<>();
        }

        return indexToDocumentFrequency.entrySet().stream()
                .map(entry -> {
                    int index = entry.getKey();
                    int df = entry.getValue();
                    return new CorpusKeyword(index, indexToWord.get(index), df);
                })
                .sorted((doc1, doc2) -> Integer.compare(doc2.documentFrequency, doc1.documentFrequency))
                .limit(num)
                .collect(Collectors.toList());
    }


    // Returns the list of categories of documents that appear in the corpus.
    public List<Integer> getDocumentCategories() {
        return documents.stream()
                .map(doc -> doc.category)
                .distinct()
                .collect(Collectors.toList());
    }


    // Returns the number of documents in the corpus (including empty documents).
    public int numDocuments() {
        return documents.size();
    }


    // Returns the number of words in the corpus.
    // Words with document frequency less than 'hapax' are not counted.
    public long numWords() {
        return indexToWord.keySet().stream()
                .filter(this::isIncludeWord)
                .count();
    }


    // Returns the average length (in number of words) of documents included in this corpus.
    public double averageDocumentLength() {
        int numDocuments = numDocuments();
        if (numDocuments == 0) {
            return 0.0;
        }

        long numWordsTotal = documents.stream()
                .mapToLong(ParsedDocument::numWords)
                .sum();

        return numWordsTotal / (double) numDocuments;
    }


    // Decides if a word with given index has the document frequency above the specified threshold.
    private boolean isIncludeWord(int index) {
        return indexToDocumentFrequency.get(index) >= hapax;
    }


    // Returns the list of unique words of this corpus.
    // Words with document frequency less than 'hapax' are ignored.
    private List<String> getWords() {
        return indexToWord.keySet().stream()
                .filter(this::isIncludeWord)
                .map(indexToWord::get)
                .collect(Collectors.toList());
    }


    // Returns the list of unique words of this corpus.
    public List<String> getWords(boolean sortAlphabetically) {
        List<String> words = getWords();

        if (sortAlphabetically) {
            Collections.sort(words);
        }

        return words;
    }
}


