package net.kayur.nlp.corpus;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.helper.FileHelper;
import net.kayur.helper.XmlUtils;
import net.kayur.nlp.FilterExpr;
import net.kayur.web.Document;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static net.kayur.Msg.msg;

/*
 * Text processing controller.
 */
public class TextEngineController {
    private final MainConfiguration config;

    private final TextEngine textEngine;
    private final CorpusManager corpusManager;

    private int lastConfigHashCode = 0;

    public TextEngineController(MainConfiguration config,
                                TextEngine textEngine,
                                CorpusManager corpusManager) {
        this.config = config;
        this.textEngine = textEngine;
        this.corpusManager = corpusManager;
    }


    public void buildCorpus(Collection<Document> documents, TextProcessingParameters params) {

        configureTextEngine();

        Disp.info("Starting text processing for %d documents.", documents.size());
        if (!params.isEnableCache()) {
            Disp.info(msg("tp_cache_disabled"));
        }

        long timeStart = System.currentTimeMillis();

        Corpus corpus = textEngine.buildCorpus(documents, params);

        long timeEnd = System.currentTimeMillis();
        long timeElapsedMillis = timeEnd - timeStart;
        long timeElapsedSec = timeElapsedMillis / 1000;

        Disp.info("Processed %d documents in %d sec (%d ms).",
                documents.size(), timeElapsedSec, timeElapsedMillis);

        corpusManager.setCorpus(corpus);
    }

    private void configureTextEngine() {

        if (config.hashCode() == lastConfigHashCode) {
            return;
        }

        configureNLPComponents(config);
        configureStopWords(config);
        configureFilters(config);

        lastConfigHashCode = config.hashCode();
    }

    private void configureNLPComponents(MainConfiguration config) {
        textEngine.setTokenizer(config.nlp.getTokenizer());

        if (config.nlp.isEnableSentenceDetector()) {
            textEngine.setSentenceDetector(config.nlp.getSentenceDetector());
        }

        if (config.nlp.isEnableStemmer()) {
            textEngine.setStemmer(config.nlp.getStemmer());
        }
    }

    public void configureFilters(MainConfiguration config) {
        List<FilterExpr> filters;

        if (config.nlp.isEnableFilters()) {
            String path = config.nlp.getFilterList();
            if (path.isEmpty())
                throw new ConfigurationException("Filters are enabled, but the path to filter settings is not set.");
            filters = loadFilters(path);
        } else {
            filters = new ArrayList<>();
        }

        textEngine.setFilters(filters);
    }

    private List<FilterExpr> loadFilters(String filename) {
        List<FilterExpr> filters;

        try {
            String xml = FileHelper.read(filename);
            filters = XmlUtils.traverse(xml,
                    a -> new FilterExpr(Integer.parseInt(a[0]), a[1], a[2]),
                    "level", "input", "output");
        } catch (IOException | SAXException | ParserConfigurationException | ParseException e) {
            throw new ConfigurationException(msg("tp_filters_load_failure"), e);
        }

        Disp.info(msg("tp_filters_load_success", filters.size()));
        return filters;
    }

    private void configureStopWords(MainConfiguration config) {
        List<String> stopWords;

        if (config.nlp.isEnableStopWords()) {
            String filename = config.nlp.getStopWordsList();
            stopWords = loadStopWords(filename);
            Disp.info(msg("tp_stop_words_load_success", stopWords.size()));
        } else {
            stopWords = new ArrayList<>();
        }

        textEngine.setStopWords(stopWords);
    }

    private List<String> loadStopWords(String path) {
        if (path.isEmpty()) {
            throw new ConfigurationException(msg("tp_stop_words_filename_not_set"));
        }

        try {
            return FileHelper.readLines(path);
        } catch (IOException e) {
            throw new ConfigurationException(msg("tp_stop_words_load_error"), e);
        }
    }

    public List<FilterExpr> getLoadedFilters() {
        return textEngine.getFilters();
    }

    public String filterText(String text, int layer) {
        return textEngine.filterText(text, layer);
    }

    public void containsKeyword(String text, String[] keywords, boolean[] results) {
        textEngine.containsKeyword(text, keywords, results);
    }
}
