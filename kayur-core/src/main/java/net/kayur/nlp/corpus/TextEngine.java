package net.kayur.nlp.corpus;

import net.kayur.Disp;
import net.kayur.helper.Util;
import net.kayur.nlp.FilterExpr;
import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.api.Tokenizer;
import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Class to convert raw text (title or contents of a bug report)
 * into numeric vectors.
 */
public class TextEngine {

    // TODO: import corpus from JSON, CSV

    // NLP classes

    @Nullable
    private SentenceDetector sentenceDetector;
    @Nullable
    private Tokenizer tokenizer;
    @Nullable
    private Stemmer stemmer;

    private final List<String> stopWords = new ArrayList<>();  // invariant: always sorted
    private final List<FilterExpr> filters = new ArrayList<>();

    @NotNull
    private Map<String, String> cache = new HashMap<>();
    @NotNull
    private final String[] sentenceHolder = new String[1];

    @NotNull
    public List<FilterExpr> getFilters() {
        return filters;
    }

    @NotNull
    public Corpus buildCorpus(Collection<Document> textCollection, @NotNull TextProcessingParameters params) {
        Map<String, Integer> wordIndexMap = new LinkedHashMap<>();      // (word -> index)
        Map<Integer, String> indexWordMap = new LinkedHashMap<>();      // (word -> index)
        Map<Integer, Integer> indexDocfreqMap = new LinkedHashMap<>();  // (index -> DF)

        int globalIndex = 0;
        List<ParsedDocument> parsedDocuments = new ArrayList<>();

        for (Document document : textCollection) {
            // get the list of unique processed words of the current document and their raw frequencies
            Map<String, Integer> wordFrequencyMap = processIssue(document, params);

            int numUniqueWords = wordFrequencyMap.size();
            List<IndexedWord> words = new ArrayList<>(numUniqueWords);

            for (Map.Entry<String, Integer> entry : wordFrequencyMap.entrySet()) {
                String word = entry.getKey();
                int frequency = entry.getValue();

                int index;

                if (wordIndexMap.containsKey(word)) {
                    // we already have met this word in another document => increment its DF
                    index = wordIndexMap.get(word);
                    indexDocfreqMap.put(index, indexDocfreqMap.get(index) + 1);
                } else {
                    // we meet this word for the first time
                    index = globalIndex++;

                    wordIndexMap.put(word, index);
                    indexWordMap.put(index, word);
                    indexDocfreqMap.put(index, 1);  // set DF to one
                }

                words.add(new IndexedWord(index, frequency));
            }

            String documentId = document.id.name;
            int tag = (document.getTag() != null) ? document.getTag() : 0;

            parsedDocuments.add(new ParsedDocument(documentId, tag, words));
        }

        Disp.debug("The corpus contains %d distinct words.", globalIndex);

        return new Corpus(parsedDocuments, indexWordMap, indexDocfreqMap);
    }

    @NotNull
    private Map<String, Integer> processIssue(@NotNull Document document, TextProcessingParameters params) {
        String text = document.getText(params.isProcessTitles(), params.isProcessContent(), params.isProcessComments());
        return getBagOfWords(text, params.isEnableCache());
    }

    @NotNull
    private Map<String, Integer> getBagOfWords(@NotNull String rawText, boolean enableCache) {
        final boolean enableStopWords = !stopWords.isEmpty();
        Map<String, Integer> tokenFrequencyMap = new LinkedHashMap<>();

        if (rawText.isEmpty())
            return tokenFrequencyMap;

        if (tokenizer == null) {
            throw new IllegalArgumentException("Tokenizer is not configured.");
        }

        String[] sentences;
        String[] tokens;
        String rawToken, token;
        boolean keepToken;

        // apply outer filters
        String filteredText = filterText(rawText, 0);

        // detect sentences
        if (sentenceDetector != null) {
            sentences = sentenceDetector.detect(filteredText);
        } else {
            sentenceHolder[0] = filteredText;
            sentences = sentenceHolder;
        }

        // buildCorpus each sentence
        for (String sentence : sentences) {
            // tokenize (required)
            tokens = tokenizer.tokenize(sentence);

            // initialize stemmer (optional)
            if (stemmer != null) {
                stemmer.init(tokens);
            }

            // buildCorpus tokens
            for (int i = 0, n = tokens.length; i < n; i++) {
                rawToken = tokens[i];
                keepToken = true;

                if (enableCache && cache.containsKey(rawToken)) { // use cache
                    token = cache.get(rawToken);
                    keepToken = (token != null);
                } else {
                    // to lower case
                    token = rawToken.toLowerCase();

                    // apply inner filters
                    token = filterText(token, 1);
                    token = token.trim();

                    if (isRemoveWord(token)) {
                        token = "";
                    }

                    // perform stemming
                    if (!token.isEmpty() && stemmer != null) {
                        token = stemmer.process(token, i);
                    }

                    if (token == null || token.isEmpty()) {
                        keepToken = false;
                    }

                    // skip stop words
                    if (keepToken && enableStopWords && Collections.binarySearch(stopWords, token) >= 0) {
                        keepToken = false;
                    }

                    cache.put(rawToken, keepToken ? token : null);
                }

                if (keepToken) {
                    Util.count(tokenFrequencyMap, token);
                }
            }
        }

        return tokenFrequencyMap;
    }

    /**
     * Applies all filters of the given level to the given text.
     *
     * @param rawText text to be processed
     * @param layer   level of the filter
     * @return filtered text
     */
    @NotNull
    public String filterText(@NotNull String rawText, int layer) {
        String text = rawText;

        for (FilterExpr f : filters) {
            if (f.getLayer() == layer) {
                text = f.filter(text);
            }
        }

        return text;
    }

    private boolean isRemoveWord(String word) {
        for (FilterExpr f : filters) {
            if (f.getLayer() == 2) {
                if (f.matches(word)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void containsKeyword(String text, String[] keywords, boolean[] results) {
        int keys_num = keywords.length;

        if (keys_num != results.length)
            throw new IllegalArgumentException("Invalid usage: arrays are not equal.");

        for (int i = 0; i < keys_num; i++) {
            results[i] = false;
        }

        Map<String, Integer> tokens = getBagOfWords(text, false);

        for (int i = 0; i < keys_num; i++) {
            for (Map.Entry<String, Integer> e : tokens.entrySet()) {
                String token = e.getKey();
                if (token.equals(keywords[i])) {
                    results[i] = true;
                    break;
                }
            }
        }
    }

    public void setFilters(@NotNull List<FilterExpr> newFilters) {
        filters.clear();
        filters.addAll(newFilters);
    }

    public void clearCache() {
        cache.clear();
    }

    public void setSentenceDetector(@Nullable SentenceDetector sentenceDetector) {
        this.sentenceDetector = sentenceDetector;
    }

    public void setTokenizer(@Nullable Tokenizer tokenizer) {
        this.tokenizer = tokenizer;
    }

    public void setStemmer(@Nullable Stemmer stemmer) {
        this.stemmer = stemmer;
    }

    public void setStopWords(List<String> words) {
        stopWords.clear();
        stopWords.addAll(words);
        Collections.sort(stopWords);
    }
}
