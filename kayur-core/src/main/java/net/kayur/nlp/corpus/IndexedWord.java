package net.kayur.nlp.corpus;

import org.jetbrains.annotations.NotNull;

/**
 * Represents a word and its raw frequency in a document.
 */
public class IndexedWord implements Comparable<IndexedWord> {
    public final int index;
    public final int frequency;
    private double weight = 1.0;

    public IndexedWord(int index, int frequency) {
        this.index = index;
        this.frequency = frequency;
    }

    @Override
    public int compareTo(@NotNull IndexedWord other) {
        return Integer.compare(index, other.index);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IndexedWord that = (IndexedWord) o;
        return index == that.index;
    }

    @Override
    public int hashCode() {
        return index;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }
}

