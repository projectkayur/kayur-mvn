package net.kayur.nlp.corpus;

/**
 * Represents a unique word in the document corpus.
 */
public class CorpusKeyword {
    public final int index;
    public final String keyword;
    public final int documentFrequency;

    public CorpusKeyword(int index, String keyword, int documentFrequency) {
        this.index = index;
        this.keyword = keyword;
        this.documentFrequency = documentFrequency;
    }

    // for Java FX

    @SuppressWarnings("unused")
    public String getKeyword() {
        return keyword;
    }

    @SuppressWarnings("unused")
    public int getDocumentFrequency() {
        return documentFrequency;
    }
}
