package net.kayur.nlp.formats;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.helper.XmlUtils;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.util.stream.Collectors;

/**
 * Class to support corpus export to Carrot XML format.
 */
public class ExportFormatCarrotXML extends ExportFormat {
    public ExportFormatCarrotXML() {
        super("xml");
    }

    @Override
    public ExportResult export(@NotNull Corpus corpus) throws ConfigurationException {
        org.w3c.dom.Document tree;

        try {
            tree = XmlUtils.newDocument();
        } catch (ParserConfigurationException e) {
            throw new ConfigurationException(e);
        }

        Element rootElement = tree.createElement("searchresult");

        for (ParsedDocument document : corpus.documents) {
            Element docElement = tree.createElement("document");
            docElement.setAttribute("id", document.id);

            Element titleElement = tree.createElement("title");
            Text titleText = tree.createTextNode(document.id); // TODO: not use ID as document title
            titleElement.appendChild(titleText);
            docElement.appendChild(titleElement);

            String documentText = corpus.getIndexedWords(document).stream()
                    .map(word -> corpus.indexToWord.get(word.index))
                    .collect(Collectors.joining(" "));

            Element snippetElement = tree.createElement("snippet");
            snippetElement.appendChild(tree.createTextNode(documentText));
            docElement.appendChild(snippetElement);

            rootElement.appendChild(docElement);
        }

        tree.appendChild(rootElement);

        String xml;
        try {
            xml = XmlUtils.toXML(tree);
        } catch (TransformerException e) {
            throw new ConfigurationException(e);
        }

        return new ExportResult(xml, ExportStatistics.EMPTY);
    }
}
