package net.kayur.nlp.formats;

import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;


public class ExportFormatCSV extends ExportFormat {
    public ExportFormatCSV() {
        super("csv");
    }

    @Override
    public ExportResult export(@NotNull Corpus corpus) {
        StringBuilder sb = new StringBuilder();

        sb.append("document,word,weight");
        sb.append(LINE_SEP);

        for (ParsedDocument document : corpus.documents) {
            for (IndexedWord word : corpus.getIndexedWords(document)) {
                sb.append(document.id).append(",");
                sb.append(word.index).append(",");
                sb.append(word.getWeight()).append(LINE_SEP);
            }
        }

        String output = sb.toString();

        return new ExportResult(output, ExportStatistics.EMPTY);
    }
}
