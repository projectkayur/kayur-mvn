package net.kayur.nlp.formats;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;

/**
 * Export to JSON format.
 */
public class ExportFormatJSON extends ExportFormat {
    public ExportFormatJSON() {
        super("json");
    }

    @Override
    public ExportResult export(@NotNull Corpus corpus) throws ConfigurationException {
        ObjectMapper mapper = new ObjectMapper();
        String output;

        try {
            output = mapper.writeValueAsString(corpus.documents);
        } catch (JsonProcessingException e) {
            Disp.error(e, "Cannot export to JSON format.");
            throw new ConfigurationException(e);
        }

        return new ExportResult(output, ExportStatistics.EMPTY);
    }
}
