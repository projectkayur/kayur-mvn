package net.kayur.nlp.formats;

import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.helper.FileHelper;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusKeyword;
import net.kayur.nlp.corpus.IndexedWord;
import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.util.*;

import static net.kayur.Msg.msg;

/**
 * Class to support corpus export in WEKA sparse ARFF format,
 * as described at http://weka.wikispaces.com/ARFF
 */
public class ExportFormatARFF extends ExportFormat {
    private static final String ATTR_NAME_ID = "_kayur_document_id_";
    private static final String ATTR_NAME_CATEGORY = "_kayur_document_tag_";

    // TODO: user can set float weights precision

    public ExportFormatARFF() {
        super("arff");
    }

    @NotNull
    private String relationName = "default";
    private boolean useNominalAttributes = false;
    private boolean includeIdAttribute = true;
    private boolean includeCategoryAttribute = false;
    private String categoryMappingFile = "";

    private Map<Integer, String> categoryMap = new HashMap<>();


    // statistics
    private static final String STAT_INSTANCES = "Instances";
    private static final String STAT_ATTRIBUTES = "Attributes";
    private static final String STAT_TDMATRIX_NUM_ELEMENTS = "Term-Document Matrix elements";
    private static final String STAT_TDMATRIX_NUM_NONZERO_ELEMENTS = "Term-Document Matrix non-zero elements";
    private static final String STAT_TDMATRIX_DENSITY = "Term-Document Matrix density";
    private static final String STAT_AVG_WORDS_PER_DOC = "Average number of words per document";



    @Override
    public ExportResult export(@NotNull Corpus corpus) {

        if (isIncludeCategoryAttribute()) {
            if (categoryMappingFile.isEmpty()) {
                throw new ConfigurationException("Category mapping file is not set.");
            }

            categoryMap = loadCategoryMapping(categoryMappingFile, corpus);
        }

        StringBuilder sb = new StringBuilder();

        // write relation name (@relation name)
        sb.append(String.format("@relation %s", checkName(relationName)));
        sb.append(LINE_SEP);
        sb.append(LINE_SEP);

        // write attribute list (@attribute name type)
        final String attrFormat = useNominalAttributes ? "{ 0, 1 }" : "numeric";

        Map<Integer, Integer> indexToAttributeIdMap = new HashMap<>();
        int pos = 0;

        for (CorpusKeyword keyword : corpus.getCorpusKeywords()) {
            indexToAttributeIdMap.put(keyword.index, pos++);

            sb.append(String.format("@attribute %s %s", checkName(keyword.keyword), attrFormat));
            sb.append(LINE_SEP);
        }

        int idAttrIndex = -1, categoryAttrIndex = -1;
        int nextAvailableAttrIndex = pos;

        if (includeIdAttribute) {
            idAttrIndex = nextAvailableAttrIndex++;
            sb.append(String.format("@attribute %s %s", ATTR_NAME_ID, "string"));
            sb.append(LINE_SEP);
        }

        if (includeCategoryAttribute) {
            categoryAttrIndex = nextAvailableAttrIndex++;
            sb.append(String.format("@attribute %s { %s }", ATTR_NAME_CATEGORY,
                    String.join(", ", categoryMap.values())));
            sb.append(LINE_SEP);
        }

        // write instances
        sb.append(LINE_SEP);
        sb.append("@data");
        sb.append(LINE_SEP);

        int numInstances = 0;
        int numWords = 0;

        for (ParsedDocument document : corpus.documents) {
            if (document.isEmpty()) {
                continue; // skip blank documents
            }

            sb.append("{");

            List<IndexedWord> words = corpus.getIndexedWords(document);

            // append attribute (word) / weight pairs
            // attribute identifiers must be ordered
            words.stream()
                    .sorted(Comparator.comparingInt(word -> indexToAttributeIdMap.get(word.index)))
                    .forEach(word -> {
                        int attributeId = indexToAttributeIdMap.get(word.index);
                        sb.append(String.format("%d %.5f, ", attributeId, word.getWeight()));
                    });

            // append document identifier
            if (includeIdAttribute)
                sb.append(String.format("%d \"%s\", ", idAttrIndex, document.id));

            // append document category
            if (includeCategoryAttribute) {
                sb.append(String.format("%d \"%s\", ", categoryAttrIndex,
                        categoryMap.get(document.category)));
            }

            sb.replace(sb.length() - 2, sb.length(), "}");
            sb.append(LINE_SEP);

            numInstances++;
            numWords += words.size();
        }

        String output = sb.toString();

        // statistics

        int numAttributes = nextAvailableAttrIndex - 1;
        int numElements = numInstances * numAttributes;

        double tdMatrixDensity = (numElements == 0) ? 0.0 :
                100 * (double) numWords / (double) numElements;

        double avgWordsInDocument = (numInstances == 0) ? 0.0 :
                (double) numWords / (double) numInstances;

        Map<String, String> stats = new LinkedHashMap<>();
        stats.put(STAT_INSTANCES, String.valueOf(numInstances));
        stats.put(STAT_ATTRIBUTES, String.valueOf(numAttributes));
        stats.put(STAT_TDMATRIX_NUM_ELEMENTS, String.valueOf(numElements));
        stats.put(STAT_TDMATRIX_NUM_NONZERO_ELEMENTS, String.valueOf(numWords));
        stats.put(STAT_TDMATRIX_DENSITY, String.format("%.2f %%", tdMatrixDensity));
        stats.put(STAT_AVG_WORDS_PER_DOC, String.format("%.2f", avgWordsInDocument));

        return new ExportResult(output, new ExportStatistics(stats));
    }


    /**
     * Replaces forbidden characters of an attribute name
     * in WEKA ARFF format.
     *
     * @param name attribute name
     * @return mangled attribute name
     */
    @NotNull
    private String checkName(@NotNull String name) {
        String result = name;
        boolean altered = false;

        // attribute name must start with a letter
        if (result.matches("(\\{|\\}|,|%|').*")) {
            result = "_" + result;
            altered = true;
        }

        // if a name contains whitespaces, the name must be quoted
        if (result.contains("\\s"))
            result = quote(result); // not a big deal to issue a warning

        if (altered)
            Disp.warning(msg("export_arff_mangling_name", result));

        return result;
    }

    private String quote(String name) {
        return String.format("\"%s\"", name);
    }


    // TODO: change format to XML or move mapping to database
    // load mapping between numeric document categories and their names
    @NotNull
    private Map<Integer, String> loadCategoryMapping(@NotNull String filename, Corpus corpus) {
        Map<Integer, String> categoryMap = new HashMap<>();

        List<String> lines;
        try {
            lines = FileHelper.readLines(filename);
        } catch (IOException e) {
            throw new ConfigurationException(msg("export_cannot_load_category_mapping"), e);
        }

        for (String s : lines) {
            String[] pair = s.split(",");
            if (pair.length == 2) {
                try {
                    int category = Integer.parseInt(pair[0]);
                    categoryMap.put(category, pair[1]);
                } catch (Exception e) {
                    Disp.debug("Category ID '%s' is not a valid integer", pair[0]);
                }
            }
        }

        Disp.debug("Loaded %d category names.", categoryMap.size());

        categoryMap = syncCategoryMapping(categoryMap, corpus);
        Disp.debug("Applied %d category names.", categoryMap.size());

        return categoryMap;
    }

    private Map<Integer, String> syncCategoryMapping(Map<Integer, String> categoryMapping, Corpus corpus) {
        Map<Integer, String> mapping = new HashMap<>();

        for (Integer category : corpus.getDocumentCategories()) {
            String name = categoryMapping.getOrDefault(category, String.valueOf(category));
            mapping.put(category, name);
        }

        return mapping;
    }

    public String getRelationName() {
        return relationName;
    }

    public void setRelationName(String relationName) {
        this.relationName = relationName;
    }

    public boolean isUseNominalAttributes() {
        return useNominalAttributes;
    }

    public void setUseNominalAttributes(boolean useNominalAttributes) {
        this.useNominalAttributes = useNominalAttributes;
    }

    public boolean isIncludeIdAttribute() {
        return includeIdAttribute;
    }

    public void setIncludeIdAttribute(boolean includeIdAttribute) {
        this.includeIdAttribute = includeIdAttribute;
    }

    public boolean isIncludeCategoryAttribute() {
        return includeCategoryAttribute;
    }

    public void setIncludeCategoryAttribute(boolean includeCategoryAttribute) {
        this.includeCategoryAttribute = includeCategoryAttribute;
    }

    public String getCategoryMappingFile() {
        return categoryMappingFile;
    }

    public void setCategoryMappingFile(String categoryMappingFile) {
        this.categoryMappingFile = categoryMappingFile;
    }
}
