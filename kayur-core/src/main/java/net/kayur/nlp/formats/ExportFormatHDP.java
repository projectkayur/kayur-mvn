package net.kayur.nlp.formats;

import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;

import java.util.List;

/**
 * Class to support corpus export to LDA/HDP plain text format, each line of which is
 * N word1:frequency1 ... wordN:frequencyN
 */
public class ExportFormatHDP extends ExportFormat {
    public ExportFormatHDP() {
        super( "txt");
    }

    @Override
    public ExportResult export(@NotNull Corpus corpus) {
        // TODO: save also the list of document's IDs

        StringBuilder sb = new StringBuilder();
        for (ParsedDocument document : corpus.documents) {
            if (document.isEmpty()) {
                // empty documents are modelled as having one special word
                sb.append("1 0"); // TODO: rework this!!!
                sb.append(LINE_SEP);
                continue;
            }

            List<IndexedWord> words = corpus.getIndexedWords(document);

            sb.append(words.size()).append(" ");

            words.forEach(word -> sb.append(word.index)
                    .append(":")
                    .append(word.frequency)
                    .append(" "));

            sb.deleteCharAt(sb.length() - 1);
            sb.append(LINE_SEP);
        }

        String output = sb.toString();

        return new ExportResult(output, ExportStatistics.EMPTY);
    }
}
