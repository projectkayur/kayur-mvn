package net.kayur.nlp.formats;


import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.export.ExportStatistics;
import org.jetbrains.annotations.NotNull;

/**
 * Superclass for all supported export formats.
 */
public abstract class ExportFormat {
    static final String LINE_SEP = System.getProperty("line.separator");

    @NotNull
    public final String extension;


    ExportFormat(@NotNull String extension) {
        this.extension = extension;
    }


    public abstract ExportResult export(@NotNull Corpus corpus) throws ConfigurationException;

    public class ExportResult {
        public final String output;
        public final ExportStatistics statistics;

        public ExportResult(String output, ExportStatistics statistics) {
            this.output = output;
            this.statistics = statistics;
        }
    }
}
