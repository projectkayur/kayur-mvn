package net.kayur.nlp.analysis;


import net.kayur.helper.Util;
import net.kayur.nlp.corpus.CorpusKeyword;

import java.util.*;
import java.util.stream.Collectors;

public class ClusterCore {
    public final int numKeywords;
    public final Map<String, Integer> docIdNumKeywordsMap;

    // index: num keywords, value: num documents
    public final int[] numDocumentsByNumKeywordsDistribution;

    public ClusterCore(List<CorpusKeyword> keywords) {
        numKeywords = keywords.size();

        docIdNumKeywordsMap = buildDocumentIdNumKeywordsMap(keywords);

        numDocumentsByNumKeywordsDistribution = Distribution.of(docIdNumKeywordsMap.values());
        Distribution.leftCumulative(numDocumentsByNumKeywordsDistribution);
    }

    // Creates map (document id, number of keywords it contain).
    private Map<String, Integer> buildDocumentIdNumKeywordsMap(List<CorpusKeyword> keywords) {
        Map<String, Integer> map = new HashMap<>();
        /*
        keywords.stream()
                .flatMap(descriptor -> descriptor.documentIdList.stream())
                .forEach(documentId -> Util.count(map, documentId));
        */
        return map;
    }

    // Returns the list of documents that contain at least <N> keywords.
    public List<String> findDocumentsWithAtLeastNKeywords(int minNumKeywords) {
        return docIdNumKeywordsMap.entrySet().stream()
                .filter(entry -> entry.getValue() >= minNumKeywords)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());
    }
}
