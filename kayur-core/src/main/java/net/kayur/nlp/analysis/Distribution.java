package net.kayur.nlp.analysis;

import java.util.Collection;

public class Distribution {
    public static int[] of(Collection<Integer> numbers) {
        int maxValue = numbers.stream()
                .max(Integer::compareTo)
                .orElse(0);

        int[] numOccurrences = new int[maxValue + 1];
        for (Integer value : numbers) {
            numOccurrences[value]++;
        }

        return numOccurrences;
    }

    public static void leftCumulative(int[] distribution) {
        for (int i = distribution.length - 2; i >= 0; --i) {
            distribution[i] += distribution[i + 1];
        }
    }
}
