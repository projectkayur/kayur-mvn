package net.kayur.nlp;

import org.jetbrains.annotations.NotNull;

import java.util.regex.Pattern;

/**
 * Filter expression: replace 'pattern' with 'value'.
 */
public class FilterExpr {
    private final int layer;
    @NotNull
    private final String regEx;
    @NotNull
    private final String replacementString;
    @NotNull
    private final Pattern compiledPattern;

    public FilterExpr(int layer, @NotNull String regEx, @NotNull String replacementString) {
        this.layer = layer;
        this.regEx = regEx;
        this.replacementString = replacementString;
        this.compiledPattern = Pattern.compile(regEx);
    }

    public String filter(String str) {
        return compiledPattern.matcher(str).replaceAll(replacementString);
    }

    public boolean matches(String str) {
        return compiledPattern.matcher(str).matches();
    }

    public int getLayer() {
        return layer;
    }

    @NotNull
    public String getReplacementString() {
        return replacementString;
    }

    @NotNull
    public String getRegEx() {
        return regEx;
    }
}
