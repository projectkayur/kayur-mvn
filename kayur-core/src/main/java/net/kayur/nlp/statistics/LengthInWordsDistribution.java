package net.kayur.nlp.statistics;

import net.kayur.helper.Util;
import net.kayur.nlp.corpus.Corpus;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class LengthInWordsDistribution {
    public final Map<Interval, Integer> result;
    public final int numDocuments;
    public final int maxNumWordsInDocument;

    public LengthInWordsDistribution(@NotNull Corpus corpus, int step) {
        if (step <= 0) {
            throw new IllegalArgumentException("Step value '" + step + "' is not allowed.");
        }

        result = new LinkedHashMap<>(); // ordered

        // create a map (#words -> #{documents with such number of words})
        Map<Integer, Integer> numWordsInDocMap = new HashMap<>();
        corpus.documents.forEach(doc ->
                Util.count(numWordsInDocMap, doc.numWords()));

        int maxNumWords = Util.maxKey(numWordsInDocMap);
        int numWords = 0;

        for (int i = 0; i <= maxNumWords; ++i) {
            if (numWordsInDocMap.containsKey(i)) {
                numWords += numWordsInDocMap.get(i);
            }
            if ((i % step) == 0 && (numWords != 0)) {
                result.put(new Interval(i - step + 1, i), numWords);
                numWords = 0;
            }
        }

        if (numWords != 0) {
            result.put(new Interval(maxNumWords - (maxNumWords % step) + 1, maxNumWords), numWords);
        }

        numDocuments = corpus.numDocuments();
        maxNumWordsInDocument = maxNumWords;
    }

    public Map<Interval, Integer> getResult() {
        return result;
    }
}
