package net.kayur.nlp.statistics;


public class Interval {
    public final int start;
    public final int end;

    public Interval(int start, int end) {
        this.start = start;
        this.end = end;
    }
}
