package net.kayur.nlp.statistics;


import net.kayur.helper.Util;
import net.kayur.web.Document;
import net.kayur.web.Metadata;

import java.util.*;
import java.util.stream.Collectors;

import static net.kayur.Msg.msg;

public class Statistics {

    // (name, (value, #occurrences)
    public Map<String, Map<String, Integer>> getMetaStats(Collection<Document> documents, double aggregateThreshold) {

        Map<String, Map<String, Integer>> map = countMeta(documents);

        map.forEach((name, freqMap) ->
                map.put(name, sortByValueDesc(aggregateMinorValues(freqMap, aggregateThreshold))));

        return map;
    }

    /**
     * Counts number of different values for each metadata.
     *
     * @return map of the form (metaName -> (metaValue -> #occurencies))
     */
    private Map<String, Map<String, Integer>> countMeta(Collection<Document> documents) {
        Map<String, Map<String, Integer>> map = new HashMap<>();

        documents.forEach(document -> {
            List<Metadata> meta = document.getMeta();
            meta.forEach(datum -> {
                Map<String, Integer> counts;
                if (map.containsKey(datum.field)) {
                    counts = map.get(datum.field);
                } else {
                    counts = new HashMap<>();
                    map.put(datum.field, counts);
                }
                Util.count(counts, datum.value);
            });
        });

        return map;
    }

    /**
     * Returns a filtered map that replaces values with relative frequencies
     * lower than the supplied threshold with the aggregated value.
     *
     * @param freqMap   input frequency map
     * @param threshold percentage below which entries to be aggregated
     * @return filtered map, which may contain the aggregated value
     */
    private Map<String, Integer> aggregateMinorValues(Map<String, Integer> freqMap, double threshold) {
        Map<String, Integer> resultMap = new LinkedHashMap<>();

        if (freqMap.isEmpty())
            return resultMap;

        // total sum of values
        double total = freqMap.values().stream()
                .mapToInt(Number::intValue)
                .sum();

        int otherElements = 0, otherGroups = 0;
        String singleOtherGroupLabel = "";

        for (Map.Entry<String, Integer> entry : freqMap.entrySet()) {
            String name = entry.getKey();
            int freq = entry.getValue();

            if (freq / total >= threshold)
                resultMap.put(name, freq);
            else {
                otherElements += freq;
                otherGroups++;
                if (otherGroups == 1)
                    singleOtherGroupLabel = name;
            }
        }

        if (otherGroups == 1)
            resultMap.put(singleOtherGroupLabel, otherElements);
        else if (otherGroups > 0)
            resultMap.put(msg("chart_aggregate_label", otherGroups), otherElements);

        return resultMap;
    }

    /**
     * Sorts a map by its values in descending order.
     *
     * @param map an input map
     * @return a sorted map
     */
    private Map<String, Integer> sortByValueDesc(Map<String, Integer> map) {
        return map.entrySet().stream()
                .sorted((e, f) -> f.getValue().compareTo(e.getValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (i, j) -> j, LinkedHashMap::new));
    }
}
