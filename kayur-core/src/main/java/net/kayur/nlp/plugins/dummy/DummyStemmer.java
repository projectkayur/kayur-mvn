package net.kayur.nlp.plugins.dummy;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.plugins.api.Stemmer;

public class DummyStemmer implements Stemmer {
    @Override
    public void init(String[] tokens) {
        throw new ConfigurationException("Please select a stemmer.");
    }

    @Override
    public String process(String token, int index) {
        throw new ConfigurationException("Please select a stemmer.");
    }

    @Override
    public String getName() {
        return "(No Stemmer)";
    }

    @Override
    public String toString() {
        return getName();
    }

}
