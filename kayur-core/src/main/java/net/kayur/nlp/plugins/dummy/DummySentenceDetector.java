package net.kayur.nlp.plugins.dummy;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.plugins.api.SentenceDetector;

public class DummySentenceDetector implements SentenceDetector {
    @Override
    public String[] detect(String text) {
        throw new ConfigurationException("Please select a sentence detector.");
    }

    @Override
    public String getName() {
        return "(No Sentence Detector)";
    }

    @Override
    public String toString() {
        return getName();
    }
}
