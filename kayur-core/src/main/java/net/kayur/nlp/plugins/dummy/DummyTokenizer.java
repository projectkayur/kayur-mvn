package net.kayur.nlp.plugins.dummy;

import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.plugins.api.Tokenizer;

public class DummyTokenizer implements Tokenizer {
    @Override
    public String[] tokenize(String sentence) {
        throw new ConfigurationException("Please select a tokenizer.");
    }

    @Override
    public String getName() {
        return "(No Tokenizer)";
    }

    @Override
    public String toString() {
        return getName();
    }
}
