package net.kayur.nlp.weights;


import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import org.jetbrains.annotations.NotNull;

public class TermWeightFrequency extends TermWeight {

    public TermWeightFrequency() {
        super("Word Frequency", "f");
    }

    @Override
    public void calculate(@NotNull ParsedDocument d) {
        for (IndexedWord word : d.words)
            word.setWeight(word.frequency);
    }
}
