package net.kayur.nlp.weights;


import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import org.jetbrains.annotations.NotNull;

public class TermWeightTF extends TermWeight {

    public TermWeightTF() {
        super("TF", "tf");
    }

    @Override
    public void calculate(@NotNull ParsedDocument d) {
        double maxFreqInCurrentDoc = 1;

        for (IndexedWord word : d.words)
            if (word.frequency > maxFreqInCurrentDoc)
                maxFreqInCurrentDoc = word.frequency;

        for (IndexedWord word : d.words)
            word.setWeight(word.frequency / maxFreqInCurrentDoc);
    }
}
