package net.kayur.nlp.weights;


import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.ParsedDocument;
import org.jetbrains.annotations.NotNull;

public abstract class TermWeight implements Comparable<TermWeight> {
    @NotNull
    public final String name;
    @NotNull
    public final String shortName;

    protected TermWeight(@NotNull String name, @NotNull String shortName) {
        this.name = name;
        this.shortName = shortName;
    }

    public void setUp(@NotNull Corpus corpus) {
    }

    public abstract void calculate(@NotNull ParsedDocument d);

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public int compareTo(@NotNull TermWeight other) {
        return this.name.compareTo(other.name);
    }
}
