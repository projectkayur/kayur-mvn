package net.kayur.nlp.weights;


import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class TermWeightTFIDF extends TermWeight {
    private long numDocuments = 1;
    private Map<Integer, Integer> dfMap = new HashMap<>();

    public TermWeightTFIDF() {
        super("TF-IDF", "tfidf");
    }

    @Override
    public void setUp(@NotNull Corpus corpus) {
        numDocuments = corpus.documents.stream()
                .filter(document -> !document.isEmpty())
                .count();
        dfMap.clear();
        dfMap.putAll(corpus.indexToDocumentFrequency);
    }

    @Override
    public void calculate(@NotNull ParsedDocument d) {
        double maxFreqInCurrentDoc = 1;

        for (IndexedWord word : d.words) {
            if (word.frequency > maxFreqInCurrentDoc) {
                maxFreqInCurrentDoc = word.frequency;
            }
        }

        for (IndexedWord word : d.words) {
            double tf = word.frequency / maxFreqInCurrentDoc;
            double tval = (double) numDocuments / (double) dfMap.get(word.index);
            double idf = Math.log(tval) / Math.log(2);
            word.setWeight(tf * idf);
        }
    }
}
