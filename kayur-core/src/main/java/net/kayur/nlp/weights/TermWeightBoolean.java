package net.kayur.nlp.weights;


import net.kayur.nlp.corpus.ParsedDocument;
import net.kayur.nlp.corpus.IndexedWord;
import org.jetbrains.annotations.NotNull;

public class TermWeightBoolean extends TermWeight {

    public TermWeightBoolean() {
        super("Boolean", "b");
    }

    @Override
    public void calculate(@NotNull ParsedDocument d) {
        for (IndexedWord word : d.words)
            word.setWeight(1.0);
    }
}
