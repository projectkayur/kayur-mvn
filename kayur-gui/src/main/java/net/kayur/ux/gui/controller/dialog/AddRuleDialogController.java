package net.kayur.ux.gui.controller.dialog;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import net.kayur.ux.gui.controller.AbstractTabController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxDialogs;
import net.kayur.web.parser.AccessToken;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class AddRuleDialogController extends AbstractTabController {
    public TextField nameField;
    public ComboBox<String> typeComboBox;
    public TextField positionField;
    public Button okButton;

    private Map<String, AccessToken.Type> typeMap;

    public AddRuleDialogController(AssetLoader assetLoader) {
        super(assetLoader);
    }

    @FXML
    public void initialize() {
        final String TAG_LABEL  = "Tag";
        final String ID_LABEL   = "Identifier (#)";
        final String CLS_LABEL  = "CSS Class";
        final String ATTR_LABEL = "Attribute";

        typeMap = new HashMap<>();
        typeMap.put(TAG_LABEL, AccessToken.Type.TAG);
        typeMap.put(ID_LABEL, AccessToken.Type.ID);
        typeMap.put(CLS_LABEL, AccessToken.Type.CLS);
        typeMap.put(ATTR_LABEL, AccessToken.Type.ATTR);

        typeComboBox.setItems(toFxList(Arrays.asList(TAG_LABEL, ID_LABEL, CLS_LABEL, ATTR_LABEL)));
        typeComboBox.getSelectionModel().selectFirst();

        okButton.setDisable(true);
        nameField.textProperty().addListener((observable, oldValue, newValue) ->
            okButton.setDisable(newValue.trim().isEmpty()));
    }

    @FXML
    public void onAddButtonAction(ActionEvent event) {
        String name = nameField.getText().trim();
        AccessToken.Type type = typeMap.get(typeComboBox.getSelectionModel().getSelectedItem());

        String position = positionField.getText().trim();
        if (position.isEmpty()) {
            position = "0";
        }

        String tokenString;
        try {
            AccessToken token = new AccessToken(name, type, position);
            tokenString = token.toString();
        } catch (ParseException e) {
            FxDialogs.warning(e);
            return;
        }

        getStage(event).getScene().setUserData(tokenString);
        closeWindow(event);
    }
}
