package net.kayur.ux.gui.component;


public interface ConfigBoundComponent<T> {
    ConfigAdapter<T> getAdapter();
}
