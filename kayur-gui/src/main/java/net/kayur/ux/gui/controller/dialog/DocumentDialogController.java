package net.kayur.ux.gui.controller.dialog;


import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import net.kayur.web.Document;
import net.kayur.web.Metadata;

import java.util.Date;
import java.util.List;

public class DocumentDialogController {
    @FXML
    public TextField idField;
    @FXML
    public TextField dateField;
    @FXML
    public TextField titleField;

    @FXML
    public TextArea contentArea;
    @FXML
    public TextArea commentsArea;

    @FXML
    public Button prevCommentButton;
    @FXML
    public Button nextCommentButton;

    @FXML
    public TableView<Metadata> metadataTable;

    private List<String> comments;
    private int commentIndex = 0;


    public void setDocument(Document document) {

        idField.setText(document.id.toString());

        Date date = document.getDateTime();
        dateField.setText(date != null ? date.toString() : "");

        titleField.setText(document.getTitle());
        contentArea.setText(document.getContent());

        comments = document.getComments();
        if (!comments.isEmpty())
            commentsArea.setText(comments.get(0));

        metadataTable.getItems().addAll(document.getMeta());

        updateCommentNavButtons();
    }

    private void updateCommentNavButtons() {
        nextCommentButton.setDisable(commentIndex >= (comments.size() - 1));
        prevCommentButton.setDisable(commentIndex <= 0);
    }

    @FXML
    public void prevCommentButtonClicked() {
        if (commentIndex > 0)
            commentsArea.setText(comments.get(--commentIndex));

        updateCommentNavButtons();
    }

    @FXML
    public void nextCommentButtonClicked() {
        if (commentIndex < (comments.size() - 1)) {
            commentsArea.setText(comments.get(++commentIndex));
        }

        updateCommentNavButtons();
    }
}
