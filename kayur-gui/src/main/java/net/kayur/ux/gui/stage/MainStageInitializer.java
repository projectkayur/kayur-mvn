package net.kayur.ux.gui.stage;

import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import net.kayur.Disp;
import net.kayur.core.App;
import net.kayur.ux.gui.helper.AssetLoader;
import org.jetbrains.annotations.NotNull;

public class MainStageInitializer {
    private final AssetLoader assetLoader;

    public MainStageInitializer(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    public void setUpPrimaryStage(@NotNull Stage primaryStage) {
        Parent mainWindow = assetLoader.loadForm("main");
        primaryStage.setScene(new Scene(mainWindow));

        primaryStage.setTitle(String.format("%s %s", App.NAME, App.VERSION));

        setAppPictogram(primaryStage);
    }

    private void setAppPictogram(@NotNull Stage stage) {
        Image image;
        try {
            image = assetLoader.loadPictogram("kayur");
        } catch (Exception e) {
            e.printStackTrace();
            Disp.warning("Unable to set application pictogram.");
            return;
        }

        stage.getIcons().add(image);
    }

}
