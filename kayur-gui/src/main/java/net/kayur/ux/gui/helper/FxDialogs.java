package net.kayur.ux.gui.helper;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.layout.Region;
import net.kayur.Disp;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.export.ExportException;
import net.kayur.web.WebModuleException;

import java.util.Optional;

public class FxDialogs {
    // Shows a warning dialog about a configuration problem.
    public static void warning(Throwable e) {
        final String title = (e instanceof ConfigurationException) ?
                "Configuration Problem" :
                "Warning";
        warning(title, e.getMessage());
    }

    public static void warning(String content) {
        warning("Warning", content);
    }

    public static void warning(String title, String content) {
        Alert alert = new Alert(Alert.AlertType.WARNING);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(content);
        alert.showAndWait();
    }

    // Shows an error dialog with the exception stack trace.
    public static void error(Throwable exception) {
        String errorDetails;
        Throwable cause = Disp.findCause(exception);
        if (cause instanceof ConfigurationException ||
                cause instanceof WebModuleException ||
                cause instanceof ExportException) {
            errorDetails = cause.getMessage();
        } else {
            String clsFullName = cause.getClass().getName();
            String clsName = clsFullName.substring(clsFullName.lastIndexOf(".") + 1, clsFullName.length());
            errorDetails = String.format("(%s) %s", clsName, cause.getMessage());
        }

        Alert alert = new Alert(Alert.AlertType.ERROR, errorDetails);
        alert.setTitle("Program Error");
        alert.setHeaderText(exception.getMessage());

        alert.setResizable(true);
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);

        Disp.error(exception, exception.getMessage());

        /*
        String stackTrace;

        try (StringWriter sw = new StringWriter();
             PrintWriter pw = new PrintWriter(sw)) {
            exception.printStackTrace(pw);
            stackTrace = sw.toString();
        } catch (IOException ignored) {
            stackTrace = null;
        }

        if (stackTrace != null) {
            TextArea textArea = new TextArea(stackTrace);
            textArea.setEditable(false);
            textArea.setWrapText(true);
            textArea.setMaxWidth(Double.MAX_VALUE);
            textArea.setMaxHeight(Double.MAX_VALUE);

            alert.getDialogPane().setExpandableContent(textArea);
        }
        */

        alert.showAndWait();
    }

    // Shows a notification dialog
    public static void notification(String text, String title) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(text);
        alert.showAndWait();
    }

    public static void notification(String text) {
        notification(text, "Notification");
    }

    public static boolean showConfirmationDialog(String title, String header, String content) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        Optional<ButtonType> result = alert.showAndWait();
        return result.isPresent() && (result.get() == ButtonType.OK);
    }
}
