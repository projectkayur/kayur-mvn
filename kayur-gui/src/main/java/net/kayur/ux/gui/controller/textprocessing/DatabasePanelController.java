package net.kayur.ux.gui.controller.textprocessing;

import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import net.kayur.core.DocumentSelection;
import net.kayur.database.DocumentRepository;
import net.kayur.database.SelectQueryParameters;
import net.kayur.helper.SimpleTimer;
import net.kayur.ux.gui.controller.AbstractTabController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxDialogs;
import net.kayur.web.Document;
import net.kayur.web.module.ModuleLoader;

import java.util.List;

import static net.kayur.Msg.msg;

public class DatabasePanelController extends AbstractTabController {
    @FXML
    public TextField moduleSelField;
    @FXML
    public TextField idSelField;
    @FXML
    public TextField titleSelField;
    @FXML
    public TextField contentSelField;
    @FXML
    public TextField tagSelField;
    @FXML
    public TextField metaSelField;
    @FXML
    public DatePicker startDateSelDatePicker;
    @FXML
    public DatePicker endDateSelDatePicker;
    @FXML
    public CheckBox keepSelectionCheckBox;

    @FXML
    public TableView<Document> selectionTable;

    // TODO: [OPTIONAL] auto-resize the result table
    // TODO: nicer table date format
    // TODO: table columns customization

    private final DocumentSelection selection;
    private final DocumentRepository repository;
    private final ModuleLoader moduleLoader;

    public DatabasePanelController(AssetLoader assetLoader,
                                   DocumentSelection selection,
                                   DocumentRepository repository,
                                   ModuleLoader moduleLoader) {
        super(assetLoader);
        this.selection = selection;
        this.repository = repository;
        this.moduleLoader = moduleLoader;
    }

    @FXML
    public void onQueryButtonClicked() {
        SelectQueryParameters params = new SelectQueryParameters();
        params.setStartDate(getDate(startDateSelDatePicker));
        params.setEndDate(getDate(endDateSelDatePicker));

        params.setDocumentId(get(idSelField));
        params.setTitle(get(titleSelField));
        params.setContent(get(contentSelField));

        String modulePattern = get(moduleSelField);
        if (!modulePattern.isEmpty()) {
            params.setModules(moduleLoader.findModuleIdentifiers(modulePattern));
        }

        boolean keepOldSelection = keepSelectionCheckBox.isSelected();

        // TODO: tag, metadata

        SimpleTimer timer = new SimpleTimer();

        executeUserTask(
                msg("query_dialog_title"),
                new Task<List<Document>>() {
                    @Override
                    protected List<Document> call() {
                        updateMessage(msg("db_query"));
                        return repository.select(params);
                    }
                },
                documents -> {
                    long opTime = timer.time();
                    if (keepOldSelection) {
                        selection.add(documents);
                    } else {
                        selection.set(documents);
                        selectionTable.getItems().clear();
                    }

                    selectionTable.getItems().addAll(documents);

                    FxDialogs.notification(msg("db_select_success", documents.size(), opTime));
                }
        );
    }

    @FXML
    public void onSelectionTableClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            Document document = selectionTable.getSelectionModel().getSelectedItem();
            execute(() -> showDocumentDialog(document));
        }
    }
}
