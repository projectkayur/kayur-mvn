package net.kayur.ux.gui.controller.dialog;

import net.kayur.ux.gui.helper.AssetLoader;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import net.kayur.ux.gui.controller.AbstractTabController;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.TemplateEngine;

public class WizardController extends AbstractTabController {
    @FXML
    public Pane mainWindow;
    @FXML
    public Pane jiraWindow;
    @FXML
    public Pane bugzillaWindow;

    @FXML
    public Button backButton;
    @FXML
    public Button nextButton;

    @FXML
    public RadioButton jiraRadioButton;
    @FXML
    public RadioButton bugzillaRadioButton;

    @FXML
    public TextField moduleNameField;

    @FXML
    public TextField jiraUrlField;
    @FXML
    public TextField jiraProjectField;

    @FXML
    public TextField bugzillaUrlField;

    private Pane currentPane;

    private final TemplateEngine templateEngine;


    public WizardController(AssetLoader assetLoader, TemplateEngine templateEngine) {
        super(assetLoader);
        this.templateEngine = templateEngine;
    }

    @FXML
    public void initialize() {
        currentPane = mainWindow;

        updateNavButtons();
    }

    @FXML
    public void backButtonClicked() {
        switchPane(mainWindow);
    }

    @FXML
    public void nextButtonClicked() {
        String currentId = currentPane.getId();

        if (currentId.equals(mainWindow.getId())) {
            // navigate from the First Window
            if (jiraRadioButton.isSelected()) {
                switchPane(jiraWindow);
            } else if (bugzillaRadioButton.isSelected()) {
                switchPane(bugzillaWindow);
            }

        } else if (currentId.equals(jiraWindow.getId())) {
            // Finish (create JIRA module)
            generateJiraConfig();
            closeWindow(mainWindow);

        } else if (currentId.equals(bugzillaWindow.getId())) {
            // Finish (create Bugzilla module)
            generateBugzillaConfig();
            closeWindow(mainWindow);
        }
    }

    @FXML
    public void cancelButtonClicked() {
        closeWindow(mainWindow);
    }

    private void updateNavButtons() {
        boolean firstPage = currentPane.getId().equals(mainWindow.getId());
        backButton.setDisable(firstPage);
        nextButton.setText(firstPage ? "  Next > " : "  Finish  ");
    }

    private void switchPane(Pane pane) {
        currentPane.setVisible(false);
        currentPane = pane;
        currentPane.setVisible(true);
        updateNavButtons();
    }

    private void registerConfig(ModuleConfiguration config) {
        config.setName(get(moduleNameField));
        mainWindow.getScene().setUserData(config);
    }

    private void generateJiraConfig() {
        String projectName = get(jiraProjectField);
        String jiraUrl = get(jiraUrlField);

        execute(() -> {
            ModuleConfiguration config = templateEngine.generateJiraConfig(projectName, jiraUrl);
            registerConfig(config);
        });
    }

    private void generateBugzillaConfig() {
        String bugzillaUrl = get(bugzillaUrlField);

        execute(() -> {
            ModuleConfiguration config = templateEngine.generateBugzillaConfig(bugzillaUrl);
            registerConfig(config);
        });
    }
}
