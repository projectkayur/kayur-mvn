package net.kayur.ux.gui.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.ux.gui.controller.dialog.DocumentDialogController;
import net.kayur.ux.gui.controller.dialog.ServiceDialogController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxConsumer;
import net.kayur.ux.gui.helper.FxDialogs;
import net.kayur.ux.gui.helper.FxExecutable;
import net.kayur.web.Document;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import static net.kayur.Msg.msg;

public abstract class AbstractTabController {
    protected final AssetLoader assetLoader;

    protected AbstractTabController(AssetLoader assetLoader) {
        this.assetLoader = assetLoader;
    }

    // Gets text from a TextField without leading and trailing spaces.
    @NotNull
    protected String get(TextField field) {
        return field.getText().trim();
    }

    // Same as get(), but prohibits empty input.
    @NotNull
    protected String getRequired(@NotNull TextField field, String violationMessage) throws ConfigurationException {
        String text = get(field);
        if (text.isEmpty()) {
            throw new ConfigurationException(violationMessage);
        }
        return text;
    }

    @Nullable
    protected static Date getDate(DatePicker datePicker) {
        Date result;
        LocalDate localDate = datePicker.getValue();

        if (localDate == null) {
            result = null;
        } else {
            Instant instant = Instant.from(localDate.atStartOfDay(ZoneId.systemDefault()));
            result = Date.from(instant);
        }

        return result;
    }

    @Nullable
    protected <T> T getUserData(Stage stage, Class<T> cls) {
        return cls.cast(stage.getScene().getUserData());
    }

    protected void execute(@NotNull FxExecutable f) {
        execute(f, null);
    }

    private static void handleException(Throwable ex) {
        if (ex != null) {
            if (ex instanceof ConfigurationException) {
                FxDialogs.warning(ex);
            } else {
                ex.printStackTrace();
                FxDialogs.error(ex);
            }
        } else {
            FxDialogs.error(new Exception("No information is available."));
        }
    }

    // Executes a method that can throw exceptions.
    // When an exception is thrown, displays the appropriate dialog;
    // otherwise, displays a notification (if set).
    protected void execute(@NotNull FxExecutable f, @Nullable String successMsg) {
        try {
            f.run();
            if (successMsg != null) {
                FxDialogs.notification(successMsg);
            }
        } catch (Throwable ex) {
            handleException(ex);
        }
    }

    // Connects a CheckBox to an UI element, so they are disabled or enabled simultaneously
    protected void relate(CheckBox checkBox, Node node) {
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> node.setDisable(!newValue));
        node.setDisable(!checkBox.isSelected());
    }

    protected void bind(TextField textField, Consumer<String> f) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> f.accept(newValue));
    }

    protected void bind(CheckBox checkBox, Consumer<Boolean> f) {
        checkBox.selectedProperty().addListener((observable, oldValue, newValue) -> f.accept(newValue));
    }

    protected <T> void bind(Spinner<T> spinner, Consumer<T> f) {
        spinner.valueProperty().addListener((observable, oldValue, newValue) -> f.accept(newValue));
    }

    protected <T> void bind(ComboBox<T> comboBox, Consumer<T> f) {
        comboBox.valueProperty().addListener((observable, oldValue, newValue) -> f.accept(newValue));
    }

    protected <T> void setItems(ComboBox<T> comboBox, List<T> items) {
        comboBox.setItems(toFxList(items));
    }

    protected void setValue(TextField textField, String value) {
        textField.setText(value);
    }

    protected void setValue(CheckBox checkBox, boolean value) {
        checkBox.setSelected(value);
    }

    protected <T> void setValue(Spinner<T> spinner, T value) {
        spinner.getValueFactory().setValue(value);
    }

    protected <T> void setValue(ComboBox<T> comboBox, T value) {
        comboBox.valueProperty().setValue(value);
    }

    /**
     * Executes a concurrent task, and shows a dialog for tracking progress.
     *
     * @param dialogTitle title of the dialog that tracks progress
     * @param task a function to be executed on the background
     * @param onSuccess a callback function executed when task is finished successfully
     * @param <T> the return type of the function executed on the background
     * @throws IOException when FXML dialog bound to the background task cannot be loaded
     * @throws ConfigurationException when FXML dialog bound to the background task cannot be located
     */
    protected <T> void runUserTask(String dialogTitle, Task<T> task, FxConsumer<T> onSuccess)
            throws IOException, ConfigurationException {

        FXMLLoader loader = assetLoader.getDialogLoader("serviceDialog");
        Parent dialog = loader.load();

        Stage stage = createStage(dialog, dialogTitle);

        task.setOnCancelled(state -> stage.close());

        task.setOnSucceeded(state -> {
            stage.close();
            execute(() -> onSuccess.accept(task.getValue()));
        });

        task.setOnFailed(state -> {
            stage.close();
            handleException(task.getException());
        });

        ServiceDialogController controller = loader.getController();
        controller.setTask(task);

        stage.show();

        controller.run();
    }

    protected <T> void executeUserTask(String dialogTitle, Task<T> task, FxConsumer<T> onSuccess) {
        execute(() -> runUserTask(dialogTitle, task, onSuccess));
    }

    // Creates a new window and sets its title.
    protected Stage createStage(Parent root, String title) {
        Stage stage = new Stage();
        stage.setScene(new Scene(root));
        stage.setTitle(title);
        return stage;
    }

    // Displays a dialog to view a document.
    protected void showDocumentDialog(Document document) throws IOException, ConfigurationException {
        FXMLLoader loader = assetLoader.getDialogLoader("documentDialog");
        Parent dialog = loader.load();

        String title = msg("dialog_issue_title", document.id.name);
        Stage stage = createStage(dialog, title);

        setCloseOnEscape(stage);

        DocumentDialogController controller = loader.getController();
        controller.setDocument(document);

        stage.show();
    }

    // Adds to a window the behaviour to close on ESC key press.
    protected void setCloseOnEscape(Stage stage) {
        stage.addEventHandler(KeyEvent.KEY_PRESSED, event -> {
            if (event.getCode() == KeyCode.ESCAPE)
                stage.close();
        });
    }

    public void closeWindow(Event sceneEvent) {
        getStage(sceneEvent).close();
    }

    protected void closeWindow(Node node) {
        Stage stage = (Stage) node.getScene().getWindow();
        stage.close();
    }

    protected Stage getStage(Event sceneEvent) {
        final Node source = (Node) sceneEvent.getSource();
        return getStage(source);
    }

    private Stage getStage(Node node) {
        return (Stage) node.getScene().getWindow();
    }

    protected static void fixSpinner(Spinner<Integer> spinner) {
        SpinnerValueFactory<Integer> factory = spinner.getValueFactory();
        TextFormatter<Integer> formatter = new TextFormatter<>(factory.getConverter(), factory.getValue());
        spinner.getEditor().setTextFormatter(formatter);
        factory.valueProperty().bindBidirectional(formatter.valueProperty());
        spinner.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                factory.setValue(0);
            }
        });
    }

    protected <T> ObservableList<T> toFxList(List<T> list) {
        return FXCollections.observableArrayList(list);
    }
}
