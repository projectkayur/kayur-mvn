package net.kayur.ux.gui.component;

import javafx.scene.control.ComboBox;
import net.kayur.core.configuration.Parameter;


public class ConfigComboBox extends ComboBox<String> implements ConfigBoundComponent<String> {
    private ConfigAdapter<String> adapter = new ConfigAdapter<>("",
            value -> {
                String val = (value != null) ? value : "";
                if (!getItems().contains(value))
                    getItems().add(val);

                getSelectionModel().select(val);
            });

    public ConfigComboBox() {
        valueProperty().addListener(adapter.getListener());
    }

    public void assign(Parameter<String> parameter) {
        adapter.assign(parameter);
    }

    @Override
    public ConfigAdapter<String> getAdapter() {
        return adapter;
    }
}
