package net.kayur.ux.gui.helper;

@FunctionalInterface
public interface FxConsumer<T> {
    void accept(T t) throws Exception;
}
