package net.kayur.ux.gui.helper;

@FunctionalInterface
public interface FxExecutable {
    void run() throws Exception;
}
