package net.kayur.ux.gui.controller.textprocessing;


import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import net.kayur.core.DocumentSelection;
import net.kayur.helper.SimpleTimer;
import net.kayur.nlp.corpus.TextEngineController;
import net.kayur.nlp.corpus.TextProcessingParameters;
import net.kayur.ux.gui.controller.AbstractTabController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxDialogs;

import static net.kayur.Msg.msg;

public class CorpusPanelController extends AbstractTabController {
    @FXML
    public CheckBox processContentCheckBox;
    @FXML
    public CheckBox processTitlesCheckBox;
    @FXML
    public CheckBox processCommentsCheckBox;
    @FXML
    public CheckBox enableCacheCheckBox;

    // TODO: consider automatic reconfiguration for Text Processing (when, for example, filters are changed)

    private final DocumentSelection selection;
    private final TextEngineController textEngineController;

    public CorpusPanelController(AssetLoader assetLoader,
                                 DocumentSelection selection,
                                 TextEngineController textEngineController) {
        super(assetLoader);
        this.selection = selection;
        this.textEngineController = textEngineController;
    }

    @FXML
    public void textProcessingButtonClicked() {
        if (selection.isEmpty()) {
            FxDialogs.warning(msg("msg_empty_selection"));
            return;
        }

        TextProcessingParameters params = TextProcessingParameters.builder()
                .processTitles(processTitlesCheckBox.isSelected())
                .processContent(processContentCheckBox.isSelected())
                .processComments(processCommentsCheckBox.isSelected())
                .enableCache(enableCacheCheckBox.isSelected())
                .build();

        SimpleTimer timer = new SimpleTimer();

        executeUserTask(
                msg("dialog_textprocessing_title"),
                new Task<Void>() {
                    @Override
                    protected Void call() {
                        updateMessage(msg("tp_processing_msg", selection.size()));
                        textEngineController.buildCorpus(selection.get(), params);
                        return null;
                    }
                },
                nothing -> FxDialogs.notification(msg("tp_success", timer.time()))
        );
    }
}
