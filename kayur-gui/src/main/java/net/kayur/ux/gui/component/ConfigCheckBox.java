package net.kayur.ux.gui.component;

import javafx.scene.control.CheckBox;
import net.kayur.core.configuration.Parameter;
import org.jetbrains.annotations.NotNull;

public class ConfigCheckBox extends CheckBox implements ConfigBoundComponent<Boolean> {
    private ConfigAdapter<Boolean> adapter = new ConfigAdapter<>(false, this::setSelected);

    public ConfigCheckBox() {
        selectedProperty().addListener(adapter.getListener());
    }

    public void assign(Parameter<Boolean> parameter) {
        adapter.assign(parameter);
    }

    public void setDefaultValue(@NotNull Boolean value) {
        adapter.setDefaultValue(value);
    }

    @Override
    public ConfigAdapter<Boolean> getAdapter() {
        return adapter;
    }
}
