package net.kayur.ux.gui.controller.dialog;


import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

public class ServiceDialogController {
    @FXML
    public Label statusLabel;
    @FXML
    public ProgressBar progressBar;

    private Task task;

    public void setTask(Task task) {
        this.task = task;
        statusLabel.textProperty().bind(task.messageProperty());
        progressBar.progressProperty().bind(task.progressProperty());
    }

    public void run() {
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    @FXML
    public void onAbortButtonClicked() {
        if (task != null)
            task.cancel();
    }
}
