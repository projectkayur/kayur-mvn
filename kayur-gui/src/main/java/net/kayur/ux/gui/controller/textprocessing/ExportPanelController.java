package net.kayur.ux.gui.controller.textprocessing;

import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Spinner;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;
import javafx.stage.FileChooser;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.helper.FileHelper;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusManager;
import net.kayur.nlp.export.ExportController;
import net.kayur.nlp.export.ExportManagerParameters;
import net.kayur.nlp.export.ExportStatistics;
import net.kayur.nlp.formats.*;
import net.kayur.nlp.weights.TermWeight;
import net.kayur.ux.gui.controller.AbstractTabController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxDialogs;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static net.kayur.Msg.msg;

public class ExportPanelController extends AbstractTabController {
    @FXML
    public ComboBox<String> exportFormatComboBox;
    @FXML
    public ComboBox<TermWeight> exportWeightsComboBox;
    @FXML
    public Spinner<Integer> exportHapaxSpinner;
    @FXML
    public TextField exportPathField;

    @FXML
    public CheckBox saveWordListCheckBox;
    @FXML
    public CheckBox sortWordListCheckBox;

    @FXML
    public Pane arffPane;
    @FXML
    public TextField arffRelNameField;
    @FXML
    public CheckBox arffNominalAttrCheckBox;
    @FXML
    public CheckBox arffIncludeTagsCheckBox;

    private final MainConfiguration config;
    private final CorpusManager corpusManager;
    private final ExportController exportController;

    public ExportPanelController(MainConfiguration config,
                                 AssetLoader assetLoader,
                                 CorpusManager corpusManager,
                                 ExportController exportController) {
        super(assetLoader);
        this.config = config;
        this.corpusManager = corpusManager;
        this.exportController = exportController;
    }

    @FXML
    public void initialize() {
        // load export formats
        exportFormatComboBox.setItems(toFxList(ExportController.supportedFormats));
        exportFormatComboBox.getSelectionModel().selectFirst();
        onExportFormatChanged();

        // load term weight methods
        exportWeightsComboBox.setItems(toFxList(ExportController.supportedWeights));
        exportWeightsComboBox.getSelectionModel().selectFirst();

        // initialize default values
        exportHapaxSpinner.getValueFactory().setValue(ExportController.DEFAULT_HAPAX);
    }

    // ------------------------- EVENT HANDLERS ------------------------- //

    @FXML
    public void exportButtonClicked() {
        execute(() -> {

            String filename = getExportFilename();
            if (filename.isEmpty()) {
                throw new ConfigurationException(msg("export_filename_not_specified"));
            }

            ExportManagerParameters parameters = ExportManagerParameters.builder()
                    .filename(filename)
                    .termWeight(getSelectedTermWeights())
                    .hapax(exportHapaxSpinner.getValue())
                    .isSaveWordList(saveWordListCheckBox.isSelected())
                    .isSortedWordList(sortWordListCheckBox.isSelected())
                    .format(getSelectedExportFormat())
                    .build();

            Corpus corpus = corpusManager.getCorpus();
            ExportStatistics stats = exportController.export(corpus, parameters);

            FxDialogs.notification(msg("export_success", stats));
        });
    }

    @FXML
    public void onExportFormatChanged() {
        arffPane.setVisible(isArffFormatSelected());
        restrictTermWeightChoice();
    }

    @FXML
    public void onSelectPathButtonClicked(ActionEvent event) {
        execute(() -> {
            String formatName = getSelectedExportFormatName();
            ExportFormat format = getExportFormat(formatName);

            final FileChooser fileChooser = new FileChooser();

            String oldPath = getExportFilename();
            if (!oldPath.isEmpty()) {
                String oldDirectoryName = FileHelper.getFullPath(oldPath);
                String oldFileName = FileHelper.getBaseName(oldPath);

                if (!oldDirectoryName.isEmpty())
                    fileChooser.setInitialDirectory(new File(oldDirectoryName));
                if (!oldFileName.isEmpty())
                    fileChooser.setInitialFileName(oldFileName);
            }

            fileChooser.getExtensionFilters().add(
                    new FileChooser.ExtensionFilter(formatName, String.format("*.%s", format.extension)));

            File file = fileChooser.showSaveDialog(getStage(event));

            if (file != null)
                exportPathField.setText(file.getAbsolutePath());
        });
    }

    @FXML
    public void onSaveWordListCheckBoxStateChanged() {
        sortWordListCheckBox.setDisable(!saveWordListCheckBox.isSelected());
    }

    // ------------------------------------------------------------------ //


    private void restrictTermWeightChoice() {
        String formatName = getSelectedExportFormatName();

        TermWeight lastTermWeights = getSelectedTermWeights();

        exportWeightsComboBox.getItems().clear();
        List<TermWeight> supportedWeights;

        switch (formatName) {
            case ExportController.FORMAT_CARROT_XML:
                supportedWeights = Collections.singletonList(ExportController.TERM_WEIGHT_BOOLEAN);
                break;
            case ExportController.FORMAT_HDP:
                supportedWeights = Arrays.asList(
                        ExportController.TERM_WEIGHT_BOOLEAN, ExportController.TERM_WEIGHT_FREQUENCY);
                break;
            default:
                supportedWeights = ExportController.supportedWeights;
                break;
        }

        exportWeightsComboBox.setItems(FXCollections.observableArrayList(supportedWeights));

        if (exportWeightsComboBox.getItems().contains(lastTermWeights))
            exportWeightsComboBox.getSelectionModel().select(lastTermWeights);
        else
            exportWeightsComboBox.getSelectionModel().selectFirst();
    }

    private boolean isArffFormatSelected() {
        return getSelectedExportFormatName().equals(ExportController.FORMAT_ARFF);
    }

    private String getSelectedExportFormatName() {
        return exportFormatComboBox.getSelectionModel().getSelectedItem();
    }

    private ExportFormat getSelectedExportFormat() throws ConfigurationException {
        return getExportFormat(getSelectedExportFormatName());
    }

    private ExportFormat getExportFormat(String formatName) throws ConfigurationException {
        switch (formatName) {
            case ExportController.FORMAT_CARROT_XML:
                return new ExportFormatCarrotXML();
            case ExportController.FORMAT_CSV:
                return new ExportFormatCSV();
            case ExportController.FORMAT_HDP:
                return new ExportFormatHDP();
            case ExportController.FORMAT_JSON:
                return new ExportFormatJSON();
            case ExportController.FORMAT_ARFF:
                ExportFormatARFF format = new ExportFormatARFF();
                format.setRelationName(getRequired(arffRelNameField, "Relation name cannot be empty."));
                format.setUseNominalAttributes(arffNominalAttrCheckBox.isSelected());
                format.setIncludeCategoryAttribute(arffIncludeTagsCheckBox.isSelected());
                format.setCategoryMappingFile(config.nlp.getCategoryMapping());
                return format;
            default:
                throw new RuntimeException("Unsupported export format.");
        }
    }

    private TermWeight getSelectedTermWeights() {
        return exportWeightsComboBox.getSelectionModel().getSelectedItem();
    }

    private String getExportFilename() {
        return FileHelper.normalizePath(get(exportPathField));
    }
}
