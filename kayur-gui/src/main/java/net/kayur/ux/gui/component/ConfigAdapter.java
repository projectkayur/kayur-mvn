package net.kayur.ux.gui.component;


import javafx.beans.value.ChangeListener;
import net.kayur.core.configuration.Configuration;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Parameter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.function.Consumer;

/**
 * Provides an automatic update of a configuration parameter from a GUI component,
 * and vice versa.
 *
 * @param <T> type of the value
 */
public final class ConfigAdapter<T> {
    @Nullable
    private Configuration config;
    @Nullable
    private Parameter<T> parameter;
    @NotNull
    private T defaultValue;

    private ChangeListener<T> listener = (observable, oldValue, newValue) -> updateConfigValue(newValue);
    private Consumer<T> componentValueSetter;


    ConfigAdapter(@NotNull T defaultValue, Consumer<T> setter) {
        this.defaultValue = defaultValue;
        this.componentValueSetter = setter;
    }

    ChangeListener<T> getListener() {
        return listener;
    }

    public void setConfig(@Nullable Configuration config) {
        this.config = config;
        updateComponentValue();
    }

    // Updates the value of the GUI component from the configuration.
    private void updateComponentValue() {
        if (config != null && parameter != null) {
            T value;
            try {
                value = config.getValidated(parameter);
            } catch (ConfigurationException ignored) {
                value = defaultValue;
            }
            componentValueSetter.accept(value);
        }
    }

    // Updates the configuration with the given value.
    private void updateConfigValue(T value) {
        if (config != null && parameter != null)
            config.set(parameter, value);
    }

    public void assign(Parameter<T> parameter) {
        this.parameter = parameter;
    }

    void setDefaultValue(@NotNull T value) {
        this.defaultValue = value;
    }
}
