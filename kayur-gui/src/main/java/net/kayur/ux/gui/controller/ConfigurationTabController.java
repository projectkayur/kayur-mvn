package net.kayur.ux.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.*;
import net.kayur.core.configuration.ConfigurationLoader;
import net.kayur.core.configuration.MainConfiguration;
import net.kayur.core.configuration.NLPConfiguration;
import net.kayur.database.ConnectionHub;
import net.kayur.database.DatabaseConfiguration;
import net.kayur.nlp.corpus.TextEngineModuleLocator;
import net.kayur.nlp.plugins.api.SentenceDetector;
import net.kayur.nlp.plugins.api.Stemmer;
import net.kayur.nlp.plugins.api.Tokenizer;
import net.kayur.ux.gui.helper.AssetLoader;

import static net.kayur.Msg.msg;

public class ConfigurationTabController extends AbstractTabController {
    @FXML
    public ComboBox<Tokenizer> tokenizer;
    @FXML
    public ComboBox<SentenceDetector> sentenceDetector;
    @FXML
    public ComboBox<Stemmer> stemmer;
    @FXML
    public TextField filtersField;
    @FXML
    public TextField stopWordsField;
    @FXML
    public TextField categoryMappingField;

    @FXML
    public TextField dbDriverField;
    @FXML
    public TextField dbConnectionStringField;
    @FXML
    public TextField dbUsernameField;
    @FXML
    public TextField dbPasswordField;

    @FXML
    public Spinner<Integer> dbTitleLimit;
    @FXML
    public Spinner<Integer> dbContentLimit;
    @FXML
    public Spinner<Integer> dbCommentLimit;
    @FXML
    public Spinner<Integer> dbMetaLimit;

    @FXML
    public CheckBox sentenceDetectorCheckBox;
    @FXML
    public CheckBox stemmerCheckBox;
    @FXML
    public CheckBox filtersCheckBox;
    @FXML
    public CheckBox stopWordsCheckBox;

    private final MainConfiguration config;
    private final ConfigurationLoader configurationLoader;
    private final ConnectionHub connectionHub;
    private final TextEngineModuleLocator textEngineModuleLocator;

    public ConfigurationTabController(MainConfiguration config,
                                      AssetLoader assetLoader,
                                      ConfigurationLoader configurationLoader,
                                      ConnectionHub connectionHub,
                                      TextEngineModuleLocator textEngineModuleLocator) {
        super(assetLoader);
        this.config = config;
        this.configurationLoader = configurationLoader;
        this.connectionHub = connectionHub;
        this.textEngineModuleLocator = textEngineModuleLocator;
    }

    @FXML
    public void initialize() {
        setConfig(config);

        bindNLPConfigurationUI(config.nlp);
        bindDatabaseConfigurationUI(config.database);

        relate(sentenceDetectorCheckBox, sentenceDetector);
        relate(stemmerCheckBox, stemmer);
        relate(filtersCheckBox, filtersField);
        relate(stopWordsCheckBox, stopWordsField);

        fixSpinners();
    }

    private void bindNLPConfigurationUI(NLPConfiguration config) {
        setItems(tokenizer, textEngineModuleLocator.getTokenizers());
        bind(tokenizer, config::setTokenizer);

        setItems(sentenceDetector, textEngineModuleLocator.getSentenceDetectors());
        bind(sentenceDetector, config::setSentenceDetector);

        setItems(stemmer, textEngineModuleLocator.getStemmers());
        bind(stemmer, config::setStemmer);

        bind(filtersField, config::setFilterList);
        bind(stopWordsField, config::setStopWordsList);
        bind(categoryMappingField, config::setCategoryMapping);

        bind(sentenceDetectorCheckBox, config::setEnableSentenceDetector);
        bind(stemmerCheckBox, config::setEnableStemmer);
        bind(filtersCheckBox, config::setEnableFilters);
        bind(stopWordsCheckBox, config::setEnableStopWords);
    }

    private void bindDatabaseConfigurationUI(DatabaseConfiguration config) {
        bind(dbDriverField, config::setDriver);
        bind(dbConnectionStringField, config::setConnectionString);
        bind(dbUsernameField, config::setUsername);
        bind(dbPasswordField, config::setPassword);

        bind(dbTitleLimit, config.truncation::setTitleLen);
        bind(dbContentLimit, config.truncation::setContentLen);
        bind(dbCommentLimit, config.truncation::setCommentLen);
        bind(dbMetaLimit, config.truncation::setMetaLen);
    }

    private void setConfig(MainConfiguration config) {
        setValue(tokenizer, config.nlp.getTokenizer());
        setValue(sentenceDetector, config.nlp.getSentenceDetector());
        setValue(stemmer, config.nlp.getStemmer());

        setValue(filtersField, config.nlp.getFilterList());
        setValue(stopWordsField, config.nlp.getStopWordsList());
        setValue(categoryMappingField, config.nlp.getCategoryMapping());

        setValue(sentenceDetectorCheckBox, config.nlp.isEnableSentenceDetector());
        setValue(stemmerCheckBox, config.nlp.isEnableStemmer());
        setValue(filtersCheckBox, config.nlp.isEnableFilters());
        setValue(stopWordsCheckBox, config.nlp.isEnableStopWords());

        setValue(dbDriverField, config.database.getDriver());
        setValue(dbConnectionStringField, config.database.getConnectionString());
        setValue(dbUsernameField, config.database.getUsername());
        setValue(dbPasswordField, config.database.getPassword());

        setValue(dbTitleLimit, config.database.truncation.getTitleLen());
        setValue(dbContentLimit, config.database.truncation.getContentLen());
        setValue(dbCommentLimit, config.database.truncation.getCommentLen());
        setValue(dbMetaLimit, config.database.truncation.getMetaLen());
    }

    private void fixSpinners() {
        fixSpinner(dbTitleLimit);
        fixSpinner(dbContentLimit);
        fixSpinner(dbCommentLimit);
        fixSpinner(dbMetaLimit);
    }

    @FXML
    public void onTestConnectionButtonClicked() {
        execute(() -> {
            DatabaseConfiguration config = new DatabaseConfiguration();

            config.setDriver(getRequired(dbDriverField, msg("db_driver_not_specified")));
            config.setConnectionString(getRequired(dbConnectionStringField, msg("db_conn_str_not_specified")));
            config.setUsername(get(dbUsernameField));
            config.setPassword(get(dbPasswordField));

            connectionHub.testConnection(config);

        }, msg("db_test_success"));
    }

    @FXML
    public void loadDefaultsButtonClicked() {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Confirmation");
        alert.setHeaderText("Load Default Settings");
        alert.setContentText("Replace current settings with default values?");

        alert.showAndWait().filter(pressedButton -> pressedButton == ButtonType.OK)
                .ifPresent(result -> {
                    config.initializeDefaultConfiguration(textEngineModuleLocator);
                    setConfig(config);
                });
    }

    @FXML
    public void saveButtonClicked() {
        execute(() -> configurationLoader.saveMainConfig(config), msg("cfg_save"));
    }
}
