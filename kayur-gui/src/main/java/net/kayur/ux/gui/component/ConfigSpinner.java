package net.kayur.ux.gui.component;

import javafx.scene.control.Spinner;
import net.kayur.core.configuration.Parameter;
import org.jetbrains.annotations.NotNull;


public class ConfigSpinner extends Spinner<Integer> implements ConfigBoundComponent<Integer> {
    private ConfigAdapter<Integer> adapter = new ConfigAdapter<>(0,
            value -> getValueFactory().setValue(value));

    public ConfigSpinner() {
        valueProperty().addListener(adapter.getListener());
    }

    public void assign(Parameter<Integer> parameter) {
        adapter.assign(parameter);
    }

    public void setDefaultValue(@NotNull Integer value) {
        adapter.setDefaultValue(value);
    }

    @Override
    public ConfigAdapter<Integer> getAdapter() {
        return adapter;
    }
}
