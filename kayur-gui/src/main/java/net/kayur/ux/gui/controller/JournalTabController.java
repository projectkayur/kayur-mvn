package net.kayur.ux.gui.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import net.kayur.Disp;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class JournalTabController {
    @FXML
    public TextArea logArea;

    @FXML
    public void initialize() {
        Disp.appendHandler(new GuiLogHandler());
    }

    class GuiLogHandler extends Handler {
        @Override
        public void publish(LogRecord record) {
            if (isLoggable(record)) {
                // append message in JavaFX thread
                Platform.runLater(() -> logArea.appendText(getFormatter().format(record)));
            }
        }

        @Override
        public void flush() {
        }

        @Override
        public void close() throws SecurityException {
        }
    }
}
