package net.kayur.ux.gui.helper;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.kayur.core.configuration.ConfigurationException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.URL;

import static net.kayur.Msg.msg;

/**
 * Loads UI forms and UI-related application resources.
 */
public class AssetLoader implements ApplicationContextAware {
    private ApplicationContext context;

    // Loads a FXML form using its resource path.
    public Parent loadForm(@NotNull String resourceName) {
        try {
            return getFormLoader(resourceName).load();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Stage loadDialog(@NotNull String resourceName, @NotNull String dialogTitle) {
        Stage stage = createDialog(resourceName);
        stage.setTitle(dialogTitle);
        stage.initStyle(StageStyle.UTILITY);
        stage.setResizable(false);
        stage.getScene().getWindow().centerOnScreen();
        return stage;
    }

    private Stage createDialog(@NotNull String resourceName) {
        Parent dialog;
        try {
            dialog = getDialogLoader(resourceName).load();
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }

        Stage stage = new Stage();
        stage.setScene(new Scene(dialog));
        return stage;
    }

    @NotNull
    public FXMLLoader getDialogLoader(@NotNull String resourceName) {
        return getFormLoader(String.format("dialog/%s", resourceName));
    }

    @NotNull
    public Image loadPictogram(String resourceName) {
        var imageName = String.format("/pictograms/%s.png", resourceName);
        var imageStream = AssetLoader.class.getResourceAsStream(imageName);
        if (imageStream == null) {
            throw new ConfigurationException(String.format("Unable to load image: '%s'.", imageName));
        }
        return new Image(imageStream);
    }

    // The form path must be specified relative to the 'resources' directory.
    @NotNull
    private FXMLLoader getFormLoader(@NotNull String resourceName) {
        var formName = String.format("/fx/%s.fxml", resourceName);
        URL url = AssetLoader.class.getResource(formName);
        if (url == null) {
            throw new ConfigurationException(msg("ui_load_form_error", formName));
        }

        var loader = new FXMLLoader(url);
        loader.setControllerFactory(context::getBean);

        return loader;
    }

    @Override
    public void setApplicationContext(@NotNull ApplicationContext applicationContext) throws BeansException {
        this.context = applicationContext;
    }
}
