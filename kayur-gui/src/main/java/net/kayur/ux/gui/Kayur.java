package net.kayur.ux.gui;

import javafx.application.Application;
import javafx.stage.Stage;
import net.kayur.core.App;
import net.kayur.ux.gui.helper.FxDialogs;
import net.kayur.ux.gui.stage.MainStageInitializer;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.ModuleLoader;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import static net.kayur.Msg.msg;

/**
 * Main class for Kayur GUI (JavaFX-based).
 */
public class Kayur extends Application {
    private static ApplicationContext context;
    private static App app;

    public static void main(String[] args) {
        context =  new ClassPathXmlApplicationContext("beans-gui.xml");
        app = context.getBean(App.class);

        if (app.initialize()) {
            launch(args);
        } else {
            app.shutdown();
        }
    }

    @Override
    public void start(@NotNull Stage primaryStage) {
        try {
            context.getBean(MainStageInitializer.class).setUpPrimaryStage(primaryStage);
        } catch (Exception e) {
            e.printStackTrace();
            app.shutdown();
            return;
        }

        primaryStage.show();
    }

    @Override
    public void stop() {
        // check unsaved changes in module configuration
        promptSavingChanges(context.getBean(ModuleLoader.class));

        app.shutdown();
    }

    private void promptSavingChanges(ModuleLoader moduleLoader) {
        for (ModuleConfiguration cfg : moduleLoader.getChangedModules()) {
            // show a dialog to confirm saving changes
            if (FxDialogs.showConfirmationDialog(
                    msg("dialog_unsaved_changes_title"),
                    msg("dialog_module_has_unsaved_changes_header", cfg.getName()),
                    msg("dialog_module_has_unsaved_changes_prompt"))) {
                moduleLoader.save(cfg);
            }
        }
    }
}
