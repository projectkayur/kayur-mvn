package net.kayur.ux.gui.helper;


@FunctionalInterface
public interface FxFunction<T, U> {
    U apply(T t) throws Exception;
}
