package net.kayur.ux.gui.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TabPane;

import java.util.Objects;

public class MainWindowController {
    private static final Integer STATISTICS_TAB_INDEX = 2;

    @FXML
    public TabPane tabPane;


    @FXML
    public void initialize() {
        tabPane.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            if (Objects.equals(newValue, STATISTICS_TAB_INDEX)) {
                StatisticsTabController controller = ControllerVault.statisticsTabController;
                if (controller != null) {
                    controller.updateSelectionStats();
                    controller.updateCorpusStats();
                }
            }
        });
    }
}
