package net.kayur.ux.gui.controller.dialog;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import net.kayur.web.module.MetadataRuleHolder;
import net.kayur.web.module.ModuleConfiguration;
import org.jetbrains.annotations.Nullable;

import java.util.List;


public class MetadataRuleEditorController {
    @FXML
    public TextField nameField;
    @FXML
    public TextField ruleField;
    @FXML
    public TextField patternField;
    @FXML
    public Button okButton;

    @Nullable
    private ModuleConfiguration config;
    @Nullable
    private MetadataRuleHolder holder;
    @Nullable
    private TableView<MetadataRuleHolder> table;

    private String initTimeName = "";

    @FXML
    public void initialize() {
        configureOkButton(nameField.getText().trim());
        nameField.textProperty().addListener((observable, oldValue, newValue) -> configureOkButton(newValue));
    }

    private void configureOkButton(String fieldName) {
        okButton.setDisable(fieldName.isEmpty());
    }

    public void init(ModuleConfiguration config, MetadataRuleHolder holder, TableView<MetadataRuleHolder> table) {
        this.config = config;
        this.holder = holder;
        this.table = table;

        this.initTimeName = holder.getField();

        nameField.setText(initTimeName);
        ruleField.setText(holder.getRule());
        patternField.setText(holder.getPattern());
    }

    @FXML
    public void onOkButtonClicked() {
        if (config == null || holder == null || table == null) {
            finish();
            return;
        }

        List<MetadataRuleHolder> holders = table.getItems();

        // remove old metadata holder from the representation
        int pos = holders.indexOf(holder);
        holders.remove(holder);

        // create a new metadata holder
        MetadataRuleHolder hdr = new MetadataRuleHolder();
        hdr.setField(nameField.getText().trim());
        hdr.setRule(ruleField.getText().trim());
        hdr.setPattern(patternField.getText().trim());

        // handle field rename
        final String currentName = hdr.getField();
        if (!initTimeName.isEmpty() && !initTimeName.equals(currentName)) {
            config.removeMetaRule(initTimeName);
        }

        // update representation
        if (pos != - 1) {
            holders.add(pos, hdr);
        } else {
            holders.add(hdr);
        }

        // update module configuration
        config.setMetaRule(hdr);

        // close the dialog
        finish();
    }

    // Closes this dialog.
    private void finish() {
        ((Stage) nameField.getScene().getWindow()).close();
    }
}
