package net.kayur.ux.gui.component;

import javafx.scene.control.TextField;
import net.kayur.core.configuration.Parameter;
import org.jetbrains.annotations.NotNull;

/**
 * Subclass of JavaFX TextField that holds and updates on-the-fly
 * a configuration parameter.
 */
public class ConfigField extends TextField implements ConfigBoundComponent<String> {
    private ConfigAdapter<String> adapter = new ConfigAdapter<>("", this::setText);

    public ConfigField() {
        textProperty().addListener(adapter.getListener());
    }

    public void assign(Parameter<String> parameter) {
        adapter.assign(parameter);
    }

    public void setDefaultValue(@NotNull String value) {
        adapter.setDefaultValue(value);
    }

    @Override
    public ConfigAdapter<String> getAdapter() {
        return adapter;
    }
}
