package net.kayur.ux.gui.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.PieChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.*;
import net.kayur.Disp;
import net.kayur.core.DocumentSelection;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.nlp.corpus.Corpus;
import net.kayur.nlp.corpus.CorpusKeyword;
import net.kayur.nlp.corpus.CorpusManager;
import net.kayur.nlp.statistics.Interval;
import net.kayur.nlp.statistics.LengthInWordsDistribution;
import net.kayur.nlp.statistics.Statistics;
import net.kayur.ux.gui.helper.AssetLoader;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.stream.Collectors;

public class StatisticsTabController extends AbstractTabController {
    @FXML
    public Spinner<Integer> numKeywordSpinner;
    @FXML
    public TableView<CorpusKeyword> keywordTable;

    @FXML
    public ComboBox<String> metadataComboBox;

    @FXML
    public PieChart chart;
    @FXML
    public BarChart<String, Number> lengthBarChart;

    @FXML
    public Slider aggregateThresholdSlider;
    @FXML
    public Label numDocumentsLabel;
    @FXML
    public Label numWordsLabel;
    @FXML
    public Label avgDocumentLengthLabel;

    private final DocumentSelection selection;
    private final CorpusManager corpusManager;
    private final Statistics statistics;

    private final Map<String, Map<String, Integer>> stats = new HashMap<>();
    @Nullable
    private UUID selectionId = null;
    @Nullable
    private UUID corpusId;


    public StatisticsTabController(AssetLoader assetLoader,
                                   DocumentSelection selection,
                                   CorpusManager corpusManager,
                                   Statistics statistics) {
        super(assetLoader);
        this.selection = selection;
        this.corpusManager = corpusManager;
        this.statistics = statistics;

        ControllerVault.statisticsTabController = this;
    }


    @FXML
    public void initialize() {
        // fix JavaFX spinner bug
        fixSpinner(numKeywordSpinner);

        // init default values
        numKeywordSpinner.getValueFactory().setValue(100);

        metadataComboBox.setOnAction(event -> {
            String name = metadataComboBox.getSelectionModel().getSelectedItem();
            Map<String, Integer> data = this.stats.get(name);
            if (data != null) {
                updateMetadataChart(name, data);
            }
        });

        aggregateThresholdSlider.valueProperty().addListener((observable, oldValue, newValue) -> {
            if (!oldValue.equals(newValue)) {
                updateMetadataStats();
            }
        });

        lengthBarChart.setTitle("Document Length Distribution (in Words)");
        lengthBarChart.getXAxis().setLabel("Number of Words");
        lengthBarChart.getYAxis().setLabel("Number of Documents");
    }

    // ------------------------- EVENT HANDLERS ------------------------- //

    @FXML
    public void onRefreshButtonClicked() {
        Corpus corpus;
        try {
            corpus = corpusManager.getCorpus();
        } catch (ConfigurationException e) {
            // corpus is not ready, nothing to do
            return;
        }
        updateTopKeywords(corpus);
    }

    // ------------------------------------------------------------------ //

    void updateSelectionStats() {
        if (selection.isEmpty()) {
            // nothing to do
            return;
        }

        if (selection.id().equals(selectionId)) {
            // we have already processed this selection
            return;
        }

        updateMetadataStats();

        this.selectionId = selection.id();
    }

    void updateCorpusStats() {
        UUID currentCorpusId = corpusManager.getId();
        if (currentCorpusId == null || currentCorpusId.equals(corpusId)) {
            // the corpus is not set, or we have already processed this corpus
            return;
        }

        Corpus corpus;
        try {
            corpus = corpusManager.getCorpus();
        } catch (ConfigurationException e) {
            // corpus is not ready, nothing to do
            return;
        }

        updateCorpusStatistics(corpus);
        updateTopKeywords(corpus);
        updateLengthDistribution(corpus);

        this.corpusId = currentCorpusId;
    }

    private void updateTopKeywords(@NotNull Corpus corpus) {
        final int num = numKeywordSpinner.getValue();

        Task<List<CorpusKeyword>> task = new Task<>() {
            @Override
            protected List<CorpusKeyword> call() {
                return corpus.getAllCorpusKeywords(num);
            }
        };

        task.run();
        task.setOnSucceeded(state -> {
            keywordTable.getItems().clear();
            keywordTable.getItems().addAll(task.getValue());
        });
    }

    private void updateMetadataStats() {
        final double threshold = aggregateThresholdSlider.getValue() / 100.0;

        Task<Map<String, Map<String, Integer>>> task = new Task<>() {
            @Override
            protected Map<String, Map<String, Integer>> call() {
                return statistics.getMetaStats(selection.get(), threshold);
            }
        };

        task.run();
        task.setOnSucceeded(state -> {
            this.stats.clear();
            this.stats.putAll(task.getValue());

            updateMetadataComboBox(this.stats.keySet());
        });
    }

    private void updateMetadataComboBox(Set<String> data) {
        List<String> items = metadataComboBox.getItems();
        items.clear();
        items.addAll(data);

        if (!items.isEmpty()) {
            metadataComboBox.getSelectionModel().select(0);
        } else {
            // clear the chart
            updateMetadataChart("Selected documents do not contain metadata.", new HashMap<>());
        }
    }

    private void updateMetadataChart(String name, Map<String, Integer> data) {
        chart.setTitle(name);

        List<PieChart.Data> list = data.entrySet().stream()
                .map(entry -> new PieChart.Data(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        chart.setData(FXCollections.observableArrayList(list));
    }

    private void updateLengthDistribution(@NotNull Corpus corpus) {
        Task<Map<Interval, Integer>> task = new Task<>() {
            @Override
            protected Map<Interval, Integer> call() {
                LengthInWordsDistribution ld = new LengthInWordsDistribution(corpus, 20);
                return ld.getResult();
            }
        };

        task.setOnSucceeded(state -> updateLengthDistributionChart(task.getValue()));

        task.run();
    }

    private void updateLengthDistributionChart(Map<Interval, Integer> data) {
        XYChart.Series<String, Number> series = new XYChart.Series<>();
        ObservableList<XYChart.Data<String, Number>> list = series.getData();

        data.forEach((wi, nd) -> list.add(new XYChart.Data<>(String.format("%d-%d", wi.start, wi.end), nd)));

        lengthBarChart.getData().clear();
        lengthBarChart.getData().add(series);
    }

    private void updateCorpusStatistics(@NotNull Corpus corpus) {
        numDocumentsLabel.setText(String.valueOf(corpus.numDocuments()));
        numWordsLabel.setText(String.valueOf(corpus.numWords()));
        avgDocumentLengthLabel.setText(String.format("%.1f", corpus.averageDocumentLength()));
    }
}
