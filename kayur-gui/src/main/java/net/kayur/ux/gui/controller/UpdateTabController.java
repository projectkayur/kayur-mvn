package net.kayur.ux.gui.controller;


import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import net.kayur.core.configuration.ConfigurationException;
import net.kayur.core.configuration.Parameter;
import net.kayur.ux.gui.component.*;
import net.kayur.ux.gui.controller.dialog.MetadataRuleEditorController;
import net.kayur.ux.gui.helper.AssetLoader;
import net.kayur.ux.gui.helper.FxDialogs;
import net.kayur.web.Document;
import net.kayur.web.module.MetadataRuleHolder;
import net.kayur.web.module.ModuleConfiguration;
import net.kayur.web.module.ModuleLoader;
import net.kayur.web.module.generator.IdGenerator;
import net.kayur.web.module.generator.IdGeneratorFactory;
import net.kayur.web.module.generator.IterativeGenerator;
import net.kayur.web.module.generator.SearchGenerator;
import net.kayur.web.parser.RequestSettings;
import net.kayur.web.update.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.text.ParseException;
import java.util.*;

import static net.kayur.Msg.msg;

// TODO: ask saving module changes upon exit

public class UpdateTabController extends AbstractTabController {
    @FXML
    public ComboBox<ModuleConfiguration> moduleComboBox;

    // Import

    @FXML
    public TextField testDocumentIdField;

    @FXML
    public Spinner<Integer> numDocsSpinner;
    @FXML
    public TextField startIdField;
    @FXML
    public CheckBox overwriteCheckBox;

    // Module Configuration

    @FXML
    public TreeItem<String> textualDataNode;
    @FXML
    public TreeItem<String> dateTimeNode;
    @FXML
    public TreeItem<String> documentNode;
    @FXML
    public TreeItem<String> iterativeProcessNode;
    @FXML
    public TreeItem<String> numDocsNode;
    @FXML
    public TreeItem<String> connectionSettingsNode;
    @FXML
    public TreeItem<String> metadataNode;

    @FXML
    public Pane textualDataPane;
    @FXML
    public Pane dateTimeDataPane;
    @FXML
    public Pane documentPane;
    @FXML
    public Pane numDocsPane;
    @FXML
    public Pane iterativeProcessPane;
    @FXML
    public Pane connectionSettingsPane;
    @FXML
    public Pane metadataPane;

    @FXML
    public Pane iterativeGeneratorPane;
    @FXML
    public Pane searchGeneratorPane;

    @FXML
    public ConfigField titleRuleField;
    @FXML
    public Button titleRuleAdd;

    @FXML
    public ConfigField titlePatternField;
    @FXML
    public ConfigField contentRuleField;
    @FXML
    public ConfigField contentPatternField;
    @FXML
    public ConfigField commentsRuleField;
    @FXML
    public ConfigField commentsPatternField;
    @FXML
    public ConfigField dateFormatField;
    @FXML
    public ConfigField dateRuleField;
    @FXML
    public ConfigField datePatternField;

    @FXML
    public ConfigField documentUrlField;
    @FXML
    public ConfigField numDocsUrlField;
    @FXML
    public ConfigField numDocsRuleField;
    @FXML
    public ConfigField numDocsPatternField;

    @FXML
    public TableView<MetadataRuleHolder> metadataTable;

    @FXML
    public ConfigComboBox typeComboBox;
    @FXML
    public ConfigSpinner iterativeIncrementSpinner;
    @FXML
    public ConfigSpinner iterativeFirstIdSpinner;
    @FXML
    public ConfigField searchInitPageField;
    @FXML
    public ConfigField searchNextPageField;
    @FXML
    public ConfigField searchLinkRuleField;
    @FXML
    public ConfigField searchLinkPatternField;

    @FXML
    public ConfigSpinner pagetTimeoutSpinner;
    @FXML
    public ConfigSpinner accessDelaySpinner;
    @FXML
    public ConfigField userAgentField;
    @FXML
    public ConfigCheckBox followRedirectsCheckBox;

    @FXML
    public TextArea dateFormatHelpArea;

    @FXML
    public TreeView<String> moduleConfigTree;

    private String currentNode = null;
    private final Map<String, Pane> sidePaneMap = new HashMap<>();

    private List<ConfigBoundComponent> configBoundComponents;

    private final Fetcher fetcher;
    private final ModuleLoader moduleLoader;

    public UpdateTabController(AssetLoader assetLoader,
                               Fetcher fetcher,
                               ModuleLoader moduleLoader) {
        super(assetLoader);
        this.fetcher = fetcher;
        this.moduleLoader = moduleLoader;
    }

    @FXML
    public void initialize() {
        // init TreeView
        initModuleConfigTree();

        // load available modules and select first of them
        updateModuleList();
        moduleComboBox.getSelectionModel().selectFirst();
        onModuleComboBoxAction(); // load config of the selected module

        titleRuleAdd.setOnAction(event -> showAddRuleDialog());

        // fix JavaFX glitches with Spinners
        fixSpinners();
    }


    // ------------------------- EVENT HANDLERS ------------------------- //

    @FXML
    public void testButtonClicked() {
        execute(() -> {
            ModuleConfiguration config = getSelectedModule();
            String documentId = getRequired(testDocumentIdField, msg("testmodule_missing_document_id"));

            runUserTask(msg("testmodule_dialog_title"),
                    new Task<>() {
                        @Override
                        protected Document call() {
                            updateMessage(msg("testmodule_dialog_status", documentId));
                            return fetcher.test(config, documentId);
                        }
                    },
                    this::showDocumentDialog);
        });
    }

    @FXML
    public void importButtonClicked() {
        execute(() -> {
            fetcher.setModule(getSelectedModule());

            UpdateParameters params = new UpdateParameters();
            params.setLimit(numDocsSpinner.getValue());
            params.setOverwrite(overwriteCheckBox.isSelected());
            params.setLastProcessedDocument(new Document(0, get(startIdField)));

            runUserTask(msg("update_dialog_title"),
                    new Task<UpdateTracker>() {
                        @Override
                        protected UpdateTracker call() throws Exception {
                            updateMessage(msg("update_dialog_calculating_size_msg"));
                            UpdateCheckResult updateCheckResult = fetcher.calculateUpdateSize(params);

                            updateMessage(msg("update_dialog_start_import_msg", updateCheckResult.numToUpdate));
                            UpdateSession session = fetcher.startUpdateSession(params, updateCheckResult);
                            UpdateTracker tracker = session.getTracker();

                            updateMessage(tracker.getProcessingReport());
                            updateProgress(0, updateCheckResult.numToUpdate);

                            while (session.update() && !isCancelled()) {
                                updateMessage(tracker.getProcessingReport());
                                updateProgress(tracker.getIteration(), updateCheckResult.numToUpdate);
                            }

                            updateMessage(tracker.getFinalReport());
                            updateProgress(tracker.getIteration(), updateCheckResult.numToUpdate);

                            Thread.sleep(1000); // to see last messages and full progress bar

                            return tracker;
                        }
                    },
                    tracker -> FxDialogs.notification(
                            tracker.getFinalReport(),
                            msg("update_dialog_done_title"))
            );
        });
    }

    @FXML
    public void checkButtonClicked() {
        execute(() -> {
            fetcher.setModule(getSelectedModule());

            runUserTask(msg("check_updates_dialog_title"),
                    new Task<UpdateCheckResult>() {
                        @Override
                        protected UpdateCheckResult call() {
                            updateMessage(msg("update_dialog_calculating_size_msg"));
                            return fetcher.calculateUpdateSize(new UpdateParameters());
                        }
                    },
                    checkUpdateData -> FxDialogs.notification(
                            msg("checkstatus_dialog_status_msg", checkUpdateData.numToUpdate, checkUpdateData.numDocumentsTotal),
                            msg("checkstatus_dialog_done_title"))
            );
        });
    }

    @FXML
    public void onModuleComboBoxAction() {
        ModuleConfiguration config = moduleComboBox.getSelectionModel().getSelectedItem();
        setConfig(config);
    }

    @FXML
    public void moduleConfigTreeMouseClicked() {
        TreeItem<String> item = moduleConfigTree.getSelectionModel().getSelectedItem();
        String node = item.getValue();

        if (!currentNode.equals(node) && sidePaneMap.containsKey(node))
            switchToConfigPanel(node);
    }

    @FXML
    public void onTypeComboBoxAction() {
        if (typeComboBox.getValue().equals(IdGeneratorFactory.TYPE_ITERATIVE)) {
            iterativeGeneratorPane.setVisible(true);
            searchGeneratorPane.setVisible(false);
        } else if (typeComboBox.getValue().equals(IdGeneratorFactory.TYPE_SEARCH)) {
            iterativeGeneratorPane.setVisible(false);
            searchGeneratorPane.setVisible(true);
        } else {
            iterativeGeneratorPane.setVisible(false);
            searchGeneratorPane.setVisible(false);
        }
    }

    @FXML
    public void onAddMetadataAction() {
        showEditMetadataRuleDialog(new MetadataRuleHolder());
    }

    @FXML
    public void onRemoveMetadataAction() {
        MetadataRuleHolder holder = metadataTable.getSelectionModel().getSelectedItem();
        execute(() -> getSelectedModule().removeMetaRule(holder.getField()));
        metadataTable.getItems().remove(holder);
    }

    @FXML
    public void onMetadataTableClicked(MouseEvent event) {
        if (event.getClickCount() == 2) {
            MetadataRuleHolder holder = metadataTable.getSelectionModel().getSelectedItem();
            showEditMetadataRuleDialog(holder);
        }
    }

    // ------------------------- END OF EVENT HANDLERS ------------------------- //


    private void showEditMetadataRuleDialog(MetadataRuleHolder holder) {
        execute(() -> {
            FXMLLoader loader = assetLoader.getDialogLoader("editMetadataRuleDialog");
            Parent dialog = loader.load();

            Stage stage = createStage(dialog, "Edit Metadata Rule");
            setCloseOnEscape(stage);

            MetadataRuleEditorController controller = loader.getController();
            controller.init(getSelectedModule(), holder, metadataTable);

            stage.show();
        });
    }

    // Loads the list of available modules.
    private void updateModuleList() {
        moduleComboBox.setItems(toFxList(moduleLoader.getModules()));
    }

    // Replaces config already present in the ComboBox.
    // This is necessary to correctly update UI when module name changes.
    private void replaceModuleInList(ModuleConfiguration config) {
        ObservableList<ModuleConfiguration> items = moduleComboBox.getItems();
        int index = items.indexOf(config);
        items.remove(config);
        items.add(index, config);
        moduleComboBox.setValue(config);
    }

    // Selects a particular config from the ComboBox.
    private void selectModule(@NotNull ModuleConfiguration config) {
        moduleComboBox.getSelectionModel().select(config);
        setConfig(config);
    }

    // Gets currently selected config from the ComboBox.
    // Throws exception if no value is selected.
    @NotNull
    private ModuleConfiguration getSelectedModule() throws ConfigurationException {
        ModuleConfiguration config = moduleComboBox.getSelectionModel().getSelectedItem();
        if (config == null)
            throw new ConfigurationException(msg("module_not_selected"));
        return config;
    }

    // Updates values in all config-bound UI components.
    public void setConfig(@Nullable ModuleConfiguration config) {
        if (config != null) {
            configBoundComponents.forEach(c -> c.getAdapter().setConfig(config));
            onTypeComboBoxAction();

            try {
                metadataTable.setItems(FXCollections.observableArrayList(config.getMetadataRuleHolders()));
            } catch (ParseException e) {
                FxDialogs.error(e);
            }
        }
    }

    private void initModuleConfigTree() {
        // buildCorpus map (tree_node_name -> panel_to_show)
        sidePaneMap.put(textualDataNode.getValue(), textualDataPane);
        sidePaneMap.put(dateTimeNode.getValue(), dateTimeDataPane);
        sidePaneMap.put(documentNode.getValue(), documentPane);
        sidePaneMap.put(iterativeProcessNode.getValue(), iterativeProcessPane);
        sidePaneMap.put(numDocsNode.getValue(), numDocsPane);
        sidePaneMap.put(connectionSettingsNode.getValue(), connectionSettingsPane);
        sidePaneMap.put(metadataNode.getValue(), metadataPane);

        dateFormatHelpArea.setText(msg("help_date_format"));

        // select one panel by default
        moduleConfigTree.getSelectionModel().select(documentNode);
        switchToConfigPanel(documentNode.getValue());

        // bind configuration fields to the corresponding config parameters
        assignRule(titleRuleField, titlePatternField, ModuleConfiguration.OPTION_TITLE_RULE);
        assignRule(contentRuleField, contentPatternField, ModuleConfiguration.OPTION_CONTENT_RULE);
        assignRule(commentsRuleField, commentsPatternField, ModuleConfiguration.OPTION_COMMENTS_RULE);

        assignRule(dateRuleField, datePatternField, ModuleConfiguration.OPTION_DATE_RULE);
        dateFormatField.assign(ModuleConfiguration.OPTION_DOC_DATE_FMT);

        documentUrlField.assign(ModuleConfiguration.OPTION_DOC_URL_TEMPLATE);

        numDocsUrlField.assign(ModuleConfiguration.OPTION_NUM_DOCS_URL);
        assignRule(numDocsRuleField, numDocsPatternField, ModuleConfiguration.OPTION_NUM_DOCS_RULE);

        // Iterative Process

        typeComboBox.assign(IdGenerator.OPTION_ID_GENERATOR_CLS);
        typeComboBox.getItems().add(IdGeneratorFactory.TYPE_ITERATIVE);
        typeComboBox.getItems().add(IdGeneratorFactory.TYPE_SEARCH);

        iterativeIncrementSpinner.assign(IterativeGenerator.OPTION_STEP);
        iterativeFirstIdSpinner.assign(IterativeGenerator.OPTION_START_ID);

        iterativeIncrementSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(Integer.MIN_VALUE, Integer.MAX_VALUE));
        iterativeIncrementSpinner.setDefaultValue(1);

        iterativeFirstIdSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE));

        searchInitPageField.assign(SearchGenerator.OPTION_INITIAL_SEARCH_PAGE_URL);
        searchNextPageField.assign(SearchGenerator.OPTION_NEXT_SEARCH_PAGE_URL);
        assignRule(searchLinkRuleField, searchLinkPatternField, SearchGenerator.OPTION_LINKS_RULE);

        // Connection Settings

        pagetTimeoutSpinner.assign(ModuleConfiguration.OPTION_WAIT_TIMEOUT);
        pagetTimeoutSpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE));
        pagetTimeoutSpinner.setDefaultValue(RequestSettings.DEFAULT_TIMEOUT);

        accessDelaySpinner.assign(ModuleConfiguration.OPTION_ACCESS_DELAY);
        accessDelaySpinner.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, Integer.MAX_VALUE));
        accessDelaySpinner.setDefaultValue(RequestSettings.DEFAULT_ACCESS_DELAY);

        userAgentField.assign(ModuleConfiguration.OPTION_USER_AGENT);
        userAgentField.setDefaultValue(RequestSettings.DEFAULT_USER_AGENT);

        followRedirectsCheckBox.assign(ModuleConfiguration.OPTION_FOLLOW_REDIRECTS);
        followRedirectsCheckBox.setDefaultValue(RequestSettings.DEFAULT_FOLLOW_REDIRECTS_SETTING);

        //

        configBoundComponents = Arrays.asList(
                titleRuleField, titlePatternField,
                contentRuleField, contentPatternField,
                commentsRuleField, commentsPatternField,
                dateFormatField, dateRuleField, datePatternField,
                documentUrlField, numDocsUrlField, numDocsRuleField, numDocsPatternField,
                typeComboBox,
                iterativeIncrementSpinner, iterativeFirstIdSpinner,
                searchInitPageField, searchNextPageField, searchLinkRuleField, searchLinkPatternField,
                pagetTimeoutSpinner, accessDelaySpinner, userAgentField, followRedirectsCheckBox);
    }

    private void fixSpinners() {
        fixSpinner(numDocsSpinner);
        fixSpinner(pagetTimeoutSpinner);
        fixSpinner(accessDelaySpinner);
    }

    private void assignRule(ConfigField sequenceField, ConfigField patternField, Parameter parameter) {
        sequenceField.assign(ModuleConfiguration.getAccessSequenceParameter(parameter));
        patternField.assign(ModuleConfiguration.getExtractionPatternParameter(parameter));
    }

    private void switchToConfigPanel(String node) {
        if (currentNode != null)
            sidePaneMap.get(currentNode).setVisible(false);
        currentNode = node;
        sidePaneMap.get(node).setVisible(true);
    }

    // Displays a dialog to view a document.
    private void showAddRuleDialog() {
        String title = msg("dialog_add_access_token_title");
        Stage stage = assetLoader.loadDialog("addRuleDialog", title);

        setCloseOnEscape(stage);

        //DocumentDialogController controller = loader.getController();
        //controller.setDocument(document);
        stage.initModality(Modality.APPLICATION_MODAL);
        stage.showAndWait();

        String token = getUserData(stage, String.class);
        if (token != null) {
            titleRuleField.setText(titleRuleField.getText() + token);
        }
    }


    // ------------------------- TOOLBAR ------------------------- //

    @FXML
    public void onNewModuleButtonClicked() {
        TextInputDialog dialog = new TextInputDialog(ModuleLoader.NEW_MODULE_NAME);
        dialog.setTitle("New Module");
        dialog.setHeaderText("Create Empty Module");
        dialog.setContentText("Module Name:");

        Optional<String> name = dialog.showAndWait();

        if (name.isPresent()) {
            ModuleConfiguration config = moduleLoader.addEmptyModule(name.get());
            updateModuleList();
            selectModule(config);
        }
    }

    @FXML
    public void onRenameButtonClicked() {
        ModuleConfiguration config;
        try {
            config = getSelectedModule();
        } catch (ConfigurationException e) {
            FxDialogs.warning(e);
            return;
        }

        TextInputDialog dialog = new TextInputDialog(config.getName());
        dialog.setTitle("Rename Module");
        dialog.setHeaderText("Rename Module");
        dialog.setContentText("Module Name:");

        Optional<String> name = dialog.showAndWait();

        if (name.isPresent()) {
            config.setName(name.get());
            replaceModuleInList(config);
        }
    }

    @FXML
    public void onSaveConfigButtonClicked() {
        execute(() -> moduleLoader.save(getSelectedModule()),
                msg("module_cfg_save_success"));
    }

    @FXML
    public void onWizardButtonClicked() {
        execute(() -> {
            String title = msg("wizard_dialog_title");
            Stage stage = assetLoader.loadDialog("wizardDialog", title);
            stage.setResizable(false);

            stage.showAndWait();

            ModuleConfiguration config = getUserData(stage, ModuleConfiguration.class);
            if (config != null) {
                moduleLoader.addModule(config);
                updateModuleList();
                selectModule(config);
            }
        });
    }

    // ------------------------- END OF TOOLBAR ------------------------- //
}
