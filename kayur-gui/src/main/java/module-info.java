module net.kayur.gui {
    requires net.kayur;

    requires org.jetbrains.annotations;
    requires java.logging;

    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    exports net.kayur.ux.gui to javafx.graphics;  // GUI entry point
    exports net.kayur.ux.gui.component to javafx.fxml;  // custom GUI components
    exports net.kayur.ux.gui.controller;
    exports net.kayur.ux.gui.controller.dialog;
    exports net.kayur.ux.gui.controller.textprocessing;

    // DI
    requires spring.beans;
    requires spring.context;

    exports net.kayur.ux.gui.helper to spring.beans;
    exports net.kayur.ux.gui.stage to spring.beans;

    opens pictograms;
    opens style;
}
