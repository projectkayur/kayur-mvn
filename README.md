Build Instructions
------------------

Requirements:
- JDK 11 (or higher)
- Maven 3.6 (or higher)

To prepare the distribution package of Kayur, execute:

```
#!bash

mvn clean package -DskipTests
```

The binary distribution will be available in 
`./kayur-assembly/target/kayur.tar.gz` (.zip for Windows).
